# -*- coding: utf-8 -*-
"""
Created on Fri Dec 16 14:40:47 2022

@author: thocza
This is the property window and property data
alpha 0.0.1
    first UI
"""

# info variables
PROGRAM_NAME = "deltaTau logwindow"
FILE_NAME = "logWindow.py"
FILE_CREATION = "2022-12-16"
FILE_UPDATE = "2022-12-16"
FILE_VERSION = "alpha 0.0.1"
DT_VERSION = "0.12.0"
PRINT_INFO = True


# libraries
from PyQt5 import QtWidgets, QtCore, QtGui, QtOpenGL, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QStyle
import sys
import ctypes
import sys
import os.path
import pickle as pc
from style import *

def printinfo():
    """shows all information about this file"""

    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)


class LogWindow(QWidget):
    """Property Window for edditing properties dt
        """
    
    def __init__(self, log, pos=None):
        """intits the Window
            """
        
        super().__init__()
        self.log = log
        appicon = QtGui.QIcon()
        appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
        self.setWindowIcon(appicon)
        self.setWindowTitle("deltaTau")
        self.errordialog = None
        if pos == None:
            pos = QtCore.QPoint(0, 0)
        self.initUI(pos)

    def initUI(self, pos):
        """inits the window"""
        
        self.setGeometry(pos.x() + 300, pos.y() + 300, 500, 500)
        # self.setFixedSize(500, 500)
        self.setWindowTitle(f"deltaTau properties")
        
        self.hLayoutWindow = QtWidgets.QHBoxLayout(self)
        self.vLayoutWindow = QtWidgets.QVBoxLayout()
        
        self.textWidget = QtWidgets.QPlainTextEdit()
        self.textWidget.setMinimumSize(QtCore.QSize(500, 50))
        self.textWidget.insertPlainText(self.log)
        self.textWidget.setReadOnly(True)
        
        self.vLayoutWindow.addWidget(self.textWidget)
        self.hLayoutWindow.addLayout(self.vLayoutWindow)

"""prints the information about this file"""
if PRINT_INFO:
    printinfo()