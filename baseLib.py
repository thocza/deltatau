# -*- coding: utf-8 -*-
"""
@author: tomcz
This is the main Window for deltaTau
alpha 0.0.1
    added resistor inductance and capacotor
alpha 0.0.2
    LibTree class for easier structuring of Libraries was added
alpha 0.0.3
    added the element to propertyelement
alpha 0.0.4
    added a list of controllinputs to the Propertyelement
beta 0.1.0
    added the ampermeter for circuitelements
    now functional in creating the dt model
beta 0.1.1
    added a args cast to the property element
beta 0.1.2
    added funciton to clear property element before compiling again
beta 0.1.3
    added the controloutput
beta 0.1.4
    size of inputs and outputs is now changable
beta 0.1.5
    added the labeltype for labels
beta 0.1.6
    changed variablenames
beta 0.1.7
    added undo and redo
beta 0.1.8
    made function elements with changebale inputs and output number possible
beta 0.1.9
    make loadable libraries possible
beta 0.1.10
    added smaller zero
beta 0.1.11
    removed the current meter
beta 0.1.12
    added labes for propertyelement
beta 0.1.13
    added finit and not zero
beta 0.1.14
    added list of two floats constraints function
beta 0.1.15
    bug fixed function outputs were not be cleared
    added steady incremental condition
beta 0.1.16
    added CEType, FEType, CLabelType and FLabelType to add element type to Propertyelement
    update of Propertyelement
"""

# info variables
PROGRAM_NAME = "deltaTau basis Library"
FILE_NAME = "baseLib.py"
FILE_CREATION = "2022-01-28"
FILE_UPDATE = "2024-08-09"
FILE_VERSION = "beta 0.1.16"
DT_VERSION = "0.12.0"
PRINT_INFO = True

LIBRARY =[]

# libraries
import ggelements as baseimages
import copy as copy
# import functionLib as flib
import numpy as np

def printinfo():
    """shows all information about this file"""

    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)


class LibTree:
    """class for library organisation in easy structure"""
    
    def __init__(self, name):
        """inits with the name and no children"""
        
        self.name = name
        self.children = []
        
    def append(self, child):
        """appends a child which is eighter a LibTree object or a PropertyElement"""
        
        self.children.append(child)


class PropertyElement:
    """saves all properties of the elements"""
    
    def __init__(self, name, icon, image, creationfunction, argsvalues, argsnames, argsunits, argscast, argscompatibility, argsinfo, inputs=[None], outputs=[None], controlinput=[], controloutput=[], sizechangeable=[False, False], labels=None, orientation=0):
        """inits all values"""
        
        self.name = name
        self.icon = f"./library/icons/{icon}"
        self.image = image
        # self.currentmeasurement = baseimages.drawCurrentMeter
        # self.ampermeter = None
        self.creationfunction = creationfunction
        self.argsvalues = argsvalues
        self.argscombos = []
        self.argsnames = argsnames
        self.argsunits = argsunits
        self.argscast = argscast
        self.argscompatibility = argscompatibility
        self.argsinfo = argsinfo
        self.inputs = inputs
        self.outputs = outputs
        self.controlinput = controlinput
        self.controloutput = controloutput
        self.controloutputelement = []
        self.sizechangable = sizechangeable
        self.element = None
        self.labels = labels
        self.elementtype = None
        self.orientation = orientation
        if len(self.controlinput) > 1:
            raise TypeError(f"{len(self.controlinput)} is bigger than 1")
        self.chechlabels()
        for i, a in enumerate(self.argsvalues):
            if type(a) == list and not self.argscast[i] == list:
                self.argscombos.append(a)
                self.argsvalues[i] = a[0]
            else:
                self.argscombos.append(None)
                
    def chechlabels(self):
        """chechs if the labels string is compliant"""
        try:
            if self.labels == None:
                raise TypeError("not compliant")
            elif len(self.labels) > 0:
                if not len(self.labels[0]) == len(self.inputs):
                    self.labels[0] = []
                    for _ in self.inputs:
                        self.labels[0].append("")
                else:
                    for l in self.labels[0]:
                        if not type(l) == str:
                            raise TypeError("not compliant")
                if len(self.labels) > 1:
                    if not len(self.labels[1]) == len(self.outputs):
                        self.labels[1] = []
                        for _ in self.inputs:
                            self.labels[1].append("")
                    else:
                        for l in self.labels[1]:
                            if not type(l) == str:
                                raise TypeError("not compliant")
                    if len(self.labels) > 2:
                        if not len(self.labels[2]) == len(self.controlinput):
                            self.labels[2] = []
                            for _ in self.inputs:
                                self.labels[2].append("")
                        else:
                            for l in self.labels[2]:
                                if not type(l) == str:
                                    raise TypeError("not compliant")
                        if len(self.labels) > 3:
                            if not len(self.labels[3]) == len(self.controloutput):
                                self.labels[3] = []
                                for _ in self.inputs:
                                    self.labels[3].append("")
                            else:
                                for l in self.labels[3]:
                                    if not type(l) == str:
                                        raise TypeError("not compliant")
                            if len(self.labels) > 4:
                                self.labels = self.labels[0:4]
                        else:
                            self.labels.append([])
                            for _ in self.controloutput:
                                self.labels[-1].append("") 
                    else:
                       self.labels.append([])
                       for _ in self.controlinput:
                           self.labels[-1].append("")
                       self.labels.append([])
                       for _ in self.controloutput:
                           self.labels[-1].append("") 
                else:
                    self.labels.append([])
                    for _ in self.outputs:
                        self.labels[-1].append("")
                    self.labels.append([])
                    for _ in self.controlinput:
                        self.labels[-1].append("")
                    self.labels.append([])
                    for _ in self.controloutput:
                        self.labels[-1].append("")
            else:
                raise TypeError("not compliant")
        except:
            self.labels = []
            self.labels.append([])
            for _ in self.inputs:
                self.labels[-1].append("")
            self.labels.append([])
            for _ in self.outputs:
                self.labels[-1].append("")
            self.labels.append([])
            for _ in self.controlinput:
                self.labels[-1].append("")
            self.labels.append([])
            for _ in self.controloutput:
                self.labels[-1].append("")
    
    def creat(self):
        """creats the dt model element from the properties"""
        
        numberios = []
        if not self.sizechangable[0] == False:
            numberios.append(len(self.inputs))
        if not self.sizechangable[1] == False:
            numberios.append(len(self.outputs))
        # self.controloutputelement = []
        # if len(self.controloutput)>0:
        #     self.controloutputelement = [self.controloutput[0](self.name)]
        # return(self.creationfunction(self.name, *self.controlinput, *self.controloutputelement, *self.argsvalues, *numberios))
        return(self.creationfunction(self.name, *self.argsvalues, *numberios))
    
    # def setampermeter(self, ampermeter):
    #     """sets the ampermeter for CElement"""
        
    #     self.ampermeter = ampermeter
    #     return self
    
    def clear(self):
        """clears the property element"""
        
        for i, _ in enumerate(self.inputs):
            self.inputs[i] = None
        for i, _ in enumerate(self.outputs):
            self.outputs[i] = None
        for i, _ in enumerate(self.controlinput):
            self.controlinput[i] = None
        for i, _ in enumerate(self.controloutput):
            self.controloutput[i] = None
        # self.controloutputelement = []
    
    def biggerzero(value):
        """checks if  the value is bigger than zero"""
        
        if value > 0:
            return True
        return False
    
    def notnegative(value):
        """checks if  the value is bigger than zero"""
        
        if value >= 0:
            return True
        return False
    
    def smallerzero(value):
        """checks if  the value is smaller than zero"""
        
        if value < 0:
            return True
        return False
    
    def noconstrains(value):
        """no constrains allwas True"""
        
        return True
    
    def biggerzeroint(value):
        """checks if the value is an interger and bigger than zero"""
        
        if value == int(value) and value>0:
            return True
        else:
            return False
        
    def finitandnotzero(value):
        """checks if the value is finit and not zero"""
        
        if np.isinf(value) or np.isnan(value) or value == 0:
            return False
        else:
            return True
        
    def listoftwofloats(value):
        """checks if value is a list with two float entries"""
        
        try:
            if not len(value) == 2:
                return False
            for i in value:
                float(i)
        except:
            return False
        return True
    
    def steadyincreasinglist(value):
        """checks if value is a list with two float entries"""
        try:
            if len(value) == 1:
                return True
            for i, _ in enumerate(value):
                if i > 0:
                    if value[i] < value[i-1]:
                        return False
        except:
            return False
        return True
        
    def funksetargsvalues(self):
        """returns a functiont that sets the values"""
        
        argsvalues = copy.copy(self.argsvalues)
        name = self.name
        def setvalues():
            self.argsvalues = argsvalues
            self.name = name
        return setvalues
    
    def funkupdatesize(self, inputsize, outputsize):
        """returns a function that changes the size"""
        
        def updatesize():
            self.element.updatesize(inputsize, outputsize)
        return updatesize
        

class UType:
    
    def __init__(self, name, icon, image, creatfunction, argsvalues, argsnames, argsunits, argscast, argscompatibility, argsinfo, size=[1, 1], interface=["", "", "", ""], sizechangeable=[False, False], labels=None, orientation=0):
        
        self.name = name
        self.icon = f"./library/icons/{icon}"
        self.image = image
        self.creationfunction = creatfunction
        self.argsvalues = argsvalues
        self.argscombos = []
        self.argsnames = argsnames
        self.argsunits = argsunits
        self.argscast = argscast
        self.argscompatibility = argscompatibility
        self.argsinfo = argsinfo
        self.size = size
        self.interface = interface
        self.nodes = []
        self.inputs = []
        self.outputs = []
        self.sizechangable = sizechangeable
        self.labels = labels
        self.orientation = orientation
        self.element = None
        if min(self.size) < 0:
            raise TypeError("Error: Minimum value of size must be 0")
        if not len(self.size) == 2:
            raise TypeError("Error: Length of size must be 2")
        if not self.size[0]*2 + self.size[1]*2 == len(self.interface):
            raise TypeError("Error: Size and interface are not compatible")
        for i in self.interface:
            if i == "n":
                self.nodes.append(None)
            elif i == "i":
                self.inputs.append(None)
            elif i == "o":
                self.outputs.append(None)
        self.elementtype = "UE"
        for i, a in enumerate(self.argsvalues):
            if type(a) == list and not self.argscast[i] == list:
                self.argscombos.append(a)
                self.argsvalues[i] = a[0]
            else:
                self.argscombos.append(None)
        self.checklabels()
                
    def checklabels(self):
        """chechs if the labels string is compliant"""
        
        if self.labels == None:
            self.labels = [""]*len(self.interface)
        if not len(self.labels) == len(self.interface):
            raise TypeError("Size of Lables does not match interface")
        for i, _ in enumerate(self.labels):
            if not self.labels[i] == self.interface[i] and self.interface[i] == "":
                raise TypeError("Size of Lables does not match interface")
        self.labels = [self.labels[: self.size[0]], self.labels[self.size[0] : self.size[0] + self.size[1]], self.labels[self.size[0] + self.size[1] : 2*self.size[0] + self.size[1]], self.labels[2*self.size[0] + self.size[1] :]]
                
    def creat(self):
        """creats the dt model element from the properties"""
        
        numberios = []
        numberins = 0
        numberouts = 0
        for n in self.interface:
            if n == "i":
                numberins =  numberins + 1
            if n == "o":
                numberouts =  numberouts + 1
        if not self.sizechangable[0] == False:
            numberios.append(numberins)
        if not self.sizechangable[1] == False:
            numberios.append(numberouts)
        return(self.creationfunction(self.name, *self.argsvalues, *numberios))
    
    def clear(self):
        """clears the property element"""
        
        for i, _ in enumerate(self.nodes):
            self.nodes[i] = None
        for i, _ in enumerate(self.inputs):
            self.inputs[i] = None
        for i, _ in enumerate(self.outputs):
            self.outputs[i] = None
        # self.controloutputelement = []
    
    def funksetargsvalues(self):
        """returns a functiont that sets the values"""
        
        argsvalues = copy.copy(self.argsvalues)
        name = self.name
        def setvalues():
            self.argsvalues = argsvalues
            self.name = name
        return setvalues
    
    def funkupdatesize(self, inputsize, outputsize):
        """returns a function that changes the size"""
        
        def updatesize():
            self.element.updatesize(inputsize, outputsize)
        return updatesize
        

class CEType(UType):
    
    def __init__(self, name, icon, image, creatfunction, argsvalues, argsnames, argsunits, argscast, argscompatibility, argsinfo, size=[1, 1], interface=["n", "", "n", ""], sizechangeable=[False, False], labels=None, orientation=0):
        """inits the Circuit element type of the property element"""

        super().__init__(name, icon, image, creatfunction, argsvalues, argsnames, argsunits, argscast, argscompatibility, argsinfo, size=size, interface=interface, sizechangeable=sizechangeable, labels=labels, orientation=orientation)
        self.elementtype = "CE"
        
class FEType(UType):
    
    def __init__(self, name, icon, image, creatfunction, argsvalues, argsnames, argsunits, argscast, argscompatibility, argsinfo, size=[1, 1], interface=["", "i", "", "o"], sizechangeable=[False, False], labels=None, orientation=0):
        """inits the Circuit element type of the property element"""

        super().__init__(name, icon, image, creatfunction, argsvalues, argsnames, argsunits, argscast, argscompatibility, argsinfo, size=size, interface=interface, sizechangeable=sizechangeable, labels=labels, orientation=orientation)
        self.elementtype = "FE"


class LabelType:
    
    def __init__(self, label, icon, draw, connected=False, connectedtype=-1):
        """inits the label type"""
        
        self.label = label
        self.name = label
        self.draw = draw
        self.icon = f"./library/icons/{icon}"
        self.element = None
        self.elementin = None
        self.connectedin = connected #connected to an layer above
        self.connectedtype = connectedtype #typed: -1: No connection 0:in/out 1:in 2:out
        self.elementtype = None
        
    def funksetlabel(self):
        """returns a function that sets the label"""
        
        name = self.name
        def setname():
            self.label = name
            self.name = name
        return setname
        

class CLabelType(LabelType):
    
    def __init__(self, label, icon, draw, connected=False, connectedtype=-1):
        """inits the Circuit Label type of the LabelType element"""
        
        super().__init__(label, icon, draw, connected=connected, connectedtype=-connectedtype)
        self.elementtype = "CLabel"

    
class FLabelType(LabelType):
    
    def __init__(self, label, icon, draw, connected=False, connectedtype=-1):
        """inits the Function Label type of the LabelType element"""
        
        super().__init__(label, icon, draw, connected=connected, connectedtype=connectedtype)
        self.elementtype = "FLabel"

class SubType:
    
    def __init__(self, name, sub, icon=None, draw=None, lock=False):
        
        self.name = name
        self.icon = icon
        if self.icon == None:
            self.icon = "./library/icons/nonfunctionalLib/subempty.svg"
        else:
            self.icon = f"./library/icons/{icon}"
        self.draw = draw
        self.sub = sub
        self.vectorin = []
        self.vectorout = []
        self.nodes = []
        self.element = None
        self.lock = lock
        self.elementtype = "Sub"
        
    def funksetname(self):
        """returns a function that sets the name"""
        
        name = self.name
        def setname():
            self.name = name
        return setname
    
    def funksetlock(self):
        """returns a function that sets the lock"""
        
        lock = self.lock
        def setlock():
            self.lock = lock
        return setlock

"""prints the information about this file"""
if PRINT_INFO:
    printinfo()