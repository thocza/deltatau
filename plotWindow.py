# -*- coding: utf-8 -*-
"""
Created on Tue Aug 16 09:30:41 2022

@author: czatho
This is the plot Window for deltaTau
alpha 0.0.1
    first UI
beta 0.1.0
    basic functionality
beta 0.1.1
    small runtime improvements
beta 0.1.2
    big fixes
beta 0.1.3
    added console output infos
beta 0.1.4
    performance improvement and solved some bugs
beta 0.1.5
    changed the window positioning from absolute to relative
beta 0.1.6
    bugfixed the x and y tick calculation
beta 0.1.7
    added the import export print and remove functions
    and put some style constants to the style.py file
beta 0.1.8
    plot works now with polygonF
beta 0.1.9
    Bug-fixes
"""

# info variables
PROGRAM_NAME = "deltaTau plot Window"
FILE_NAME = "plotWindow.py"
FILE_CREATION = "2022-08-16"
FILE_UPDATE = "2023-08-22"
FILE_VERSION = "beta 0.1.9"
DT_VERSION = "0.12.0"
PRINT_INFO = True


# libraries
from PyQt5 import QtWidgets, QtCore, QtGui, QtOpenGL, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QStyle
import time
import numpy as np
import sys
import copy
import math
import pickle as pc
import csv

import plotpropertyWindow as ppw
from style import *

#static variables
FONTSIZE = 15
FONTSIZETITLE = 20
DISTANCE = 30
HISTORYLENGTH = 20
FRAMEXRIGHTNOLEGEND = 20
FRAMEXRIGHTLEGEND = 180
FRAMEXLEFT = 80
FRAMEYTOP = 25
FRAMEYBOTTOM = 50
FRAMEMARGIN = 50


def printinfo():
    """shows all information about this file"""

    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)


class PlotWindow(QMainWindow):
    """plots graphs"""
    
    closed = QtCore.pyqtSignal(object)
    add = QtCore.pyqtSignal(object)
    
    def __init__(self, name, pos=None):
        """intits the Window
            sets name and icon
            """
        
        super().__init__()
        appicon = QtGui.QIcon()
        appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
        self.name = name
        self.setWindowIcon(appicon)
        self.setWindowTitle("deltaTau")
        if pos == None:
            pos = QtCore.QPoint(0, 0)
        self.initUI(pos)
        self.savelist = []
        self.imagelist = []
        
    def initUI(self, pos):
        """initialises the UI"""
        
        self.setGeometry(pos.x() + 200, pos.y() + 200, 800, 500)
        self.setWindowTitle(f"deltaTau {self.name}")
        
        self.vLayoutWindow = QtWidgets.QVBoxLayout()
        
        #TOP LAYER
        self.toolbar = QtWidgets.QToolBar("Toolbar")
        self.toolbar.setIconSize(QtCore.QSize(25, 25))
        self.toolbar.setMovable(False)
        self.addToolBar(self.toolbar)
        
        self.actionsave = QtWidgets.QAction(ICONS["SaveFile"], "")
        self.actionsave.setToolTip("save plots")
        self.actionsave.triggered.connect(self.save)
        
        self.actionload = QtWidgets.QAction(ICONS["OpenFile"], "")
        self.actionload.setToolTip("load plots")
        self.actionload.triggered.connect(self.load)
        
        self.actionadd = QtWidgets.QAction(ICONS["Plus"], "")
        self.actionadd.setToolTip("add plots")
        self.actionadd.triggered.connect(self.addfromfile)
        
        self.actionexport = QtWidgets.QAction(ICONS["Export"], "")
        self.actionexport.setToolTip("export to .csv")
        self.actionexport.triggered.connect(self.export)
        
        self.actionimport = QtWidgets.QAction(ICONS["Import"], "")
        self.actionimport.setToolTip("import from .csv")
        self.actionimport.triggered.connect(self.import_)
        
        self.actionprintpng = QtWidgets.QAction(ICONS["Image"], "")
        self.actionprintpng.setToolTip("prints the graphs to .png")
        self.actionprintpng.triggered.connect(self.printpng)
        
        self.toolbar.addAction(self.actionsave)
        self.toolbar.addAction(self.actionload)
        self.toolbar.addAction(self.actionadd)
        self.toolbar.addAction(self.actionexport)
        self.toolbar.addAction(self.actionimport)
        self.toolbar.addAction(self.actionprintpng)
        
        self.centralWidget = QtWidgets.QWidget()
        self.centralWidget.setLayout(self.vLayoutWindow)
        self.setCentralWidget(self.centralWidget)
        
    def addstream(self, name="", xlabel="", ylabel=""):
        """adds a plot to the window"""
        
        image = ImageWidget(name, xlabel, ylabel, windowposition=self.pos)
        self.savelist.append(image.getdata)
        self.closed.connect(image.stop)
        self.vLayoutWindow.addWidget(image)
        image.remove.connect(self.remove)
        data = image.opendatastream()
        
        return data
    
    def addplot(self, name="", xlabel="", ylabel=""):
        """adds a plot to the window"""
        image = ImageWidget(name, xlabel, ylabel, windowposition=self.pos)
        self.savelist.append(image.getdata)
        self.closed.connect(image.stop)
        self.vLayoutWindow.addWidget(image)
        image.remove.connect(self.remove)
        return image
    
    def addsaveplot(self, saveplot=None):
        """adds a plot to the window"""
        
        if type(saveplot) == SaveGraph:
            image = saveplot.creat()
            self.savelist.append(image.getdata)
            self.closed.connect(image.stop)
            self.vLayoutWindow.addWidget(image)
            image.remove.connect(self.remove)
            return image
    
    def remove(self, image):
        """removes the image from the vLayoutWindow"""
        
        self.savelist.remove(image.getdata)
        self.vLayoutWindow.removeWidget(image)
        image.deleteLater()
    
    def save(self):
        """saves the graphs"""
        save = []
        if len(self.savelist) > 0:
            for s in self.savelist:
                save.append(copy.deepcopy(s()))
                if save[-1] == None:
                    self.errordialog = QtWidgets.QErrorMessage()
                    self.errordialog.showMessage("data stream is still running cannot save plot")
                    return
            if not save[0].path == "":
                path = save[0].path
            else:
                path = save[0].name
            file = QtWidgets.QFileDialog.getSaveFileName(self, "Save plots", path, "plots (*.dtimg)")
            if file[0] == "":
                return
            file = file[0]
            for s in save:
                s.path = file
            plotobject = open(file, "wb")
            pc.dump(save, plotobject, protocol=pc.HIGHEST_PROTOCOL)
            plotobject.close()
            
    def load(self):
        """loads an img"""
        
        file = QtWidgets.QFileDialog().getOpenFileName(self, "Load plots", "", "plots (*.dtimg)")
        if file[0] == "":
            return 0
        plotobject = open(file[0], "rb")
        plotlist = pc.load(plotobject)
        plotobject.close()
        check = True
        if type(plotlist) == list:
            for l in plotlist:
                if not type(l) == SaveGraph:
                    check = False
        else:
            check = False
        if check:
            while self.vLayoutWindow.count() > 0:
                self.vLayoutWindow.removeWidget(self.vLayoutWindow.itemAt(0).widget())
            self.savelist = []
            for l in plotlist:
                self.addsaveplot(saveplot=l)
                
    def addfromfile(self):
        """loads an img"""
        
        file = QtWidgets.QFileDialog().getOpenFileName(self, "Load plots", "", "plots (*.dtimg)")
        if file[0] == "":
            return 0
        plotobject = open(file[0], "rb")
        plotlist = pc.load(plotobject)
        plotobject.close()
        check = True
        if type(plotlist) == list:
            for l in plotlist:
                if not type(l) == SaveGraph:
                    check = False
        else:
            check = False
        if check:
            for l in plotlist:
                self.addsaveplot(saveplot=l)
    
    def export(self):
        """exports the graphs to .csv files"""
        
        save = []
        for s in self.savelist:
            save.append(copy.deepcopy(s()))
            if save[-1] == None:
                self.errordialog = QtWidgets.QErrorMessage()
                self.errordialog.showMessage("data stream is still running cannot save plot")
                return
            file = QtWidgets.QFileDialog.getSaveFileName(self, "Export plot", "", "Table (*.csv)")
            if file[0] == "":
                return
            file = file[0]
            with open(file, 'w', newline='') as csvfile:
                writer = csv.writer(csvfile, delimiter=';')
                if len(save[-1].y) > 0:
                    writer.writerow(save[-1].name)
                    writer.writerow([save[-1].xlabel, save[-1].ylabel])
                    row = [""]
                    for y in save[-1].yinfo:
                        row.append(y[0])
                    writer.writerow(row)
                    for i, _ in enumerate(save[-1].y[0]):
                        row = [save[-1].x[i]]
                        for c in save[-1].y:
                            row.append(c[i])
                        writer.writerow(row)
                        
    def import_(self):
        """imports a gaph from .csv"""
        
        file = QtWidgets.QFileDialog().getOpenFileName(self, "Import plots", "", "Table (*.csv)")
        if file[0] == "":
            return 0
        try:
            with open(file[0], newline='') as csvfile:
                reader = csv.reader(csvfile, delimiter=';')
                save = SaveGraph()
                ysize = 0
                for i, row in enumerate(reader):
                    if i == 0 and len(row) > 0:
                        save.name =  row[0]
                    if i == 1 and len(row) > 1:
                        save.xlabel = row[0]
                        save.ylabel = row[1]
                    elif i == 1 and len(row) < 2:
                        raise TypeError("CSV-file does not match 2")
                    if i == 2 and len(row) > 1:
                        for j, info in enumerate(row):
                            if j > 0:
                                save.yinfo.append([row[j], (0, 0, 0), True])
                        ysize = len(row) - 1
                        save.y = [[] for _ in range(ysize)]
                    elif i == 2 and len(row) < 2:
                        raise TypeError("CSV-file does not match 3")
                    if i > 2 and len(row) == ysize + 1:
                        for j, r in enumerate(row):
                            if j == 0:
                                save.x.append(float(r))
                            else:
                                save.y[j - 1].append(float(r))
                    elif i > 2 and not len(row) == ysize + 1:
                        raise TypeError("CSV-file does not match 4")
                self.addsaveplot(saveplot=save)
        except:
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage("CSV-file does not match")
    
    def printpng(self):
        """prints the graphs as pngs"""

        file = QtWidgets.QFileDialog.getSaveFileName(self, "print as png", self.name , "Picture (*.png)")
        if file[0] == "":
            return
        file = file[0]
        self.centralWidget.grab().save(file)
    
    def closeEvent(self, event):
        """closes"""
        
        self.closed.emit(self)
        # self.close()
        # QApplication.quit()
    
class ImageWidget(QWidget):
    """image widged
        handels the circuit elements
        draws and updates them
        """
    
    remove = QtCore.pyqtSignal(object)
    
    def __init__(self, name, xlabel, ylabel, parent=None, windowposition=None):
        """initialises the image"""
        
        super(ImageWidget,self).__init__(parent)
        self.parent = parent
        self.name = name
        self.windowposition = windowposition
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.pix = QtGui.QPixmap(self.size())
        self.pix.fill(BACKGROUND_COLOR)
        self.qpainter = QtGui.QPainter()
        self.x = []
        self.y = []
        self.qpoints = []
        self.fpoints = []
        self.yinfo = []
        self.w = self.width()
        self.h = self.height()
        
        self.setMinimumSize(QtCore.QSize(500, 250))
        
        self.updater = None
        self.threadupdater = None
        
        self.threaddata = None
        self.data = None
        
        self.propertywindow = None
        
        self.frame = [0, 0, 0, 0] #xmin, xmax, ymin, ymax
        self.marker = [0, 0, 0, 0] #x1 y1 x2 y2
        self.framehistory = [[0, 0, 0, 0]]
        self.framehistoryposition = 0
        self.path = ""
        
        self.frameright = FRAMEXRIGHTNOLEGEND
        self.legend = False
        
        self.errordialog = None
        
    def opendatastream(self):
        """starts an updater thread and a data thread and returns the data thread"""
        
        self.data = DataThread()
        self.data.init()
        self.threaddata = QtCore.QThread()
        self.data.moveToThread(self.threaddata)
        # self.data.finish.connect(self.update)
        self.data.finish.connect(self.threaddata.quit)
        self.threaddata.start()
        
        self.updater = UpdateLoop()
        self.updater.init()
        self.threadupdater = QtCore.QThread()
        self.updater.moveToThread(self.threadupdater)
        self.updater.finished.connect(self.threadupdater.quit)
        self.data.finish.connect(self.stop)
        self.updater.update.connect(self.update)
        self.threadupdater.started.connect(self.updater.loop)
        self.threadupdater.start()
        
        return self.data
    
    def paintEvent(self,event):
        """paint event
            redraws the image when update is called
            """
        if not self.threaddata == None:
            if self.threaddata.isRunning():
                self.fpoints = []
                self.qpoints = []
                try:
                    [self.x, self.y] = self.data.get()
                    self.frame = [0, 0, 0, 0]
                    self.yinfo = self.data.getyinfo()
                except:
                    print("Error get")
            else:
                # self.threaddata.quit()
                # self.threaddata = None
                # self.data = None
                self.fpoints = []
                self.qpoints = []
                try:
                    [self.x, self.y] = self.data.get()
                    self.yinfo = self.data.getyinfo()
                    self.threaddata = None
                except:
                    print("Error get 2")
                self.frame = [0, 0, 0, 0]
                # self.updater.stop()
                # self.updater = None
                # self.threadupdater = None
                # pass
        if not len(self.y) == len(self.yinfo):
            self.yinfo = []
            # print("set")
            for _ in self.y:
                self.yinfo.append(["", (0, 0, 0), True])
        #draw plot
        width = self.width()
        height = self.height()
        
        check = True
        if len(self.x) < 3:
            check = False
            print("Info: not enough points")
        if len(self.y) == 0:
            check = False
            print("Info: no y data")
        for y in self.y:
            if not len(self.x) == len(y):
                # self.errordialog = QtWidgets.QErrorMessage()
                # self.errordialog.showMessage("y and x data do not have the same length")
                print("length y and x does not match")
                check = False
        
        self.qpainter = QtGui.QPainter()
        self.qpainter.begin(self)
        self.pix = self.pix.scaled(self.size())
        self.pix.fill(BACKGROUND_COLOR)
        self.qpainter.drawPixmap(QtCore.QPoint(), self.pix)
        
        if check:
            if self.frame[0] == self.frame[1]:
                xmax = max(self.x)
                xmin = min(self.x)
            else:
                xmax = self.frame[1]
                xmin = self.frame[0]
            if self.frame[2] == self.frame[3]:
                ymax = np.max(self.y)
                ymin = np.min(self.y)
            else:
                ymax = self.frame[3]
                ymin = self.frame[2]
            # if not self.threaddata == None:
            #     if not self.threaddata.isRunning():
            #         self.scalex = (width - self.frameright - FRAMEXLEFT)/(xmax - xmin + 0.0001)
            #     else:
            #         self.scalex = (width - self.frameright - FRAMEXLEFT - FRAMEMARGIN/2)/(xmax - xmin + 0.0001)
            # else:
            #     self.scalex = (width - self.frameright - FRAMEXLEFT)/(xmax - xmin + 0.0001)
            # # self.scaley = (height - FRAMEYTOP - FRAMEYBOTTOM - FRAMEMARGIN)/(ymax - ymin)
            # # self.yoff = -(height - FRAMEYTOP - FRAMEYBOTTOM - FRAMEMARGIN)/2 - FRAMEYBOTTOM - self.scaley*(ymax + ymin)/2
            # self.xoff = xmin*self.scalex - FRAMEXLEFT'
            self.setyticks()
            self.setxticks()
            
            self.drawPlot()
            
            self.drawFrame()
            self.drawName()
            
            self.drawxLabel()
            self.drawyLabel()
            
            self.drawyTicks()
            self.drawxTicks()
            
            
            self.drawmarker()
            
            if self.legend:
                self.drawlegend()
            
        self.qpainter.end()
    
    def setyticks(self, ticks=None):
        """sets the y ticks"""
        
        orderdown = [10, 2, 5, 3, 4, 6, 100, 20, 50, 10]
        orderup = [10, 5, 2.5, 10/3, 2, 5, 100, 50, 25, 10]
        if not ticks == None:
            self.frame[2] = min(self.frame, ticks)
            self.frame[3] = max(self.frame, ticks)
        height = self.height()
        if self.frame[2] == self.frame[3]:
            ymin = np.min(self.y)
            ymax = np.max(self.y)
        else:
            ymax = self.frame[3]
            ymin = self.frame[2]
        topfixed = True
        bottomfixed = True
        if ymax >= np.max(self.y):
            ymax = np.max(self.y)
            topfixed = False
        if ymin <= np.min(self.y):
            bottomfixed = False
            ymin = np.min(self.y)
        ytickmin = ymax - ymin
        if not topfixed and not bottomfixed:
            ytickmin = 1.25*ytickmin
        elif not topfixed or not bottomfixed:
            ytickmin = 1.125*ytickmin
        maxnyticks = int((height - FRAMEYBOTTOM - FRAMEYTOP)/25)
        minnyticks = int((height - FRAMEYBOTTOM - FRAMEYTOP)/50)
        if ytickmin == 0:
            ytick = 1
            ytoptick = ymax + ytick*math.ceil(minnyticks/2)
            ybottomtick = ymax - ytick*math.ceil(minnyticks/2)
            self.frame[3] = ytoptick
            self.frame[2] = ybottomtick
            self.scaley = (height - FRAMEYTOP - FRAMEYBOTTOM)/(ytoptick - ybottomtick)
            self.yoff = -(height - FRAMEYTOP - FRAMEYBOTTOM )/2 - FRAMEYTOP  - self.scaley*(ytoptick + ybottomtick)/2
            self.yticks = list(np.linspace(ybottomtick, ytoptick, int((ytoptick - ybottomtick)/ytick) + 1, endpoint= True))
        else:
            ytick = 10**int(np.log10(ytickmin))
            if ytickmin/ytick < minnyticks:
                for o in orderdown:
                    if ytickmin/ytick*o > minnyticks and ytickmin/ytick*o < maxnyticks:
                        ytick = ytick/o
                        break
            elif ytickmin/ytick > maxnyticks:
                for o in orderup:
                    if ytickmin/ytick/o > minnyticks and ytickmin/ytick/o < maxnyticks:
                        ytick = ytick*o
                        break
            if topfixed:
                ytoptick = math.floor(self.frame[3]/ytick)*ytick
            else:
                ytoptick = math.ceil((ymax + (ymax - ymin)*.125)/ytick)*ytick
                self.frame[3] = ytoptick
            if bottomfixed:
                ybottomtick = math.ceil(self.frame[2]/ytick)*ytick
            else:
                ybottomtick = math.floor((ymin - (ymax - ymin)*.125)/ytick)*ytick
                self.frame[2] = ybottomtick
            self.scaley = (height - FRAMEYTOP - FRAMEYBOTTOM)/(self.frame[3] - self.frame[2])
            self.yoff = -(height - FRAMEYTOP - FRAMEYBOTTOM)/2 - FRAMEYTOP  - self.scaley*(self.frame[3] + self.frame[2])/2
            self.yticks = list(np.linspace(ybottomtick, ytoptick, math.floor((ytoptick - ybottomtick)/ytick) + 1, endpoint= True))
    
    def setxticks(self, ticks=None):
        """sets the x ticks"""
        
        orderdown = [10, 2, 5, 3, 4, 6, 100, 20, 50, 10]
        orderup = [10, 5, 2.5, 10/3, 2, 5, 100, 50, 25, 10]
        if not ticks == None:
            self.frame[0] = min(self.frame, ticks)
            self.frame[1] = max(self.frame, ticks)
        width = self.width()
        if self.frame[0] == self.frame[1]:
            xmin = np.min(self.x)
            xmax = np.max(self.x)
        else:
            xmax = self.frame[1]
            xmin = self.frame[0]
        topfixed = True
        bottomfixed = True
        if xmax >= np.max(self.x):
            xmax = np.max(self.x)
            topfixed = False
        if xmin <= np.min(self.x):
            bottomfixed = False
            xmin = np.min(self.x)
        if xmin == 0:
            bottomfixed = True
        xtickmin = xmax - xmin
        if not self.threaddata == None:
            if not self.threaddata.isRunning() and not self.frame[0] == self.frame[1]:
                topfixed = True
        elif not self.frame[0] == self.frame[1]:
            topfixed = True
        
        if not topfixed and not bottomfixed:
            xtickmin = 1.25*xtickmin
        elif not topfixed or not bottomfixed:
            xtickmin = 1.125*xtickmin
        maxnxticks = int((width - FRAMEXLEFT - self.frameright)/50)
        minnxticks = int((width - FRAMEXLEFT - self.frameright)/100)
        
        if xtickmin == 0:
            xtick = 1
            xtoptick = xmax + xtick*math.ceil(minnxticks/2)
            xbottomtick = xmax - xtick*math.ceil(minnxticks/2)
            self.frame[1] = xtoptick
            self.frame[0] = xbottomtick
            self.scalex = (width - self.frameright - FRAMEXLEFT)/(xtoptick - xbottomtick)
            self.xoff = -(width - self.frameright - FRAMEXLEFT)/2 - FRAMEXLEFT  - self.scalex*(xtoptick + xbottomtick)/2
            self.xticks = list(np.linspace(xbottomtick, xtoptick, int((xtoptick - xbottomtick)/xtick) + 1, endpoint= True))
        else:
            xtick = 10**int(np.log10(xtickmin))
            if xtickmin/xtick < minnxticks:
                for o in orderdown:
                    if xtickmin/xtick*o > minnxticks and xtickmin/xtick*o < maxnxticks:
                        xtick = xtick/o
                        break
            elif xtickmin/xtick > maxnxticks:
                for o in orderup:
                    if xtickmin/xtick/o > minnxticks and xtickmin/xtick/o < maxnxticks:
                        xtick = xtick*o
                        break
            if topfixed:
                xtoptick = math.floor(self.frame[1]/xtick)*xtick
            else:
                xtoptick = math.ceil((xmax + (xmax - xmin)*.125)/xtick)*xtick
                self.frame[1] = xtoptick
            if bottomfixed:
                xbottomtick = math.ceil(self.frame[0]/xtick)*xtick
            else:
                xbottomtick = math.floor((xmin - (xmax - xmin)*.125)/xtick)*xtick
                self.frame[0] = xbottomtick
            self.scalex = (width - self.frameright - FRAMEXLEFT)/(self.frame[1] - self.frame[0])
            self.xoff = self.frame[0]*self.scalex - FRAMEXLEFT
            self.xticks = list(np.linspace(xbottomtick, xtoptick, math.floor((xtoptick - xbottomtick)/xtick) + 1, endpoint= True))
    
    def drawName(self):
        """draws the titel name"""
        
        pen = QtGui.QPen(QtGui.QColor(*WHITE))
        pen.setWidth(1)
        self.qpainter.setPen(pen)
        font = QtGui.QFont("Arial")
        font.setPixelSize(FONTSIZETITLE)
        self.qpainter.setFont(font)
        self.qpainter.drawText(10, 20, self.name)
    
    def drawFrame(self):
        """draws the frame"""
        
        pen = QtGui.QPen(BACKGROUND_COLOR)
        pen.setWidth(0)
        self.qpainter.setPen(pen)
        brush = QtGui.QBrush(BACKGROUND_COLOR, QtCore.Qt.SolidPattern)
        self.qpainter.setBrush(brush)
        self.qpainter.drawRect(0, 0, self.width(), FRAMEYTOP)
        self.qpainter.drawRect(0, 0, FRAMEXLEFT, self.height())
        self.qpainter.drawRect(0, self.height() - FRAMEYBOTTOM, self.width(), FRAMEYBOTTOM)
        self.qpainter.drawRect(self.width() - self.frameright, 0, self.frameright, self.height())
        self.qpainter.setBrush(QtCore.Qt.NoBrush)
        
        pen = QtGui.QPen(QtGui.QColor(*WHITE))
        pen.setWidth(1)
        self.qpainter.setPen(pen)
        self.qpainter.drawRect(FRAMEXLEFT, FRAMEYTOP, self.width() - self.frameright - FRAMEXLEFT, self.height() - FRAMEYTOP - FRAMEYBOTTOM)
        
    def drawxLabel(self):
        """draws the x label"""
        
        pen = QtGui.QPen(QtGui.QColor(*WHITE))
        pen.setWidth(1)
        self.qpainter.setPen(pen)
        font = QtGui.QFont("Arial")
        font.setPixelSize(FONTSIZE)
        self.qpainter.setFont(font)
        self.qpainter.drawText(FRAMEXLEFT + 30, self.height() - FRAMEYBOTTOM - 10, self.xlabel)
        
    def drawyLabel(self):
        """draws the y label"""
        
        pen = QtGui.QPen(QtGui.QColor(*WHITE))
        pen.setWidth(1)
        self.qpainter.setPen(pen)
        font = QtGui.QFont("Arial")
        font.setPixelSize(FONTSIZE)
        self.qpainter.setFont(font)
        self.qpainter.translate(FRAMEXLEFT + 10, self.height() - FRAMEYBOTTOM - 30)
        self.qpainter.rotate(-90)
        self.qpainter.translate(0, 15/2)
        self.qpainter.drawText(0, 0, self.ylabel)
        self.qpainter.translate(0, -15/2)
        self.qpainter.rotate(90)
        self.qpainter.translate(-FRAMEXLEFT - 10, -self.height() + FRAMEYBOTTOM + 30)
    
    def drawyTicks(self):
        """draws the y ticks"""
        
        pen = QtGui.QPen(QtGui.QColor(*WHITE))
        pen.setWidth(1)
        self.qpainter.setPen(pen)
        font = QtGui.QFont("Arial")
        font.setPixelSize(FONTSIZE)
        self.qpainter.setFont(font)
        for y in self.yticks:
            self.qpainter.drawLine(int(FRAMEXLEFT - 10), int(-self.scaley*y - self.yoff), FRAMEXLEFT, int(-self.scaley*y - self.yoff))
            self.qpainter.drawText(int(FRAMEXLEFT - 70), int(-self.scaley*y - self.yoff + FONTSIZE/2), str(float('%.6g' % y)))
            
    def drawxTicks(self):
        """draws the y ticks"""
        
        pen = QtGui.QPen(QtGui.QColor(*WHITE))
        pen.setWidth(1)
        self.qpainter.setPen(pen)
        font = QtGui.QFont("Arial")
        font.setPixelSize(FONTSIZE)
        self.qpainter.setFont(font)
        for x in self.xticks:
            self.qpainter.drawLine(int(self.scalex*x - self.xoff), int(self.height() - FRAMEYBOTTOM + 10), int(self.scalex*x - self.xoff), int(self.height() - FRAMEYBOTTOM))
            self.qpainter.drawText(int(self.scalex*x - self.xoff - FONTSIZE), int(self.height() - FRAMEYBOTTOM + 40), str(float('%.6g' % x)))
    
    def drawPlot(self):
        """draws the plot"""
        
        if self.fpoints == []:
            plotx = list(self.scalex*np.array(self.x) - self.xoff)
            for j, y in enumerate(self.y):
                if self.yinfo[j][2] == True:
                    ploty = list(-self.scaley*np.array(y) - self.yoff)
                    qpoints = []
                    if self.yinfo[j][1] == (0, 0, 0):
                        color = COLORLIST[j - int(j/6)*6]
                        self.yinfo[j][1] = color
                    else:
                        color = self.yinfo[j][1]
                    x = None
                    y = None
                    fx = []
                    fy = []
                    for i, _ in enumerate(ploty):
                        # if self.x[i] >= xminborder and self.x[i] <= xmaxborder and self.y[j][i] >= yminborder and self.y[j][i] <= ymaxborder:
                        if plotx[i] > 0 and plotx[i] < self.width() and ploty[i] > 0 and ploty[i] < self.width():
                            if (not int(plotx[i]) == x or not int(ploty[i]) == y) or i == len(ploty) - 1:
                                x = int(plotx[i])
                                y = int(ploty[i])
                                fx.append(self.x[i])
                                fy.append(self.y[j][i])
                                qpoints.append(QtCore.QPointF(x, y))
                    self.qpoints.append(qpoints)
                    self.fpoints.append([fx, fy])
        elif self.qpoints == []:
            for i, p in enumerate(self.fpoints):
                plotx = list(self.scalex*np.array(p[0]) - self.xoff)
                ploty = list(-self.scaley*np.array(p[1]) - self.yoff)
                qpoints = []
                for j, y in enumerate(ploty):
                    qpoints.append(QtCore.QPointF(plotx[j], y))
                self.qpoints.append(qpoints)
        index = 0
        for j, y in enumerate(self.yinfo):
            if y[2]:
                q = self.qpoints[index]
                if len(q) > 1:
                    if self.yinfo[j][1] == (0, 0, 0):
                        color = COLORLIST[j]
                        self.yinfo[j][1] = color
                    else:
                        color = self.yinfo[j][1]
                    pen = QtGui.QPen(QtGui.QColor(*color))
                    pen.setWidth(1)
                    self.qpainter.setPen(pen)
                    self.qpainter.drawPolyline(QtGui.QPolygonF(q))
                index = index + 1
    
    def drawlegend(self):
        """draws the legend"""
        
        index = 0
        lenyinfo = 0
        for y in self.yinfo:
            if y[2]:
                lenyinfo = lenyinfo + 1
        for i, y in enumerate(self.yinfo):
            if y[2]:
                pen = QtGui.QPen(QtGui.QColor(*WHITE))
                pen.setWidth(1)
                self.qpainter.setPen(pen)
                font = QtGui.QFont("Arial")
                font.setPixelSize(FONTSIZE)
                self.qpainter.setFont(font)
                self.qpainter.drawText(int(self.width() - self.frameright + 70), int(self.height()/2 + index*50 - lenyinfo*25), f"{y[0]}")
                brush = QtGui.QBrush(QtGui.QColor(*self.yinfo[i][1]), QtCore.Qt.SolidPattern)
                self.qpainter.setBrush(brush)
                pen = QtGui.QPen(QtGui.QColor(*self.yinfo[i][1]))
                pen.setWidth(1)
                self.qpainter.setPen(pen)
                self.qpainter.drawRect(int(self.width() - self.frameright + 30), int(self.height()/2 + index*50 - FONTSIZE/2 - lenyinfo*25), 30 , 3)
                self.qpainter.setBrush(QtCore.Qt.NoBrush)
                index = index + 1
    
    def setFrame(self, frame):
        """sets the frame"""
        
        # if self.frame[0] == self.frame[1]:
        #     xmin = (FRAMEXLEFT + self.xoff)/self.scalex
        #     xmax = (self.width() - self.frameright + self.xoff)/self.scalex
        # else:
        #     xmax = self.frame[1]
        #     xmin = self.frame[0]
        # if self.frame[2] == self.frame[3]:
        #     ymin = -(self.height() - FRAMEYBOTTOM + self.yoff)/self.scaley
        #     ymax = -(FRAMEYTOP + self.yoff)/self.scaley
        # else:
        #     ymax = self.frame[3]
        #     ymin = self.frame[2]
        # xminmarker = min(frame[0], frame[1])
        # xmaxmarker = max(frame[0], frame[1])
        # yminmarker = min(frame[2], frame[3])
        # ymaxmarker = max(frame[2], frame[3])
        if self.framehistoryposition < len(self.framehistory) - 1:
            self.framehistory = self.framehistory[0: self.framehistoryposition + 1]
        self.framehistoryposition = len(self.framehistory)
        self.framehistory.append(copy.copy(self.frame))
        self.frame[0] = min(frame[0], frame[1])
        self.frame[1] = max(frame[0], frame[1])
        self.frame[2] = min(frame[2], frame[3])
        self.frame[3] = max(frame[2], frame[3])
    
    def add(self, data):
        """adds data to the lists"""
        
        self.qpoints = []
        if len(data) == 2:
            if len(data[0]) == 1 and len(data[1]) > 0:
                self.x.append(data[0][0])
                if self.y == []:
                    self.y = [[] for _ in data[1]]
                for i, d in enumerate(data[1]):
                    self.y[i].append(d)
                self.update()
                    
    def _set(self, data):
        """sets the data"""
        
        print("set")
        self.fpoints = []
        self.qpoints = []
        if len(data) == 2:
            if len(data[0]) == 1 and len(data[1]) > 0:
                self.x = data[0][0]
                self.y = data[1]
                self.yinfo = []
                for _ in self.y:
                    self.yinfo.append(["", (0, 0, 0), True])
                self.update()
                
    def setx(self, x):
        """sets the x points"""
        
        self.fpoints = []
        self.qpoints = []
        if len(x) > 1:
            self.x = copy.copy(x)
            
    def addy(self, y, yname="", ycolor=(0, 0, 0), yshow=True):
        """adds a y value"""
        
        self.fpoints = []
        self.qpoints = []
        if len(y) > 1:
           self.y.append(copy.copy(y)) 
           self.yinfo.append([yname, ycolor, yshow])
           self.frame[2] = np.min(self.y)
           self.frame[3] = np.max(self.y)
           return len(self.y) - 1
        return None
    
    def setyinfo(self, yinfo):
        """sets the yinfo"""
        
        self.yinfo = copy.deepcopy(yinfo)
        self.update()
    
    def setname(self, name):
        """sets the name of the plot"""
        
        self.name = name
        
    def setxlabel(self, xlabel):
        """sets the xlabel of the plot"""
        
        self.xlabel = xlabel
        
    def setylabel(self, ylabel):
        """sets the xlabel of the plot"""
        
        self.ylabel = ylabel
    
    def setlegend(self, legend):
        """sets the legend True or False"""
        
        self.legend = legend
        if self.legend:
            self.frameright = FRAMEXRIGHTLEGEND
        else:
            self.frameright = FRAMEXRIGHTNOLEGEND
    
    def removey(self, indexlist):
        """removes graphs by index"""
        
        self.fpoints = []
        self.qpoints = []
        yremove = []
        for i in indexlist:
            yremove.append(self.y[i])
        for y in yremove:
            self.y.remove(y)
        if len(self.yinfo) > len(self.y):
            yremove = []
            for i in indexlist:
                yremove.append(self.yinfo[i])
            for y in yremove:
                self.yinfo.remove(y)
    
    def mouseReleaseEvent(self, event):
        """mouse release event called when drag operation is over"""
        
        check = False
        if not self.threaddata == None:
            if not self.threaddata.isRunning():
                check = True
        else:
            check = True
        if check:
            if not self.marker[0] == self.marker[2] and not self.marker[1] == self.marker[3]:
                self.fpoints = []
                self.qpoints = []
                if self.frame[0] == self.frame[1]:
                    xmin = (FRAMEXLEFT + self.xoff)/self.scalex
                    xmax = (self.width() - self.frameright + self.xoff)/self.scalex
                else:
                    xmax = self.frame[1]
                    xmin = self.frame[0]
                if self.frame[2] == self.frame[3]:
                    ymin = -(self.height() - FRAMEYBOTTOM + self.yoff)/self.scaley
                    ymax = -(FRAMEYTOP + self.yoff)/self.scaley
                else:
                    ymax = self.frame[3]
                    ymin = self.frame[2]
                xminmarker = min(self.marker[0], self.marker[2])
                xmaxmarker = max(self.marker[0], self.marker[2])
                yminmarker = max(self.marker[1], self.marker[3])
                ymaxmarker = min(self.marker[1], self.marker[3])
                xminmarker = (xminmarker + self.xoff)/self.scalex
                xmaxmarker = (xmaxmarker + self.xoff)/self.scalex
                yminmarker = -(yminmarker + self.yoff)/self.scaley
                ymaxmarker = -(ymaxmarker + self.yoff)/self.scaley
                if self.framehistoryposition < len(self.framehistory) - 1:
                    self.framehistory = self.framehistory[0: self.framehistoryposition + 1]
                self.framehistoryposition = len(self.framehistory)
                self.framehistory.append(copy.copy(self.frame))
                self.frame[0] = max(xmin, xminmarker)
                self.frame[1] = min(xmax, xmaxmarker)
                self.frame[2] = max(ymin, yminmarker)
                self.frame[3] = min(ymax, ymaxmarker)
                self.marker = [0, 0, 0, 0]
                self.update()
        
    def mouseMoveEvent(self, event):
        """mouse move event
            if left button pressed show a marker
            """
        
        check = False
        if not self.threaddata == None:
            if not self.threaddata.isRunning():
                check = True
        else:
            check = True
        if check:
            if event.buttons() == QtCore.Qt.LeftButton:
                if self.marker[0] == 0 and self.marker[1] == 0:
                    self.marker[0] = event.x()
                    self.marker[1] = event.y()
                self.marker[2] = event.x()
                self.marker[3] = event.y()
                self.update()
    
    def mousePressEvent(self, event):
        """activated when mouse press
            set the variabels for an drag event
            and updates all the circuit elements with the right event
            """
        
        check = False
        if not self.threaddata == None:
            if not self.threaddata.isRunning():
                check = True
        else:
            check = True
        if check:
            if event.buttons() == QtCore.Qt.RightButton:
                if self.framehistoryposition > 0:
                    self.fpoints = []
                    self.qpoints = []
                    self.frame = copy.copy(self.framehistory[self.framehistoryposition])
                    self.framehistoryposition = self.framehistoryposition - 1
                    self.update()
    
    def wheelEvent(self, event):
        """scrolls in the plot"""
        
        check = False
        if not self.threaddata == None:
            if not self.threaddata.isRunning():
                check = True
        else:
            check = True
        if check:
            self.fpoints = []
            self.qpoints = []
            if event.modifiers() & QtCore.Qt.ShiftModifier:
                xdelta = self.frame[1] - self.frame[0] 
                xdelta = -xdelta*0.02*event.angleDelta().y()/120
                xmax = max(self.x)
                xmin = min(self.x)
                if self.frame[0] + xdelta < xmin:
                    self.frame[1] = self.frame[1] + xmin - self.frame[0]
                    self.frame[0] = xmin 
                elif self.frame[1] + xdelta > xmax:
                    self.frame[0] = self.frame[0] + xmax - self.frame[1]
                    self.frame[1] = xmax
                else:
                    self.frame[0] = self.frame[0] + xdelta
                    self.frame[1] = self.frame[1] + xdelta
                self.update()
            else:
                if not self.frame[2] == self.frame[3]:
                    ydelta = self.frame[3] - self.frame[2] 
                    ydelta = -ydelta*0.02*event.angleDelta().y()/120
                    ymax = np.max(self.y) + ydelta*0.1
                    ymin = np.min(self.y) - ydelta*0.1
                    if self.frame[2] + ydelta < ymin:
                        self.frame[3] = self.frame[3] + ymin - self.frame[2]
                        self.frame[2] = ymin 
                    elif self.frame[3] + ydelta > ymax:
                        self.frame[2] = self.frame[2] + ymax - self.frame[3]
                        self.frame[3] = ymax
                    else:
                        self.frame[2] = self.frame[2] + ydelta
                        self.frame[3] = self.frame[3] + ydelta
                    self.update()
    
    def mouseDoubleClickEvent(self, event):
        """mouse double click event
            to change properties of the elements
            """
        
        check = False
        if not self.threaddata == None:
            if not self.threaddata.isRunning():
                check = True
        else:
            check = True
        if check:
            if event.buttons() == QtCore.Qt.LeftButton:
                if self.frame[0] == self.frame[1]:
                    xmin = (FRAMEXLEFT + self.xoff)/self.scalex
                    xmax = (self.width() - self.frameright + self.xoff)/self.scalex
                else:
                    xmax = self.frame[1]
                    xmin = self.frame[0]
                if self.frame[2] == self.frame[3]:
                    ymin = -(self.height() - FRAMEYBOTTOM + self.yoff)/self.scaley
                    ymax = -(FRAMEYTOP + self.yoff)/self.scaley
                else:
                    ymax = self.frame[3]
                    ymin = self.frame[2]
                self.propertywindow = ppw.PlotPropertyWindow(self.name, self.xlabel, self.ylabel, [xmin, xmax, ymin, ymax], self.legend, self.yinfo, self.removeimage, pos=self.windowposition())
                self.propertywindow.namechange.connect(self.setname)
                self.propertywindow.xlabelchange.connect(self.setxlabel)
                self.propertywindow.ylabelchange.connect(self.setylabel)
                self.propertywindow.framechange.connect(self.setFrame)
                self.propertywindow.legendchange.connect(self.setlegend)
                self.propertywindow.removeplots.connect(self.removey)
                self.propertywindow.yinfochange.connect(self.setyinfo)
                self.propertywindow.closed.connect(self.closepropertywindow)
                self.propertywindow.show()
       
    def resizeEvent(self, event):
        """resize the frame"""

        self.qpoints = []
        # self.setxticks()
        # self.setyticks()         
       
    def closepropertywindow(self):
        """closes the property window"""
        
        self.propertywindow = None
        self.update()
    
    def drawmarker(self):
        """draws the marker"""
        
        if not self.marker[0] == self.marker[2] and not self.marker[1] == self.marker[3]:
            pen = QtGui.QPen(QtGui.QColor(*GRIDCOLOR))
            pen.setWidth(1)
            self.qpainter.setPen(pen)
            self.qpainter.drawRect(self.marker[0], self.marker[1], self.marker[2] - self.marker[0], self.marker[3] - self.marker[1])
    
    def stop(self):
        """stops the loop"""
        
        if not self.updater == None:
            self.updater.run = False
        # if not self.updater == None:
        #     self.updater.stop()
        # self.data.quit()
        
    def getdata(self):
        """returns the data to save it"""
        
        check = False
        if not self.threaddata == None:
            if not self.threaddata.isRunning():
                check = True
        else:
            check = True
        if check:
            return SaveGraph(self)
        
    def removeimage(self):
        """removes the image from the plot-windo"""
        
        if not self.threadupdater == None:
            self.threadupdater.quit()
        if not self.threaddata == None:
            self.threaddata.quit()
        self.remove.emit(self)
    
class SaveGraph:
    """data type to save graphs"""
    
    def __init__(self, image=None):
        """saves the data"""
        
        if not image == None:
            self.name = image.name
            self.xlabel = image.xlabel
            self.ylabel = image.ylabel
            self.x = image.x
            self.y = image.y
            self.path = image.path
            self.yinfo = image.yinfo
        else:
            self.name = "name"
            self.xlabel = "xlabel"
            self.ylabel = "ylabel"
            self.x = []
            self.y = [[]]
            self.path = ""
            self.yinfo = []
        
    def creat(self):
        """creats an image widget from data"""
        
        image = ImageWidget(self.name, self.xlabel, self.ylabel)
        image.path = self.path
        image.x = self.x
        image.y = self.y
        image.yinfo = self.yinfo
        return image
        
                
class UpdateLoop(QtCore.QObject):
    """updateloop class for continous updatig the plot"""
    
    update = QtCore.pyqtSignal()
    finished = QtCore.pyqtSignal()
    
    def init(self):
        """inits the worker"""
        
        self.run = True
        
    def loop(self):
        """runs a infinite loop"""
        
        index = 0
        while(self.run):
            self.update.emit()
            time.sleep(0.1)
            # print(f"run{index}")
            index = index + 1
        self.finished.emit()
        
    def stop(self):
        """stops the loop"""
        
        # print("stop")
        self.run = False

class DataThread(QtCore.QObject):
    
    finish = QtCore.pyqtSignal()
    
    def init(self):
        """inits the data"""
        
        self.x = []
        self.y = []
        self.yinfo = []
        self.oldx = []
        self.oldy = []
        self.mutex = QtCore.QMutex()
        # self.mutex.unlock()
        
    def add(self, data):
        """adds data to the lists"""
        
        try:
            self.mutex.lock()
            if len(data) == 2:
                if len(data[0]) == 1 and len(data[1]) > 0:
                    self.x.append(data[0][0])
                    if self.y == []:
                        self.y = [[] for _ in data[1]]
                    for i, d in enumerate(data[1]):
                        self.y[i].append(d)
            self.mutex.unlock()
        except: pass
                    
    def _set(self, data):
        """sets the data"""
        
        if self.mutex.tryLock():
            if len(data) == 2:
                if type(data[0]) == list and len(data[1]) > 0:
                    self.x = copy.deepcopy(data[0])
                    self.y = copy.deepcopy(data[1])
            self.mutex.unlock()
        else:
            pass
            # print("set: except")
    
    def get(self):
        """returns a copy of x and y"""
        
        try:
            self.mutex.lock()
            oldx = copy.deepcopy(self.x)
            oldy = copy.deepcopy(self.y)
            self.mutex.unlock()
            return [oldx, oldy]
        except:
            print("Error: Mutex lock error")
    
    def setyinfo(self, yinfo):
        """sets the yinfo"""
        
        if self.mutex.tryLock():
            self.yinfo = copy.deepcopy(yinfo)
            self.mutex.unlock()
        
    def getyinfo(self):
        """returns the yinfo"""
        
        self.mutex.lock()
        yinfo = copy.deepcopy(self.yinfo)
        self.mutex.unlock()
        return yinfo
    
    def close(self):
        """closes the thread"""
        
        self.mutex.lock()
        self.finish.emit()
        self.mutex.unlock()
        
                
# class Test(QtCore.QObject):
    
#     finished = QtCore.pyqtSignal()
    
#     def init(self):
#         self.window = PlotWindow("")
#         self.window.show()
#         self.data = self.window.addstream("Test", "time", "ylabel")
#         self.data2 = self.window.addstream("Test", "time", "ylabel")
        
#     def run(self):
#         x = np.linspace(0, 50, 1000)
#         y1 = np.sin(x)+1
#         y2 = np.cos(x)
#         for i, _ in enumerate(x):
#             time.sleep(0.0005)
#             self.data.add([[x[i]],[y1[i], y2[i]]])
#             self.data2.add([[x[i]],[y1[i]]])
#         self.data.close()
#         self.data2.close()
#         self.finished.emit()
        
    
if PRINT_INFO:
    printinfo()
    
# """plotWindow test"""
# app = QApplication(sys.argv)
# # # window = PlotWindow("test")
# # # window.show()
# # # x = np.linspace(1, 50, 1000)
# # # y = np.sin(x)
# # # plot = window.addplot()
# # # plot._set([[list(x)], [list(y)]])
# # # plot.setyinfo([["Test", (0, 0, 0), True]])
# # # window.addstream()
# # # # window.test()
# # # # adds = QtCore.pyqtSignal(object)
# # # # adds.connect(add)
# # # sys.exit(app.exec_())
# # # for i, _ in enumerate(x):
# # #     time.sleep(0.001)
# # #     print(i)
# #     # adds.emit([[x[i]],[y[i]]])
# obj = Test()
# obj.init()
# window = obj.window
# thread = QtCore.QThread()
# obj.moveToThread(thread)
# obj.finished.connect(thread.quit)
# thread.started.connect(obj.run)
# thread.start()
# sys.exit(app.exec_())