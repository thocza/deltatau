# -*- coding: utf-8 -*-
"""
Created on Fri Dec 16 09:09:06 2022

@author: thocza
This is the property window and property data
alpha 0.0.1
    first UI
    and property data class
alpha 0.0.2
    added library expand
alpha 0.0.3
    corrected typo
alpha 0.0.4
    added window position
"""

# info variables
PROGRAM_NAME = "deltaTau property window GUI"
FILE_NAME = "mainProperty.py"
FILE_CREATION = "2022-12-16"
FILE_UPDATE = "2023-11-24"
FILE_VERSION = "alpha 0.0.4"
DT_VERSION = "0.12.0"
PRINT_INFO = True


# libraries
from PyQt5 import QtWidgets, QtCore, QtGui, QtOpenGL, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QStyle
import sys
import ctypes
import sys
import os.path
import pickle as pc
from style import *

def printinfo():
    """shows all information about this file"""

    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)
    
    
class Property:
    
    def __init__(self):
        """inits the propery class"""
        
        self.ignoreloop = False
        self.libraryexpand = True
        self.windowposition = [200, 200, 1200, 800]
        self.maximized = False
        self.path = "./pp.bit"
        
    def load(self):
        """checks if the property file exist then loads it
        if not creats the file with the standard"""
        
        if os.path.exists(self.path):
            if os.path.isfile(self.path):
                try:
                   propertyobject = open(self.path, "rb")
                   loadobject = pc.load(propertyobject)
                   propertyobject.close()
                   if type(loadobject) == Property:
                       return loadobject
                except:
                    pass
        self.save()
        return self
    
    def setwindowstate(self, position, maximized):
        """sets the window position"""
        
        self.windowposition = position
        self.maximized = maximized
    
    def save(self):
        """saves the properties"""
        
        propertyobject = open(self.path, "wb")
        pc.dump(self, propertyobject, protocol=pc.HIGHEST_PROTOCOL)
        propertyobject.close()
        
        
class PropertyWindow(QWidget):
    """Property Window for edditing properties dt
        """
    
    def __init__(self, propertydata, pos=None):
        """intits the Window
            """
        
        super().__init__()
        self.property = propertydata
        appicon = QtGui.QIcon()
        appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
        self.setWindowIcon(appicon)
        self.setWindowTitle("deltaTau")
        self.errordialog = None
        if pos == None:
            pos = QtCore.QPoint(0, 0)
        self.initUI(pos)

    def initUI(self, pos):
        """inits the window"""
        
        self.setGeometry(pos.x() + 300, pos.y() + 300, 500, 100)
        self.setFixedSize(500, 100)
        self.setWindowTitle(f"deltaTau properties")
        
        self.hLayoutWindow = QtWidgets.QHBoxLayout(self)
        self.vLayoutWindow = QtWidgets.QVBoxLayout()
        
        self.hLayoutexpand = QtWidgets.QHBoxLayout()
        self.expandLabel = QtWidgets.QLabel()
        self.expandLabel.setMinimumSize(QtCore.QSize(450, 0))
        self.expandLabel.setMaximumSize(QtCore.QSize(450, 25))
        self.expandLabel.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.expandLabel.setText("Expand library on launch")
        self.expandcheck = QtWidgets.QCheckBox()
        self.expandcheck.setChecked(self.property.libraryexpand)
        
        self.hLayoutexpand.addWidget(self.expandcheck)
        self.hLayoutexpand.addWidget(self.expandLabel)
        
        self.hLayoutloop = QtWidgets.QHBoxLayout()
        self.loopLabel = QtWidgets.QLabel()
        self.loopLabel.setMinimumSize(QtCore.QSize(450, 0))
        self.loopLabel.setMaximumSize(QtCore.QSize(450, 25))
        self.loopLabel.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.loopLabel.setText("If a loop is detected, it will be ignored")
        self.loopcheck = QtWidgets.QCheckBox()
        self.loopcheck.setChecked(self.property.ignoreloop)
        
        self.hLayoutloop.addWidget(self.loopcheck)
        self.hLayoutloop.addWidget(self.loopLabel)
        
        self.hLayoutButtons = QtWidgets.QHBoxLayout()
        
        self.savebutton = QtWidgets.QPushButton("Save")
        self.savebutton.setIcon(ICONS["SaveFile"])
        self.savebutton.clicked.connect(self.save)
        self.okbutton = QtWidgets.QPushButton("OK")
        self.okbutton.setIcon(ICONS["Apply"])
        self.okbutton.clicked.connect(self.ok)
        self.cancelbutton = QtWidgets.QPushButton("Cancel")
        self.cancelbutton.setIcon(ICONS["Discard"])
        self.cancelbutton.clicked.connect(self.close)
        
        self.hLayoutButtons.addWidget(self.savebutton)
        self.hLayoutButtons.addWidget(self.okbutton)
        self.hLayoutButtons.addWidget(self.cancelbutton)
        
        self.vLayoutWindow.addLayout(self.hLayoutexpand)
        self.vLayoutWindow.addLayout(self.hLayoutloop)
        self.vLayoutWindow.addLayout(self.hLayoutButtons)
        self.hLayoutWindow.addLayout(self.vLayoutWindow)
        
    def closeEvent(self, event):
        """close Window"""
        
        self.close()
        
    def save(self):
        """save input values"""
        
        self.property.ignoreloop = self.loopcheck.isChecked()
        self.property.libraryexpand = self.expandcheck.isChecked()
        self.property.save()
        return True
    
    def ok(self):
        """save input values and close window when values are valid"""
        
        if self.save() == True:
            self.close()
        
    def keyPressEvent(self, event):
        """save event when enter key is pressed"""
        
        if event.key() == QtCore.Qt. Key_Return:
            if self.save() == True:
                self.close()
        
        
"""prints the information about this file"""
if PRINT_INFO:
    printinfo()