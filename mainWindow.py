# -*- coding: utf-8 -*-
"""
@author: tomcz
This is the main Window for deltaTau
alpha 0.0.1
    first UI for tests
alpha 0.0.2
    delta displays Userinformation
alpha 0.0.3
    now compiles and runs the code
beta 0.1.0
    basic functionality is esablished
    source code is fully commented
beta 0.1.1
    automated add library elements
beta 0.1.2
    added a logo picture while loading the window
    and added status edit
beta 0.1.3
    added the creation of an dt model and then compiling and running it
beta 0.1.4
    added the possibility to alter the parameters by the window
    and removes the floating elements from the cad surface
    if a new one shall be added
beta 0.1.5
    Bug fixes
beta 0.2.0
    calculation runs now in thread, there are many issues
    saving and loading circuits is now possible
beta 0.2.1
    implemented multithreading for the simulation
beta 0.3.0
    added the elements list to display the results
beta 0.3.1
    bugfix
beta 0.3.2
    autoremove can now be set
beta 0.3.3
    cosmetical changes
beta 0.4.0
    tabs added
beta 0.4.1
    added save check
beta 0.4.2
    made subcircuits possible
beta 0.4.3
    bugfix
beta 0.4.4
    added toolbar and new icons
beta 0.4.5
    bug fix
beta 0.4.6
    added undo and redo
beta 0.4.7
    language change to englisch
beta 0.4.8
    load libraries from library directory
beta 0.4.9
    added new icons
beta 0.4.10
    added the new plotwindow to the resultlist
beta 0.4.11
    added the possibility to add more results to one graph
beta 0.4.12
    made ready for the new deltaTau engine
beta 0.4.13
    improved the result-list-itemes text
beta 0.4.14
    added manual
beta 0.4.15
    enabled arrays in the the results list
beta 0.4.16
    added the new circiut elements architecture
beta 0.4.17
    added the epsilon parameter#
beta 0.4.18
    bug fix
beta 0.4.19
    added changes for solvers and subwindow positioning
beta 0.4.20
    addded the posibility to make pngs of the cad surface
    and put some style constants to the style.py file
beta 0.4.21
    bug fixes
beta 0.4.22
    logger added and property window
beta 0.4.23
    added close emit for the Mainwindow
beta 0.4.24
    added library expand to property
beta 0.4.25
    checks if inf or nan is in public variables
beta 0.4.26
    Bug-fixes
beta 0.4.27
    Window opens in the state it was closed
beta 0.4.28
    when closing the main window all other windows close aswell
    error catch when saving or loading defect files
beta 0.4.29
    updated to new baseLib functions
beta 0.4.30
    size of bottomstatus increased
"""

# info variables
PROGRAM_NAME = "deltaTau main Window"
FILE_NAME = "mainWindow.py"
FILE_CREATION = "2022-01-26"
FILE_UPDATE = "2024-10-27"
FILE_VERSION = "beta 0.4.30"
DT_VERSION = "0.12.0"
PRINT_INFO = True


# libraries
from PyQt5 import QtWidgets, QtCore, QtGui, QtOpenGL, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QStyle
import numpy as np
import sys
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import guigraphics as gg
import ggelements as gge
import ctypes
import baseLib as bl
import copy
import threading as th
import time
import pickle as pc
import glob
import os
import webbrowser

# import electricLib as eLib
# import functionLib as fLib
# import nonfunctionalLib as nfLib
import dtengine as dtcore
import namechangeWindow as ncw
import plotWindow as pw
import mainProperty as mp
import logWindow as lw
from style import *



#static variables
FONTSIZE = 12
FONTSIZELIST = 10
BASELIBRARIES = [
    "SubLib.lib",
    "ELabelLib.lib",
    "electricLib.lib",
    "FLabelLib.lib",
    "functionLib.lib"
    ]


def printinfo():
    """shows all information about this file"""

    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)


class MainWindow(QMainWindow):
    """Main Window of the programm
        inherits from QWidget
        """
    
    endapp = QtCore.pyqtSignal()
    
    def __init__(self, IOHandler):
        """intits the Window
            also sets up the IOHandler for communication with the dt Framework
            sets name and icon
            """
        
        super().__init__()
        appicon = QtGui.QIcon()
        appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
        self.IOHandler = IOHandler
        self.setWindowIcon(appicon)
        self.setWindowTitle(f"deltaTau {DT_VERSION}")
        self.property = mp.Property()
        self.property = self.property.load()
        self.propertywindow = None
        self.initUI()
        self.namechangewindow = None
        self.plotwindows = []
        self.resultwindows = []
        self.logger = ""
        self.loggerWindow = None
        self.errordialog = None
        
    def initUI(self):
        """builds main Window"""
        
        self.setGeometry(*self.property.windowposition)
        if self.property.maximized:
            self.showMaximized()
        self.setWindowTitle(f"deltaTau {DT_VERSION}")
        
        # self.hLayoutWindow = QtWidgets.QHBoxLayout(self)
        self.vLayoutWindow = QtWidgets.QVBoxLayout()
        
        #TOP LAYER
        self.toolbar = QtWidgets.QToolBar("Toolbar")
        self.toolbar.setIconSize(QtCore.QSize(25, 25))
        self.toolbar.setMovable(False)
        self.addToolBar(self.toolbar)
        
        self.actionnewcircuit = QtWidgets.QAction(ICONS["NewFile"], "")
        self.actionnewcircuit.setToolTip("new circuit")
        self.actionnewcircuit.triggered.connect(self.newcircuit)
        
        self.actionsave = QtWidgets.QAction(ICONS["SaveFile"], "")
        self.actionsave.setToolTip("save circuit")
        self.actionsave.triggered.connect(self.savecirciut)
        
        self.actionload = QtWidgets.QAction(ICONS["OpenFile"], "")
        self.actionload.setToolTip("load a circuit")
        self.actionload.triggered.connect(self.loadcirciut)
        
        self.actionundo = QtWidgets.QAction(ICONS["Undo"], "")
        self.actionundo.setToolTip("Undo")
        self.actionundo.triggered.connect(self.undo)
        
        self.actionredo = QtWidgets.QAction(ICONS["Redo"], "")
        self.actionredo.setToolTip("Redo")
        self.actionredo.triggered.connect(self.redo)
        
        self.actionproperties = QtWidgets.QAction(ICONS["Tool"], "")
        self.actionproperties.setToolTip("properties")
        self.actionproperties.triggered.connect(self.openpropertywindow)
        
        self.actionhelp = QtWidgets.QAction(ICONS["Help"], "")
        self.actionhelp.setToolTip("help")
        self.actionhelp.triggered.connect(self.openmanual)
        
        self.actioninfo = QtWidgets.QAction(ICONS["Info"], "")
        self.actioninfo.setToolTip("info")
        self.actioninfo.triggered.connect(self.openinfo)
        
        self.actiondelete = QtWidgets.QAction(ICONS["Delete"], "")
        self.actiondelete.setToolTip("Deletemode")
        self.actiondelete.triggered.connect(self.IOHandler.toggeldeletemode)
        
        self.actionsim = QtWidgets.QAction(ICONS["Play"], "")
        self.actionsim.setToolTip("Simulate")
        self.actionsim.triggered.connect(self.IOHandler.comSimClicked)
        
        self.autoremoveDescription = QtWidgets.QLabel()
        self.autoremoveDescription.setMinimumSize(QtCore.QSize(80, 20))
        self.autoremoveDescription.setMaximumSize(QtCore.QSize(80, 20))
        self.autoremoveDescription.setText("auto-remove:")
        self.autoremoveDescription.setStyleSheet("padding :5px")
        self.autoremoveDescription.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        self.autoremovecheck = QtWidgets.QCheckBox()
        self.autoremovecheck.setChecked(False)
        self.autoremovecheck.stateChanged.connect(self.autoremovechanged)
        
        self.simulationtimeDescription = QtWidgets.QLabel()
        self.simulationtimeDescription.setMinimumSize(QtCore.QSize(60, 30))
        self.simulationtimeDescription.setMaximumSize(QtCore.QSize(60, 30))
        self.simulationtimeDescription.setText("time [s]:")
        self.simulationtimeDescription.setStyleSheet("padding :5px")
        self.simulationtimeDescription.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        self.simulationtimeEdit = QtWidgets.QLineEdit()
        self.simulationtimeEdit.setMinimumSize(QtCore.QSize(40, 20))
        self.simulationtimeEdit.setMaximumSize(QtCore.QSize(40, 20))
        self.simulationtimeEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.simulationtimeEdit.setText("0.1")
        
        self.timestepDescription = QtWidgets.QLabel()
        self.timestepDescription.setMinimumSize(QtCore.QSize(50, 30))
        self.timestepDescription.setMaximumSize(QtCore.QSize(50, 30))
        self.timestepDescription.setText("Δ[s]:")
        self.timestepDescription.setStyleSheet("padding :5px")
        self.timestepDescription.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        self.timestepEdit = QtWidgets.QLineEdit()
        self.timestepEdit.setMinimumSize(QtCore.QSize(40, 20))
        self.timestepEdit.setMaximumSize(QtCore.QSize(40, 20))
        self.timestepEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.timestepEdit.setText("1e-4")
        
        self.minimumstepDescription = QtWidgets.QLabel()
        self.minimumstepDescription.setMinimumSize(QtCore.QSize(50, 30))
        self.minimumstepDescription.setMaximumSize(QtCore.QSize(50, 30))
        self.minimumstepDescription.setText("ε[%]:")
        self.minimumstepDescription.setStyleSheet("padding :5px")
        self.minimumstepDescription.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        self.minimumstepEdit = QtWidgets.QLineEdit()
        self.minimumstepEdit.setMinimumSize(QtCore.QSize(40, 20))
        self.minimumstepEdit.setMaximumSize(QtCore.QSize(40, 20))
        self.minimumstepEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.minimumstepEdit.setText("1e-5")
        
        self.integrationalgorythmDescription = QtWidgets.QLabel()
        self.integrationalgorythmDescription.setMinimumSize(QtCore.QSize(60, 30))
        self.integrationalgorythmDescription.setMaximumSize(QtCore.QSize(60, 30))
        self.integrationalgorythmDescription.setText("Solver:")
        self.integrationalgorythmDescription.setStyleSheet("padding :5px")
        self.integrationalgorythmDescription.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        self.integrationalgorythmCombo = QtWidgets.QComboBox()
        self.integrationalgorythmCombo.addItems(["Radau", "LSODA", "BDF"])
        self.integrationalgorythmCombo.setMinimumSize(QtCore.QSize(60, 20))
        self.integrationalgorythmCombo.setMaximumSize(QtCore.QSize(60, 20))
        
        self.zoomDescription = QtWidgets.QLabel()
        self.zoomDescription.setMinimumSize(QtCore.QSize(80, 30))
        self.zoomDescription.setMaximumSize(QtCore.QSize(80, 30))
        self.zoomDescription.setText("Zoom [%]:")
        self.zoomDescription.setStyleSheet("padding :5px")
        self.zoomDescription.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        self.zoomEdit = QtWidgets.QLineEdit()
        self.zoomEdit.setMinimumSize(QtCore.QSize(40, 20))
        self.zoomEdit.setMaximumSize(QtCore.QSize(40, 20))
        self.zoomEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.zoomEdit.setText("100")
        self.zoomEdit.returnPressed.connect(self.IOHandler.zoomEditenter)
        
        # self.htopSpacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        
        self.actionplotwindow = QtWidgets.QAction(ICONS["PlotWindow"], "")
        self.actionplotwindow.setToolTip("Open a blank Plot Window")
        self.actionplotwindow.triggered.connect(self.openplotwindow)
        
        self.actionexport = QtWidgets.QAction(ICONS["Image"], "")
        self.actionexport.setToolTip("Exports the visiblie CAD area to .png")
        self.actionexport.triggered.connect(self.exportcad)
        
        
        #MIDDLE LAYER
        self.hLayoutMiddle = QtWidgets.QHBoxLayout()
        
        self.tabwidget = QtWidgets.QTabWidget()
        self.tabwidget.setMovable(True)
        self.tabwidget.setTabsClosable(True)
        self.tabwidget.currentChanged.connect(self.tabchange)
        self.tabwidget.tabCloseRequested.connect(self.closetab)
        self.tabwidget.tabBarDoubleClicked.connect(self.namechange)
        
        self.cadcircuit = gg.ImageWidget(name="Circuit 1", mainWindowposition=self.pos)
        self.cadcircuit.changezoom.connect(self.zoomchange)
        self.cadcircuit.opensubcircuit.connect(self.opensubcircuit)
        self.cadcircuit.setfocus.connect(self.clearfocus)
        
        self.tabwidget.addTab(self.cadcircuit, "Circuit 1")
        self.cadcircuit.changetounsaved.connect(self.tabtounsaved())
        
        self.library = QtWidgets.QTreeView(self)
        self.library.setHeaderHidden(True)
        self.library.setMaximumSize(QtCore.QSize(350, 10000))
        self.library.setMinimumSize(QtCore.QSize(350 ,200))
        
        treeModel = Qt.QStandardItemModel()
        rootNode = treeModel.invisibleRootItem()
        
        for l in BASELIBRARIES:
            self.loadlibrary("./library/"+l, rootNode)
            
        customlibraries = glob.glob("./library/custom/*.lib")
        
        for l in customlibraries:
            self.loadlibrary(l, rootNode)
        
        self.library.setModel(treeModel)
        if self.property.libraryexpand:
            self.library.expandAll()
        else:
            self.library.expandToDepth(0)
        
        self.library.setIconSize(QtCore.QSize(30, 30))
        
        self.library.clicked.connect(self.itemClicked)
        
        self.vLayoutResult = QtWidgets.QVBoxLayout()
        
        self.resultslist = QtWidgets.QListWidget()
        self.resultslist.setMaximumSize(QtCore.QSize(200, 10000))
        self.resultslist.setMinimumSize(QtCore.QSize(200 ,200))
        self.resultslist.itemDoubleClicked.connect(self.listItemClicked)
        
        self.hLayoutResultBottom = QtWidgets.QHBoxLayout()
        
        self.newresultwindowbutton = QtWidgets.QPushButton("")
        self.newresultwindowbutton.setIcon(ICONS["PlotWindow"])
        self.newresultwindowbutton.setToolTip("Opens a new plot-window")
        self.newresultwindowbutton.clicked.connect(self.addresultwindow)
        self.newresultwindowbutton.setEnabled(False)
        
        self.newresultgraphbutton = QtWidgets.QPushButton("")
        self.newresultgraphbutton.setIcon(ICONS["Plus"])
        self.newresultgraphbutton.setToolTip("Opens a new graph in the plot-window")
        self.newresultgraphbutton.clicked.connect(self.addgraph)
        self.newresultgraphbutton.setEnabled(False)
        
        self.logbutton = QtWidgets.QPushButton("")
        self.logbutton.setIcon(ICONS["Clock"])
        self.logbutton.setToolTip("Opens the log file of the simulation")
        self.logbutton.clicked.connect(self.openlogwindow)
        self.logbutton.setEnabled(False)
        
        self.hresulSpacer = QtWidgets.QSpacerItem(70, 0, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        
        #BOTTOM LAYER
        self.bottomState = QtWidgets.QFrame()
        self.bottomState.setMaximumSize(QtCore.QSize(10000, 40))
        self.bottomState.setMinimumSize(QtCore.QSize(1100 ,40))
        self.bottomState.setFrameShadow(QtWidgets.QFrame.Plain)
        self.bottomState.setFrameShape(QtWidgets.QFrame.StyledPanel)
        
        self.hLayoutBottom = QtWidgets.QHBoxLayout(self.bottomState)
        
        self.bottomStatus = QtWidgets.QLabel(self.bottomState)
        self.bottomStatus.setMinimumSize(QtCore.QSize(550, 0))
        
        self.bottomInfo = QtWidgets.QLabel(self.bottomState)
        self.bottomInfo.setMinimumSize(QtCore.QSize(150, 0))
        
        self.progressBar = QtWidgets.QProgressBar(self.bottomState)
        self.progressBar.setMinimumSize(QtCore.QSize(150, 0))
        self.progressBar.setTextVisible(False)
        
        self.hbottomSpacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        
        
        #ADD LAYERS TO WINDOW
        self.toolbar.addAction(self.actionnewcircuit)
        self.toolbar.addAction(self.actionsave)
        self.toolbar.addAction(self.actionload)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.actionproperties)
        self.toolbar.addAction(self.actionhelp)
        self.toolbar.addAction(self.actioninfo)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.actionundo)
        self.toolbar.addAction(self.actionredo)
        self.toolbar.addAction(self.actiondelete)
        self.toolbar.addWidget(self.autoremoveDescription)
        self.toolbar.addWidget(self.autoremovecheck)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.actionsim)
        self.toolbar.addWidget(self.simulationtimeDescription)
        self.toolbar.addWidget(self.simulationtimeEdit)
        self.toolbar.addWidget(self.timestepDescription)
        self.toolbar.addWidget(self.timestepEdit)
        self.toolbar.addWidget(self.minimumstepDescription)
        self.toolbar.addWidget(self.minimumstepEdit)
        self.toolbar.addWidget(self.integrationalgorythmDescription)
        self.toolbar.addWidget(self.integrationalgorythmCombo)
        self.toolbar.addWidget(self.zoomDescription)
        self.toolbar.addWidget(self.zoomEdit)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.actionplotwindow)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.actionexport)
        
        # self.hLayoutTop.addWidget(self.newcircuitButton)
        # self.hLayoutTop.addWidget(self.saveButton)
        # self.hLayoutTop.addWidget(self.loadButton)
        # self.hLayoutTop.addWidget(self.comSimButton)
        # self.hLayoutTop.addWidget(self.deleteButton)
        # self.hLayoutTop.addWidget(self.autoremoveDescription)
        # self.hLayoutTop.addWidget(self.autoremovecheck)
        # self.hLayoutTop.addWidget(self.simulationtimeDescription)
        # self.hLayoutTop.addWidget(self.simulationtimeEdit)
        # self.hLayoutTop.addWidget(self.timestepDescription)
        # self.hLayoutTop.addWidget(self.timestepEdit)
        # self.hLayoutTop.addWidget(self.integrationalgorythmDescription)
        # self.hLayoutTop.addWidget(self.integrationalgorythmCombo)
        # self.hLayoutTop.addWidget(self.zoomDescription)
        # self.hLayoutTop.addWidget(self.zoomEdit)
        # self.hLayoutTop.addItem(self.htopSpacer)
        
        self.hLayoutMiddle.addWidget(self.library)
        self.hLayoutMiddle.addWidget(self.tabwidget)
        
        self.hLayoutResultBottom.addWidget(self.newresultwindowbutton)
        self.hLayoutResultBottom.addWidget(self.newresultgraphbutton)
        self.hLayoutResultBottom.addWidget(self.logbutton)
        self.hLayoutResultBottom.addItem(self.hresulSpacer)
        self.vLayoutResult.addWidget(self.resultslist)
        self.vLayoutResult.addLayout(self.hLayoutResultBottom)
        
        self.hLayoutMiddle.addLayout(self.vLayoutResult)
        
        self.hLayoutBottom.addWidget(self.bottomStatus)
        self.hLayoutBottom.addItem(self.hbottomSpacer)
        self.hLayoutBottom.addWidget(self.bottomInfo)
        self.hLayoutBottom.addWidget(self.progressBar)
        
        self.hLayoutBottom.setStretch(0, 1)
        self.hLayoutBottom.setStretch(1, 4)
        self.hLayoutBottom.setStretch(2, 1)
        
        # self.vLayoutWindow.addWidget(self.topFList)
        self.vLayoutWindow.addLayout(self.hLayoutMiddle)
        self.vLayoutWindow.addWidget(self.bottomState)
        
        self.centralWidget = QtWidgets.QWidget()
        self.centralWidget.setLayout(self.vLayoutWindow)
        self.setCentralWidget(self.centralWidget)
        
        # self.hLayoutWindow.addLayout(self.vLayoutWindow)
        
    def closeEvent(self, event):
        """close Window"""
        
        self.property.maximized = int(self.windowState()) == int(QtCore.Qt.WindowMaximized)
        if not self.property.maximized:
            self.property.windowposition = [self.x() + 1, self.y() + 31, self.width(), self.height()]
        self.property.save()
        
        unsavedcircuits = False
        for i in range(self.tabwidget.count()):
            unsavedcircuits = unsavedcircuits or self.tabwidget.widget(i).unsaved
        if unsavedcircuits:
            message = QtWidgets.QMessageBox.critical(self, "dt", "Save changes?", buttons=QtWidgets.QMessageBox.Discard | QtWidgets.QMessageBox.SaveAll | QtWidgets.QMessageBox.Cancel, defaultButton=QtWidgets.QMessageBox.Cancel)
            if message == QtWidgets.QMessageBox.Cancel:
                event.ignore()
                return
            elif message == QtWidgets.QMessageBox.SaveAll:
                for i in range(self.tabwidget.count()):
                    if self.tabwidget.widget(i).unsaved:
                        self.cadcircuit = self.tabwidget.widget(i)
                        self.savecirciut()
        for window in QApplication.topLevelWidgets():
            window.close()
        self.close()
        # self.endapp.emit()
        
    def itemClicked(self, val):
        """runs the clicked function of the clicked library element"""
        
        self.library.model().itemFromIndex(val).clicked()
        
    def keyPressEvent(self, event):
        """gives the key press event to the CAD widged"""
        
        unused = True
        if event.modifiers() & QtCore.Qt.ControlModifier:
            if event.key() == QtCore.Qt.Key_S:
                self.savecirciut()
                unused = False
            if event.key() == QtCore.Qt.Key_X:
                self.autoremovecheck.setChecked(not self.autoremovecheck.isChecked())
                unused = False
        if unused == True:
            self.cadcircuit.keyPressEvent(event)
        
    def keyReleaseEvent(self, event):
        """gives the releaseevent to the CAD widged"""
        
        self.cadcircuit.keyReleaseEvent(event)
        
    def addLibrary(self, root, library):
        """adds the library to the root node in the library tree in the Gui
            works recursive
            """
            
        if type(library) == bl.LibTree:
            child = CaptionTreeItem(library.name)
            for i in library.children:
                self.addLibrary(child, i)
        else:
            if library.elementtype == "CE":
                child = CadTreeItem(library, gg.UElement, self)
            elif library.elementtype == "FE":
                child = CadTreeItem(library, gg.UElement, self)
            elif library.elementtype == "Sub":
                child = CadTreeItem(library, gg.SubCircuitElement, self)
            elif library.elementtype == "FLabel":
                child = CadTreeItem(library, gg.FLabel, self)
            elif library.elementtype == "CLabel":
                child = CadTreeItem(library, gg.CLabel, self)
            elif library.elementtype == "UE":
                child = CadTreeItem(library, gg.UElement, self)
            # else:
            #     child = CadTreeItem(library, elementtype, self)
        root.appendRow(child)
        
    def newcircuit(self):
        """adds a new circuit"""
        
        self.tabwidget.addTab(gg.ImageWidget(name=f"Circuit {self.tabwidget.count() + 1}", mainWindowposition=self.pos), f"Circuit {self.tabwidget.count() + 1}")
        self.cadcircuit = self.tabwidget.widget(self.tabwidget.count() - 1)
        self.tabwidget.setCurrentWidget(self.cadcircuit)
        self.cadcircuit.changetounsaved.connect(self.tabtounsaved())
        self.cadcircuit.changezoom.connect(self.zoomchange)
        self.cadcircuit.opensubcircuit.connect(self.opensubcircuit)
        self.cadcircuit.setfocus.connect(self.clearfocus)
        self.cadcircuit.autoremove = self.autoremovecheck.isChecked()
        
    def opensubcircuit(self, subcircuit):
        """adds a new circuit"""
        
        index = self.tabwidget.indexOf(subcircuit.cadcircuit.cadsurface.qWidget)
        if index > 0:
            self.tabwidget.setCurrentIndex(index)
        else:
            self.tabwidget.addTab(gg.ImageWidget(name=f"Circuit {self.tabwidget.count() + 1}", mainWindowposition=self.pos), f"Circuit {self.tabwidget.count() + 1}")
            self.cadcircuit.restoresignal.connect(self.tabwidget.widget(self.tabwidget.count() - 1).restore)
            self.cadcircuit = self.tabwidget.widget(self.tabwidget.count() - 1)
            self.tabwidget.setCurrentWidget(self.cadcircuit)
            self.cadcircuit.load(subcircuit.cadcircuit)
            self.cadcircuit.changetounsaved.connect(self.tabtounsaved())
            self.cadcircuit.changetounsaved.connect(subcircuit.changecheck)
            self.cadcircuit.changezoom.connect(self.zoomchange)
            self.cadcircuit.opensubcircuit.connect(self.opensubcircuit)
            self.cadcircuit.setfocus.connect(self.clearfocus)
            self.cadcircuit.autoremove = self.autoremovecheck.isChecked()
            self.tabwidget.setTabText(self.tabwidget.currentIndex(), self.cadcircuit.name)
       
    def zoomchange(self, zoom):
        """sets the text in the zoom edit"""
        
        self.zoomEdit.setText(f"{zoom}")
    
    def tabchange(self):
        """changes the cadcircuit to the active tab"""
        
        self.cadcircuit = self.tabwidget.currentWidget()
        self.cadcircuit.update()
        self.zoomEdit.setText(f"{int(self.cadcircuit.cadsurface.zoom * 100)}")
    
    def closetab(self, index):
        """close tab"""
        
        if self.tabwidget.widget(index).unsaved:
            message = QtWidgets.QMessageBox.critical(self, "dt", "Save changes?", buttons=QtWidgets.QMessageBox.Discard | QtWidgets.QMessageBox.Save | QtWidgets.QMessageBox.Cancel, defaultButton=QtWidgets.QMessageBox.Cancel)
            if message == QtWidgets.QMessageBox.Cancel:
                return
            elif message == QtWidgets.QMessageBox.Save:
                self.cadcircuit = self.tabwidget.widget(index)
                if not self.savecirciut():
                    return
        if self.tabwidget.count() == 1:
            self.cadcircuit = gg.ImageWidget(name=f"Circuit {self.tabwidget.count()}", mainWindowposition=self.pos)
            self.tabwidget.addTab(self.cadcircuit, f"Circuit {self.tabwidget.count()}")
            self.tabwidget.setCurrentWidget(self.cadcircuit)
        self.tabwidget.removeTab(index)
        self.cadcircuit = self.tabwidget.currentWidget()
    
    def namechange(self, index):
        """allows a namechange of the circuit"""
        
        self.namechangewindow = ncw.NameChangeWindow(self.tabwidget.widget(index).name)
        self.namechangewindow.show()
        self.namechangewindow.namechange.connect(self.tabnamechange())
        self.namechangewindow.closed.connect(self.namechangewindowclose)
        
    def tabnamechange(self):
         """changes the name of the circuit"""
         
         widget = self.tabwidget.currentWidget()
         index = self.tabwidget.currentIndex()
         def setname(name):
             widget.name = name
             self.tabwidget.setTabText(index, name)
             widget.changetounsaved.emit()
         return setname
    
    def namechangewindowclose(self):
        """sets the namechangewindow to None"""
        
        self.namechangewindow = None
    
    def tabtounsaved(self):
        """changes the tab name if changes are unsaved"""
        
        widget = self.tabwidget.currentWidget()
        index = self.tabwidget.currentIndex()
        def settounsaved():
            self.tabwidget.setTabText(self.tabwidget.indexOf(widget), f"{widget.name}*")
        return settounsaved
    
    def savecirciut(self, saveas=False):
        """saves the circiut"""
        try:
            if self.cadcircuit.filepath == "" or saveas == True:
                file = QtWidgets.QFileDialog.getSaveFileName(self, "Save Circuit", self.cadcircuit.name, "circiut (*.dt)")
                if file[0] == "":
                    return False
                file = file[0]
                self.cadcircuit.filepath = file
            self.cadcircuit.cleardtmodel()
            circuitobject = open(self.cadcircuit.filepath, "wb")
            saveobject = gg.Savecircuit(self.cadcircuit)
            saveobject.readyforsave()
            pc.dump(saveobject, circuitobject, protocol=pc.HIGHEST_PROTOCOL)
            circuitobject.close()
            self.cadcircuit.restore()
            self.cadcircuit.unsaved = False
            self.tabwidget.setTabText(self.tabwidget.currentIndex(), f"{self.tabwidget.currentWidget().name}")
            return True
        except Exception as e:
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage(f"Failed to save: {e}")
            appicon = QtGui.QIcon()
            appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
            self.errordialog.setWindowIcon(appicon)
            self.errordialog.setWindowTitle("deltaTau")
            self.cadcircuit.restore()
        return False
        
    def loadcirciut(self):
        """loads the saved circuit"""
        
        try:
            file = QtWidgets.QFileDialog().getOpenFileName(self, "Load Circuit", "", "circuit (*.dt)")
            if file[0] == "":
                return 0
            circuitobject = open(file[0], "rb")
            loadobject = pc.load(circuitobject)
            circuitobject.close()
            if type(loadobject) == gg.Savecircuit:
                self.tabwidget.addTab(gg.ImageWidget(name=f"Circuit {self.tabwidget.count() + 1}", mainWindowposition=self.pos), f"Schaltung {self.tabwidget.count() + 1}")
                self.cadcircuit = self.tabwidget.widget(self.tabwidget.count() - 1)
                self.cadcircuit.changetounsaved.connect(self.tabtounsaved())
                self.cadcircuit.changezoom.connect(self.zoomchange)
                self.cadcircuit.opensubcircuit.connect(self.opensubcircuit)
                self.cadcircuit.setfocus.connect(self.clearfocus)
                self.cadcircuit.autoremove = self.autoremovecheck.isChecked()
                self.tabwidget.setCurrentWidget(self.cadcircuit)
                self.cadcircuit.load(loadobject)
                self.tabwidget.setTabText(self.tabwidget.currentIndex(), self.cadcircuit.name)
                self.cadcircuit.filepath = file[0]
        except Exception as e:
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage(f"Failed to load: {e}")
            self.appicon = QtGui.QIcon()
            self.appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
            self.errordialog.setWindowIcon(appicon)
            self.errordialog.setWindowTitle("deltaTau")
            
    def listItemClicked(self, item):
        """when list item is doubleclicked it will plot the results"""
        
        item.clicked()
                
    def undo(self):
        """undo"""
        
        self.cadcircuit.historydown()
        
    def redo(self):
        """redo"""
        
        self.cadcircuit.historyup()
    
    def autoremovechanged(self):
        """sets the autoremove if changed"""
        
        for i in range(self.tabwidget.count()):
            self.tabwidget.widget(i).autoremove = self.autoremovecheck.isChecked()
    
    def openinfo(self):
        """shows the logo and prints information"""
        
        img = mpimg.imread('./icons/dtLogo.png')
        impgplot = plt.imshow(img)
        plt.axis('off')
        printinfo()
        
    def loadlibrary(self, path, rootNode):
        """loads a library and returns it"""
        
        libraryobject = open(path, "rb")
        library = pc.load(libraryobject)
        libraryobject.close()
        if type(library) == bl.LibTree:
            self.addLibrary(rootNode, library)
                
    def openplotwindow(self):
        """opens an empty plotwindow"""
        
        self.plotwindows.append(pw.PlotWindow(f"Graphs {len(self.plotwindows) + 1}", pos=self.pos()))
        self.plotwindows[-1].closed.connect(self.plotwindowclosed)
        self.plotwindows[-1].show()
        
    def plotwindowclosed(self, window):
        """removes the closed window from the plotwindowlist"""
        
        self.plotwindows.remove(window)
    
    def addresultwindow(self):
        """adds a result window and a graph"""
        
        window = pw.PlotWindow(f"Graphs {len(self.plotwindows) + 1}", pos=self.pos())
        window.closed.connect(self.resultwindowclose)
        graph = window.addplot()
        self.resultwindows.append([window, graph])
        window.show()
        
    def addgraph(self):
        """adds a new graph to the window"""
        
        if len(self.resultwindows) > 0:
            graph = self.resultwindows[-1][0].addplot()
            self.resultwindows[-1].append(graph)
        else:
            self.addresultwindowplain()
            
    def resultwindowclose(self, window):
        """removes the closed window from the resultwindows"""
        
        for w in self.resultwindows:
            if w[0] == window:
                self.resultwindows.remove(w)
                break
            
    def openmanual(self):
        """opens the manual"""
        
        # if os.name == "nt":
        #     os.startfile(".\\Manual\\User-Manual.pdf")
        # else:
        # os.system("https://gitlab.com/thocza/deltatau/-/blob/main/README.md")
        webbrowser.open("https://gitlab.com/thocza/deltatau/-/blob/main/README.md")
            
    def exportcad(self):
        """exports the cad area to png"""
        
        file = QtWidgets.QFileDialog.getSaveFileName(self, "Export cad surface", self.cadcircuit.name, "Picture (*.png)")
        if file[0] == "":
            return
        file = file[0]
        self.cadcircuit.grab().save(file)
        
    def clearfocus(self):
        """removes the focus from the lineedits"""
        
        self.simulationtimeEdit.clearFocus()
        self.timestepEdit.clearFocus()
        self.minimumstepEdit.clearFocus()
        self.zoomEdit.clearFocus()
        
    def openpropertywindow(self):
        """opens the property window"""
        
        self.propertywindow = mp.PropertyWindow(self.property, pos=self.pos())
        self.propertywindow.show()
        
    def openlogwindow(self):
        """opens the log window"""
        
        self.loggerWindow = lw.LogWindow(self.logger, pos=self.pos())
        self.loggerWindow.show()
        
class UserIOHandler:
    """class for cummincation between the window and the ciruit simulation (dt Framework)"""
    
    def __init__(self):
        """inits the IOHandler
            opens the window
            """
        
        app = QApplication(sys.argv)
        # img = mpimg.imread('./icons/dtLogo.png')
        # impgplot = plt.imshow(img)
        myappid = 'mycompany.myproduct.subproduct.version' # arbitrary string
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
        # plt.axis('off')
        # plt.show
        self.window = MainWindow(self)
        self.window.endapp.connect(app.exit)
        # plt.close()
        self.window.show()
        # myappid = 'mycompany.myproduct.subproduct.version' # arbitrary string
        # ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
        self.dt = False
        self.comsimStatus = 0 #0: ready 1:compiled 2:simulated -1:disabled
        self.setEditable()
        # app.exec()
        sys.exit(app.exec_())
    
    def setEditable(self):
        """set the status to edit"""
        
        self.window.bottomStatus.setText("edit")
    
    def setReady(self):
        """set the status label to ready"""
        
        self.window.bottomStatus.setText("ready")
        
    def setSimulating(self):
        """set the status label to simulating"""
        
        self.window.bottomStatus.setText("Simulating")
        # self.window.comSimButton.setIcon(self.window.style().standardIcon(getattr(QStyle, "SP_TitleBarCloseButton")))
        self.window.actionsim.setIcon(ICONS["Stop"])
        # self.window.comSimButton.setToolTip("terminate")
        self.window.actionsim.setToolTip("terminate")
        
    def setProgress(self, progress):
        """sets the value of the progressbar"""
        
        self.window.progressBar.setValue(progress)
        self.window.progressBar.update()
        self.window.bottomStatus.setText(f"Simulating - {progress}%")
        
    def setCompile(self):
        """sets the status label to compiling"""
        
        self.window.bottomStatus.setText("compiling")
        
    def setCompiled(self):
        """sets the status to compiled and activates the possiblility to simulate"""
        
        self.window.bottomStatus.setText("compiled")
        # self.window.comSimButton.setToolTip("Simulate")
        self.window.actionsim.setToolTip("Simulate")
        self.window.library.setEnabled(False)
        
    def setDone(self):
        """sets the status to done simulation"""
        
        self.window.bottomStatus.setText("Done")
        # self.window.comSimButton.setIcon(self.window.style().standardIcon(getattr(QStyle, "SP_DialogResetButton")))
        self.window.actionsim.setIcon(ICONS["Cancel"])
        # self.window.comSimButton.setEnabled(True)
        self.window.actionsim.setEnabled(True)
        self.window.library.setEnabled(True)
        self.comsimStatus = 1
        self.window.logger = copy.deepcopy(self.dt.logger.log)
        # self.window.comSimButton.setToolTip("Done")
        self.window.actionsim.setToolTip("Done")
        for c in self.dt.updateList:
            publicvariables = c.getpublicvariables()
            for v in publicvariables:
                self.window.resultslist.addItem(ResultListItem(v, self.window))
            # self.window.resultslist.addItem(ResultListItem(f"{c.name}.U", c.V, c.t, self.window))
            # self.window.resultslist.addItem(ResultListItem(f"{c.name}.I", c.I, c.t, self.window))
        # for f in self.dt.functionList:
        #     for i, o in enumerate(f.outputs):
        #         self.window.resultslist.addItem(ResultListItem(f"{f.name}.output{i}", o, f.time, self.window))
        self.window.newresultgraphbutton.setEnabled(True)
        self.window.logbutton.setEnabled(True)
        self.window.newresultwindowbutton.setEnabled(True)
        
    def setFailure(self, info):
        
        self.window.bottomStatus.setText(info)
        # self.window.comSimButton.setIcon(self.window.style().standardIcon(getattr(QStyle, "SP_DialogResetButton")))
        self.window.actionsim.setIcon(ICONS["Cancel"])
        # self.window.comSimButton.setEnabled(True)
        self.window.actionsim.setEnabled(True)
        self.window.library.setEnabled(True)
        self.comsimStatus = 1
        self.window.logger = copy.deepcopy(self.dt.logger.log)
        self.window.logbutton.setEnabled(True)
        # self.window.comSimButton.setToolTip("Done")
        self.window.actionsim.setToolTip("Done")
        for i in range(self.window.tabwidget.count()):
            self.window.tabwidget.widget(i).update()
                
        
    def setCleaned(self):
        """cleans the simulation so that it can run again
            removes the data by the previous simulation"""
        
        self.window.resultslist.clear()
        self.window.bottomStatus.setText("")
        self.window.newresultgraphbutton.setEnabled(False)
        self.window.logbutton.setEnabled(False)
        self.window.newresultwindowbutton.setEnabled(False)
        # self.window.comSimButton.setIcon(self.window.style().standardIcon(getattr(QStyle, "SP_MediaPlay")))
        self.window.actionsim.setIcon(ICONS["Play"])
        # self.window.comSimButton.setEnabled(True)
        self.window.actionsim.setEnabled(True)
        # self.window.comSimButton.setToolTip("Cleared")
        self.window.actionsim.setToolTip("Cleared")
        
    def setInfo(self, info):
        """sets the Info label to info"""
        
        self.window.bottomInfo.setText(info)
        
    def zoomEditenter(self):
        """checks the input and sets it if valid"""
        
        try:
            zoom = int(self.window.zoomEdit.text())
            zoom = max(min(150, zoom), 50)
            self.window.zoomEdit.setText(f"{zoom}")
            self.window.cadcircuit.cadsurface.zoom = zoom/100
            self.window.cadcircuit.update()
        except:
            self.window.zoomEdit.setText(f"{int(self.window.cadcircuit.cadsurface.zoom*100)}")
        
    def toggeldeletemode(self):
        """togles the deletemode in the cadcircuit"""
        
        self.window.cadcircuit.deletemode = not self.window.cadcircuit.deletemode
        self.window.cadcircuit.update()
        
    def comSimClicked(self):
        """compiles the circuit or starts the simulation dependend on the status"""
        
        if self.comsimStatus == 0:
            self.setCompile()
            elements = self.window.cadcircuit.getdtmodel()
            wrongparameters = False
            simulationtime = 0.1
            try:
                simulationtime = float(self.window.simulationtimeEdit.text())
                self.window.simulationtimeEdit.setStyleSheet("")
                if simulationtime <= 0:
                    simulationtime = 0.1
                    self.window.simulationtimeEdit.setText(str(simulationtime))
                    self.window.simulationtimeEdit.setStyleSheet("border: 1px solid red;")
                    wrongparameters = True
                    self.setInfo("Simulation Time error")
            except:
                simulationtime = 0.1
                self.window.simulationtimeEdit.setText(str(simulationtime))
                self.window.simulationtimeEdit.setStyleSheet("border: 1px solid red;")
                wrongparameters = True
                self.setInfo("Simulation Time error")
            timestep = 1e-6
            try:
                timestep = float(self.window.timestepEdit.text())
                if timestep <= 0:
                    timestep = simulationtime/100
                    self.window.timestepEdit.setText(str(timestep))
                    wrongparameters = True
                    self.setInfo("Time step error")
                if simulationtime < 10*timestep:
                    timestep = simulationtime/100
                    wrongparameters = True
                    self.window.timestepEdit.setText(str(timestep))
                    self.setInfo("Time step error")
            except:
                timestep = simulationtime/100
                self.window.timestepEdit.setText(str(timestep))
                wrongparameters = True
                self.setInfo("Time step error")
            try:
                rerror = float(self.window.minimumstepEdit.text())
                if rerror <= 0:
                    rerror = 1e-5
                    self.window.minimumstepEdit.setText(str(rerror))
                    wrongparameters = True
                    self.setInfo("relative Error Error")
                if rerror > 1:
                    rerror = 1e-5
                    wrongparameters = True
                    self.window.minimumstepEdit.setText(str(rerror))
                    self.setInfo("relative Error Error")
            except:
                rerror = 1e-5
                self.window.minimumstepEdit.setText(str(rerror))
                wrongparameters = True
                self.setInfo("relative Error error")
            if wrongparameters == False:
                if elements == False:
                    self.window.bottomStatus.setText("inconsistent")
                    self.window.actionsim.setIcon(ICONS["Cancel"])
                    self.window.actionsim.setEnabled(True)
                    self.comsimStatus = 1
                    # self.setCleaned()
                else:
                    param = dtcore.Parameters(simulationtime, timestep, rerror, dtcore.SOLVERS[self.window.integrationalgorythmCombo.currentIndex()], self.window.property.ignoreloop)
                    self.dt = dtcore.deltaTau(*elements, param, False)
                    try:
                        self.dt.compile()
                    except Exception as e:
                        self.setFailure(f"Compiling failed: {e}")
                        return
                    self.setCompiled()
                    self.obj = Worker()
                    self.obj.init(self.dt)
                    self.thread = QtCore.QThread()
                    self.obj.moveToThread(self.thread)
                    self.obj.finished.connect(self.thread.quit)
                    self.obj.finished.connect(self.setDone)
                    self.obj.cadupdatefailure.connect(self.thread.quit)
                    self.obj.progress.connect(self.setProgress)
                    self.obj.cadupdatefailure.connect(self.setFailure)
                    # self.thread.finished.connect(self.setDone)
                    self.thread.started.connect(self.obj.run)
                    self.thread.start()
                    self.comsimStatus = 2
                    self.setSimulating()
            else:
                self.window.cadcircuit.cleardtmodel()
                self.dt = None
                self.setCleaned()
        elif self.comsimStatus == 1:
            self.window.cadcircuit.cleardtmodel()
            self.dt = None
            self.comsimStatus = 0
            self.setCleaned()
        elif self.comsimStatus == 2:
            self.obj.setstate(1)
            self.comsimStatus = 1
        

class CadTreeItem(Qt.QStandardItem):
    """Tree item with an element that can be added to the cad surface"""

    def __init__(self, propertyelement, elementtype, window):
        """initialises the item so that the correct element will be created"""
    
        super().__init__(propertyelement.name)
        self.propertyelement = propertyelement
        self.elementtype = elementtype
        self.window = window
        self.imagewidget = self.window.cadcircuit
        self.setText(propertyelement.name)
        self.setEditable(False)
        self.setData(self)
        self.setIcon(QtGui.QIcon(self.propertyelement.icon))
        self.setSelectable(False)
        font = self.font()
        font.setPointSize(FONTSIZE)
        self.setFont(font)
        
    def clicked(self):
        """adds the element to the cad surface"""
        
        self.imagewidget = self.window.cadcircuit
        if not self.imagewidget==None:
            if self.imagewidget.deletemode == False:
                for e in self.imagewidget.elements:
                    if type(e) == gg.UElement or type(e) == gg.CLabel or type(e) == gg.FLabel or type(e) == gg.SubCircuitElement:
                        if e.floating == True:
                            self.imagewidget.elements.remove(e)
                            if len(self.imagewidget.elementsreverse) > len(self.imagewidget.elements):
                                self.imagewidget.elementsreverse.remove(e)
                    elif type(e) == gg.Line or type(e) == gg.Vector:
                        if e.active == True:
                            self.imagewidget.elements.remove(e)
                            if len(self.imagewidget.elementsreverse) > len(self.imagewidget.elements):
                                self.imagewidget.elementsreverse.remove(e)
                self.imagewidget.elements.append(self.elementtype(self.imagewidget, copy.deepcopy(self.propertyelement)))
        
        
class CaptionTreeItem(Qt.QStandardItem):
    """caption tree item"""
    
    def __init__(self, text=""):
        """initialises the caption"""
        
        super().__init__(text)
        self.setText(text)
        self.setEditable(False)
        self.setData(self)
        self.setSelectable(False)
        font = self.font()
        font.setPointSize(FONTSIZE)
        self.setFont(font)
        
    def clicked(self):
        """does nothing"""
        
        pass
    
    
class Worker(QtCore.QObject):
    """worker class for threading the simulation"""
    
    finished = QtCore.pyqtSignal()
    state = QtCore.pyqtSignal(int)
    progress = QtCore.pyqtSignal(int)
    cadupdatefailure = QtCore.pyqtSignal(str)
    
    def init(self, dt):
        """gets the dt model"""
        
        self.dt = dt
        
    def run(self):
        """runs the model and emits the progress"""
        
        state = self.dt.run(self.progress)
        self.progress.emit(0)
        if state == True:
            self.finished.emit()
        else:
            self.cadupdatefailure.emit(state)
        
    def setstate(self, state):
        """sets the state of the simulation whether it will be paused or
            stoped"""
        
        self.dt.state = state


class ResultListItem(QtWidgets.QListWidgetItem):
    """the list item for the resultlist"""
    
    def __init__(self, get, mainwindow):
        """inits the listitem with the name and the data"""
        
        self.get = get
        results =  self.get[1]
        name =  self.get[0]
        namestr = ""
        if type(results[0]) == list or type(results[0]) == np.ndarray:
            if len(results) > 1:
                namestr = f"{name} [{results[0]}, {results[1]}, ...]"
            else:
                namestr = f"{name} [{str(float('%.4g' % results[0][0]))}, {str(float('%.4g' % results[0][1]))}, {str(float('%.4g' % results[0][2]))}, ...]"
        else:
            namestr = f"{name} [{str(float('%.4g' % results[0]))}, {str(float('%.4g' % results[1]))}, {str(float('%.4g' % results[2]))}, ...]"
        if len(namestr) > 37:
            namestr = namestr[0:35]+"..."
        super().__init__(namestr)
        self.notplottable = False
        font = self.font()
        font.setPointSize(FONTSIZELIST)
        self.setFont(font)
        self.mainwindow = mainwindow
        # self.figwindow = None
    
    def clicked(self):
        """when doubleclicked plots the data"""
        
        results = self.get[1]
        name = self.get[0]
        time = self.get[2]
        self.notplottable = False
        if type(results[0]) == list or type(results[0]) == np.ndarray:
            if type(results[0][0]) == list or type(results[0][0]) == np.ndarray:
                self.notplottable = True
            else:
                resultsforplots = [[] for _ in results[0]]
                for r in results:
                    for i, n in enumerate(r):
                        resultsforplots[i].append(n)
                resultsforplots = results
        else:
            resultsforplots = [results]
        
        if not self.notplottable:
            if np.any(np.isinf(resultsforplots)) or np.any(np.isnan(resultsforplots)):
                self.errordialog = QtWidgets.QErrorMessage()
                self.errordialog.showMessage(f"cannot plot inf and nan")
                return
            if not len(self.mainwindow.resultwindows) > 0:
                self.mainwindow.addresultwindow()
            self.mainwindow.resultwindows[-1][-1].setx(time)#
            for i, r in enumerate(resultsforplots):
                if len(resultsforplots) > 1:
                    self.mainwindow.resultwindows[-1][-1].addy(r, yname=name+f"[{i}]")
                else:
                    self.mainwindow.resultwindows[-1][-1].addy(r, yname=name)
            self.mainwindow.resultwindows[-1][-1].setxlabel("time [s]")
            self.mainwindow.resultwindows[-1][-1].setylabel("V, A, value")
            self.mainwindow.resultwindows[-1][-1].update()
        else:
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage(f"cannot plot this variable")
        
            

if PRINT_INFO:
    printinfo()