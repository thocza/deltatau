# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 12:57:26 2022


@author: czatho

This is the test circute to test functionalities during the development
works with
deltaTau: version beta 0.2.0
deltaTau Electric Libraray: beta 0.5.13
deltaTau Function Libraray: beta 0.2.8
deltaTau nonFunction Libraray: beta 0.1.2
deltaTau main Window: beta 0.4.30
"""

import dtengine as deltaTau
import electricLib as eL
import functionLib as fL
import nonfunctionalLib as nfL
import mainWindow as gui
import sys

if __name__ == "__main__":
    UIHandler = gui.UserIOHandler()