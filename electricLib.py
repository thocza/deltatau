"""
Created on Fri Oct 29 10:23:59 2021

@author: czatho

This is a library of all different electric elements
It contains the following elements
    resistor
    inductance
    capacitance
    constant Voltage source
    constant Current source
    sinus voltage source
    3ph voltage source
    sinus current source
    ideal Diod
    switch
    mosfet
    mosfet with Diod
    ideal npn igbt
    ideal Thyristor
    ideal Zenerdiod
    igbt with diod
    real mosfet
    controlled voltage source
    controlled current source
    3ph voltmeter
    voltmeter
    3ph voltmeter
    3ph voltmeter star
    3ph ampermeter
    ampermeter
    coupler
    ceout
alpha 0.0.1:
    first draft not working
    under work
alpha 0.0.2:
    first five elements are implemented
    when they are working go to version beta 0.1.0
beta 0.1.0
    test was successfull
beta 0.1.1
    sinus voltage source was added und tested
beta 0.1.2
    Diods were added and tested
beta 0.1.3
    added controlled voltage and current sourece 
    after testing changing to version beta 0.1.4
beta 0.1.4
    controlled voltage source was tested and works
beta 0.1.5
    added deltaTau Version numbers to Info
beta 0.1.6
    Library is added to LIBRARY constant for easy integration to GUI
beta 0.1.7
    added Diod to Librarytree
beta 0.1.8
    added voltage and current sources to LIBRARY
beta 0.1.9
    added ampermeter to parameterelement
beta 0.1.10
    added changes to compy with changes to the new baselib
beta 0.1.11
    added controled voltage source to LIBRARY
    changed current direction of current source
beta 0.2.0
    added the posebility to add a control output
    added voltmeter
beta 0.2.1
    added the ampermeter element
beta 0.2.2
    corrected spelling mistake
beta 0.2.3
    added the switch
beta 0.3.0
    changed the circuit elements creation elements
beta 0.3.1
    bug fixes
beta 0.3.2
    language change to english
beta 0.3.3
    save library as .lib
beta 0.3.4
    added the stop and action function to the function elements
beta 0.4.0
    made ready for the new dt engine
beta 0.4.1
    solves spelling error
beta 0.4.2
    added the ideal n IGBT
beta 0.5.0
    changed the architecture of the cicuit elements
beta 0.5.1
    added changes to make a dynamic delta possible
beta 0.5.2
    added the thyristor and the zenerdiod
beta 0.5.3
    changes for new solvers
beta 0.5.4
    bug fixes
beta 0.5.5
    bug fixes and added 3ph voltmeter
beta 0.5.6
    added 3ph star voltmeter
    3ph ampermeter and 3 ph voltage source
beta 0.5.7
    added labels
beta 0.5.8
    ceout is has the new function-architecture
beta 0.5.9
    value is now reversed
beta 0.5.10
    bugfixes
beta 0.5.11
    bugfixes
    added:
        mosfet
        mosfet with diod
        real mosfet
beta 0.5.12
    added rectifier and variable resistor
beta 0.5.13
    updated to new baseLib functions
"""

# info variables
PROGRAM_NAME = "deltaTau Electric Libraray"
FILE_NAME = "electricLib.py"
FILE_CREATION = "2021-10-29"
FILE_UPDATE = "2024-08-09"
FILE_VERSION = "beta 0.5.13"
DT_VERSION = "0.12.0"
PRINT_INFO = True

# libraries
import dtengine as deltaTau
import math as m 
import baseLib as bl
import ggelements as baseimages
import pickle as pc
import numpy as np


def printinfo():
    """shows all information about this file"""

    print(f"deltaTau {DT_VERSION}")    
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)
    
#Resistor######################################################################

resistorargs = [[10],
                ["Resistor"],
                ["Ω"],
                [float],
                [bl.PropertyElement.finitandnotzero],
                ["Resistor\nValue bigger than 0 in Ω"]]
    
def resistor(name, resistance):
    """resistor circute element with g = 1/R"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, resistance):
            super().__init__(name)
            self.celement = deltaTau.CircuteElement(name, "R", resistance)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
                
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
    
    return UpdateCE(name, resistance)

#Inductance####################################################################

inductanceargs = [[1, 0],
                  ["Inductance", "Current"],
                  ["H", "A"],
                  [float, float],
                  [bl.PropertyElement.finitandnotzero, bl.PropertyElement.noconstrains],
                  ["Inductance\nValue bigger than 0 in H", "Current at start of simulation"]]

def inductance(name, inductance, current=0):
    """inductance circute element with di = u/L"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, inductance):
            super().__init__(name)
            self.celement = deltaTau.CircuteElement(name, "L", inductance, current)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
                
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name, inductance)

#Capacitance###################################################################

capacitanceargs = [[1, 0],
                   ["Capacitance", "Voltage"],
                   ["F", "V"],
                   [float, float],
                   [bl.PropertyElement.finitandnotzero, bl.PropertyElement.noconstrains],
                   ["Capacitance\nValue bigger than 0 in F", "Voltage at start of simulation"]]

def capacitance(name, capacitance, voltage=0):
    """capacitance circute element with du = i/C"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, capacitance):
            super().__init__(name)
            self.celement = deltaTau.CircuteElement(name, "C", capacitance, voltage)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
                
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name, capacitance)

#Variable Resistor#############################################################
variableresistorargs = [[], [], [], [], [], []]

def variableresistor (name):
    """a ideal mosfet not voltage-forward"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            self.celement = deltaTau.CircuteElement(name, "R", 10)
            self.felement = deltaTau.FunctionELement(name, "D", 0)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            if len(inputs) == 1:
                self.felement.setinput(inputs[0])
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the currentsource"""
            
            if len(self.felement.input.data) == 1:
                if not 1/np.nan_to_num(self.felement.input.data[-1]) == self.celement.value:
                    if self.felement.input.data[-1] > 0:
                        self.celement.setvalue(self.felement.input.data[-1])
                    else:
                        self.celement.setvalue(1e-12)
                    return 2
            else:
                if not self.felement.input.data[-1] == self.felement.input.data[-2]:
                    if not 1/np.nan_to_num(self.felement.input.data[-1]) == self.celement.value:
                        if self.felement.input.data[-1] > 0:
                            self.celement.setvalue(self.felement.input.data[-1])
                        else:
                            self.celement.setvalue(1e-12)
                        return 2
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name)

#Constant Voltage Source#######################################################

constantVoltageSourceargs = [[1],
                             ["Voltage"],
                             ["V"],
                             [float],
                             [bl.PropertyElement.noconstrains],
                             ["Voltager in Volt"]]

def constantVoltageSource(name, voltage):
    """constant voltage source circute element with U = V"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, voltage):
            super().__init__(name)
            self.celement = deltaTau.CircuteElement(name, "U", voltage)  
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
                
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name, voltage)

#Simus Voltage Source#########################################################

sinusVoltageSourceargs = [[1, 50, 0],
                          ["Amplitude", "Frequency", "Phase"],
                          ["V", "Hz", "°"],
                          [float, float, float],
                          [bl.PropertyElement.biggerzero, bl.PropertyElement.biggerzero, bl.PropertyElement.noconstrains],
                          ["Amplitude\nin Volt bigger 0", "Frequency\nValue bigger than 0 in Hz", "Phase\nValue in Degree"]]

def sinusVoltageSource(name, peakvoltage, frequency, phase = 0):
    """sinus voltage source with U = U_peak * sin(2 * pi * f * t + phase * pi / 180)"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, peakvoltage, frequency, phase):
            super().__init__(name)
            self.peakvoltage = peakvoltage
            self.frequency = frequency
            self.phase = phase
            self.celement = deltaTau.CircuteElement(name, "U", peakvoltage * m.sin(0 + phase * m.pi / 180))
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            self.celement.setvalue(self.peakvoltage * m.sin(2 * m.pi * self.frequency * time + self.phase * m.pi / 180))
            return 1
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name, peakvoltage, frequency, phase)

#3 Phase Voltage Source########################################################

triphvoltagesourceargs = [[1, 50, 0],
                          ["Amplitude", "Frequency", "Phase"],
                          ["V", "Hz", "°"],
                          [float, float, float],
                          [bl.PropertyElement.biggerzero, bl.PropertyElement.biggerzero, bl.PropertyElement.noconstrains],
                          ["Amplitude\nin Volt bigger 0", "Frequency\nValue bigger than 0 in Hz", "Phase\nValue in Degree"]]

def triphvoltagesource(name, peakvoltage, frequency, phase = 0):
    """3 phase voltage source"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, peakvoltage, frequency, phase):
            super().__init__(name)
            self.peakvoltage = peakvoltage
            self.frequency = frequency
            self.phase = phase
            self.celementV1 = deltaTau.CircuteElement(name, "U", peakvoltage * m.sin(0 + phase * m.pi / 180))
            self.celementV2 = deltaTau.CircuteElement(name, "U", peakvoltage * m.sin(0 + (phase + 120) * m.pi / 180))
            self.celementV3 = deltaTau.CircuteElement(name, "U", peakvoltage * m.sin(0 + (phase + 240) * m.pi / 180))
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 4:
                self.celementV1.setNodes(nodes[0], nodes[3])
                self.celementV2.setNodes(nodes[1], nodes[3])
                self.celementV3.setNodes(nodes[2], nodes[3])
            else:
                raise TypeError("wronge node-size from input")
                
        def update(self, major, time):
            """updates the voltmeter"""
            
            self.celementV1.setvalue(self.peakvoltage * m.sin(2 * m.pi * self.frequency * time + self.phase * m.pi / 180))
            self.celementV2.setvalue(self.peakvoltage * m.sin(2 * m.pi * self.frequency * time + (self.phase + 120) * m.pi / 180))
            self.celementV3.setvalue(self.peakvoltage * m.sin(2 * m.pi * self.frequency * time + (self.phase + 240) * m.pi / 180))
            return 1
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celementV1, self.celementV2, self.celementV3]
        
        def setpublicvariables(self):
            """returns the public variables"""

            self.publicvariables = [[self.name+".V1", self.celementV1.V, self.celementV1.t], [self.name+".V2", self.celementV2.V, self.celementV2.t], [self.name+".V3", self.celementV3.V, self.celementV3.t], [self.name+".I1", self.celementV1.I, self.celementV1.t], [self.name+".I2", self.celementV2.I, self.celementV2.t], [self.name+".I3", self.celementV3.I, self.celementV3.t]]
    
    return UpdateCE(name, peakvoltage, frequency, phase)

#Constant Current Source#######################################################

constantCurrentSourceargs = [[1],
                             ["Current"],
                             ["A"],
                             [float],
                             [bl.PropertyElement.noconstrains],
                             ["Current\nin Amper"]]

def constantCurrentSource(name, current):
    """constant current source circute element with I = I"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, current):
            super().__init__(name)
            self.celement = deltaTau.CircuteElement(name, "I", current)  
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
                
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name, current)

#Sinus Current Source##########################################################

sinusCurrentSourceargs = [[1, 50, 0],
                          ["Amplitude", "Frequency", "Phase"],
                          ["A", "Hz", "°"],
                          [float, float, float],
                          [bl.PropertyElement.biggerzero, bl.PropertyElement.biggerzero, bl.PropertyElement.noconstrains],
                          ["Amplitude\nin Amper bigger than 0", "Frequency\nValue bigger than 0 in Hz", "Phase\nValue in Grad"]]

def sinusCurrentSource(name, peakcurrent, frequency, phase = 0):
    """sinus current source with I = I_peak * sin(2 * pi * f * t + phase * pi / 180)"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, peakvoltage, frequency, phase):
            super().__init__(name)
            self.peakcurrent = peakcurrent
            self.frequency = frequency
            self.phase = phase
            self.celement = deltaTau.CircuteElement(name, "I", peakvoltage * m.sin(0 + phase * m.pi / 180))
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
        
        def update(self, major, time):
            """updates the currentsource"""
            
            self.celement.setvalue(self.peakcurrent * m.sin(2 * m.pi * self.frequency * time + self.phase * m.pi / 180))
            return 1
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name, peakcurrent, frequency, phase)

#Controled Voltage Source######################################################

controledVoltageSourceargs = [[], [], [], [] ,[], []]

def controledVoltageSource(name):
    """a voltage source that will be controlled by the inputs of a
    function Block """
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            self.celement = deltaTau.CircuteElement(name, "U", 0)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            if len(inputs) == 1:
                self.celement.setvalue(inputs[0])
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name)


#Controled Current Source######################################################

controledCurrentSourceargs = [[], [], [], [] ,[], []]

def controlledCurrentSource(name):
    """a current source that will be controlled by the inputs of a fucntion Block"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            self.celement = deltaTau.CircuteElement(name, "I", 0)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            if len(inputs) == 1:
                self.celement.setvalue(inputs[0])
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the currentsource"""
            
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name)

#Ideal Diod####################################################################

idealDiodargs = [[0.1, 1e-3, 1e6],
                 ["Turn-on voltage", "on-resistance", "off-resistance"],
                 ["V", "Ω", "Ω"],
                 [float, float, float],
                 [bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero],
                 ["Voltage at which the diod turns on\nin Volt", "Resistance at on state\nValue bigger than 0 in Ω", "Resistance at off state\nValue bigger than 0 in Ω"]]

def idealDiod(name, onvoltage, onresistance, offresistance):
    """ideal Diod R_on =  onresistance and R_off = offresistance
    switches on when U_diod > onvoltage
    switches off when U_diod < onvoltage"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, onvoltage, onresistance, offresistance):
            super().__init__(name)
            self.turnoncurrent = onvoltage/offresistance
            self.onvoltage = onvoltage*(1 - onresistance/offresistance)
            self.onresistance = onresistance
            self.offresistance = offresistance
            self.celement1 = deltaTau.CircuteElement(name, "R", offresistance)
            self.celement2 = deltaTau.CircuteElement(name, "U", 0)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                node = deltaTau.Node()
                self.celement1.setNodes(nodes[0], node)
                self.celement2.setNodes(node, nodes[1])
            else:
                raise TypeError("wronge node-size from input")
        
        def update(self, major, time):
            """updates the currentsource"""
            
            # if len(self.functionInput[0]) > 0:
            if self.celement2.value == 0 and self.celement1.I[-1] > self.turnoncurrent:
                self.celement2.setvalue(self.onvoltage)
                self.celement1.setvalue(self.onresistance)
                # print(f"{name} turn on")
                return 2
            elif self.celement1.I[-1] < self.turnoncurrent and self.celement2.value == self.onvoltage:
                self.celement2.setvalue(0)
                self.celement1.setvalue(self.offresistance)
                # print(f"{name} turn off1")
                return 2
            return 0   
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement1, self.celement2]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            V = np.array(self.celement1.V) + np.array(self.celement2.V)
            self.publicvariables = [[self.name+".V", V, self.celement1.t], [self.name+".I", self.celement1.I, self.celement1.t]]
        
    return UpdateCE(name, onvoltage, onresistance, offresistance)

#Ideal Switch##################################################################

switchargs = [[1e-3, 1e6],
              ["on-resistance", "off-resistance"],
              ["Ω", "Ω"],
              [float, float],
              [bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero],
              ["Resistance at on state\nValue bigger than 0 in Ω", "Resistanace at off state\nValue bigger than 0 in Ω"]]

def switch(name, onresistance, offresistance):
    """a swith is a resistor wich is controlled by a function Block"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, onresistance, offresistance):
            super().__init__(name)
            self.onresistance = onresistance
            self.offresistance = offresistance
            self.state = False
            self.celement = deltaTau.CircuteElement(name, "R", offresistance)
            self.felement = deltaTau.FunctionELement(name, "D", 0)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            if len(inputs) == 1:
                self.felement.setinput(inputs[0])
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the currentsource"""
            
            # if len(self.functionInput[0]) > 0:
            if self.felement.input.data[-1] > 0.5 and self.state == False:
                self.state = True
                self.celement.setvalue(self.onresistance)
                return 2
            elif self.felement.input.data[-1] < 0.5 and self.state == True:
                self.state = False
                self.celement.setvalue(self.offresistance)
                return 2
            return 0
            # else:
            #     self.celement.setvalue(self.offresistance)
            #     return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name, onresistance, offresistance)

#Ideal MosFET##################################################################

idealmosfetargs = [[1e-3, 1e6],
                   ["on-resistance", "off-resistance"],
                   ["Ω", "Ω"],
                   [float, float],
                   [bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero],
                   ["Resistance at on state\nValue bigger than 0 in Ω", "Resistanace at off state\nValue bigger than 0 in Ω"]]

def idealmosfet(name, onresistance, offresistance):
    """a ideal mosfet not voltage-forward"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, onresistance, offresistance):
            super().__init__(name)
            self.onresistance = onresistance
            self.offresistance = offresistance
            self.state = False
            self.celement = deltaTau.CircuteElement(name, "R", offresistance)
            self.felement = deltaTau.FunctionELement(name, "D", 0)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            if len(inputs) == 1:
                self.felement.setinput(inputs[0])
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the currentsource"""
            
            # if len(self.functionInput[0]) > 0:
            if self.felement.input.data[-1] > 0.5 and self.state == False and self.celement.I[-1] >= 0:
                self.state = True
                self.celement.setvalue(self.onresistance)
                return 2
            elif self.felement.input.data[-1] < 0.5 and self.state == True or self.state == True and self.celement.I[-1] < 0:
                self.state = False
                self.celement.setvalue(self.offresistance)
                return 2
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name, onresistance, offresistance)

#Ideal MosFET with Diod########################################################

idealmosfetwithdiodargs = [[1e-3, 1e6, 1e-3, 1e6],
                           ["on-resistance MOSFET", "off-resistance MOSFET", "on-resistance Diod", "off-resistance Diod"],
                           ["Ω", "Ω", "Ω", "Ω"],
                           [float, float, float, float],
                           [bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero],
                           ["Resistance at on state of MOSFET\nValue bigger than 0 in Ω",
                            "Resistanace at off state of MOSFET\nValue bigger than 0 in Ω",
                            "Resistance at on state of Diod\nValue bigger than 0 in Ω",
                            "Resistanace at off state of Diod\nValue bigger than 0 in Ω"]]

def idealmosfetwithdiod(name, onresistance, offresistance, onresistancediod, offresistancediod):
    """a ideal mosfet not voltage-forward"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, onresistance, offresistance, onresistancediod, offresistancediod):
            super().__init__(name)
            self.onresistance = onresistance
            self.offresistance = offresistance
            self.state = False
            self.statediod = False
            self.onresistancediod = onresistance
            self.offresistancediod = offresistance
            self.celement1 = deltaTau.CircuteElement(name, "R", offresistance)
            self.celement2 = deltaTau.CircuteElement(name, "R", offresistancediod)
            self.felement = deltaTau.FunctionELement(name, "D", 0)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement1.setNodes(nodes[0], nodes[1])
                self.celement2.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            if len(inputs) == 1:
                self.felement.setinput(inputs[0])
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the currentsource"""
            
            # if len(self.functionInput[0]) > 0:
            if self.felement.input.data[-1] > 0.5 and self.state == False and self.celement1.I[-1] >= 0:
                self.state = True
                self.celement1.setvalue(self.onresistance)
                self.celement2.setvalue(self.offresistancediod)
                self.statediod = False
                return 2
            elif self.felement.input.data[-1] < 0.5 and self.state == True or self.state == True and self.celement1.I[-1] < 0:
                self.state = False
                self.celement1.setvalue(self.offresistance)
                if self.celement1.I[-1] <= 0:
                    self.celement2.setvalue(self.onresistancediod)
                    self.statediod = True
                return 2
            elif self.statediod == False and self.celement2.I[-1] <= 0:
                self.celement2.setvalue(self.onresistancediod)
                self.statediod = True
                return 2
            elif self.statediod == True and self.celement2.I[-1] > 0:
                self.celement2.setvalue(self.offresistancediod)
                self.statediod = False
                return 2
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement1, self.celement2]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            V = np.array(self.celement1.V)
            I = np.array(self.celement1.I) + np.array(self.celement2.I)
            self.publicvariables = [[self.name+".V", V, self.celement1.t], [self.name+".I", I, self.celement1.t]]
        
    return UpdateCE(name, onresistance, offresistance, onresistancediod, offresistancediod)

#Ideal IGBT####################################################################

idealnigbtargs = [[1e-3, 1e6, 0.1],
                  ["on-resistance", "off-resistance", "Turn-on voltage"],
                  ["Ω", "Ω", "V"],
                  [float, float, float],
                  [bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero],
                  ["Resistance at on state\nValue bigger than 0 in Ω", "Resistance at off state\nValue bigger than 0 in Ω", "Voltage at which a turn on is possible"]]

def idealnigbt(name, onresistance, offresistance, onvoltage=0):
    """a swith is a resistor wich is controlled by a function Block"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, onresistance, offresistance, onvoltage):
            super().__init__(name)
            self.turnoncurrent = onvoltage/offresistance
            self.onvoltage = onvoltage*(1 - onresistance/offresistance)
            self.onresistance = onresistance
            self.offresistance = offresistance
            self.celement1 = deltaTau.CircuteElement(name, "R", offresistance)
            self.celement2 = deltaTau.CircuteElement(name, "U", 0)
            self.felement = deltaTau.FunctionELement(name, "D", 0)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                node = deltaTau.Node()
                self.celement1.setNodes(nodes[0], node)
                self.celement2.setNodes(node, nodes[1])
            else:
                raise TypeError("wronge node-size from input")
           
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            if len(inputs) == 1:
                self.felement.setinput(inputs[0])
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the currentsource"""
            
            # if len(self.functionInput[0]) > 0:
            if self.celement2.value == 0 and self.celement1.I[-1] > self.turnoncurrent and self.felement.input.data[-1] > 0.5:
                self.celement2.setvalue(self.onvoltage)
                self.celement1.setvalue(self.onresistance)
                # print(f"{name} turn on")
                return 2
            elif self.celement1.I[-1] < self.turnoncurrent and self.celement2.value == self.onvoltage:
                self.celement2.setvalue(0)
                self.celement1.setvalue(self.offresistance)
                # print(f"{name} turn off1")
                return 2
            elif self.felement.input.data[-1] < 0.5 and self.celement2.value == self.onvoltage:
                self.celement2.setvalue(0)
                self.celement1.setvalue(self.offresistance)
                # print(f"{name} turn off2")
                return 2
            return 0     
            
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement1, self.celement2]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            V = np.array(self.celement1.V) + np.array(self.celement2.V)
            self.publicvariables = [[self.name+".V", V, self.celement1.t], [self.name+".I", self.celement1.I, self.celement1.t]]
        
    return UpdateCE(name,  onresistance, offresistance, onvoltage)

#Ideal Thyristor###############################################################

idealthyristorargs = [[1e-3, 1e6, 0.1],
                      ["on-resistance", "off-resistance", "Turn-on voltage"],
                      ["Ω", "Ω", "V"], [float, float, float],
                      [bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero],
                      ["Resistance at on state\nValue bigger than 0 in Ω", "Resistance at off state\nValue bigger than 0 in Ω", "Voltage at which a turn on is possible"]]

def idealthyristor(name, onresistance, offresistance, onvoltage=0):
    """a swith is a resistor wich is controlled by a function Block"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, onresistance, offresistance, onvoltage):
            super().__init__(name)
            self.turnoncurrent = onvoltage/offresistance
            self.onvoltage = onvoltage*(1 - onresistance/offresistance)
            self.onresistance = onresistance
            self.offresistance = offresistance
            self.celement1 = deltaTau.CircuteElement(name, "R", offresistance)
            self.celement2 = deltaTau.CircuteElement(name, "U", 0)
            self.felement = deltaTau.FunctionELement(name, "D", 0)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                node = deltaTau.Node()
                self.celement1.setNodes(nodes[0], node)
                self.celement2.setNodes(node, nodes[1])
            else:
                raise TypeError("wronge node-size from input")
                
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            if len(inputs) == 1:
                self.felement.setinput(inputs[0])
            else:
                raise TypeError("TypeError: input is of wrong size")
                
        def update(self, major, time):
            """updates the currentsource"""
            
            if self.celement2.value == 0 and self.celement1.I[-1] > self.turnoncurrent and self.felement.input.data[-1] > 0.5:
                self.celement2.setvalue(self.onvoltage)
                self.celement1.setvalue(self.onresistance)
                # print(self.name + " on"+str(time))
                return 2
            elif self.celement1.I[-1] < self.turnoncurrent and self.celement2.value == self.onvoltage:
                self.celement2.setvalue(0)
                self.celement1.setvalue(self.offresistance)
                # print(self.name + " off"+str(time))
                # print(self.celement1.V[-1])
                return 2
            return 0     
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement1, self.celement2]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            V = np.array(self.celement1.V) + np.array(self.celement2.V)
            self.publicvariables = [[self.name+".V", V, self.celement1.t], [self.name+".I", self.celement1.I, self.celement1.t]]
        
    return UpdateCE(name, onresistance, offresistance, onvoltage)

#Ideal Zener Diod##############################################################

idealZenerdiodargs = [[0.1, 1e-3, -5, 1e-3, 1e6],
                      ["Forward-turn-on voltage", "forward-on-resistance", "Backward-turn-on voltage", "Backward-on-resistance", "off-resistance"],
                      ["V", "Ω", "V", "Ω", "Ω"],
                      [float, float, float, float, float],
                      [bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero],
                      ["Positive Voltage at which the diod turns on\n in forward direction in Volt",
                       "Resistance at on state in forward direction\nValue bigger than 0 in Ω",
                       "Negative Voltage at which the diod turns on\n backward direction in Volt",
                       "Resistance at on state in backward direction\nValue bigger than 0 in Ω",
                       "Resistance at off state\nValue bigger than 0 in Ω"]]

def idealZenerdiod(name, onfvoltage, onfresistance, onbvoltage, onbresistance, offresistance):
    """ideal Diod R_on =  onresistance and R_off = offresistance
    switches on when U_diod > onvoltage
    switches off when U_diod < onvoltage"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, onfvoltage, onfresistance, onbvoltage, onbresistance, offresistance):
            super().__init__(name)
            self.fturnoncurrent = onfvoltage/offresistance
            self.onfvoltage = onfvoltage*(1 - onfresistance/offresistance)
            self.bturnoncurrent = onbvoltage/offresistance
            self.onbvoltage = onbvoltage*(1 - onbresistance/offresistance)
            self.onfresistance = onfresistance
            self.onbresistance = onbresistance
            self.offresistance = offresistance
            self.celement1 = deltaTau.CircuteElement(name, "R", offresistance)
            self.celement2 = deltaTau.CircuteElement(name, "U", 0)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                node = deltaTau.Node()
                self.celement1.setNodes(nodes[0], node)
                self.celement2.setNodes(node, nodes[1])
            else:
                raise TypeError("wronge node-size from input")
        
        def update(self, major, time):
            """updates the currentsource"""
            
            if self.celement2.value == 0 and self.celement1.I[-1] > self.fturnoncurrent:
                self.celement2.setvalue(self.onfvoltage)
                self.celement1.setvalue(self.onfresistance)
                # print(self.name + " on"+str(time))
                return 2
            elif self.celement1.I[-1] < self.fturnoncurrent and self.celement2.value == self.onfvoltage:
                self.celement2.setvalue(0)
                self.celement1.setvalue(self.offresistance)
                # print(self.name + " off"+str(time))
                return 2
            elif self.celement2.value == 0 and self.celement1.I[-1] < self.bturnoncurrent:
                self.celement2.setvalue(self.onbvoltage)
                self.celement1.setvalue(self.onbresistance)
                # print(self.name + " on"+str(time))
                return 2
            elif self.celement1.I[-1] > self.bturnoncurrent and self.celement2.value == self.onbvoltage:
                self.celement2.setvalue(0)
                self.celement1.setvalue(self.offresistance)
                # print(self.name + " off"+str(time))
                return 2
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement1, self.celement2]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            V = np.array(self.celement1.V) + np.array(self.celement2.V)
            self.publicvariables = [[self.name+".V", V, self.celement1.t], [self.name+".I", self.celement1.I, self.celement1.t]]
        
    return UpdateCE(name, onfvoltage, onfresistance, onbvoltage, onbresistance, offresistance)

#Ideal IGBT with Diod##########################################################

idealnigbtwithdiodargs = [[1e-3, 1e6, 0.1, 1e-3, 1e6, 0.1],
                          ["on-resistance IGBT", "off-resistance IGBT", "Turn-on voltage IGBT", "on-resistance Diod", "off-resistance Diod", "Turn-on voltage Diod"],
                          ["Ω", "Ω", "V", "Ω", "Ω", "V"],
                          [float, float, float, float, float, float],
                          [bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero,
                           bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero],
                          ["Resistance at on state\nValue bigger than 0 in Ω IGBT",
                           "Resistance at off state\nValue bigger than 0 in Ω IGBT",
                           "Voltage at which a turn on is possible IGBT",
                           "Resistance at on state\nValue bigger than 0 in Ω Diod",
                           "Resistance at off state\nValue bigger than 0 in Ω Diod",
                           "Voltage at which a turn on is possible Diod"]]

def idealnigbtwithdiod(name, onresistanceigbt, offresistanceigbt, onvoltageigbt, onresistancediod, offresistancediod, onvoltagediod):
    """a swith is a resistor wich is controlled by a function Block"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, onresistanceigbt, offresistanceigbt, onvoltageigbt, onresistancediod, offresistancediod, onvoltagediod):
            super().__init__(name)
            self.turnoncurrentigbt = onvoltageigbt/offresistanceigbt
            self.onvoltageigbt = onvoltageigbt*(1 - onresistanceigbt/offresistanceigbt)
            self.onresistanceigbt = onresistanceigbt
            self.offresistanceigbt = offresistanceigbt
            self.turnoncurrentdiod = onvoltagediod/offresistancediod
            self.onvoltagediod = onvoltagediod*(1 - onresistancediod/offresistancediod)
            self.onresistancediod = onresistancediod
            self.offresistancediod = offresistancediod
            self.celement1 = deltaTau.CircuteElement(name, "R", offresistanceigbt)
            self.celement2 = deltaTau.CircuteElement(name, "U", 0)
            self.celement3 = deltaTau.CircuteElement(name, "R", offresistancediod)
            self.felement = deltaTau.FunctionELement(name, "D", 0)
            self.diodon = False
            self.igbton = False
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                node = deltaTau.Node()
                self.celement1.setNodes(nodes[0], node)
                self.celement2.setNodes(node, nodes[1])
                self.celement3.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
             
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            if len(inputs) == 1:
                self.felement.setinput(inputs[0])
            else:
                raise TypeError("TypeError: input is of wrong size")   
             
        def update(self, major, time):
            """updates the currentsource"""
            
            if self.diodon == False and self.igbton == False and self.celement1.I[-1] > self.turnoncurrentigbt and self.felement.input.data[-1] > 0.5:
                self.celement2.setvalue(self.onvoltageigbt)
                self.celement1.setvalue(self.onresistanceigbt)
                self.igbton = True
                # print(f"{name} on 1")
                return 2
            if self.diodon == False and self.igbton == False and self.celement3.I[-1] < -self.turnoncurrentdiod:
                self.celement2.setvalue(-self.onvoltagediod)
                self.celement1.setvalue(self.onresistancediod)
                self.celement3.setvalue(self.offresistanceigbt)
                self.diodon = True
                # print(f"{name} on 2 {self.celement3.I[-1]}")
                return 2
            elif self.celement1.I[-1] < self.turnoncurrentigbt and self.igbton == True:
                self.celement2.setvalue(0)
                self.celement1.setvalue(self.offresistanceigbt)
                self.igbton = False
                # print(f"{name} on 3.0")
                if self.celement1.I[-1] < -self.turnoncurrentdiod:
                    self.celement2.setvalue(-self.onvoltagediod)
                    self.celement1.setvalue(self.onresistancediod)
                    self.celement3.setvalue(self.offresistanceigbt)
                    self.diodon = True
                    # print(f"{name} on 3.1 {self.celement1.I[-1]}")
                return 2
            elif self.felement.input.data[-1] < 0.5 and self.igbton == True:
                self.celement2.setvalue(0)
                self.celement1.setvalue(self.offresistanceigbt)
                self.igbton = False
                # print(f"{name} on 4.0")
                if self.celement1.I[-1] < -self.turnoncurrentdiod:
                    self.celement2.setvalue(-self.onvoltagediod)
                    self.celement1.setvalue(self.onresistancediod)
                    self.celement3.setvalue(self.offresistanceigbt)
                    self.diodon = True
                    # print(f"{name} on 4 {self.celement1.I[-1]}")
                return 2
            elif self.diodon == True and self.celement1.I[-1] > -self.turnoncurrentdiod:
                self.celement2.setvalue(0)
                self.celement1.setvalue(self.offresistanceigbt)
                self.celement3.setvalue(self.offresistancediod)
                self.diodon = False
                # print(f"{name} on 5.0 {self.celement1.I[-1]}")
                if self.celement1.I[-1] > self.turnoncurrentigbt and self.felement.input.data[-1] > 0.5:
                    self.celement2.setvalue(self.onvoltageigbt)
                    self.celement1.setvalue(self.onresistanceigbt)
                    self.igbton = True
                    # print(f"{name} on 5 {self.celement1.I[-1]}")
                return 2
            return 0     
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement1, self.celement2, self.celement3]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            V = np.array(self.celement1.V) + np.array(self.celement2.V)
            I = np.array(self.celement2.I) + np.array(self.celement3.I)
            self.publicvariables = [[self.name+".V", V, self.celement1.t], [self.name+".I", I, self.celement1.t]]
        
    return UpdateCE(name, onresistanceigbt, offresistanceigbt, onvoltageigbt, onresistancediod, offresistancediod, onvoltagediod)

#Rectifier#####################################################################

rectifierargs = [[0.1, 1e-3, 1e6],
                 ["Turn-on voltage", "on-resistance", "off-resistance"],
                 ["V", "Ω", "Ω"], [float, float, float],
                 [bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero],
                 ["Voltage at which the diods turns on\nin Volt", "Resistance at on state\nValue bigger than 0 in Ω", "Resistance at off state\nValue bigger than 0 in Ω"]]

def rectifier(name, onvoltage, onresistance, offresistance):
    """ideal Diod R_on =  onresistance and R_off = offresistance
    switches on when U_diod > onvoltage
    switches off when U_diod < onvoltage
       d1----d3---dc+
    in--  in--
       d2----d4---dc-
    """
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, onvoltage, onresistance, offresistance):
            super().__init__(name)
            self.turnoncurrent = onvoltage/offresistance
            self.onvoltage = onvoltage*(1 - onresistance/offresistance)
            self.onresistance = onresistance
            self.offresistance = offresistance
            self.celementdr1 = deltaTau.CircuteElement(name, "R", offresistance)
            self.celementdu1 = deltaTau.CircuteElement(name, "U", 0)
            self.celementdr2 = deltaTau.CircuteElement(name, "R", offresistance)
            self.celementdu2 = deltaTau.CircuteElement(name, "U", 0)
            self.celementdr3 = deltaTau.CircuteElement(name, "R", offresistance)
            self.celementdu3 = deltaTau.CircuteElement(name, "U", 0)
            self.celementdr4 = deltaTau.CircuteElement(name, "R", offresistance)
            self.celementdu4 = deltaTau.CircuteElement(name, "U", 0)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 4:
                node1 = deltaTau.Node()
                node2 = deltaTau.Node()
                node3 = deltaTau.Node()
                node4 = deltaTau.Node()
                self.celementdr1.setNodes(nodes[1], node1)
                self.celementdu1.setNodes(node1, nodes[2])
                self.celementdr2.setNodes(nodes[2], node2)
                self.celementdu2.setNodes(node2, nodes[0])
                self.celementdr3.setNodes(nodes[1], node3)
                self.celementdu3.setNodes(node3, nodes[3])
                self.celementdr4.setNodes(nodes[3], node4)
                self.celementdu4.setNodes(node4, nodes[0])
            else:
                raise TypeError("wronge node-size from input")
        
        def update(self, major, time):
            """updates the currentsource"""
            
            updatetype = 0
            # if len(self.functionInput[0]) > 0:
            if self.celementdu1.value == 0 and self.celementdr1.I[-1] > self.turnoncurrent:
                self.celementdu1.setvalue(self.onvoltage)
                self.celementdr1.setvalue(self.onresistance)
                # print(f"{name} turn on")
                updatetype = 2
            elif self.celementdr1.I[-1] < self.turnoncurrent and self.celementdu1.value == self.onvoltage:
                self.celementdu1.setvalue(0)
                self.celementdr1.setvalue(self.offresistance)
                # print(f"{name} turn off1")
                updatetype = 2
            if self.celementdu2.value == 0 and self.celementdr2.I[-1] > self.turnoncurrent:
                self.celementdu2.setvalue(self.onvoltage)
                self.celementdr2.setvalue(self.onresistance)
                # print(f"{name} turn on")
                updatetype = 2
            elif self.celementdr2.I[-1] < self.turnoncurrent and self.celementdu2.value == self.onvoltage:
                self.celementdu2.setvalue(0)
                self.celementdr2.setvalue(self.offresistance)
                # print(f"{name} turn off1")
                updatetype = 2
            if self.celementdu3.value == 0 and self.celementdr3.I[-1] > self.turnoncurrent:
                self.celementdu3.setvalue(self.onvoltage)
                self.celementdr3.setvalue(self.onresistance)
                # print(f"{name} turn on")
                updatetype = 2
            elif self.celementdr3.I[-1] < self.turnoncurrent and self.celementdu3.value == self.onvoltage:
                self.celementdu3.setvalue(0)
                self.celementdr3.setvalue(self.offresistance)
                # print(f"{name} turn off1")
                updatetype = 2
            if self.celementdu4.value == 0 and self.celementdr4.I[-1] > self.turnoncurrent:
                self.celementdu4.setvalue(self.onvoltage)
                self.celementdr4.setvalue(self.onresistance)
                # print(f"{name} turn on")
                updatetype = 2
            elif self.celementdr4.I[-1] < self.turnoncurrent and self.celementdu4.value == self.onvoltage:
                self.celementdu4.setvalue(0)
                self.celementdr4.setvalue(self.offresistance)
                # print(f"{name} turn off1")
                updatetype = 2
            return updatetype
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celementdr1, self.celementdu1, self.celementdr2, self.celementdu2, self.celementdr3, self.celementdu3, self.celementdr4, self.celementdu4]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            Vin = np.array(self.celementdr1.V) + np.array(self.celementdu1.V) - np.array(self.celementdr3.V) - np.array(self.celementdu3.V)
            Vout = -(np.array(self.celementdr1.V) + np.array(self.celementdu1.V) + np.array(self.celementdr2.V) + np.array(self.celementdu2.V))
            Iin = np.array(self.celementdr1.I) - np.array(self.celementdr2.I)
            Iout = np.array(self.celementdr1.I) - np.array(self.celementdr3.I)
            self.publicvariables = [[self.name+".V_ac", Vin, self.celementdr1.t], [self.name+".V_dc", Vout, self.celementdr1.t], [self.name+".I_ac", Iin, self.celementdr1.t], [self.name+".I_dc", Iout, self.celementdr1.t]]
        
    return UpdateCE(name, onvoltage, onresistance, offresistance)

#Real MosFET###################################################################

realmosfetargs = [ [[1e6, 0.1, 2], [3, 25], 30, 0],
                  ["R", "Vgs", "g", "Cgs"],
                  ["Ω", "Ω", "S", "F"],
                  [list, list, float, float],
                  [bl.PropertyElement.noconstrains, bl.PropertyElement.listoftwofloats, bl.PropertyElement.finitandnotzero, bl.PropertyElement.finitandnotzero],
                  ["[Roff, Ron, Rsat]\n off resistance, off resistance and virtual resistance during saturation\nValue bigger than 0 in Ω",
                   "[Vgson, Vgssat]\n gate voltage for turn on and gate voltage for saturation state\nValue bigger than 0 in Ω",
                   "Forwarde-Gain in S",
                   "Gate-Source Capacitance in F"]]

def realmosfet(name, resistances, gatevoltages, gains, gatecapacitance):
    """real n-tpye mosfet"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, resistances, gatevoltages, gains, gatecapacitance):
            super().__init__(name)
            self.functionSignal = deltaTau.FunctionSignal(self)
            if len(resistances) < 3:
                TypeError("size input1 too small")
            if len(gatevoltages) < 2:
                TypeError("size input2 too small")
            self.offresistance = resistances[0]
            self.onresistance = resistances[2]
            self.linresistance = resistances[1]
            self.gatevoltageon = gatevoltages[0]
            self.gatevoltagesat = gatevoltages[-1]
            self.gatecapacitance = gatecapacitance
            
            if type(gains) == list:
                pass
            else:
                self.ggain = gains
            
            self.state = 0
            self.celement1 = deltaTau.CircuteElement(name, "R", self.offresistance)
            self.celement2 = deltaTau.CircuteElement(name, "I", 0, value2=self.functionSignal) #voltmeter
            self.celement3 = deltaTau.CircuteElement(name, "I", 0) #controled source
            if not self.gatecapacitance == 0:
                self.celement4 = deltaTau.CircuteElement(name, "C", self.gatecapacitance, 0)
            self.offset = deltaTau.FunctionELement(name, "Q", 0)
            self.onoffgain = deltaTau.FunctionELement(name, "G", 0)
            self.gain = deltaTau.FunctionELement(name, "G", self.ggain)
            self.adder = deltaTau.FunctionELement(name, "A", [self.offset.getoutput()])
            
            self.celement3.setvalue(self.gain.getoutput())
            self.gain.setinput(self.adder.getoutput())
            self.adder.setinput(self.onoffgain.getoutput())
            self.onoffgain.setinput(self.functionSignal)
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 3:
                self.celement1.setNodes(nodes[0], nodes[1])
                self.celement2.setNodes(nodes[2], nodes[1])
                self.celement3.setNodes(nodes[0], nodes[1])
                if not self.gatecapacitance == 0:
                    self.celement4.setNodes(nodes[2], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
        
        def update(self, major, time):
            """updates the currentsource"""
            
            # if len(self.functionInput[0]) > 0:
            if self.celement2.V[-1] < self.gatevoltageon and not self.state == 0:
                self.onoffgain.setvalue(0)
                self.offset.setvalue(0)
                self.celement1.setvalue(self.offresistance)
                self.state = 0
                return 2
            elif self.celement2.V[-1] > self.gatevoltageon and self.celement1.V[-1] > 0 and self.state == 0:
                self.celement1.setvalue(self.linresistance)
                self.state = 1
                return 2
            elif self.celement2.V[-1] > self.gatevoltageon and self.celement2.V[-1] < self.gatevoltagesat and self.celement1.I[-1] > ((self.celement2.V[-1] - self.gatevoltageon)*self.ggain + self.celement1.V[-1]/self.onresistance)  and self.state == 1:
                self.celement1.setvalue(self.onresistance)
                self.onoffgain.setvalue(1)
                self.offset.setvalue(-self.gatevoltageon)
                self.state = 2
                return 2
            elif self.celement2.V[-1] > self.gatevoltageon and self.celement2.V[-1] > self.gatevoltagesat and self.celement1.I[-1] > ((self.celement2.V[-1] - self.gatevoltageon)*self.ggain + self.celement1.V[-1]/self.onresistance)  and self.state == 1:
                self.celement1.setvalue(self.onresistance)
                self.offset.setvalue(-self.gatevoltageon + self.gatevoltagesat)
                self.state = 3
                return 2
            elif self.celement2.V[-1] > self.gatevoltageon and self.celement2.V[-1] > self.gatevoltagesat and self.state == 2:
                self.onoffgain.setvalue(0)
                self.offset.setvalue(-self.gatevoltageon + self.gatevoltagesat)
                self.state = 3
                return 2
            elif self.celement2.V[-1] > self.gatevoltageon and (self.celement3.I[-1] + self.celement1.I[-1]) > self.celement1.V[-1]/self.linresistance and (self.state == 2 or self.state == 3):
                self.onoffgain.setvalue(0)
                self.offset.setvalue(0)
                self.celement1.setvalue(self.linresistance)
                self.state = 1
                return 2
            elif self.celement2.V[-1] > self.gatevoltageon and self.celement1.V[-1] < 0  and (self.state == 1 or self.state == 2 or self.state == 3):
                self.onoffgain.setvalue(0)
                self.offset.setvalue(0)
                self.celement1.setvalue(self.offresistance)
                self.state = 0
                return 2
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            if self.gatecapacitance == 0:
                return [self.celement1, self.celement2, self.celement3]
            else:
                return [self.celement1, self.celement2, self.celement3, self.celement4]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.offset, self.onoffgain, self.gain, self.adder]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            I = np.array(self.celement1.I) + np.array(self.celement3.I)
            self.publicvariables = [[self.name+".Vgs", self.celement2.V, self.celement2.t], [self.name+".Vds", self.celement1.V, self.celement1.t], [self.name+".Ids", I, self.celement1.t]]
        
    return UpdateCE(name, resistances, gatevoltages, gains, gatecapacitance)

#Voltmeter#####################################################################

voltmeterargs = [[], [], [], [], [], []]

def voltmeter(name):
    """returns the voltage between two nodes through the functionelement"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            self.functionSignal = deltaTau.FunctionSignal(self)
            self.celement = deltaTau.CircuteElement(name, "I", 0, value2=self.functionSignal)
            self.outputs = [deltaTau.FunctionArray(self)]
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
                
        def update(self, major, time):
            """updates the voltmeter"""
            
            # self.functionElement.setinputs(self.celement.V[-1])
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def setarray(self):
            """set outputsignals"""
            
            self.outputs[0].setarray([self.functionSignal])
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
    
    return UpdateCE(name)

#3 Phase Voltmeter#############################################################

triphvoltmeterargs = [[], [], [], [], [], []]

def triphvoltmeter(name):
    """returns the voltage between two nodes through the functionelement"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            self.functionSignala = deltaTau.FunctionSignal(self)
            self.functionSignalb = deltaTau.FunctionSignal(self)
            self.functionSignalc = deltaTau.FunctionSignal(self)
            self.celementI12 = deltaTau.CircuteElement(name, "I", 0, value2=self.functionSignala)
            self.celementI23 = deltaTau.CircuteElement(name, "I", 0, value2=self.functionSignalb)
            self.celementI31 = deltaTau.CircuteElement(name, "I", 0, value2=self.functionSignalc)
            self.celementV1 = deltaTau.CircuteElement(name, "U", 0)
            self.celementV2 = deltaTau.CircuteElement(name, "U", 0)
            self.celementV3 = deltaTau.CircuteElement(name, "U", 0)
            self.output = []
            self.outputs = [deltaTau.FunctionArray(self)]
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 6:
                self.celementV1.setNodes(nodes[0], nodes[5])
                self.celementV2.setNodes(nodes[1], nodes[4])
                self.celementV3.setNodes(nodes[2], nodes[3])
                self.celementI12.setNodes(nodes[0], nodes[1])
                self.celementI23.setNodes(nodes[1], nodes[2])
                self.celementI31.setNodes(nodes[2], nodes[0])
            else:
                raise TypeError("wronge node-size from input")
                
        def setarray(self):
            """set outputsignals"""
            
            va = deltaTau.FunctionArray(self)
            vb = deltaTau.FunctionArray(self)
            vc = deltaTau.FunctionArray(self)
            va.setarray([self.functionSignala])
            vb.setarray([self.functionSignalb])
            vc.setarray([self.functionSignalc])
            self.outputs[0].setarray([va, vb, vc])
        
        def update(self, delta, time):
            """updates the voltmeter"""
            
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celementI12, self.celementI23, self.celementI31, self.celementV1, self.celementV2, self.celementV3]
        
        def setpublicvariables(self):
            """returns the public variables"""

            self.publicvariables = [[self.name+".V12", self.celementI12.V, self.celementI12.t], [self.name+".V23", self.celementI23.V, self.celementI23.t], [self.name+".V31", self.celementI31.V, self.celementI31.t]]
    
    return UpdateCE(name)

#3 Phase Star Voltmeter########################################################

triphvoltmeterstarargs = [[], [], [], [], [], []]

def triphvoltmeterstar(name):
    """returns the voltage between two nodes through the functionelement"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            self.functionSignala = deltaTau.FunctionSignal(self)
            self.functionSignalb = deltaTau.FunctionSignal(self)
            self.functionSignalc = deltaTau.FunctionSignal(self)
            self.celementI1s = deltaTau.CircuteElement(name, "I", 0, value2=self.functionSignala)
            self.celementI2s = deltaTau.CircuteElement(name, "I", 0, value2=self.functionSignalb)
            self.celementI3s = deltaTau.CircuteElement(name, "I", 0, value2=self.functionSignalc)
            self.output = []
            self.outputs = [deltaTau.FunctionArray(self)]
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 4:
                self.celementI1s.setNodes(nodes[0], nodes[3])
                self.celementI2s.setNodes(nodes[1], nodes[3])
                self.celementI3s.setNodes(nodes[2], nodes[3])
            else:
                raise TypeError("wronge node-size from input")
           
        def setarray(self):
            """set outputsignals"""
            
            va = deltaTau.FunctionArray(self)
            vb = deltaTau.FunctionArray(self)
            vc = deltaTau.FunctionArray(self)
            va.setarray([self.functionSignala])
            vb.setarray([self.functionSignalb])
            vc.setarray([self.functionSignalc])
            self.outputs[0].setarray([va, vb, vc])
        
        def update(self, major, time):
            """updates the voltmeter"""
            
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celementI1s, self.celementI2s, self.celementI3s]
        
        def setpublicvariables(self):
            """returns the public variables"""

            self.publicvariables = [[self.name+".V1", self.celementI1s.V, self.celementI1s.t], [self.name+".V2", self.celementI2s.V, self.celementI2s.t], [self.name+".V3", self.celementI3s.V, self.celementI3s.t]]
    
    return UpdateCE(name)

#3 Phase Ampermeter############################################################

triphampermeter = [[], [], [], [], [], []]

def triphpampermeter(name):
    """returns the voltage between two nodes through the functionelement"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            self.functionSignala = deltaTau.FunctionSignal(self)
            self.functionSignalb = deltaTau.FunctionSignal(self)
            self.functionSignalc = deltaTau.FunctionSignal(self)
            self.celementV1 = deltaTau.CircuteElement(name, "U", 0, value2=self.functionSignala)
            self.celementV2 = deltaTau.CircuteElement(name, "U", 0, value2=self.functionSignalb)
            self.celementV3 = deltaTau.CircuteElement(name, "U", 0, value2=self.functionSignalc)
            self.output = []
            self.outputs = [deltaTau.FunctionArray(self)]
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 6:
                self.celementV1.setNodes(nodes[0], nodes[5])
                self.celementV2.setNodes(nodes[1], nodes[4])
                self.celementV3.setNodes(nodes[2], nodes[3])
            else:
                raise TypeError("wronge node-size from input")
                
        def setarray(self):
            """set outputsignals"""
            
            ia = deltaTau.FunctionArray(self)
            ib = deltaTau.FunctionArray(self)
            ic = deltaTau.FunctionArray(self)
            ia.setarray([self.functionSignala])
            ib.setarray([self.functionSignalb])
            ic.setarray([self.functionSignalc])
            self.outputs[0].setarray([ia, ib, ic])
                
        def update(self, major, time):
            """updates the voltmeter"""
            
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celementV1, self.celementV2, self.celementV3]
        
        def setpublicvariables(self):
            """returns the public variables"""

            self.publicvariables = [[self.name+".I1", self.celementV1.I, self.celementV1.t], [self.name+".I2", self.celementV2.I, self.celementV2.t], [self.name+".I3", self.celementV3.I, self.celementV3.t]]
    
    return UpdateCE(name)

#Ampermeter####################################################################

ampermeterargs = [[], [], [], [], [], []]

def ampermeter(name):
    """returns current of the meter through the functionelement"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            self.functionSignal = deltaTau.FunctionSignal(self)
            self.celement = deltaTau.CircuteElement(name, "U", 0, value2=self.functionSignal)
            self.outputs = [deltaTau.FunctionArray(self)]
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 2:
                self.celement.setNodes(nodes[0], nodes[1])
            else:
                raise TypeError("wronge node-size from input")
                
        def update(self, major, time):
            """updates the currentmeter"""
            
            # self.functionElement.setinputs([self.celement.I[-1]])
            return 0
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement]
        
        def setarray(self):
            """set outputsignals"""
            
            self.outputs[0].setarray([self.functionSignal])
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V", self.celement.V, self.celement.t], [self.name+".I", self.celement.I, self.celement.t]]
        
    return UpdateCE(name)

#Coupler#######################################################################

couplerargs = [ [1, 1],
               ["N", "n"],
               ["", ""],
               [float, float],
               [bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains],
               ["input factor", "output factor"]]

def coupler(name, N, n):
    """a coupler element two elements can be connected"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, N, n):
            super().__init__(name)
            self.celement1 = deltaTau.CircuteElement(name, "T", N, value2=None)  
            self.celement2 = deltaTau.CircuteElement(name, "T", n, value2=self.celement1)  
            
        def setnodes(self, nodes):
            """sets the Nodes"""
            
            if len(nodes) == 4:
                self.celement1.setNodes(nodes[0], nodes[3])
                self.celement2.setNodes(nodes[1], nodes[2])
            else:
                raise TypeError("wronge node-size from input")
        
        def getCE(self):
            """returns the CE Elements"""
            
            return [self.celement1, self.celement2]
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".V_N", self.celement1.V, self.celement1.t], [self.name+".I_N", self.celement1.I, self.celement1.t], [self.name+".V_n", self.celement2.V, self.celement2.t], [self.name+".I_n", self.celement2.I, self.celement2.t]]
        
    return UpdateCE(name, N, n)

LIBRARY = bl.LibTree("electric elements")
passiveElements = bl.LibTree("passive elements")
passiveElements.append(
    bl.CEType("Resistor", "electricLib/resistor.svg", baseimages.drawResistor, resistor, *resistorargs)
    )
passiveElements.append(
    bl.CEType("Inductance", "electricLib/inductance.svg", baseimages.drawInductance, inductance, *inductanceargs)
    )
passiveElements.append(
    bl.CEType("Capacitance", "electricLib/capacitance.svg", baseimages.drawCapacitance, capacitance, *capacitanceargs)
    )
passiveElements.append(
    bl.CEType("Variable Resistor", "electricLib/variableresistor.svg", baseimages.drawVariableresistor, variableresistor, *variableresistorargs, size=[1, 1], interface=["n", "", "n", "i"])
    )
sourceElements = bl.LibTree("Sources")
sourceElements.append(
    bl.CEType("Constant-voltage-source", "electricLib/constantvoltagesource.svg", baseimages.drawConstantVoltageSource, constantVoltageSource, *constantVoltageSourceargs)
    )
sourceElements.append(
    bl.CEType("Sine-voltage-source", "electricLib/sinvoltagesource.svg", baseimages.drawSinusVoltageSource, sinusVoltageSource, *sinusVoltageSourceargs)
    )
sourceElements.append(
    bl.CEType("Constant-current-source", "electricLib/constantcurrentsource.svg", baseimages.drawConstantCurrentSource, constantCurrentSource, *constantCurrentSourceargs)
    )
sourceElements.append(
    bl.CEType("Sine-current-source", "electricLib/sincurrentsource.svg", baseimages.drawSinusCurrentSource, sinusCurrentSource, *sinusCurrentSourceargs)
    )
sourceElements.append(
    bl.CEType("Controled-voltage-source", "electricLib/controlledvoltagesource.svg", baseimages.drawControlledVoltageSource, controledVoltageSource, *controledVoltageSourceargs, size=[1, 1], interface=["n", "", "n", "i"])
    )
sourceElements.append(
    bl.CEType("Controled-current-source", "electricLib/controlledcurrentsource.svg", baseimages.drawControlledCurrentSource, controlledCurrentSource, *controledCurrentSourceargs, size=[1, 1], interface=["n", "", "n", "i"])
    )
sourceElements.append(
    bl.CEType("3ph voltage source", "electricLib/3phvoltagesource.svg", baseimages.draw3phVoltagesource, triphvoltagesource, *triphvoltagesourceargs, size=[3, 1], interface=["n", "n", "n", "", "", "n", "", ""], labels=["a", "b", "c", "", "", "N", "", ""], orientation=3)
    )
activeElements = bl.LibTree("power Semiconductors")
activeElements.append(
    bl.CEType("Ideal diode", "electricLib/diod.svg", baseimages.drawDiod, idealDiod, *idealDiodargs)
    )
activeElements.append(
    bl.CEType("Ideal switch", "electricLib/switch.svg", baseimages.drawSwitch, switch, *switchargs, size=[1, 1], interface=["n", "", "n", "i"])
    )
activeElements.append(
    bl.CEType("Ideal MOSFET", "electricLib/idealmosfet.svg", baseimages.drawMosFET, idealmosfet, *idealmosfetargs, size=[1, 1], interface=["n", "", "n", "i"])
    )
activeElements.append(
    bl.CEType("Ideal MOSFET with Diod", "electricLib/idealmosfetanddiod.svg", baseimages.drawMosFETwithdiod, idealmosfetwithdiod, *idealmosfetwithdiodargs, size=[1, 1], interface=["n", "", "n", "i"])
    )
activeElements.append(
    bl.CEType("NPN ideal IGBT", "electricLib/idealnigbt.svg", baseimages.drawNIGBT, idealnigbt, *idealnigbtargs, size=[1, 1], interface=["n", "", "n", "i"])
    )
activeElements.append(
    bl.CEType("ideal Thyristor", "electricLib/thyristor.svg", baseimages.drawThyristor, idealthyristor, *idealthyristorargs, size=[1, 1], interface=["n", "", "n", "i"])
    )
activeElements.append(
    bl.CEType("Ideal Zenerdiode", "electricLib/zenerdiod.svg", baseimages.drawZenerdiod, idealZenerdiod, *idealZenerdiodargs)
    )
activeElements.append(
    bl.CEType("NPN ideal IGBT with Diod", "electricLib/idealnigbtanddiod.svg", baseimages.drawNIGBTwithDiod, idealnigbtwithdiod, *idealnigbtwithdiodargs, size=[1, 1], interface=["n", "", "n", "i"])
    )
realactiveElements = bl.LibTree("real Semiconductors")
realactiveElements.append(
    bl.CEType("Real MOSFET", "electricLib/idealmosfet.svg", baseimages.drawMosFET, realmosfet, *realmosfetargs, size=[1, 1], interface=["n", "", "n", "n"])
    )
semiconductorModulesElements = bl.LibTree("Semiconductors-modules")
semiconductorModulesElements.append(
    bl.CEType("Rectifier", "electricLib/rectifier.svg", baseimages.drawRectifier, rectifier, *rectifierargs, size=[2, 1], interface=["n", "n", "", "n", "n", ""], orientation=3)
    )
probesElements = bl.LibTree("Meters")
probesElements.append(
    bl.CEType("Voltmeter", "electricLib/voltmeter.svg", baseimages.drawVoltmeter, voltmeter, *voltmeterargs, size=[1, 1], interface=["n", "o", "n", ""])
    )
probesElements.append(
    bl.CEType("Ampermeter", "electricLib/ampermeter.svg", baseimages.drawAmpermeter, ampermeter, *ampermeterargs, size=[1, 1], interface=["n", "o", "n", ""])
    )
probesElements.append(
    bl.CEType("triangular 3ph Voltmeter", "electricLib/3phvoltmeter.svg", baseimages.draw3phVoltmeter, triphvoltmeter, *triphvoltmeterargs, size=[3, 1], interface=["n", "n", "n", "o", "n", "n", "n", ""], labels=["a", "b", "c", "(3)", "c", "b", "a", ""])
    )
probesElements.append(
    bl.CEType("star 3ph Voltmeter", "electricLib/3phvoltmeterstar.svg", baseimages.draw3phVoltmeterstar, triphvoltmeterstar, *triphvoltmeterstarargs, size=[3, 1], interface=["n", "n", "n", "o", "", "n", "", ""], labels=["a", "b", "c", "(3)", "", "N", "", ""])
    )
probesElements.append(
    bl.CEType("3ph Ampermeter", "electricLib/3phampermeter.svg", baseimages.draw3phAmpermeter, triphpampermeter, *triphampermeter, size=[3, 1], interface=["n", "n", "n", "o", "n", "n", "n", ""], labels=["a", "b", "c", "(3)", "c", "b", "a", ""])
    )
couplerElements = bl.LibTree("Transformer")
couplerElements.append(
    bl.CEType("Transformer", "electricLib/coupler.svg", baseimages.drawCoupler, coupler, *couplerargs, size=[2, 1], interface=["n", "n", "","n", "n", ""])
    )

activeElements.append(realactiveElements)
activeElements.append(semiconductorModulesElements)

LIBRARY.append(passiveElements)
LIBRARY.append(sourceElements)
LIBRARY.append(activeElements)
LIBRARY.append(probesElements)
LIBRARY.append(couplerElements)

"""prints the information about this file"""
if PRINT_INFO:
    printinfo()

"""creat electricLib.lib"""

savelibrary = open("./library/electricLib.lib", "wb")
pc.dump(LIBRARY, savelibrary, protocol=pc.HIGHEST_PROTOCOL)
savelibrary.close()
print("electricLib is saved")