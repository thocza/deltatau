# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 09:54:05 2022

@author: czatho

alpha 0.0.1
    created
alpha 0.0.2
    added the flables
alpha 0.0.3
    adde the empty subcircuit
beta 0.1.0
    language change to englisch
beta 0.1.1
    save libraries under .lib files
beta 0.1.2
    updated to new baseLib functions
"""

# info variables
PROGRAM_NAME = "deltaTau non functional Elements Libraray"
FILE_NAME = "nonfunctionalLib.py"
FILE_CREATION = "2022-07-08"
FILE_UPDATE = "2024-08-09"
FILE_VERSION = "beta 0.1.2"
DT_VERSION = "0.12.0"
PRINT_INFO = True


# libraries
import dtengine as deltaTau
import math as m 
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Qt5Agg')
import time
import baseLib as bl
import ggelements as baseimages
import numpy as np
import pickle as pc


def printinfo():
    """shows all information about this file"""
    
    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)
    
    
CLIBRARY = bl.LibTree("Non-functional circuit elements")
CElements = bl.LibTree("Labels")
CElements.append(
    bl.CLabelType("Label", "nonfunctionalLib/Clabel.svg", baseimages.drawCText)
    )
CElements.append(
    bl.CLabelType("Node", "nonfunctionalLib/Cnode.svg", baseimages.drawCNode, connected=False, connectedtype=0)
    )
CElements.append(
    bl.CLabelType("In/Out", "nonfunctionalLib/Cio.svg", baseimages.drawCio, connected=True)
    )

CLIBRARY.append(CElements)

FLIBRARY = bl.LibTree("Non-functional function elements")
FElements = bl.LibTree("Labels")
FElements.append(
    bl.FLabelType("Label", "nonfunctionalLib/Flabel.svg", baseimages.drawFText)
    )
FElements.append(
    bl.FLabelType("Arrow", "nonfunctionalLib/Farrow.svg", baseimages.drawFArrow, connected=False, connectedtype=0)
    )
FElements.append(
    bl.FLabelType("In", "nonfunctionalLib/Fin.svg", baseimages.drawFin, connected=True, connectedtype=1)
    )
FElements.append(
    bl.FLabelType("Out", "nonfunctionalLib/Fout.svg", baseimages.drawFout, connected=True, connectedtype=2)
    )

FLIBRARY.append(FElements)

SUBLIBRARY = bl.LibTree("Subcircuits")
SUBLIBRARY.append(
    bl.SubType("Subcircuit empty", None)
    )

"""prints the information about this file"""
if PRINT_INFO:
    printinfo()

savelibrary = open("./library/ELabelLib.lib", "wb")
pc.dump(CLIBRARY, savelibrary, protocol=pc.HIGHEST_PROTOCOL)
savelibrary.close()

savelibrary = open("./library/FLabelLib.lib", "wb")
pc.dump(FLIBRARY, savelibrary, protocol=pc.HIGHEST_PROTOCOL)
savelibrary.close()

savelibrary = open("./library/SubLib.lib", "wb")
pc.dump(SUBLIBRARY, savelibrary, protocol=pc.HIGHEST_PROTOCOL)
savelibrary.close()

print("functionLib is saved")