# -*- coding: utf-8 -*-
"""
@author: tomcz
This is the property Window for deltaTau
alpha 0.0.1
    first UI for tests
beta 0.1.0
    functionality is given
beta 0.1.1
    added checkbox to add current measuremnet to an CElement
beta 0.1.2
    changes due to changes in the baseLib
beta 0.1.3
    possibility to activate the current measurement now only apears when
    there is no controloutput
beta 0.1.4
    made changes so that CElements with multiple Nodes cannot have an ampermeter
beta 0.1.5
    size of inputs and outputs is now changable
beta 0.1.6
    new icons
beta 0.1.7
    added undo and redo
beta 0.1.8
    language change to english
    bug fixes
beta 0.1.9
    added new icons
beta 0.1.10
    removed the current measurement
beta 0.1.11
    changed the window positioning from absolute to relative
beta 0.1.12
    put some style constants to the style.py file
beta 0.1.13
    Error Dialog Window has now the dt Logo and Window name
"""

# info variables
PROGRAM_NAME = "deltaTau proptery window GUI"
FILE_NAME = "propertyWindow.py"
FILE_CREATION = "2022-03-27"
FILE_UPDATE = "2023-02-13"
FILE_VERSION = "beta 0.1.13"
DT_VERSION = "0.12.0"
PRINT_INFO = True


# libraries
from PyQt5 import QtWidgets, QtCore, QtGui, QtOpenGL, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QStyle
import sys
import ctypes
import sys
import guigraphics as gg
from style import *
from cmath import *

#static variables
FONTSIZE = 12

def printinfo():
    """shows all information about this file"""

    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)

class PropertyWindow(QWidget):
    """Property Window for edditing properties of elements
        inherits from QWidget
        """
    
    def __init__(self, element, pos=None):
        """intits the Window
            also sets up the IOHandler for communication with the dt Framework
            sets name and icon
            """
        
        super().__init__()
        self.element = element
        appicon = QtGui.QIcon()
        appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
        self.setWindowIcon(appicon)
        self.setWindowTitle("deltaTau")
        self.errordialog = None
        if pos == None:
            pos = QtCore.QPoint(0, 0)
        self.initUI(pos)

    def initUI(self, pos):
        """inits the window"""
        
        self.setGeometry(pos.x() + 300, pos.y() + 300, 350, 100 + len(self.element.argsvalues)*50 + (not self.element.sizechangable[0] == False)*50 + (not self.element.sizechangable[1] == False)*50)
        self.setFixedSize(350, 100 + len(self.element.argsvalues)*50 + (not self.element.sizechangable[0] == False)*50 + (not self.element.sizechangable[1] == False)*50)
        self.setWindowTitle(f"deltaTau: {self.element.name}")
        
        self.hLayoutWindow = QtWidgets.QHBoxLayout(self)
        self.vLayoutWindow = QtWidgets.QVBoxLayout()
        
        self.hLayoutName = QtWidgets.QHBoxLayout()
        self.nameLabel = QtWidgets.QLabel()
        self.nameLabel.setMinimumSize(QtCore.QSize(125, 0))
        self.nameLabel.setMaximumSize(QtCore.QSize(125, 25))
        self.nameLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.nameLabel.setText("Name:")
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.nameEdit.setText(self.element.name)
        self.unitLabel = QtWidgets.QLabel()
        self.unitLabel.setMinimumSize(QtCore.QSize(30, 0))
        self.unitLabel.setMaximumSize(QtCore.QSize(30, 25))
        self.infoLabel = QtWidgets.QLabel()
        self.infoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
        self.infoLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.infoLabel.setMaximumSize(QtCore.QSize(25, 25))
        self.infoLabel.setToolTip("Element name")
        
        self.hLayoutName.addWidget(self.nameLabel)
        self.hLayoutName.addWidget(self.nameEdit)
        self.hLayoutName.addWidget(self.unitLabel)
        self.hLayoutName.addWidget(self.infoLabel)
        
        self.hLayoutsargs = []
        self.properties = []
        
        for i, arg in enumerate(self.element.argsvalues):
            hLayoutarg = QtWidgets.QHBoxLayout()
            nameLabel = QtWidgets.QLabel()
            nameLabel.setMinimumSize(QtCore.QSize(125, 0))
            nameLabel.setMaximumSize(QtCore.QSize(125, 25))
            nameLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            nameLabel.setText(f"{self.element.argsnames[i]}:")
            if type(self.element.argscombos[i]) == list:
                valueEdit = QtWidgets.QComboBox()
                valueEdit.addItems(self.element.argscombos[i])
                valueEdit.setCurrentIndex(self.element.argscombos[i].index(arg))
                # valueEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
                self.properties.append(valueEdit.currentText)
            else:
                valueEdit = QtWidgets.QLineEdit()
                valueEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
                valueEdit.setText(str(arg))
                self.properties.append(valueEdit.text)
            unitLabel = QtWidgets.QLabel()
            unitLabel.setMinimumSize(QtCore.QSize(30, 0))
            unitLabel.setMaximumSize(QtCore.QSize(30, 25))
            unitLabel.setText(f"[{self.element.argsunits[i]}]")
            infoLabel = QtWidgets.QLabel()
            infoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
            infoLabel.setMinimumSize(QtCore.QSize(25, 0))
            infoLabel.setMaximumSize(QtCore.QSize(25, 25))
            infoLabel.setToolTip(self.element.argsinfo[i])
            # self.properties.append(valueEdit.text)
            hLayoutarg.addWidget(nameLabel)
            hLayoutarg.addWidget(valueEdit)
            hLayoutarg.addWidget(unitLabel)
            hLayoutarg.addWidget(infoLabel)
            self.hLayoutsargs.append(hLayoutarg)
             
        if not self.element.sizechangable[0] == False:
            hLayoutarg = QtWidgets.QHBoxLayout()
            nameLabel = QtWidgets.QLabel()
            nameLabel.setMinimumSize(QtCore.QSize(125, 0))
            nameLabel.setMaximumSize(QtCore.QSize(125, 25))
            nameLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            nameLabel.setText("# inputs:")
            self.inputsEdit = QtWidgets.QLineEdit()
            self.inputsEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            self.inputsEdit.setText(str(len(self.element.inputs)))
            unitLabel = QtWidgets.QLabel()
            unitLabel.setMinimumSize(QtCore.QSize(30, 0))
            unitLabel.setMaximumSize(QtCore.QSize(30, 25))
            unitLabel.setText(f"{str(self.element.sizechangable[0])}")
            infoLabel = QtWidgets.QLabel()
            infoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
            infoLabel.setMinimumSize(QtCore.QSize(25, 0))
            infoLabel.setMaximumSize(QtCore.QSize(25, 25))
            infoLabel.setToolTip("Sets the number of inputs")
            hLayoutarg.addWidget(nameLabel)
            hLayoutarg.addWidget(self.inputsEdit)
            hLayoutarg.addWidget(unitLabel)
            hLayoutarg.addWidget(infoLabel)
            self.hLayoutsargs.append(hLayoutarg)
        if not self.element.sizechangable[1] == False:
            hLayoutarg = QtWidgets.QHBoxLayout()
            nameLabel = QtWidgets.QLabel()
            nameLabel.setMinimumSize(QtCore.QSize(125, 0))
            nameLabel.setMaximumSize(QtCore.QSize(125, 25))
            nameLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            nameLabel.setText("# outputs:")
            self.outputsEdit = QtWidgets.QLineEdit()
            self.outputsEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            self.outputsEdit.setText(str(len(self.element.outputs)))
            unitLabel = QtWidgets.QLabel()
            unitLabel.setMinimumSize(QtCore.QSize(30, 0))
            unitLabel.setMaximumSize(QtCore.QSize(30, 25))
            unitLabel.setText(f"{str(self.element.sizechangable[1])}")
            infoLabel = QtWidgets.QLabel()
            infoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
            infoLabel.setMinimumSize(QtCore.QSize(25, 0))
            infoLabel.setMaximumSize(QtCore.QSize(25, 25))
            infoLabel.setToolTip("Sets the number of outputs")
            hLayoutarg.addWidget(nameLabel)
            hLayoutarg.addWidget(self.outputsEdit)
            hLayoutarg.addWidget(unitLabel)
            hLayoutarg.addWidget(infoLabel)
            self.hLayoutsargs.append(hLayoutarg)
        
        self.hLayoutButtons = QtWidgets.QHBoxLayout()
        
        self.savebutton = QtWidgets.QPushButton("Save")
        self.savebutton.setIcon(ICONS["SaveFile"])
        self.savebutton.clicked.connect(self.save)
        self.okbutton = QtWidgets.QPushButton("OK")
        self.okbutton.setIcon(ICONS["Apply"])
        self.okbutton.clicked.connect(self.ok)
        self.cancelbutton = QtWidgets.QPushButton("Cancel")
        self.cancelbutton.setIcon(ICONS["Discard"])
        self.cancelbutton.clicked.connect(self.close)
        
        self.hLayoutButtons.addWidget(self.savebutton)
        self.hLayoutButtons.addWidget(self.okbutton)
        self.hLayoutButtons.addWidget(self.cancelbutton)
        
        self.vLayoutWindow.addLayout(self.hLayoutName)
        for l in self.hLayoutsargs:
            self.vLayoutWindow.addLayout(l)
        self.vLayoutWindow.addLayout(self.hLayoutButtons)
        self.hLayoutWindow.addLayout(self.vLayoutWindow)
        
    def closeEvent(self, event):
        """close Window"""
        
        self.element.element.p = None
        # self.element.element.surface.addhistory()
        self.close()
        
    def save(self):
        """save input values"""
        
        reverse = self.element.funksetargsvalues()
        self.element.name = self.nameEdit.text()
        inputsize = len(self.element.inputs)
        outputsize = len(self.element.outputs)
        if not self.element.sizechangable[0] == False:
            if int(self.inputsEdit.text()) >= self.element.sizechangable[0][0] and (self.element.sizechangable[0][1] == -1 or int(self.inputsEdit.text()) <= self.element.sizechangable[0][1]):
                inputsize = int(self.inputsEdit.text())
            else:
                self.errordialog = QtWidgets.QErrorMessage()
                self.errordialog.showMessage(f"'{self.inputsEdit.text()}' type does not match")
                return False
        if not self.element.sizechangable[1] == False:
            if int(self.outputsEdit.text()) >= self.element.sizechangable[1][0] and (self.element.sizechangable[1][1] == -1 or int(self.outputsEdit.text()) <= self.element.sizechangable[1][1]):
                outputsize = int(self.outputsEdit.text())
            else:
                self.errordialog = QtWidgets.QErrorMessage()
                self.errordialog.showMessage(f"'{self.outputsEdit.text()}' type does not match")
                return False
        if not inputsize == len(self.element.inputs) or not outputsize == len(self.element.outputs):
            sizereverse = self.element.funkupdatesize(len(self.element.inputs), len(self.element.outputs))
            self.element.element.updatesize(inputsize, outputsize)
            sizerestore = self.element.funkupdatesize(inputsize, outputsize)
            self.element.element.surface.addhistory(sizereverse, sizerestore)
        try:
            for i, p in enumerate(self.properties):
                try:
                    if self.element.argscast[i] == str:
                        value = self.element.argscast[i](p())
                    else:
                        value = self.element.argscast[i](eval(p()))
                    if self.element.argscompatibility[i](value) == False:
                        self.errordialog = QtWidgets.QErrorMessage()
                        self.errordialog.showMessage(f"'{p()}' type does not match")
                        appicon = QtGui.QIcon()
                        appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
                        self.errordialog.setWindowIcon(appicon)
                        self.errordialog.setWindowTitle("deltaTau")
                        return False
                    else:
                        self.element.argsvalues[i] = value
                except:
                    self.errordialog = QtWidgets.QErrorMessage()
                    self.errordialog.showMessage(f"'{p()}' type does not match")
                    appicon = QtGui.QIcon()
                    appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
                    self.errordialog.setWindowIcon(appicon)
                    self.errordialog.setWindowTitle("deltaTau")
                    return False
            restore = self.element.funksetargsvalues()
            self.element.element.surface.addhistory(reverse, restore)
            return True
        except:
            return False
        return True
    
    def ok(self):
        """save input values and close window when values are valid"""
        
        if self.save() == True:
            self.element.element.p = None
            self.close()
        
    def keyPressEvent(self, event):
        """save event when enter key is pressed"""
        
        if event.key() == QtCore.Qt. Key_Return:
            if self.save() == True:
                self.element.element.p = None
                self.close()

"""prints the information about this file"""
if PRINT_INFO:
    printinfo()