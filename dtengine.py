# -*- coding: utf-8 -*-
"""
Created on Wed Sep 21 13:43:05 2022

@author: czatho

This is the deltaTau engine file
Version overview
version beta 0.1.0:
    new version of the deltaTau dtengine file
    new version uses singular value decomposition 
version beta 0.1.1
    added a failure detection for the function elements
version beta 0.1.2
    new architectrue for the cicruit elements
    now the update objects changes them
    and Nodes objects are used to connect them
version beta 0.1.3
    delta is now adaptive
    performance gaines have still to be made
    to influenc on the function elements still has to be added
version beta 0.1.4
    added ode solvers to the simulation
    now availiabe are Radau, LSODA and BDF
version beta 0.1.5
    bugfixes
version beta 0.1.6
    logger added
    and loop ignore
version beta 0.1.7
    new architecture for the function elements
    function elements are now in time with circuit simulation
version beta 0.1.8
    bugfixed function calculation with solver
version beta 0.1.9
    value in CElements is now inverse
version 0.1.10
    Bug-fixes and Error handling
version 0.2.0
    Bug-fixes
    discrete time step added called major step
"""
    
# info variables
PROGRAM_NAME = "deltaTau"
FILE_NAME = "dtengine.py"
FILE_CREATION = "2022-09-21"
FILE_UPDATE = "2023-11-24"
FILE_VERSION = "beta 0.2.0"
DT_VERSION = "0.12.0"
PRINT_INFO = True


# libraries
import numpy as np
import math as m
# import mainWindow
import copy
import scipy.integrate as ode
import scipy.linalg as linalg
import datetime as datetime
# import sympy as sym
# import mpmath as mp

def printinfo():
    """shows all information about this file"""

    print(f"deltaTau {DT_VERSION}")    
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)

class Node:
    """Class for Node element"""
    
    def __init__(self):
        """creats the node List with all conected elements
        and the ID"""
        
        self.elementslist = []
        self.Id = -1
        self.groundnotecheck = False
        
    def append(self, element):
        """appends the new element"""
        
        if not element in self.elementslist:
            self.elementslist.append(element)
        
    def remove(self, element):
        """removes the element from the list"""
        
        if element in self.elementslist:
            self.elementslist.remove(element)
            
    def conncat(self, node):
        """concats two nodes"""
        
        for e in self.elementslist:
            node.append(e)
        self = node
        
    def getgroundnode(self):
        """returns the groundnode or -1"""
        
        if self.groundnotecheck == False:
            self.groundnotecheck = True
            groundnode = self.Id
            node1 = -1
            node2 = -1
            for e in self.elementslist:
                node1 = e.nodes[0].getgroundnode()
                node2 = e.nodes[1].getgroundnode()
                if node1 == -1:
                    node1 = self.Id
                if node2 == -1:
                    node2 = self.Id
                groundnode = min(groundnode, node1, node2)
            return groundnode
        else:
            return -1
    

class UpdateCE:
    """parent class for the updates"""
    
    def __init__(self, name, *args):
        """sets the name"""
        
        self.name = name
        self.fail = False
        self.dataformatError = False
        self.inputs = []
        self.outputs = []
        self.publicvariables = []
        self.majordelta = 0
      
    def setmajordelta(self, majordelta):
        "sets the major delta"
        
        self.majordelta = majordelta
    
    def setnodes(self, nodes):
        """sets the Nodes"""
        
        pass
    
    def setinputs(self, inputs):
        """sets the Signals"""
        
        self.inputs = inputs
        for i in self.inputs:
            i.append(self)
    
    def getoutputs(self):
        """returns the outputs"""
        
        return self.outputs
    
    def update(self, major, time):
        """updates the elements"""
        
        return 0
    
    def getCE(self):
        """returns the CE Elements"""
        
        return []
    
    def getFE(self):
        """returns the CE Elements"""
        
        return []
    
    def setpublicvariables(self):
        """returns the public variables"""
        
        return []
    
    def getpublicvariables(self):
        """returns the public variables"""
        
        # getlist = []
        # for i, v in enumerate(self.publicvariables):
        #     def get():
        #         return v
        #     getlist.append(v)
        # return getlist
        return self.publicvariables
    
    def action(self):
        """activates the userfunciton"""
        
        pass
    
    def setarray(self, *args):
        """checks the dataformat and sets the outputsignals to the right dataformat"""
        
        pass
    
    def connect(self):
        """connects the elements and the signals"""
        
        pass
    
    def clear(self):
        """enables to clear possible open Windows etc."""
        
        pass

    
class CircuteElement:
    """Class for basic Circute Elements
    This is the basic Element that comprises all circutes and
    the different cirutelement class create one of these Elements
    or a list of them"""
    
    def __init__(self, name, _type, value1, value2=0):
        """Init funktion
            type can be R, L, C, U, I, T
            value1 is the value of the circuit element for R in Ohm, L in H, C in F
            U in V and I in A and for T the number of windings
            Updatefunction profides the class to update the circuit
            value2 profides more values for C, L and T
            for C is the start voltage, for L is the startcurrent and for T the coupled element
            note the value2 of the coupled element has to be another different coupled element
            """
            
        self.name = name
        self.type = _type
        self.value = value1
        self.value2 = 0
        # self.factor = [[1], [1]]
        self.coupledElement = None
        self.nodes = []
        
        self.nodeIdx = [-1, -1]
        
        self.V = []
        self.I = []
        self.t = []
        
        self.Vmajor = []
        self.Imajor = []
        self.tmajor = []
        
        self.lastupdatemajor = False
        
        self.update = False
        
        self.initvalue2(value2)
        
        self.check()
        
    def initvalue2(self, value2):
        """makes the library compatible with the new elements"""
        
        if self.type == "C":
            self.value2 = value2
        elif self.type == "L":
            self.value2 = value2
        elif self.type == "U":
            self.value2 = value2
        elif self.type == "I":
            self.value2 = value2
        elif self.type == "T":
            self.coupledElement = value2
            if not value2 == None:
                self.coupledElement.coupledElement = self
        if self.type == "R" or self.type == "L" or self.type == "C":
            if self.value == 0:
                self.value = np.inf
            else:
                self.value = 1/self.value
                
    def check(self):
        """checks the values for validity"""
        
        if not self.type == "C" and not self.type == "L" and not self.type == "R" and not self.type == "U" and not self.type == "I" and not self.type == "T":
            raise TypeError(f"Variable type of {self.name} is not as expected")
        if not self.value == float(self.value):
            raise TypeError("Variable value of {self.name} is not as expected")
            
    def addVIt(self, V, I, t, major):
        """updates the voltage, current and time"""
        
        self.V.append(V)
        self.I.append(I)
        self.t.append(t)
        if major:
            self.Vmajor.append(V)
            self.Imajor.append(I)
            self.tmajor.append(t)
        if type(self.value2) == FunctionSignal:
            if self.type == "U":
                self.value2.addDt(I, t, major)
            elif self.type == "I":
                self.value2.addDt(V, t, major)
            else:
                raise TypeError(f"TypeError: {self.name} variable2 is of wrong type")
                
        
    def removeVIt(self, major):
        """updates the voltage, current and time"""
        
        self.V.pop(-1)
        self.I.pop(-1)
        self.t.pop(-1)
        if major:
            self.Vmajor.pop(-1)
            self.Imajor.pop(-1)
            self.tmajor.pop(-1)
        if type(self.value2) == FunctionSignal:
            self.value2.removeDt(major)
        
    def setNodes(self, node1, node2):
        """sets the nodes of the element"""
            
        self.nodes = [node1, node2]
        node1.append(self)
        node2.append(self)
        
    def setvalue(self, value):
        """sets the value"""
        
        if self.type == "R" or self.type == "L" or self.type == "C":
            if value == 0:
                self.value = np.nan_to_num(np.inf)
            else:
                self.value = 1/np.nan_to_num(value)
        else:
            self.value = value


class FunctionSignal:
    """Signal that connects different FunctionElements"""
    
    def __init__(self, inputelement, dataformat=float):
        """creats the signal"""
        
        self.inputelement = inputelement
        self.outputelements = []
        self.id = -1
        self.data = []
        self.t = []
        self.datamajor = []
        self.tmajor = []
        
    def append(self, element):
        """adds an outputelement"""
        
        if not element in self.outputelements:
            self.outputelements.append(element)
        
    def remove(self, element):
        """removes the element"""
        
        if element in self.outputelements:
            self.outputelements.remove(element)
    
    def setnewinputelement(self, element):
        """sets a new inputelement"""
        
        self.inputelement = element
    
    def addDt(self, data, time, major):
        """adds data and time point"""
        
        self.data.append(data)
        self.t.append(time)
        if major:
            self.datamajor.append(data)
            self.tmajor.append(time)
        
    def removeDt(self, major):
        """removes the last datapoint"""
        
        self.data.pop(-1)
        self.t.pop(-1)
        if major:
            self.datamajor.pop(-1)
            self.tmajor.pop(-1)
        
    def getdata(self):
        """returns data"""
        
        return self.data
    
    def time(self):
        """returns time"""
        
        return self.t        


class FunctionELement:
    """Parent class for Function Elements"""
    
    def __init__(self, name, _type, value1):
        """creats the Functionelement
        type can be Q for Source, S for State, G for Gain, A for Adder, and D for DeadEnd"""
        
        self.name = name
        self.type = _type
        self.value = value1
        self.value2 = 0
        
        self.input = None
        self.output = FunctionSignal(self)
        
        self.check()
           
    def check(self):
        """checks the values for validity"""
        
        if self.type == "D":
            self.output = None
        if not self.type == "Q" and not self.type == "S" and not self.type == "G" and not self.type == "A" and not self.type == "D":
            raise TypeError(f"Variable type of {self.name} is not as expected")
        if self.type == "A":
            if not type(self.value) == list:
                raise TypeError(f"Variable value of {self.name} is not as expected")
            for i in self.value:
                if not type(i) == FunctionSignal:
                    raise TypeError(f"Variable value of {self.name} is not as expected")
        else:
            if not self.value == float(self.value):
                raise TypeError(f"Variable value of {self.name} is not as expected")
            
    def getoutput(self):
        """returns the outputsignal"""
        
        return self.output
    
    def setinput(self, inputsignal):
        """sets the inputsignal for Element"""
        
        self.input = inputsignal
        self.input.append(self)
        
    def setoutput(self, outputsignal):
        """sets the inputsignal for Element"""
        
        self.output = outputsignal
        self.output.setnewinputelement(self)
        
    def setvalue(self, value):
        """sets the value"""
        
        self.value = value
        self.check()
        
    def copy(self):
        """copys element"""
        
        return copy.deepcopy(self)
        
        
class FunctionArray:
    """connects the UpdateCEs"""
    
    def __init__(self, inputelement):
        """creats the element"""
         
        self.input = inputelement
        self.outputs = []
        self.signals = []
        
    def append(self, output):
        """appends an output"""
        
        if output not in self.outputs:
            self.outputs.append(output)
    
    def setarray(self, signals):
        """sets the dataformat"""
        
        oldsize = self.size()
        self.signals = signals
        newsize = self.size()
        if not oldsize == newsize:
            for e in self.outputs:
                e.setarray()
                
    def __len__(self):
        """defines len"""
        
        return len(self.signals)
    
    def copy(self):
        """returns copy"""
        
        return copy.deepcopy(self)
    
    def copysignals(self):
        """returns copy"""
        
        signals = []
        for s in self.signals:
            if type(s) == FunctionSignal:
                signals.append(FunctionSignal(None))
            else:
                array = FunctionArray(None)
                array.signals = s.copysignals()
                signals.append(array)
        return signals
    
    def __getitem__(self, key):
        """returns key"""
        
        return self.signals[key]
    
    def getdata(self):
        """returns the data"""
        
        data = []
        if len(self.signals) == 1:
            if type(self.signals[0]) == FunctionSignal:
                return self.signals[0].getdata()
        for e in self.signals:
            data.append(e.getdata())
        return data
            
    def getsignals(self):
        """returns a list with all signals"""        
        
        signals = []
        for s in self.signals:
            if type(s) == FunctionArray:
                signals = signals + s.getsignals()
            else:
                signals.append(s)
        return signals
    
    def time(self):
        """returns time"""
        
        for s in self.signals:
            return s.time()
        
    def size(self):
        """returns the size"""
         
        size = [0]
        for s in self.signals:
            if type(s) == FunctionSignal:
                if type(size[-1]) == int:
                    size[-1] = size[-1] + 1
                else:
                    size.append(1)
            else:
                if size == [0]:
                    size = []
                size.append(s.size())
        return size
    
    def checksize(self, types):
        """checks if signals are fo correct type"""
        
        if self.size() in types:
            return True
        else:
            for s in self.signals:
                if type(s) == FunctionArray:
                    if not s.checksize(types):
                        return False
                else:
                    return False
            return True
        
        
class Logger:
    """logs the actions"""
    
    def __init__(self):
        """intis the logger"""
        
        self.log = "----deltTau Log file----\n"
        self.index = 0
    
    def addlog(self, info):
        """adds a new log item"""
        
        self.log = self.log + f"[{self.index:02d}] ({datetime.datetime.utcnow().strftime('%Y.%m.%d %H:%M:%S.%f')}): {info} \n"
        self.index = self.index + 1
        
        
class Parameters:
    """contains all the simulation parameters
    they are the stoptime and the delta for every step"""
    
    def __init__(self, stop, delta, epsilon, solver, ignoreloop):
        """initializing the parameters"""
        
        self.stop = stop
        self.delta = delta
        self.solver = solver
        self.epsilon = epsilon
        self.ignoreloop = ignoreloop


class SystemOutput:
    """object that handels the user output so that it has the same standard for all outputs"""
    
    def __init__(self):
        """intis the system output it sets the length of the information that can be displayed and the length of the progressbar"""
        
        self.taskdecimals = 2
        self.namelenght = 20
        self.barlenght = 50
        self.infolenght = 50
        self.noTask = -1
        self.taskList =[]
            
    def newTask(self, name):
        """creates a new progressbar and gives the number of the Task that currently is worked on"""
           
        self.noTask = self.noTask + 1
        class Taskoutput:
        
            def __init__(self, number, name, state, info):
                    self.number = number
                    self.name = name
                    self.state = state
                    self.info = info
        newTask = Taskoutput(self.noTask, name, 0, "init Task")
        self.taskList.append(newTask)
        self.refreshTask()
        
        
    def updatestate(self, state):
        """updates the state and reprints the"""
        
        if self.taskList[-1].state < round(state):
            self.taskList[-1].state = round(state)
            self.refreshTask()
    
    def updateinfo(self, info):
        """updates the info message"""
        
        if not self.taskList[-1].info == info:
            self.taskList[-1].info = info
            self.refreshTask()
            
    def endTask(self):
        """stops the task"""
        
        self.refreshTask(stop=True)
        self.taskList.pop(-1)
        self.refreshTask()
        
    def failTask(self):
        """stops the task"""
        
        self.refreshTask(stop=True, fail=True)
        self.taskList.pop(-1)
        self.refreshTask()
        
        
    def refreshTask(self, stop=False, fail=False):
        """reprints the task"""
        
        if len(self.taskList) > 0:
            tasknumber = str(self.taskList[-1].number)
            tasknumber = "0"*(self.taskdecimals - len(tasknumber)) + tasknumber
            tasknumber = "[" + tasknumber + "]"
            taskname = self.taskList[-1].name
            if len(taskname) > self.namelenght:
                taskname = taskname[:-3] + "..."
            else:
                taskname = taskname + " "*(self.namelenght - len(taskname))
            state = self.taskList[-1].state
            bar = " "
            if not stop:
                bar = "[" + "#"*round(self.barlenght*state/100) + "."*(self.barlenght - round(self.barlenght*state/100)) + "]"
            percentage = str(state)
            if len(percentage) < 2:
                percentage = "0" + percentage
            percentage = percentage + "%"
            if stop:
                percentage = ""
            info = self.taskList[-1].info
            if len(info) > self.infolenght:
                info = info[:-3] + "..."
            else:
                info = info + " "*(self.infolenght - len(info))
            if stop:
                if fail:
                    info = "Fail" + " "*(self.barlenght + self.infolenght + 10)
                else:
                    info = "Done" + " "*(self.barlenght + self.infolenght + 10)
            if not stop:
                print("\r" + tasknumber + taskname + ": " + bar + " " + percentage + " " + info, end=" ")
            else:
                print("\r" + tasknumber + taskname + ": " + bar + " " + percentage + " " + info)
            

class deltaTau:
    """Simulation engine of the deltaTau Simulation Programm
        does the initialisation and the simulation
        """
    
    def __init__(self, updateList, functionList, parameters, *args):
        """inits the simulation engine"""
        
        self.circuitList = []
        self.updateList = updateList + functionList
        # self.functionList = functionList
        self.functionList = []
        self.parameters = parameters
        # self.algorithm = self.parameters.algorithm.function
        self.systemOutput = SystemOutput()
        self.logger = Logger()
        self.state = 0
        self.error = 0
        # self.factorialfBlocks = m.factorial(len(self.functionList) + 1)
        
        self.independend = []
        
        self.nIdx = 0
        self.sIdx = 0
        
        self.groundNodes = []
        
        self.resistances = []
        # self.resistancesinf = []
        
        self.voltagesources = []
        self.currentsources = []
        self.voltagestates = []
        self.currentstates = []
        
        self.couplers = []
        
        self.UCnodes = []
        self.Lnodes = []
        self.Uelements = []
        
        self.functionsources = []
        self.functionstates = []
        self.functiongains = []
        self.functionadders = []
        self.functiondeadeands = []
        
        #states
        self.x = []
        self.y = []
        
        #voltage and current and signals
        self.u = []
        self.i = []
        self.s = []
        
        #time
        self.t = []
        
        # densoutput of solver
        self.denseoutput = None
        self.tnextmajor = 0
        
        #circuit calculation
        self.uinvGy = np.array([])  # u = invG*y
        self.uinvGx = np.array([])  # u = invG*x
        self.iinvGy = np.array([])  # i = invG*y
        self.iinvGx = np.array([])  # i = invG*x
        self.sinvGy = np.array([])  # s = invG*y
        self.sinvGx = np.array([])  # s = invG*x
        
        #state calculation
        self.Aself = np.array([])
        self.Asource = np.array([])
        
    def precompile(self):
        """precompile: sets all information before compiling"""
        
        n = 14
        
        for u in self.updateList:
            u.setmajordelta(self.parameters.delta)
        
        self.logger.addlog("Info: start precompile")
        self.systemOutput.newTask("precompile")
        
        self.prepare()
        
        self.creatcircuitList()
        self.creatfunctionList()
        self.systemOutput.updatestate(round(100/n*1))
        
        self.setNodeIdx()
        self.systemOutput.updatestate(round(100/n*2))
        
        self.setresistances()
        self.systemOutput.updatestate(round(100/n*3))
        
        self.setcircuitsources()
        self.systemOutput.updatestate(round(100/n*4))
        
        self.setcircuitstates()
        self.systemOutput.updatestate(round(100/n*5))
        
        self.setcouplers()
        self.systemOutput.updatestate(round(100/n*6))
        
        self.setUnodes()
        self.systemOutput.updatestate(round(100/n*7))
        
        self.setLnodes()
        self.systemOutput.updatestate(round(100/n*8))
        
        self.setfunctiondeadends()
        self.systemOutput.updatestate(round(100/n*9))
        
        self.setSignalIdx()
        self.systemOutput.updatestate(round(100/n*10))
        
        self.setfunctionsources()
        self.systemOutput.updatestate(round(100/n*11))
        
        self.setfunctionstates()
        self.systemOutput.updatestate(round(100/n*12))
        
        self.setfunctiongains()
        self.systemOutput.updatestate(round(100/n*13))
        
        self.setfunctionadders()
        self.systemOutput.updatestate(round(100/n*14))
        
        self.systemOutput.endTask()
        self.logger.addlog("Info: end precompile")
    
    
    def compile(self):
        """compiles the circuit for calculation"""
        
        n = 19
        
        self.precompile()
        self.error = 0
        self.logger.addlog("Info: start compile")
        self.systemOutput.newTask("compile")
        
        # [infR, infL, infC] = self.countinfelements()
        
        Ai = self.createAi()
        Au = self.createAu()
        As = self.creatAs()
        self.systemOutput.updatestate(round(100/n*1))
        
        nUnodes, Tc = self.creatTc()
        self.systemOutput.updatestate(round(100/n*2))
        
        Gy, Myy, Myx = self.creatGy(nUnodes)
        self.systemOutput.updatestate(round(100/n*3))
        
        Gx, Mxy, Mxx = self.creatGx(nUnodes)
        self.systemOutput.updatestate(round(100/n*4))
        
        Gr, Mry, Mrx = self.creatGr(nUnodes)
        self.systemOutput.updatestate(round(100/n*5))
        
        Gc, Mcy, Mcx = self.creatGc(nUnodes, Tc)
        self.systemOutput.updatestate(round(100/n*6))
        
        Gn, Mny, Mnx = self.creatGn(nUnodes)
        self.systemOutput.updatestate(round(100/n*7))
        
        Gb, Mby, Mbx = self.creatGb(nUnodes)
        self.systemOutput.updatestate(round(100/n*8))
        
        Gt, Mty, Mtx = self.creatGt(nUnodes, Tc)
        self.systemOutput.updatestate(round(100/n*9))
        
        Gl, Mly, Mlx = self.creatGl(nUnodes)
        self.systemOutput.updatestate(round(100/n*10))
        
        Gg, Mgy, Mgx = self.creatGg(nUnodes)
        self.systemOutput.updatestate(round(100/n*11))
        
        Ga, May, Max = self.creatGa(nUnodes)
        self.systemOutput.updatestate(round(100/n*12))
        
        Gm, Mmy, Mmx = self.creatGm(nUnodes)
        self.systemOutput.updatestate(round(100/n*13))
        
        # print(Gy.shape)
        # print(Gx.shape)
        # print(Gr.shape)
        # print(Gc.shape)
        # print(Gn.shape)
        # print(Gb.shape)
        # print(Gt.shape)
        # print(Gl.shape)
        # print(Gg.shape)
        # print(Ga.shape)
        # print(Gm.shape)
        G = np.block([[Gy], [Gx], [Gr], [Gc], [Gb], [Gn], [Gt], [Gl], [Gg], [Ga], [Gm]])
        self.systemOutput.updatestate(round(100/n*14))
        # print(G)
        # print(sym.Matrix(G).rref())
        self.systemOutput.updateinfo("creat My and Mx")
        My = np.block([[Myy], [Mxy], [Mry], [Mcy], [Mby], [Mny], [Mty], [Mly], [Mgy], [May], [Mmy]])
        Mx = np.block([[Myx], [Mxx], [Mrx], [Mcx], [Mbx], [Mnx], [Mtx], [Mlx], [Mgx], [Max], [Mmx]])
        self.systemOutput.updatestate(round(100/n*15))
        
        Mu = self.creatMu(nUnodes)
        Mi = self.creatMi(nUnodes)
        Ms = self.creatMs(nUnodes)
        # Mi = self.creatMi()
        self.systemOutput.updatestate(round(100/n*16))
        
        # _, self.independend = sym.Matrix(G).T.rref()
        invG = self.creatinvG(G)
        self.systemOutput.updatestate(round(100/n*17))
        
        # My = My[self.independend, :]
        # Mx = Mx[self.independend, :]
        self.uinvGy = self.matmulmatmul(Mu, invG, My)
        self.uinvGx = self.matmulmatmul(Mu, invG, Mx)
        self.iinvGy = self.matmulmatmul(Mi, invG, My)
        self.iinvGx = self.matmulmatmul(Mi, invG, Mx)
        self.sinvGy = self.matmulmatmul(Ms, invG, My)
        self.sinvGx = self.matmulmatmul(Ms, invG, Mx)
        self.systemOutput.updatestate(round(100/n*18))
        
        self.creatAself(Au, Ai, As)
        self.creatAsource(Au, Ai, As)
        self.systemOutput.updatestate(round(100/n*19))
        
        # self.systemOutput.updateinfo("set algorythm")
        # self.parameters.algorithm.setMatrices(self.Aself, self.Asource)
        # self.parameters.algorithm.setdelta(self.parameters.delta)
        
        self.systemOutput.endTask()
        self.logger.addlog("Info: end compile")
    
    def update(self):
        """updates the circuit for calculation"""
        
        self.error = 0
        
        Ai = self.createAi()
        Au = self.createAu()
        As = self.creatAs()
        
        nUnodes, Tc = self.creatTc()
        
        Gy, Myy, Myx = self.creatGy(nUnodes)
        
        Gx, Mxy, Mxx = self.creatGx(nUnodes)
        
        Gr, Mry, Mrx = self.creatGr(nUnodes)
        
        Gc, Mcy, Mcx = self.creatGc(nUnodes, Tc)
        
        Gn, Mny, Mnx = self.creatGn(nUnodes)
        
        Gb, Mby, Mbx = self.creatGb(nUnodes)
        
        Gt, Mty, Mtx = self.creatGt(nUnodes, Tc)
        
        Gl, Mly, Mlx = self.creatGl(nUnodes)
        
        Gg, Mgy, Mgx = self.creatGg(nUnodes)
        
        Ga, May, Max = self.creatGa(nUnodes)
        
        Gm, Mmy, Mmx = self.creatGm(nUnodes)
        
        G = np.block([[Gy], [Gx], [Gr], [Gc], [Gb], [Gn], [Gt], [Gl], [Gg], [Ga], [Gm]])
        # print(G)
        self.systemOutput.updateinfo("creat My and Mx")
        My = np.block([[Myy], [Mxy], [Mry], [Mcy], [Mby], [Mny], [Mty], [Mly], [Mgy], [May], [Mmy]])
        Mx = np.block([[Myx], [Mxx], [Mrx], [Mcx], [Mbx], [Mnx], [Mtx], [Mlx], [Mgx], [Max], [Mmx]])
        
        Mu = self.creatMu(nUnodes)
        Mi = self.creatMi(nUnodes)
        Ms = self.creatMs(nUnodes)
        
        invG = self.creatinvG(G)
        
        # My = My[self.independend, :]
        # Mx = Mx[self.independend, :]
        self.uinvGy = self.matmulmatmul(Mu, invG, My)
        self.uinvGx = self.matmulmatmul(Mu, invG, Mx)
        self.iinvGy = self.matmulmatmul(Mi, invG, My)
        self.iinvGx = self.matmulmatmul(Mi, invG, Mx)
        self.sinvGy = self.matmulmatmul(Ms, invG, My)
        self.sinvGx = self.matmulmatmul(Ms, invG, Mx)
        
        self.creatAself(Au, Ai, As)
        self.creatAsource(Au, Ai, As)
        
        self.systemOutput.updateinfo("set algorythm")
        # self.parameters.algorithm.setMatrices(self.Aself, self.Asource)
    
    def run(self, progresssignal=None, failurereturn=None):
        """runs the simulation
            calculates the state for every timestamp"""
        
        self.logger.addlog("Info: initialize simulation")
        self.systemOutput.newTask("run simulation")
        
        self.systemOutput.updateinfo("initialize simulation")
        
        y = []
        x = []
        u = []
        i = []
        t = 0
        self.tnextmajor = 0
        t_eval_i = 0
        t_eval_step = 0
        delta = self.parameters.delta
        # self.factorialfBlocks = m.factorial(len(self.functionList) + 1)
        for e in self.circuitvoltagesources:
            y.append(e.value)
        for e in self.circuitcurrentsources:
            y.append(e.value)
        for e in self.functionsources:
            y.append(e.value)
        for e in self.circuitvoltagestates:
            x.append(e.value2)
        for e in self.circuitcurrentstates:
            x.append(e.value2)
        for e in self.functionstates:
            x.append(e.value2)
            
        y = np.array(y)
        x = np.array(x)
        
        self.state = 0
        progress = 0
        
        self.logger.addlog("Info: start run")
        self.systemOutput.updateinfo("running...")
        
        solver = None
        running = True
        while running:
            try:
                if self.state == 1:
                    self.logger.addlog("Info: simulation stoped by used input")
                    break
                elif self.state == 2:
                    while self.state == 2:
                        pass
                
                major = t == self.tnextmajor
                if major:
                    self.tnextmajor = self.tnextmajor + delta
                u, i, s, y = self.calcUI(x, t, major=major)
    
                self.t.append(t)
                self.i.append(i)
                self.u.append(u)
                self.s.append(s)
                self.x.append(x)
                self.y.append(y)
                # print(s)
                for j, e in enumerate(self.circuitList):
                    e.addVIt(u[j], i[j], t, major)
                for e in self.functionList:
                    e.output.addDt(s[e.output.id], t, major)
                
                # stepfunctionreturn = self.stepfunctionList(t, self.parameters.delta)
                # if not stepfunctionreturn == "":
                #     self.logger.addlog("Error: function simulation failed")
                #     self.logger.addlog("Error: simulation failed")
                #     self.systemOutput.failTask()
                #     return stepfunctionreturn
                # result = ode.solve_ivp(self.dxdt, [0, self.parameters.stop], x, method="Radau", t_eval=np.linspace(0, self.parameters.stop, int(self.parameters.stop/self.parameters.delta) + 1, endpoint=True), jac=self.jac, vectorized=False, max_step=delta, rtol=self.parameters.epsilon)
                # print(result.t)
                # print(result.y)
                if len(x) > 0:
                    if solver == None:
                        solver = self.parameters.solver(self.dxdt, 0, x, self.parameters.stop, max_step=delta, rtol=self.parameters.epsilon, jac=self.jac, vectorized=False)
                    #     # solver = ode.DOP853(self.dxdt, 0, x, self.parameters.stop, max_step=self.parameters.delta, rtol=self.parameters.epsilon, vectorized=False)
                      
                    solver.step()
                    
                    if solver.status == "finished":
                        self.logger.addlog("Info: solver ended")
                        break
                    elif solver.status == "failed":
                        self.logger.addlog("Error: solver failed")
                        self.systemOutput.failTask()
                        return "solver failed"
                
                    self.denseoutput = solver.dense_output()
                    
                    t = solver.t
                    
                    if t > self.tnextmajor:
                        x = self.denseoutput(self.tnextmajor)
                        
                        u, i, s, y = self.calcUI(self.denseoutput(self.tnextmajor), self.tnextmajor, major=True)
            
                        self.t.append(self.tnextmajor)
                        self.i.append(i)
                        self.u.append(u)
                        self.s.append(s)
                        self.x.append(x)
                        self.y.append(y)
                        # print(s)
                        for j, e in enumerate(self.circuitList):
                            e.addVIt(u[j], i[j], self.tnextmajor, True)
                        for e in self.functionList:
                            e.output.addDt(s[e.output.id], self.tnextmajor, True)
                        
                        self.tnextmajor = self.tnextmajor + delta
                        
                    x = solver.y
                else:
                    # stepfunctionreturn = self.stepfunctionList(t, self.parameters.delta)
                    # if not stepfunctionreturn == "":
                    #     self.logger.addlog("Error: function simulation failed")
                    #     self.logger.addlog("Error: simulation failed")
                    #     self.systemOutput.failTask()
                    #     return stepfunctionreturn
                    t = t + self.parameters.delta
                    self.tnextmajor = t
                    # delta = self.parameters.delta
                    # self.calcUI(x, t)
                    if t > self.parameters.stop:
                        break
                    
                    # stepfunctionreturn = self.stepfunctionList(factorialfBlocks, progresssignal, t, delta)
                    # if not stepfunctionreturn == "":
                    #     self.systemOutput.failTask()
                    #     return stepfunctionreturn
                self.systemOutput.updatestate(100*t/self.parameters.stop)
                if not progresssignal == None and int(100*t/self.parameters.stop) > progress:
                    progress = int(100*t/self.parameters.stop)
                    progresssignal.emit(progress)
            
            except:
                self.logger.addlog("simulation failed")
                self.systemOutput.failTask()
                return "failed"
            # stepfunctionreturn = self.stepfunctionList(t, delta)
            # if not stepfunctionreturn == "":
            #     self.logger.addlog("Error: function simulation failed")
            #     self.logger.addlog("Error: simulation failed")
            #     self.systemOutput.failTask()
            #     return stepfunctionreturn
        # stepfunctionreturn = self.stepfunctionList(t, delta)
        # if not stepfunctionreturn == "":
        #     self.logger.addlog("Error: function simulation failed at the end")
        #     self.systemOutput.failTask()
        #     return stepfunctionreturn
        self.systemOutput.endTask()
        self.finished()
        return True
    
### precompile functions

    ### get Lists
    
    def prepare(self):
        """sets the functionsignals"""
        
        for u in self.updateList:
            u.setarray()
        for u in self.updateList:
            u.connect()
    
    def creatcircuitList(self):
        """sets the circuitlist"""
        
        for u in self.updateList:
            self.circuitList = self.circuitList + u.getCE()
            
    def creatfunctionList(self):
        """sets the circuitlist"""
        
        for u in self.updateList:
            self.functionList = self.functionList + u.getFE()
    
    ### init CE
    
    def setNodeIdx(self):
        """sets the nodeIdx of all circuit elements"""
        
        self.systemOutput.updateinfo("set Node Ids")
        
        self.nIdx = 0
        self.groundNodes = []
        
        try:
            for e in self.circuitList:
                if e.nodes[0].Id == -1:
                    e.nodes[0].Id = self.nIdx
                    self.nIdx = self.nIdx + 1
                if e.nodes[1].Id == -1:
                    e.nodes[1].Id = self.nIdx
                    self.nIdx = self.nIdx + 1
                    
                    
            for e in self.circuitList:
                e.nodeIdx[0] = e.nodes[0].Id
                e.nodeIdx[1] = e.nodes[1].Id
                
            for e in self.circuitList:
                node1 = e.nodes[0].getgroundnode()
                node2 = e.nodes[1].getgroundnode()
                if node1 > -1:
                    self.groundNodes.append(node1)
                if node2 > -1:
                    self.groundNodes.append(node2)
                        
        except Exception as e:
            self.logger.addlog(f"Error: {e}")
            self.logger.addlog("Error: failed to set node index")
            raise TypeError(e)
        
    def setresistances(self):
        """creats the list with all resistances"""
        
        self.systemOutput.updateinfo("set resistances")
        
        self.resistances = []
        
        for e in self.circuitList:
            if e.type == "R":
                self.resistances.append(e)
        
    def setcircuitsources(self):
        """creates two lists with the sources"""
        
        self.systemOutput.updateinfo("set circuit sources")
        
        self.circuitvoltagesources = []
        self.circuitcurrentsources = []
        for e in self.circuitList:
            if e.type == "U":
                self.circuitvoltagesources.append(e)
        for e in self.circuitList:
            if e.type == "I":
                self.circuitcurrentsources.append(e)
        
    def setcircuitstates(self):
        """creates two lists with the states"""
        
        self.systemOutput.updateinfo("set circuit states")
        
        self.circuitvoltagestates = []
        self.circuitcurrentstates = []
        for e in self.circuitList:
            if e.type == "C":
                self.circuitvoltagestates.append(e)
        for e in self.circuitList:
            if e.type == "L":
                self.circuitcurrentstates.append(e)
                
    def setcouplers(self):
        """creats a list with all couplers"""
        
        self.systemOutput.updateinfo("set couplers")
        
        self.couplers = []
        for e in self.circuitList:
            if e.type == "T" and not e.coupledElement == None:
                self.couplers.append(e)
    
    def setUnodes(self):
        """creats a list with all voltage supernodes"""
        
        self.systemOutput.updateinfo("set Unodes")
        
        self.Ccouplers = []
        UCcouplers = []
        changes = True
        while changes:
            usedelements = []
            self.Uelements = []
            nodes = []
            changes = False
            for e in self.circuitList:
                if not e in usedelements:
                    if e in self.circuitvoltagesources or e in self.circuitvoltagestates or e in UCcouplers:
                        waitingelements = copy.copy(e.nodes[0].elementslist)
                        nodelist = [e.nodes[0]]
                        while len(waitingelements) > 0:
                            if waitingelements[0] in self.circuitvoltagesources or waitingelements[0] in self.circuitvoltagestates or waitingelements[0] in UCcouplers:
                                if not waitingelements[0].nodes[0] in nodelist:
                                    nodelist.append(waitingelements[0].nodes[0])
                                    waitingelements = waitingelements + waitingelements[0].nodes[0].elementslist
                                    self.Uelements.append(waitingelements[0])
                                elif not waitingelements[0].nodes[1] in nodelist:
                                    nodelist.append(waitingelements[0].nodes[1])
                                    waitingelements = waitingelements + waitingelements[0].nodes[1].elementslist
                                    self.Uelements.append(waitingelements[0])
                            usedelements.append(waitingelements.pop(0))
                        nodes.append(nodelist)
            
            self.UCnodes = []
            for un in nodes:
                node = []
                for n in un:
                    node.append(n.Id)
                self.UCnodes.append(node)
            
            for n in self.UCnodes:
                for c in self.couplers:
                    if not c in self.Ccouplers:
                        if c.nodeIdx[0] in n and c.nodeIdx[1] in n:
                            self.Ccouplers.append(c)
                            self.Ccouplers.append(c.coupledElement)
                            UCcouplers.append(c.coupledElement)
                            changes = True
            
            index = 0
            while index < len(UCcouplers):
                if UCcouplers[index].coupledElement in UCcouplers:
                    UCcouplers.remove(UCcouplers[index])
                    UCcouplers.remove(UCcouplers[index].coupledElement)
                else:
                    index = index + 1
            
    def setLnodes(self):
        """creats a list with all curent supernodes"""
        
        self.systemOutput.updateinfo("set Lnodes")
        
        changes = True
        
        notLcouplers = []
        self.Lcouplers = copy.copy(self.couplers)
        
        while changes:
            changes = False
            self.Lnodes = []
            for e in self.circuitList:
                if e in self.circuitvoltagesources or e in self.circuitvoltagestates or e in self.resistances or e in notLcouplers:
                    self.Lnodes.append(e.nodeIdx)
                else:
                    self.Lnodes.append([e.nodeIdx[0]])
                    self.Lnodes.append([e.nodeIdx[1]])
            found = False
            index = 0
            while index < len(self.Lnodes):
                # print(self.Lnodes)
                for i, l in enumerate(copy.copy(self.Lnodes)):
                    for n in l:
                        if i > index and n in self.Lnodes[index]:
                            self.Lnodes[index] = list(np.unique(self.Lnodes[index] + l))
                            self.Lnodes.remove(l)
                            found = True
                            break
                index = index + 1
                if index >= len(self.Lnodes) and found == True:
                    index = 0
                    found = False
            
            Lcouplers = []
            for c in self.Lcouplers:
                inone = False
                if not c in Lcouplers:
                    for l in self.Lnodes:
                        if c.nodeIdx[0] in l and c.nodeIdx[1] in l:
                            inone = True
                            changes = True
                    if inone:
                        Lcouplers.append(c)
                        Lcouplers.append(c.coupledElement)
            # self.Lcouplers = copy.copy(self.couplers)
            for c in Lcouplers:
                self.Lcouplers.remove(c)
                notLcouplers.append(c)
                    
                        
                    
    ### init FE
    
    def setfunctiondeadends(self):
        """sets the function sources list"""
        
        for e in copy.copy(self.functionList):
            if e.type == "D":
                self.functiondeadeands.append(e)
                self.functionList.remove(e)
    
    def setSignalIdx(self):
        """sets the signal Idx"""
        
        self.sIdx = 0
        for e in self.functionList:
            e.output.id = self.sIdx
            self.sIdx = self.sIdx + 1
        for e in self.circuitList:
            if type(e.value2) == FunctionSignal:
                e.value2.id = self.sIdx
                self.sIdx = self.sIdx + 1
            
    def setfunctionsources(self):
        """sets the function sources list"""
        
        for e in self.functionList:
            if e.type == "Q":
                self.functionsources.append(e)
                
    def setfunctionstates(self):
        """sets the function sources list"""
        
        for e in self.functionList:
            if e.type == "S":
                self.functionstates.append(e)
                
    def setfunctiongains(self):
        """sets the function sources list"""
        
        for e in self.functionList:
            if e.type == "G":
                self.functiongains.append(e)
                
    def setfunctionadders(self):
        """sets the function sources list"""
        
        for e in self.functionList:
            if e.type == "A":
                self.functionadders.append(e)
                            
### compilefunction

    def createAu(self):
        """creates the integration of the current by the voltage"""
        
        self.systemOutput.updateinfo("create Au")
                
        Au = np.zeros((len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates), len(self.circuitList)))
        for i, e in enumerate(self.circuitList):
            if e.type == "L":
                Au[len(self.circuitvoltagestates) + self.circuitcurrentstates.index(e)][i] = e.value
        
        return Au
            
    def createAi(self):
        """creates the integration of the voltage by the current"""
        
        self.systemOutput.updateinfo("create Ai")
                
        Ai = np.zeros((len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates), len(self.circuitList)))
        for i, e in enumerate(self.circuitList):
            if e.type == "C":
                Ai[self.circuitvoltagestates.index(e)][i] = e.value
                
        return Ai
    
    def creatAs(self):
        """creats the integration of signals"""
        
        self.systemOutput.updateinfo("create As")
        
        As = np.zeros((len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates), self.sIdx))
        for i, e in enumerate(self.functionstates):
            As[len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + i, e.input.id] = e.value
            
        return As
    
    def creatTc(self):
        """creats the Tc Motrix"""
        
        Unodes = []
        for n in self.UCnodes:
            Unodes = Unodes + n
        
        nUnodes = len(Unodes)
        
        Tc = np.zeros((self.nIdx, nUnodes))
        
        index = 0
        for i in range(self.nIdx):
            if i in Unodes:
                Tc[i, index] = 1
                index = index + 1
                
        return [nUnodes, Tc]
    
    def creatGy(self, nUnodes):
        """creats the Gy matrix"""
        
        Gyuu = np.zeros((len(self.circuitvoltagesources), self.nIdx))
        Gyuz = np.zeros((len(self.circuitvoltagesources), len(self.circuitList) + nUnodes + len(self.Lcouplers)))
        Gyus = np.zeros((len(self.circuitvoltagesources), self.sIdx))
        
        Gyiu = np.zeros((len(self.circuitcurrentsources), self.nIdx))
        Gyii = np.zeros((len(self.circuitcurrentsources), len(self.circuitList)))
        Gyiz = np.zeros((len(self.circuitcurrentsources), nUnodes + len(self.Lcouplers)))
        Gyis = np.zeros((len(self.circuitcurrentsources), self.sIdx))
        
        Gysz = np.zeros((len(self.functionsources), self.nIdx + len(self.circuitList) + nUnodes + len(self.Lcouplers)))
        Gyss = np.zeros((len(self.functionsources), self.sIdx))
        
        Myy = np.eye(len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources))
        Myx = np.zeros((len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources), len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates)))
        
        delulines = []
        
        for i, e in enumerate(self.circuitvoltagesources):
            if e in self.Uelements:
                Gyuu[i, e.nodeIdx[0]] = Gyuu[i, e.nodeIdx[0]] + 1
                Gyuu[i, e.nodeIdx[1]] = Gyuu[i, e.nodeIdx[1]] - 1
                if type(e.value) == FunctionSignal:
                    Gyus[i, e.value.id] = -1
            else:
                delulines.append(i)
        for i, e in enumerate(self.circuitcurrentsources):
            Gyii[i, self.circuitList.index(e)] = 1
            if type(e.value) == FunctionSignal:
                Gyis[i, e.value.id] = -1
        for i, e in enumerate(self.functionsources):
            Gyss[i, e.output.id] = 1
            if e.output.id < 0:
                raise TypeError("TypeError: Non valide signal id")
        
        Gyu = np.block([[Gyuu, Gyuz, Gyus]])
        Gyu = np.delete(Gyu, delulines, axis=0)
        Myy = np.delete(Myy, delulines, axis=0)
        Myx = np.delete(Myx, delulines, axis=0)
        
        return [np.block([[Gyu], [Gyiu, Gyii, Gyiz, Gyis], [Gysz, Gyss]]), Myy, Myx]
        
    def creatGx(self, nUnodes):
        """creats the Gx matrix"""
        
        Gxuu = np.zeros((len(self.circuitvoltagestates), self.nIdx))
        Gxuz = np.zeros((len(self.circuitvoltagestates), len(self.circuitList) + nUnodes + len(self.Lcouplers)))
        Gxus = np.zeros((len(self.circuitvoltagestates), self.sIdx))
        
        Gxiu = np.zeros((len(self.circuitcurrentstates), self.nIdx))
        Gxii = np.zeros((len(self.circuitcurrentstates), len(self.circuitList)))
        Gxiz = np.zeros((len(self.circuitcurrentstates), nUnodes + len(self.Lcouplers)))
        Gxis = np.zeros((len(self.circuitcurrentstates), self.sIdx))
        
        Gxsz = np.zeros((len(self.functionstates), self.nIdx + len(self.circuitList) + nUnodes + len(self.Lcouplers)))
        Gxss = np.zeros((len(self.functionstates), self.sIdx))
        
        Mxy = np.zeros((len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates), len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources)))
        Mxx = np.eye(len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates))
        
        delulines = []
        
        for i, e in enumerate(self.circuitvoltagestates):
            if e in self.Uelements:
                Gxuu[i, e.nodeIdx[0]] = Gxuu[i, e.nodeIdx[0]] + 1
                Gxuu[i, e.nodeIdx[1]] = Gxuu[i, e.nodeIdx[1]] - 1
            else:
                delulines.append(i)
        for i, e in enumerate(self.circuitcurrentstates):
            Gxii[i, self.circuitList.index(e)] = 1
        for i, e in enumerate(self.functionstates):
            Gxss[i, e.output.id] = 1
            if e.output.id < 0:
                raise TypeError("TypeError: Non valide signal id")
        
        Gxu = np.block([[Gxuu, Gxuz, Gxus]])
        Gxu = np.delete(Gxu, delulines, axis=0)
        Mxy = np.delete(Mxy, delulines, axis=0)
        Mxx = np.delete(Mxx, delulines, axis=0)
        
        return [np.block([[Gxu], [Gxiu, Gxii, Gxiz, Gxis], [Gxsz, Gxss]]), Mxy, Mxx]
    
    def creatGr(self, nUnodes):
        """creats the Gr Matrix"""
        
        Gru = np.zeros((len(self.resistances), self.nIdx))
        Gri = np.zeros((len(self.resistances), len(self.circuitList)))
        Grz = np.zeros((len(self.resistances), nUnodes + len(self.Lcouplers) + self.sIdx))
        
        Mry = np.zeros((len(self.resistances), len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources)))
        Mrx = np.zeros((len(self.resistances), len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates)))
        
        for i, e in enumerate(self.resistances):
            Gru[i, e.nodeIdx[0]] = Gru[i, e.nodeIdx[0]] + e.value
            Gru[i, e.nodeIdx[1]] = Gru[i, e.nodeIdx[1]] - e.value
            Gri[i, self.circuitList.index(e)] = -1
            
        return [np.block([Gru, Gri, Grz]), Mry, Mrx]
    
    def creatGc(self, nUnodes, Tc):
        """creats the Gc Matrix"""
        
        Gcdu = np.zeros((len(self.circuitvoltagestates), self.nIdx))
        Gcdi = np.zeros((len(self.circuitvoltagestates), len(self.circuitList)))
        Gcdd = np.zeros((len(self.circuitvoltagestates), self.nIdx))
        Gcdz = np.zeros((len(self.circuitvoltagestates), len(self.Lcouplers) + self.sIdx))
        
        Gcuz = np.zeros((len(self.circuitvoltagesources), self.nIdx + len(self.circuitList)))
        Gcud = np.zeros((len(self.circuitvoltagesources), self.nIdx))
        Gcus = np.zeros((len(self.circuitvoltagesources), len(self.Lcouplers) + self.sIdx))
        
        Gcbz = np.zeros((len(self.UCnodes), self.nIdx + len(self.circuitList)))
        Gcbd = np.zeros((len(self.UCnodes), self.nIdx))
        Gcbs = np.zeros((len(self.UCnodes), len(self.Lcouplers) + self.sIdx))
        
        Mcy = np.zeros((len(self.circuitvoltagestates) + len(self.circuitvoltagesources) + len(self.UCnodes), len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources)))
        Mcx = np.zeros((len(self.circuitvoltagestates) + len(self.circuitvoltagesources) + len(self.UCnodes), len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates)))
        
        for i, e in enumerate(self.circuitvoltagestates):
            Gcdi[i, self.circuitList.index(e)] = -e.value
            Gcdd[i, e.nodeIdx[0]] = Gcdd[i, e.nodeIdx[0]] + 1
            Gcdd[i, e.nodeIdx[1]] = Gcdd[i, e.nodeIdx[1]] - 1
        for i, e in enumerate(self.circuitvoltagesources):
            Gcud[i, e.nodeIdx[0]] = Gcud[i, e.nodeIdx[0]] + 1
            Gcud[i, e.nodeIdx[1]] = Gcud[i, e.nodeIdx[1]] - 1
        for i, u in enumerate(self.UCnodes):
            Gcbd[i, u[0]] = 1
        
        Gcdd = np.matmul(Gcdd, Tc)
        Gcud = np.matmul(Gcud, Tc)
        Gcbd = np.matmul(Gcbd, Tc)
        
        return [np.block([[Gcdu, Gcdi, Gcdd, Gcdz], [Gcuz, Gcud, Gcus], [Gcbz, Gcbd, Gcbs]]), Mcy, Mcx]
    
    def creatGn(self, nUnodes):
        """creats the Gn Matrix"""
        
        Gni = np.zeros((self.nIdx, len(self.circuitList)))
        
        for i, e in enumerate(self.circuitList):
            Gni[e.nodeIdx[0] ,i] = Gni[e.nodeIdx[0] ,i] + 1
            Gni[e.nodeIdx[1] ,i] = Gni[e.nodeIdx[1] ,i] - 1
        LgroundNode = []
        # print(self.Lnodes)
        for l in self.Lnodes:
            LgroundNode.append(l[0])
            
        Lcouplers = copy.copy(self.Lcouplers)
        
        while len(Lcouplers) > 0:
            if Lcouplers[0].nodeIdx[0] in LgroundNode:
                LgroundNode.remove(Lcouplers[0].nodeIdx[0])
                Lcouplers.remove(Lcouplers[0].coupledElement)
                Lcouplers.remove(Lcouplers[0])
            elif Lcouplers[0].nodeIdx[1] in LgroundNode:
                LgroundNode.remove(Lcouplers[0].nodeIdx[1])
                Lcouplers.remove(Lcouplers[0].coupledElement)
                Lcouplers.remove(Lcouplers[0])
            elif Lcouplers[0].coupledElement.nodeIdx[0] in LgroundNode:
                LgroundNode.remove(Lcouplers[0].coupledElement.nodeIdx[0])
                Lcouplers.remove(Lcouplers[0].coupledElement)
                Lcouplers.remove(Lcouplers[0])
            elif Lcouplers[0].coupledElement.nodeIdx[1] in LgroundNode:
                LgroundNode.remove(Lcouplers[0].coupledElement.nodeIdx[1])
                Lcouplers.remove(Lcouplers[0].coupledElement)
                Lcouplers.remove(Lcouplers[0])
            else:
                raise RuntimeError("Error: not possible to find a node for couplers")
                
        Gni = np.delete(Gni, LgroundNode, axis=0)
        Gnu = np.zeros((self.nIdx - len(LgroundNode), self.nIdx))
        Gnz = np.zeros((self.nIdx - len(LgroundNode), nUnodes + len(self.Lcouplers) + self.sIdx))
        
        Mny = np.zeros((self.nIdx - len(LgroundNode), len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources)))
        Mnx = np.zeros((self.nIdx - len(LgroundNode), len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates)))
        
        return [np.block([Gnu, Gni, Gnz]), Mny, Mnx]
    
    def creatGb(self, nUnodes):
        """creats the Gb Matrix"""
        
        Gbu = np.zeros((len(self.groundNodes), self.nIdx))
        Gbz = np.zeros((len(self.groundNodes), len(self.circuitList) + nUnodes + len(self.Lcouplers) + self.sIdx))
        
        Mby = np.zeros((len(self.groundNodes), len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources)))
        Mbx = np.zeros((len(self.groundNodes), len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates)))
        
        for i, e in enumerate(self.groundNodes):
            Gbu[i, e] = 1
            
        return [np.block([Gbu, Gbz]), Mby, Mbx]
    
    def creatGt(self, nUnodes, Tc):
        """creat Gt"""
        
        Gtuu = np.zeros((len(self.couplers), self.nIdx))
        Gtuz = np.zeros((len(self.couplers), len(self.circuitList) + nUnodes + len(self.Lcouplers) + self.sIdx))
        
        Gtiu = np.zeros((len(self.couplers), self.nIdx))
        Gtii = np.zeros((len(self.couplers), len(self.circuitList)))
        Gtiz = np.zeros((len(self.couplers), nUnodes + len(self.Lcouplers) + self.sIdx))
        
        Gtdz = np.zeros((len(self.couplers), self.nIdx + len(self.circuitList)))
        Gtdd = np.zeros((len(self.couplers), self.nIdx))
        Gtds = np.zeros((len(self.couplers), len(self.Lcouplers) + self.sIdx))
        
        Gtcz = np.zeros((len(self.Lcouplers), self.nIdx + len(self.circuitList) + nUnodes))
        Gtcc = np.zeros((len(self.Lcouplers), len(self.Lcouplers)))
        Gtcs = np.zeros((len(self.Lcouplers), self.sIdx))
        
        Mty = np.zeros((3*len(self.couplers) + len(self.Lcouplers), len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources)))
        Mtx = np.zeros((3*len(self.couplers) + len(self.Lcouplers), len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates)))
        
        coupledelements = []
        
        for i, e in enumerate(self.couplers):
            if e in self.Uelements or (not e in self.Ccouplers and not e in coupledelements):
                Gtuu[i, e.nodeIdx[0]] = Gtuu[i, e.nodeIdx[0]] + 1/e.value
                Gtuu[i, e.nodeIdx[1]] = Gtuu[i, e.nodeIdx[1]] - 1/e.value
                Gtuu[i, e.coupledElement.nodeIdx[0]] = Gtuu[i, e.coupledElement.nodeIdx[0]] - 1/e.coupledElement.value
                Gtuu[i, e.coupledElement.nodeIdx[1]] = Gtuu[i, e.coupledElement.nodeIdx[1]] + 1/e.coupledElement.value
                
            if not e in coupledelements:    
                # if not e in self.Lcouplers:
                Gtii[i, self.circuitList.index(e)] = Gtii[i, self.circuitList.index(e)] + e.value
                Gtii[i, self.circuitList.index(e.coupledElement)] = Gtii[i, self.circuitList.index(e.coupledElement)] + e.coupledElement.value
                
                if e in self.Ccouplers:
                    Gtdd[i, e.nodeIdx[0]] = Gtdd[i, e.nodeIdx[0]] + 1/e.value
                    Gtdd[i, e.nodeIdx[1]] = Gtdd[i, e.nodeIdx[1]] - 1/e.value
                    Gtdd[i, e.coupledElement.nodeIdx[0]] = Gtdd[i, e.coupledElement.nodeIdx[0]] - 1/e.coupledElement.value
                    Gtdd[i, e.coupledElement.nodeIdx[1]] = Gtdd[i, e.coupledElement.nodeIdx[1]] + 1/e.coupledElement.value
                
                if e in self.Lcouplers:
                    Gtcc[i, self.Lcouplers.index(e)] = Gtcc[i, self.Lcouplers.index(e)] + e.value
                    Gtcc[i, self.Lcouplers.index(e.coupledElement)] = Gtcc[i, self.Lcouplers.index(e.coupledElement)] + e.coupledElement.value
                
                coupledelements.append(e.coupledElement)
        
        # coupledelements = []
                
        # for i, e in enumerate(self.Lcouplers):
        #     if not e in coupledelements:
                
        #         Gtcc[i, self.Lcouplers.index(e)] = Gtcc[i, self.Lcouplers.index(e)] + e.value
        #         Gtcc[i, self.Lcouplers.index(e.coupledElement)] = Gtcc[i, self.Lcouplers.index(e.coupledElement)] + e.coupledElement.value
                
        #         coupledelements.append(e.coupledElement)
        Gtdd = np.matmul(Gtdd, Tc)
        
        Gt = np.block([[Gtuu, Gtuz], [Gtiu, Gtii ,Gtiz], [Gtdz, Gtdd, Gtds], [Gtcz, Gtcc, Gtcs]])
        delline = np.where(np.all(Gt == 0, axis=1))
        Gt = Gt[~np.all(Gt == 0, axis=1)]
        Mty = np.delete(Mty, delline, axis=0)
        Mtx = np.delete(Mtx, delline, axis=0)
        
        return [Gt, Mty, Mtx]
    
    def creatGl(self, nUnodes):
        """creats Gl matrix"""
        
        Glu = np.zeros((self.nIdx, self.nIdx))
        Glz = np.zeros((max(len(self.Lnodes) - len(self.groundNodes), 0), len(self.circuitList) + nUnodes))
        Glc = np.zeros((self.nIdx, len(self.Lcouplers)))
        Gls = np.zeros((max(len(self.Lnodes) - len(self.groundNodes), 0), self.sIdx))
        
        Mly = np.zeros((max(len(self.Lnodes) - len(self.groundNodes), 0), len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources)))
        Mlx = np.zeros((max(len(self.Lnodes) - len(self.groundNodes), 0), len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates)))
        
        Tl = np.zeros((len(self.Lnodes), self.nIdx))
        
        for e in self.circuitcurrentstates:
            Glu[e.nodeIdx[0], e.nodeIdx[0]] = Glu[e.nodeIdx[0], e.nodeIdx[0]] + e.value
            Glu[e.nodeIdx[0], e.nodeIdx[1]] = Glu[e.nodeIdx[0], e.nodeIdx[1]] - e.value
            Glu[e.nodeIdx[1], e.nodeIdx[1]] = Glu[e.nodeIdx[1], e.nodeIdx[1]] + e.value
            Glu[e.nodeIdx[1], e.nodeIdx[0]] = Glu[e.nodeIdx[1], e.nodeIdx[0]] - e.value
            
        for i, e in enumerate(self.Lcouplers):
            Glc[e.nodeIdx[0], i] = Glc[e.nodeIdx[0], i] + 1
            Glc[e.nodeIdx[1], i] = Glc[e.nodeIdx[1], i] - 1
        
        for i, l in enumerate(self.Lnodes):
            node = np.zeros(self.nIdx)
            for n in l:
                if n in self.groundNodes:
                    node = np.zeros(self.nIdx)
                    break
                node[n] = 1
            Tl[i] = node
            
        Tl = Tl[~np.all(Tl == 0, axis=1)]
        Glu = np.matmul(Tl, Glu)
        Glc = np.matmul(Tl, Glc)
        
        return [np.block([Glu, Glz, Glc, Gls]), Mly, Mlx]
    
    def creatGg(self, nUnodes):
        """creats the Gq matrix"""
        
        Ggz = np.zeros((len(self.functiongains), self.nIdx + len(self.circuitList) + nUnodes + len(self.Lcouplers)))
        Ggs = np.zeros((len(self.functiongains), self.sIdx))
        
        Mgy = np.zeros((len(self.functiongains), len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources)))
        Mgx = np.zeros((len(self.functiongains), len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates)))
        
        for i, e in enumerate(self.functiongains):
            Ggs[i, e.input.id] = Ggs[i, e.input.id] + e.value
            Ggs[i, e.output.id] = Ggs[i, e.output.id] - 1
            if e.input.id < 0 or e.output.id < 0:
                raise TypeError("TypeError: Non valide signal id")
            
        return [np.block([Ggz, Ggs]), Mgy, Mgx]
    
    def creatGa(self, nUnodes):
        """creats the Ga matrix"""
        
        Gaz = np.zeros((len(self.functionadders), self.nIdx + len(self.circuitList) + nUnodes + len(self.Lcouplers)))
        Gas = np.zeros((len(self.functionadders), self.sIdx))
        
        May = np.zeros((len(self.functionadders), len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources)))
        Max = np.zeros((len(self.functionadders), len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates)))
        
        for i, e in enumerate(self.functionadders):
            Gas[i, e.input.id] = Gas[i, e.input.id] + 1
            Gas[i, e.output.id] = Gas[i, e.output.id] - 1
            if e.input.id < 0 or e.output.id < 0:
                raise TypeError("TypeError: Non valide signal id")
            for s in e.value:
                Gas[i, s.id] = Gas[i, s.id] + 1
                if s.id < 0:
                    raise TypeError("TypeError: Non valide signal id")
            
        return [np.block([Gaz, Gas]), May, Max]
    
    def creatGm(self, nUnodes):
        """creats Gm"""
        
        vmeters = []
        ameters = []
        for e in self.circuitcurrentsources:
            if type(e.value2) == FunctionSignal:
                vmeters.append(e)
        for e in self.circuitvoltagesources:
            if type(e.value2) == FunctionSignal:
                ameters.append(e)
        Gmuu = np.zeros((len(vmeters), self.nIdx))
        Gmuz = np.zeros((len(vmeters), len(self.circuitList) + nUnodes + len(self.Lcouplers)))
        Gmus = np.zeros((len(vmeters), self.sIdx))
        
        Gmiu = np.zeros((len(ameters), self.nIdx))
        Gmii = np.zeros((len(ameters), len(self.circuitList)))
        Gmiz = np.zeros((len(ameters), nUnodes + len(self.Lcouplers)))
        Gmis = np.zeros((len(ameters), self.sIdx))
        
        Mmy = np.zeros((len(vmeters) + len(ameters), len(self.circuitvoltagesources) + len(self.circuitcurrentsources) + len(self.functionsources)))
        Mmx = np.zeros((len(vmeters) + len(ameters), len(self.circuitvoltagestates) + len(self.circuitcurrentstates) + len(self.functionstates)))
        
        for i, e in enumerate(vmeters):
            Gmuu[i, e.nodeIdx[0]] = 1
            Gmuu[i, e.nodeIdx[1]] = Gmuu[i, e.nodeIdx[1]] - 1
            Gmus[i, e.value2.id] = -1
        for i, e in enumerate(ameters):
            Gmii[i, self.circuitList.index(e)] = 1
            Gmis[i, e.value2.id] = -1
            
        return [np.block([[Gmuu, Gmuz, Gmus], [Gmiu, Gmii, Gmiz, Gmis]]), Mmy, Mmx]
    
    def creatMu(self, nUnodes):
        """returns matrix Mu"""
        
        Muu = np.zeros((len(self.circuitList), self.nIdx))
        Muz = np.zeros((len(self.circuitList), len(self.circuitList) + nUnodes + len(self.Lcouplers) + self.sIdx))
        
        for i, e in enumerate(self.circuitList):
            Muu[i, e.nodeIdx[0]] = 1
            Muu[i, e.nodeIdx[1]] = -1
            
        return np.block([Muu, Muz])
    
    def creatMi(self, nUnodes):
        """creat the matrix Mi"""
        
        Miu = np.zeros((len(self.circuitList), self.nIdx))
        Mii = np.eye(len(self.circuitList))
        Miz = np.zeros((len(self.circuitList), nUnodes + len(self.Lcouplers) + self.sIdx))
        
        return np.block([Miu, Mii, Miz])
    
    def creatMs(self, nUnodes):
        """creats the Matrix Ms"""
        
        Msz = np.zeros((self.sIdx, self.nIdx + len(self.circuitList) + nUnodes + len(self.Lcouplers)))
        Mss = np.eye(self.sIdx)
        
        return np.block([Msz, Mss])
    
    def creatinvG(self, G):
        """creats the invG Matrix"""
        
        self.systemOutput.updateinfo("creat invG")
        try:
            snot = "not "*(G.shape[0] != G.shape[1])
            self.logger.addlog(f"Info: G is {snot}square")
            if G.shape[0] == G.shape[1]:
                invG = np.linalg.inv(G)
            else:
                u, s, v = np.linalg.svd(G)
                invG = np.matmul(np.matmul(np.transpose(v), np.diag(1/s)), np.transpose(u)[:len(s)])
                self.logger.addlog("Warning: Single value decomposition used reduced accuracy")
                print("Warning: Single value decomposition used reduced accuracy")
        except Exception as e:
            self.logger.addlog(f"Error: {e}")
            self.logger.addlog("Error: Inversion of G Failed")
            self.systemOutput.failTask()
            raise RuntimeError("Error: Inversion of G Failed")

        # invG = np.matmul(np.matmul(np.transpose(v), np.diag(1/s)), np.transpose(u)[:len(s)])

        if len(invG) > 0:
            self.error = max(linalg.norm(np.matmul(invG, G) - np.eye(len(invG)), 2), self.error)
            self.logger.addlog(f"Info: Simulatiuon error is {self.error}")
        else:
            self.error = 0
            self.logger.addlog("Warning: not possible to calculate an Error")
        return invG
                
    
    def matmulmatmul(self, m1, m2, m3):
        """creats the uinvGy matrix"""
        
        return np.matmul(np.matmul(m1, m2), m3)
        
    def creatAself(self, Au, Ai, As):
        """creats the Aself matrix"""
        
        self.systemOutput.updateinfo("creat Aself")
        
        self.Aself = np.matmul(Au, self.uinvGx) + np.matmul(Ai, self.iinvGx) + np.matmul(As, self.sinvGx)
        
    def creatAsource(self, Au, Ai, As):
        """creats the Asource matrix"""
        
        self.systemOutput.updateinfo("creat Asource")
        
        self.Asource =  np.matmul(Au, self.uinvGy) + np.matmul(Ai, self.iinvGy) + np.matmul(As, self.sinvGy)
        
### run functions

    def dxdt(self, t, x):
        """returns the dy/dt*y for time t"""
        
        if t > self.tnextmajor and not self.denseoutput == None:
            _x = self.denseoutput(self.tnextmajor)
            
            u, i, s, _ = self.calcUI(self.denseoutput(self.tnextmajor), self.tnextmajor, major=True)
            
            for j, e in enumerate(self.circuitList):
                e.addVIt(u[j], i[j], self.tnextmajor, True)
            for e in self.functionList:
                e.output.addDt(s[e.output.id], self.tnextmajor, True)
        
        major = t == self.tnextmajor
        _, _, _, y = self.calcUI(x, t, major=major)
        
        
        
        if t > self.tnextmajor and not self.denseoutput == None:
            for j, e in enumerate(self.circuitList):
                e.removeVIt(True)
            for f in self.functionList:
                f.output.removeDt(True)
        
        return np.matmul(self.Aself, x) + np.matmul(self.Asource, y) 
    
    def jac(self, t, x):
        """returns the Aself"""
        
        return self.Aself
    
    def calcUI(self, x, t, major=False):
        """calcs U and I and returns them"""
        
        y = []
        for e in self.circuitvoltagesources:
            if not type(e.value) == FunctionSignal:
                y.append(e.value)
            else:
                y.append(0)
        for e in self.circuitcurrentsources:
            if not type(e.value) == FunctionSignal:
                y.append(e.value)
            else:
                y.append(0)
        for e in self.functionsources:
            y.append(e.value)
        y = np.array(y)
        u = []
        i = []
        s = []
        
        iterator = 0
        
        while True:
            iterator = iterator + 1
            if iterator > 10:
                if self.parameters.ignoreloop:
                    self.logger.addlog("Warning: loop ignored in electrical simulation")
                    return u, i, s, y
                self.logger.addlog("Error: Loop detected in electrical simulation")
                raise ZeroDivisionError("Error: Function failed")
                return
            
            u = np.matmul(self.uinvGx, x) + np.matmul(self.uinvGy, y)
            u = np.array(u)
            i = np.matmul(self.iinvGx, x) + np.matmul(self.iinvGy, y)
            i = np.array(i)
            s = np.matmul(self.sinvGx, x) + np.matmul(self.sinvGy, y)
            s = np.array(s)
            
            for j, e in enumerate(self.circuitList):
                e.addVIt(u[j], i[j], t, major)
            for j, e in enumerate(self.functionList):
                e.output.addDt(s[e.output.id], t, major)
            # if len(self.t) == 0:
            #     stepfunctionreturn = self.stepfunctionList(t, self.parameters.delta)
            # else:
            #      stepfunctionreturn = self.stepfunctionList(t, t - self.t[-1])
            # if not stepfunctionreturn == "":
            #     self.logger.addlog("Error: function simulation failed")
            #     self.systemOutput.failTask()
            #     raise RuntimeError("Error: function simulation failed")
            
            update = 0
            for ue in self.updateList:
                try:
                    update = max(update, ue.update(major, t))
                except Exception as ex:
                    ue.fail = True
                    self.logger.addlog(f"Error: {ex}")
                    raise RuntimeError(f"RuntimeError: error while updating Element {ue.name}")
                
            if update == 0:
                for j, e in enumerate(self.circuitList):
                    e.removeVIt(major)
                for f in self.functionList:
                    f.output.removeDt(major)
                break
            elif update > 1:
                try:
                    self.update()
                except Exception as ex:
                    ue.fail = True
                    self.logger.addlog(f"Error: {ex}")
                    raise RuntimeError("RuntimeError: error while updating model")
            self.systemOutput.updateinfo("running...")
            yold = y
            y = []
            for e in self.circuitvoltagesources:
                if not type(e.value) == FunctionSignal:
                    y.append(e.value)
                else:
                    y.append(0)
            for e in self.circuitcurrentsources:
                if not type(e.value) == FunctionSignal:
                    y.append(e.value)
                else:
                    y.append(0)
            for e in self.functionsources:
                y.append(e.value)
            y = np.array(y)
            for j, e in enumerate(self.circuitList):
                e.removeVIt(major)
            for f in self.functionList:
                f.output.removeDt(major)
            if np.all(yold == y) and update == 1:
                break
        
        return u, i, s, y
    
    def finished(self):
        """cleans up the simulation"""
        
        for u in self.updateList:
            u.setpublicvariables()
        self.logger.addlog("Info: Simulation finished and cleaned")
        
SOLVERS = [ode.Radau, ode.LSODA, ode.BDF]     
                

"""prints the information about this file"""
if PRINT_INFO:
    printinfo()