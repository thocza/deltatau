![image](icons/dtIcon_small.png)

# Delta-Tau

**_dynamic electric circuit simulation_**

README Version: 0.2.

|        |            |
|--------|------------|
| Issue: | 2024-10-27 |
| Author: | Thomas Czaja |
| Δτ Version: | 0.12.0 |

**0.12.0 revmoved legecy code and streamlined the elements-classes. This makes it possible to give the desing of elementes more freedome. Unfortunatly this also means circuit saved under version 0.11.1 or older cannot be opend anymore**


**Table of content:**

[1. Introduction](#_Toc1)

[2. Start](#_Toc2)

[3. Main Window](#_Toc3)

[3.1. Tool-Bar](#_Toc4)
    
[3.1.1. File operations](#_Toc5)
        
[3.1.2. Help/Info](#_Toc6)
        
[3.1.3. Design operations](#_Toc7)
        
[3.1.4. Simulation properties](#_Toc8)
        
[3.1.5. Zoom](#_Toc9)
        
[3.1.6. Plot window](#_Toc10)
        
[3.1.7. Print CAD window](#_Toc11)
        
[3.2. Cad surface](#_Toc12)
    
[3.3. Status bar](#_Toc13)
    
[3.4. Simulation results-list](#_Toc14)
    
[3.5. Library](#_Toc15)
    
[3.5.1. Subcircuits](#_Toc16)
        
[3.5.2. Non-functional circuit elements](#_Toc17)

[3.5.2.1. Labels](#_Toc18)
            
[3.5.3. Electric elements](#_Toc19)
        
[3.5.3.1. Passive elements](#_Toc20)

[3.5.3.2. Sources](#_Toc21)
            
[3.5.3.3. Power Seiconductors](#_Toc22)
            
[3.5.3.3.1. Real Semiconductors](#_Toc23)
                
[3.5.3.4. Meters](#_Toc24)
            
[3.5.3.5. Transformer](#_Toc25)
            
[3.5.4. Non functional function elements](#_Toc26)
        
[3.5.4.1. Labels](#_Toc27)
            
[3.5.5. Functional elements](#_Toc28)
        
[3.5.5.1. Sources](#_Toc29)
            
[3.5.5.2. Math elements](#_Toc30)
            
[3.5.5.2.1. Math function elements](#_Toc31)
                
[3.5.5.2.2. Trigonometric elements](#_Toc32)

[3.5.5.3. Functions & Tables](#_Toc33)
                
[3.5.5.4. Control elements](#_Toc34)
            
[3.5.5.5. Routing](#_Toc35)
            
[3.5.5.5.1. Power electronic modulators](#_Toc36)
                
[3.5.5.6. Transformations](#_Toc37)
            
[3.5.5.7. Logic elements](#_Toc38)
            
[3.5.5.8. Time domain filter elements](#_Toc39)
            
[3.5.5.9. Delays](#_Toc40)
   
[3.5.5.10. Z domain](#_Toc41)
            
[3.5.5.11. Analog-digital](#_Toc42)
            
[3.5.5.12. Graphical output](#_Toc43)
            
[4. Plot-window](#_Toc44)

[4.1. Plot-property-window](#_Toc45)

[5. Background](#_Toc46)

[5.1. Roadmap](#_Toc47)

[5.2. How Δτ works](#Toc_48)


# 1. Introduction

Δτ is a python based simulation software. It allows to simulate electric-circuits and mathematical functions. It uses space-time states and numerical integration methods to achieve this. This document describes the functionality of the graphical user interface (GUI).

Δτ uses a similar simulating-approach to [plecs](https://www.plexim.com/de/products/plecs) from [Plexim](https://www.plexim.com/de/home) and [Simulink](https://www.mathworks.com/products/simulink.html) from [Mathworks](https://www.mathworks.com/), but does not have the same scope of functionalities. Δτ gives the deveoper an additional cost-free tool to [LTSpice](https://www.analog.com/en/design-center/design-tools-and-calculators/ltspice-simulator.html) to simulate more complex control structures.

# 2. Start

Currently the program has to be downloaded from the [Git](https://gitlab.com/thocza/deltatau) repository. It can be executed in a python console by running the main file or the Batch-file "deltatau.bat" has to be executed. The following libraries have be installed beforehand:

- Math
- Numpy
- PyQt5
- Copy
- Pickle

After executing the following window should be visible. This is the main window of the programm and looks as shown in Figure 1.

![image](_README_/Window.png)

**Figure 1: deltaTau main window**

# 3. Main Window

The main window as shown in Figure 1 can be separated into five sections. These are:

- Tool-bar at the top

- Computer-aided-design (Cad) surface

- Status-bar at the bottom

- Simulation-output-list on the right-hand side

- Library at the left-hand side of the window

## 3.1. Tool-Bar

The Tool-Bar is separated into seven sections as can be seen in Figure 2. These six sections are used for

- File operations
- Help/Info
- Design operations
- Simulation properties
- Zoom
- Plot window 
- Print CAD-Window

![image](_README_/Toolbar.png)

**Figure 2: sections of Tool-bar**

### 3.1.1. File operations

There are three file operations available.

|        |            |
|--------|------------|
| ![image](icons/actionicons/newfile.svg) | Creates a new file. This opens a new blank Cad-surface |
| ![image](icons/actionicons/savefile.svg) | Saves the circuit in the currently open Cad-surface<br>Tip: Str+S also saves the circuit |
| ![image](icons/actionicons/openfile.svg) | Opens a file browser and a \*.dt file can be loaded |

### 3.1.2. Help/Info

There are three operations available:

|        |            |
|--------|------------|
| ![image](icons/actionicons/tool.svg) | Opens the properties window |
| ![image](icons/actionicons/help.svg) | Opens the user manual. |
| ![image](icons/actionicons/info.svg) | Opens window with information about the state of the program like version numbers etc. |

### 3.1.3. Design operations

|        |            |
|--------|------------|
| ![image](icons/actionicons/undo.svg) | Undo |
| ![image](icons/actionicons/redo.svg) | Redo |
| ![image](icons/actionicons/delete.svg) | Toggles the delete-mode of the Cad-surface. The delete-mode is explained in chapter ![3.2.](#_Toc12) |

The auto-remove checkbox enables the auto-remove of elements when copying. This function is described in greater detail in chapter ![3.2.](#_Toc12)

### 3.1.4. Simulation properties

|        |            |
|--------|------------|
| ![image](icons/actionicons/play.svg) | A simulation can be started, when pressing the play button |
| ![image](icons/actionicons/stop.svg) | To interuppt and end the simulation the stop button can be pressed |
| ![image](icons/actionicons/cancel.svg) | When the simulation reaches a stop-event, the cancel-button appears. By pressing it the simulation-data from the last simulation will be removed and a new simulation can be started. |

In the box _time[s]_ the simulation time can be set. The time is in seconds and has to be positive.

In de box _delta[s]_ the biggest time-step can be set. The time-step is in seconds and has to be positive.

Currently, there are three algorithms in the drop-down menu available:

- [Radau](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.Radau.html)
- [LSODA](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.LSODA.html#scipy.integrate.LSODA)
- [BDF](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.BDF.html#scipy.integrate.BDF)

### 3.1.5. Zoom

Shows the zoom-factor of the current Cad-surface. The factor can be set manually in the text-box.

|        |            |
|--------|------------|
| 50 % | Smallest zoom-factor. The Cad-surface is zoomed in. |
| 100 % | Standard zoom-factor |
| 150 % | Biggest zoom-factor. The Cad-surface is zoomed out. |

### 3.1.6. Plot window

Opens a blank Plot-window as shown in chapter ![4](#_Toc43).

### 3.1.7. Print CAD window

Circuit that is visible in the CAD window can be saved as an ".png" image

## 3.2. Cad surface

![image](_README_/cadexample.png)

**Figure 3: Example of a circuit**

In the Cad surface area the user draws the circuit that should be simulated.

|        |            |
|--------|------------|
| ![image](Manual/Icons/leftclick.png) | By pressing the left mouse button and dragging the surface can be moved relative to the mouse |


|        |            |
|--------|------------|
| ![image](_README_/floatingelement.png) | A floating element |


|        |            |
|--------|------------|
| ![image](Manual/Icons/leftclick.png) | Components can be placed after a component was chosen in the library (see ![3.5](#_Toc15)). The element can be selected be left-clicking on it. Then the element hovers at the position of the cursor in the Cad window. The element can be placed as often as wished by left-clicking.|
| ![image](Manual/Icons/rightclick.png) | By right-clicking the element can be unselected. Otherwise by pressing _DEL_.|
| ![image](Manual/Icons/wheel.png) | The element can be rotated by scrolling the mouse-wheel, or by pressing the key combination _Ctrl + R_ or _Ctrl + L._ |

The colour-code of the elements is explained below.

**Table 3-1: colour-description**

| Color of element | Meaning of Color|
|------------------|-----------------|
|white| Element is floating an can be placed |
|red| Floating element: It has a collision with another element and cannot be placed.<br>Placed element: Simulation error of the element (see ![3.4](#_Toc14)) |
|turquoise| Electrical circuit element |
|green| Functional element |
|yellow| Subcircuit |
|violet| Marked element |
|blue| Element can be deleted by left-clicking on it |

|  |  |
| --- | --- |
| ![image](_README_/opencircuit.png) | Open Circuit connection |
| ![image](_README_/openfunction.png) | Open Function connection |

|  |  |
|----|-----|
| ![image](Manual/Icons/leftclick.png) | By left-clicking on the end of a circuit connection or a signal vector a connection can be added.|
| ![image](Manual/Icons/leftclick.png)| By left-clicking the connection will be placed. If the new end hits a connection or another open end, the end will be closed. Otherwise a new connection will be opened automatically.|
| ![image](Manual/Icons/rightclick.png) | The floating connection will be removed|
| ![image](Manual/Icons/leftclick.png) | By holding _SHIFT_ pressed and left-clicking on a closed node a new connection can be created |

|    |   |
| --- | --- |
| ![image](_README_/circuitelement.png) | Circuit Element |
| ![image](_README_/functionelement.png) | Function Element |
| ![image](_README_/subcircuit.png) | Subcircuit |

|     |    |
|----|-----|
| ![image](Manual/Icons/leftclick.png) | By double left-clicking on an element the property window will open (If it is a functional element and the simulation results are still loaded, to open the property window of the element hold _SHIFT_ while double-clicking)|
| ![image](Manual/Icons/leftclick.png) | By right-clicking on the element it will be copied|
| ![image](Manual/Icons/wheel.png) | By scrolling it can be zoomed in or out of the window|
| ![image](Manual/Icons/leftclick.png) | By holding and dragging the surface can be moved around |

![image](_README_/deletemode.png)

**Figure 4: Circuit in delete-mode**

By pressing _DEL_ the delete-mode can be toggled.

|    |   |
| --- | --- |
| ![image](Manual/Icons/leftclick.png) | By left-clicking on the element it will be removed |

|    |   |
| --- | --- |
| ![image](_README_/markedelement.png) | Marked element |

|    |   |
| --- | --- |
| ![image](Manual/Icons/leftclick.png) | By left-clicking on an element it will be marked. When marking the next element the marked elements won't be unmarked.|
| ![image](Manual/Icons/leftclick.png) | By left-clicking on a marked element it will be unmarked.|
| ![image](Manual/Icons/leftclick.png) | By double-clicking on a free area all marked elements will be unmarked|
| ![image](Manual/Icons/leftclick.png) | By pressing right mouse button and dragging all elements in the ahown area will be marked|
| _DEL_ | By pressing _DEL_ all marked elements will be removed.|

## 3.3. Status bar

The status bar displays information about the status of the simulation

## 3.4. Simulation results-list

When the simulation stops, all public simulation variables will be displayed in a list. By double left-clicking on a variable the variable will be shown in a plot-window.

|    |   |
| --- | --- |
| ![image](icons/actionicons/plotwindow.svg) | Will open a new plot-window to display variables |
| ![image](icons/actionicons/plus.svg) | Will add a new plot-surface to the plot-window (see ![4](#_Toc43)) |
| ![image](icons/actionicons/clock.svg) | Will open the log-window of the simulation. The log-window shows all the messages generated by the simulation |

**Remark:** The plot of the variable will always be added to the last plot-surface that was opened.

## 3.5. Library

The Library is divided into the sub-libraries that are available for the user. These are:

- Subcircuits
- Non-functional circuit elements
- Electric elements
- Non-functional function elements
- Functional elements

|   |   |
| --- | --- |
| ![image](Manual/Icons/leftclick.png) | By clicking with the left mouse-button on one of the elements and then moving the mouse over the Cad-surface the element appears on the cad-surface. |


**Remark: Drag and drop does not work!**

### 3.5.1. Subcircuits
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/nonfunctionalLib/subempty.svg)<br>Subcircuit empty | Empty Subcircuit |- Name <br>- Lock subcircuit. The Sub-circuit cannot be opend by double-clicking on it. Hold _SHIFT_ and then double click to open the subcircuit <br>- Save subcircuit as circuit as a ".dt" file <br>- Load a ".dt" file as a subcircuit<br>*Remark* to open the parameter-window of the unlocked subcircuit hold _SHIFT_ and double-click on the subcircuit|

### 3.5.2. Non-functional circuit elements
#### 3.5.2.1. Labels
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/nonfunctionalLib/Clabel.svg)<br>**Label** | Labels a circuit-rout or -node |- Name |
| ![image](library/icons/nonfunctionalLib/Cnode.svg)<br>**Node** | Connects two nodes or routs. By coping the node (right-click on the Node) both nodes are connected. By holding "SHIFT" while right-clicking on the node or holding "SHIFT" while placing the node(left-click) a new entity will be created that has no connection to the other Nodes<br>**Remark 1: Two Nodes with the same name must not be connected**<br>**Remark 2: By marking the Node (right-click on the Node) a line to all connected Nodes will be visible** | - Name |
| ![image](library/icons/nonfunctionalLib/Cio.svg)<br>**In/Out** | Creates an In/Out-put for a Subcircuit | - Name |

### 3.5.3. Electric elements

#### 3.5.3.1. Passive elements
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/electricLib/resistor.svg)<br>**Resistor**<br>continuous | Resistor |- Name<br>- Value of the resistor in Ω |
| ![image](library/icons/electricLib/inductance.svg)<br>**Inductance**<br>continuous | Inductance |- Name<br>- Inductance in H<br>- Current in inductance at the start of the simulation in A |
| ![image](library/icons/electricLib/capacitance.svg)<br>**Capacitance**<br>continuous | Capacitance |- Name<br>- Capacitance in F<br>- Voltage in capacitance at the start of the simulation in V |
| ![image](library/icons/electricLib/variableresistor.svg)<br>**Variable Resistor**<br>continuous | Variable resistor with function input. Function input must be bigger than 0 |- Name |

#### 3.5.3.2. Sources
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/electricLib/constantvoltagesource.svg)<br>**Constant-voltage-source**<br>discontinuous | Constant-voltage-source |- Name<br>- Voltage in V |
| ![image](library/icons/electricLib/sinvoltagesource.svg)<br>**Sin-voltage-source**<br>discontinuous | Sin-voltage-source |- Name<br>- Amplitude in V<br>- Frequency in Hz<br>- Phase-angle in ° |
| ![image](library/icons/electricLib/constantcurrentsource.svg)<br>**Constant-current-source**<br>discontinuous | Constant-current-source |- Name<br>- Current in A |
| ![image](library/icons/electricLib/sincurrentsource.svg)<br>**Sin-current-source**<br>discontinuous | Sin-current-source |- Name<br>- Amplitude in A<br>- Frequency in Hz<br>- Phase-angle in ° |
| ![image](library/icons/electricLib/controlledvoltagesource.svg)<br>**Controled-voltage-source**<br>discontinuous | Controled-voltage-source<br>Voltage of source is controled by function-input. |- Name |
| ![image](library/icons/electricLib/controlledcurrentsource.svg)<br>**Controled-current-source**<br>discontinuous | Controled-current-source<br>Current of source is controled by function input | - Name |
| ![image](library/icons/electricLib/3phvoltagesource.svg)<br>**3-phase-voltage source**<br>discontinuous | 3-phase-voltage-source |- Name<br>- Amplitude in V<br>- Frequency in Hz<br>- Phase-angle in ° |

#### 3.5.3.3. Power Seiconductors
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/electricLib/diod.svg)<br>**Diod**<br>continuous | Diod |- Name<br>- Turn-on-voltage in V<br>- On-resistance in Ω<br>- Off-resistance in Ω |
| ![image](library/icons/electricLib/switch.svg)<br>**Switch**<br>continuous | Switch |- Name<br>- On-resistance in Ω<br>- Off-resistance in Ω |
| ![image](library/icons/electricLib/idealmosfet.svg)<br>**Ideal MOSFET**<br>continuous | MOSFET can be turned on and off by function-input. In contrast to the IGBT the MOSFET has not Forward-Voltage-Drop. |- Name<br>- On-resistance in Ω<br>- Off-resistance in Ω|
| ![image](library/icons/electricLib/idealmosfetanddiod.svg)<br>**Ideal MOSFET with Diod**<br>continuous | MOSFET with reverse conducting Diod. |- Name<br>- On-resistance of MOSFET in Ω<br>- Off-resistamce of MOSFET in Ω<br>- On-resistamce of Diod in Ω<br>- Off-resistance of Diod in Ω |
| ![image](library/icons/electricLib/idealnigbt.svg)<br>**Ideal-IGBT**<br>continuous | IGBT can be turned on and off with function-input |- Name<br>- On-resistance in Ω<br>- Off-resistance in Ω<br>- Turn-on-voltage in V|
| ![image](library/icons/electricLib/idealthyristor.svg)<br>**Ideal-Tyhristor**<br>continuous | Thyristor can be turdned on with function input |- Name<br>- On-resistance in Ω<br>- Off-resistance in Ω<br>- Turn-on-voltage in V|
| ![image](library/icons/electricLib/zenerdiod.svg)<br>**Zenerdiod**<br>continuous | Zenerdiod |- Name<br>- Forward-turn-on-voltage in V<br>- Forward-on-resistance in Ω<br>- Backward-on-voltage in V<br>- Backward-on-resistance in Ω<br>- Off-resistance in Ω|
| ![image](library/icons/electricLib/idealnigbtanddiod.svg)<br>**ideal IGBT with Diod**<br>continuous | IGBT with reverse-diod |- Name<br>- On-resistance IGBT in Ω<br>- Off-resistance IGBT in Ω<br>- Turn-on-voltage IGBT in V<br>- On-resistance Diod in Ω<br>- Off-resistance Diod in Ω<br>- Turn-on-voltage Diod in V|

##### 3.5.3.3.1 Real Semiconductors
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/electricLib/idealmosfet.svg)<br>**Real MOSFET**<br>continuous | MOSFET is turned on by a gate-source voltage. MOSFET can be either non-conducting, in ohmic-phase or saturated. If gate-source capacitance-value is 0 MOSFET model has no capacitance and can be directly operated by a voltage source. |- Name<br>- [Roff, Ron, Rsat]<br>  - Roff is Off-resistance between Drain and Source<br>  - Ron is the On resistance between Draina nd Source during ohmic operation<br>  - Rsat ist a virtual resistance which emulates the change in current during saturation on change of the Drain-Source-voltage<br>- [Vgs-min, Vgs-max]<br>  - Vgs-min is the minimal voltage needed to turn the MOSFET on<br>  - Vgs-max is the maximal gate-source voltage<br>- Gain during saturation<br>- Gate-source capacitance|

##### 3.5.3.3.2 Semiconductor-Modules
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/electricLib/rectifier.svg)<br>**Rectifier**<br>continuous | Full-bridge rectifier|- Name<br>- Turn-on-voltage in V<br>- On-resistance in Ω<br>- Off-resistance in Ω |


#### 3.5.3.4. Meters
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/electricLib/voltmeter.svg)<br>**Voltmeter**<br>continuous | Voltmeter converts voltage to function-output |- Name |
| ![image](library/icons/electricLib/ampermeter.svg)<br>**Ampermeter**<br>continuous | Ampermeter converts current to function-output |- Name |
| ![image](library/icons/electricLib/3phvoltmeter.svg)<br>**Triangular 3ph Voltmeter**<br>continuous | 3-phase triangular voltagemeter converts voltage to array-function-output |- Name |
| ![image](library/icons/electricLib/3phvoltmeterstar.svg)<br>**Star 3ph Voltmeter**<br>continuous | 3-phase-star-voltemeter converts voltage to array-function-output |- Name |
| ![image](library/icons/electricLib/3phampermeter.svg)<br>**3ph Ampermeter**<br>continuous | 3-phase-ampermeter converts current to array-function-output |- Name |

#### 3.5.3.5. Transformer
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/electricLib/coupler.svg)<br>**Transformer**<br>continuous | Transformer with ratio N/n |- Name<br>- Ratio N<br>- Ratio n |

### 3.5.4. Non functional function elements

#### 3.5.4.1. Labels
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/nonfunctionalLib/Flabel.svg)<br>**Label** | Label of function-vector |- Name |
| ![image](library/icons/nonfunctionalLib/Farrow.svg)<br>**Arrow** | Connetcs function vectors with each other. One input-arrow can have multiple output-arrows By copying the arrow (Right-click on the Node) both arrows are connected. If the copied arrow is in input-arrow the new arrow will automatically be an output-arrow. If the copied output-arrow has no input-arrow the new arrow will automatically be an input-arrow. By holding "SHIFT" while right-clicking on the arrow or holding "SHIFT" while placing the node(left-click) a new entity will be created that has no connection to the other arrows.<br>**Remark 1: Two Arrows with the same name must not be connected**<br>**Remark 2: By marking the Arrow (right click on the Node) a line to all connected Arrows will be visible** |- Name |
| ![image](library/icons/nonfunctionalLib/Fin.svg)<br>**In** | Creates an input for a sub-circuit |- Name |
| ![image](library/icons/nonfunctionalLib/Fin.svg)<br>**Out** | Creates an output for a sub-circuit |- Name |

### 3.5.5. Functional elements

#### 3.5.5.1. Sources
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/constant.svg)<br>**Constant**<br>discontinuous | Constant function-source<br>(singular) | - Name<br>- Value |
| ![image](library/icons/functionLib/zero.svg)<br>**Zero**<br>discontinuous | Constant function-source with output 0<br>(singular) |- Name |
| ![image](library/icons/functionLib/one.svg)<br>**One**<br>discontinuous | Constant function-source with output 1<br>(singular) |- Name |
| ![image](library/icons/functionLib/sinfunction.svg)<br>**Sine**<br>discontinuous | Sine-function-source<br>(singular) |- Name<br>- Amplitude of output<br>- Frequency in Hz<br>- Phase-angle in °|
| ![image](library/icons/functionLib/sawtooth.svg)<br>**Saw-tooth**<br>discontinuous | Saw-tooth<br>(singular) |- Name<br>- [max-value, min-value]<br>- Time from min-value to max-value in s<br>- Start-offset-time in p.u.|
| ![image](library/icons/functionLib/triangular.svg)<br>**Triangular**<br>discontinuous | Symmetrical triangular wave-form<br>(singular) |- Name<br>- [max-value, min-value]<br>- Time from min-value to next min-value<br>- Start-offset-time in p.u.|
| ![image](library/icons/functionLib/step.svg)<br>**Step**<br>discontinuous | Step<br>(singular) |- Name<br>- Start-value<br>- End-value<br>- Time when output-value changes from start-value to end-value in s|
| ![image](library/icons/functionLib/clock.svg)<br>**Clock**<br>discontinuous | Symmetrical-rectangular-wave-form<br>(singular) |- Name<br>- Frequceny of clock in Hz|
| ![image](library/icons/functionLib/time.svg)<br>**Time**<br>discontinuous | In simulation time<br>(singular) |- Name |
| ![image](library/icons/functionLib/ramp.svg)<br>**Ramp**<br>discontinuous | The source outputs a ramp from start-value to end-value starting at start-time with the given slope.<br>(singular) |- Name<br>- Slope of ramp in 1/s<br>- Start-value<br>- End-value<br>- Start time of ramp in s|

#### 3.5.5.2. Math elements
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/abs.svg)<br>**Absolute**<br>discontinuous | Absolute-function<br>(singular and array) |- Name |
| ![image](library/icons/functionLib/norm.svg)<br>**Norm**<br>discontinuous | Norms the input<br>(singular and array) |- Name |
| ![image](library/icons/functionLib/gain.svg)<br>**Gain**<br>continuous | Multiplies the input with gain<br>(singular and array) |- Name<br>- Gain |
| ![image](library/icons/functionLib/sum.svg)<br>**Sum**<br>continuous | Sums all inputs of the array<br>(singular and array) |- Name |
| ![image](library/icons/functionLib/adder.svg)<br>**Adder**<br>continuous | Adds both inputs. Both inputs must either have the same size or one has to be a singular-input<br>(singular and array)|- Name|
| ![image](library/icons/functionLib/max.svg)<br>**Maximum**<br>discontinuous | Returns the maximum of inputs<br>(singular and array)|- Name<br>- Number of inputs (Value between 1 and 25)|
| ![image](library/icons/functionLib/max.svg)<br>**Minimum**<br>discontinuous | Returns the minimum of inputs<br>(singular and array) |- Name<br>- Number of inputs (Value between 1 and 25)|
| ![image](library/icons/functionLib/offset.svg)<br>**Offset**<br>continuous | Adds an offset to inputs<br>(singular and array) |- Name<br>- Offset|
| ![image](library/icons/functionLib/subtractor.svg)<br>**Subtracter**<br>continuous | Subtracts both inputs. Both inputs must either have the same size or one has to be a singular-input<br>(singular and array)|- Name |
| ![image](library/icons/functionLib/multiplicator.svg)<br>**Multiplicator**<br>discontinuous | Multiplication of both inputs. Both inputs must either have the same size or one has to be a singular-input<br>(singular and array)|- Name|
| ![image](library/icons/functionLib/divider.svg)<br>**Divider**<br>discontinuous | Division of both inputs. Both inputs must either have the same size or one has to be a singular-input<br>(singular and array)|- Name|
| ![image](library/icons/functionLib/sig.svg)<br>**Signum**<br>discontinuous | Returns the signum of all inputs<br>(singular and array) |- Name |
| ![image](library/icons/functionLib/limiter.svg)<br>**Limiter**<br>discontinuous | Limits the output to the top- and bottom-limit<br>(singular and array) |- Name<br>- Top-limit<br>- Bottom-limit|
| ![image](library/icons/functionLib/ratelimiter.svg)<br>**Rate-Limiter**<br>discontinuous | Limits the Rate of the rising or falling function<br>(singular and array) |- Name<br>- Rising rate limit<br>- Falling rate limit|
| ![image](library/icons/functionLib/hysteresis.svg)<br>**Relay**<br>discontinuous | Relay(singular and array) |- Name<br>- [upper Threshold, lowerThreshold]<br>- [output on, output off]|
| ![image](library/icons/functionLib/deadzone.svg)<br>**Dead-Zone**<br>discontinuous | Dead-zone element<br>Output is zero when input is in dead-zone |- Name<br>- High-limit<br>- Low-limit|

##### 3.5.5.2.1. Math function elements
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/square.svg)<br>**Square**<br>discontinuous | Squares the input<br>(singular and array) |- Name|
| ![image](library/icons/functionLib/sqrt.svg)<br>**Square-root**<br>discontinuous | Returns square-root of input<br>(singular and array) |- Name|
| ![image](library/icons/functionLib/exp.svg)<br>**Exponential**<br>discontinuous | Exponential function<br>(singular and array) |- Name |
| ![image](library/icons/functionLib/ln.svg)<br>**Natural Logarithm**<br>discontinuous | Natural logarithm<br>(singular and array) |- Name|
| ![image](library/icons/functionLib/log2.svg)<br>**Logarithm 2**<br>discontinuous | Logarithm to the base of 2<br>(singular and array) |- Name |
| ![image](library/icons/functionLib/log10.svg)<br>**Logarithm 10**<br>discontinuous | Logarithm to the base of 10<br>(singular and array) |- Name|
| ![image](library/icons/functionLib/power.svg)<br>**Power**<br>discontinuous | Power-function Input1 over input2<br>Both inputs must either have the same size or one has to be a singular-input<br>(singular and array)|- Name|
| ![image](library/icons/functionLib/mod.svg)<br>**Modulo**<br>discontinuous | Modulo function Input1 % input2<br>Both inputs must either have the same size or one has to be a singular-input<br>(singular and array)|- Name|
| ![image](library/icons/functionLib/rem.svg)<br>**Remainder**<br>discontinuous | Remainder of input1/input2<br>Both inputs must either have the same size or one has to be a singular-input<br>(singular and array)|- Name|

##### 3.5.5.2.2. Trigonometric elements
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/sin.svg)<br>**Sine**<br>discontinuous | Sine-function<br>(singular or array) |- Name<br>- Input-unit (degree or radian)|
| ![image](library/icons/functionLib/cos.svg)<br>**Cosine**<br>discontinuous | Cosine-function<br>(singular or array) |- Name<br>- Input unit (degree or radian)|
| ![image](library/icons/functionLib/tan.svg)<br>**Tangent**<br>discontinuous | Tangent-function<br>(singular or array) |- Name<br>- Input unit (degree or radian)|
| ![image](library/icons/functionLib/asin.svg)<br>**Arcsine**<br>discontinuous | Arcsine-function<br>(singular or array) |- Name<br>- Output unit (degree or radian)|
| ![image](library/icons/functionLib/acos.svg)<br>**Arccosine**<br>discontinuous | Arccosine-function<br>(singular or array) |- Name<br>- Output unit (degree or radian)|
| ![image](library/icons/functionLib/atan.svg)<br>**Arctangent**<br>discontinuous | Arctangent-function<br>(singular or array) |- Name<br>- Output unit (degree or radian)|

#### 3.5.5.3. Functions & Tables
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/1d.svg)<br>**1D Look-Up-Table**<br>discontinuous | Look-up-table<br>given by two arrays x and y. x and y must have the same size. x has to be steady-increasing. If input is outside the range of x the fist or last value of y will be set. Inbetween output will be liniar interpolated.<br>(singular or array) |- Name<br>x (array)<br>y (array)|
| ![image](library/icons/functionLib/2d.svg)<br>**2D Look-Up-Table**<br>discontinuous | Look-up-table<br>given by two arrays x and y and matrix z. z must be the square size of x*y. x and y have to be steady-increasing. If input is outside the range of x or y the fist or last value of z will be set for the given axis. Inbetween output will be liniar interpolated.<br>(singular or array) |- Name<br>x (array)<br>y (array)<br>z (matrix)|

#### 3.5.5.4. Control elements
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/integrator.svg)<br>**Integrator**<br>continuous or discontinuous | Continuous or discrete integrator<br>(singular or array) |- Name<br>- Continuous or discrete can be selected|
| ![image](library/icons/functionLib/dt.svg)<br>**Derivative**<br>continuous | Continuous derivative<br>(singular or array) |- Name|
| ![image](library/icons/functionLib/PI.svg)<br>**PI-Controller**<br>continuous | PI-Controller<br>(singular or array) |- Name<br>- Gain on integrative part<br>- Gain on proportional part|
| ![image](library/icons/functionLib/PID.svg)<br>**PID- Controller**<br>continuous | PID-Controller<br>(singular or array) |- Name<br>- Gain on integrative part<br>- Gain on proportional part<br>- Gain on derivative part<br>- Gain in derivative|
| ![image](library/icons/functionLib/timedelay.svg)<br>**Time delay**<br>discontinuous | Delays the input<br>(singular or array) |- Name<br>- Time delay in s|
| ![image](library/icons/functionLib/PLL.svg)<br>**PLL**<br>discontinuous | Simple digital PLL |- Name<br>- Gain on integrative part of PI-Controller in PLL<br>- Gain on proportional part of PI-Controller in PLL<br>- Start-frequency in Hz<br>- Output (frequency, sin-signal, rect-signal, phase-angle)|
| ![image](library/icons/functionLib/PLL.svg)<br>**PLL 3ph**<br>discontinuous | 3-phase PLL<br>Input-array of size 3 |- Name<br>- Gain on integrative part of PID-Controller in PLL<br>- Gain on proportional part of PID-Controller in PLL<br>- Gain on derivative part of PID-Controller in PLL<br>- Start-frequency in Hz<br>- Output (frequency, sin-signal, rect-signal, phase-angle, amplitude)|

#### 3.5.5.5. Routing
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/arrayin.svg)<br>**Array in**<br>continuous | Combines different signals to array |- Name<br>- Numer of inputs (1 to 25)|
| ![image](library/icons/functionLib/arrayout.svg)<br>**Array out**<br>continuous | Splits array to singular vectorsInput<br>Output-size must match |- Name<br>- Number of outputs (1 to 25)|

##### 3.5.5.5.1. Power electronic modulators
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/PWM.svg)<br>**Sawtooth PWM**<br>discontinuous | PWM based on a sawtooth carrier signal. During regular sampling mode, the input signal is sampled when sawtooth-function is discontinuous.<br>(singular and array) |- Name<br>- Sampling-method:<br>1. Natural means carrier-signal is always compared to the actual input<br>2. Regular means carrier signal is compared to a constant value for each period of the carrier-signal<br>- [max-value of carrier-signal, min-value of carrier-signal]<br>- [high output, low output]<br>- Offset of the carrier-signal in p.u.|
| ![image](library/icons/functionLib/PWM.svg)<br>**Symetrical PWM**<br>discontinuous | PWM based on a triangular wave carrier-signal. Provides four different sampling methods.<br>(singular and array) |- Name<br>- Sampling-method<br>1. Natural means carrier-signal is always compared to the actual input<br>2. Single high means sampling once a period when carrier-signal at the maximum value<br>3. Single-low means sampling once a period when carriersignal is the minimum value<br>4. Double means sampling twice a period, when carrier-signla is the maximum and at the minumum<br>- [max-value of carrier-signal, min-value of carrier-signal]<br>- [high output, low output]<br>- Offset of the carrier-signal in p.u. |

#### 3.5.5.6. Transformations
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/c2p.svg)<br>**cartesian to polar**<br>discontinuous | Converts cartesian coordinates to polar coordinates<br>Inputs size: 2<br>Output size: 2|- Name|
| ![image](library/icons/functionLib/p2c.svg)<br>**polar to cartesian**<br>discontinuous | Converts polar coordinates to cartesian coordinates<br>Inputs size: 2 <br>Output size: 2|- Name|
| ![image](library/icons/functionLib/alphabeta.svg)<br>**αβ array**<br>discontinuous | abc to αβ (array)<br>Input size: 3 <br>Output size: 2 |- Name |
| ![image](library/icons/functionLib/alphabeta.svg)<br>**αβ singular**<br>discontinuous | abc to αβ (singular)<br>Input size: 3 <br>Output size: 2 |- Name|
| ![image](library/icons/functionLib/alphabetareverse.svg)<br>**reverse αβ array**<br>discontinuous | αβ to abc (array)<br>Input size: 2 <br>Output size: 3 |- Name |
| ![image](library/icons/functionLib/alphabetareverse.svg)<br>**reverse αβ singular**<br>discontinuous | αβ to abc (singular)<br>Input size: 2<br> Output size: 3 |- Name|
| ![image](library/icons/functionLib/dq.svg)<br>**dq array**<br>discontinuous | abc to dq (array)<br>Input size: 3 + 1 (Phase-angle in radian)<br>Output size: 2 |- Name |
| ![image](library/icons/functionLib/dq.svg)<br>**dq singular**<br>discontinuous| abc to dq (singular)<br>Input size: 4 (Phase-angle in radian)<br>Output size: 2 |- Name|
| ![image](library/icons/functionLib/dqreverse.svg)<br>**dq reverse array**<br>discontinuous | dq to abc (array) Input size: 2 + 1 (Phase angle in radian)<br>Output size: 3 |- Name|
| ![image](library/icons/functionLib/dqreverse.svg)<br>**dq reverse singular**<br>discontinuous | dq to abc (singular)<br>Input size: 3 (Phase angle in radian)<br>Output size: 3 |- Name|
| ![image](library/icons/functionLib/alphabetatodq.svg)<br>**αβ to dq array**<br>discontinuous | αβ to dq (array)<br>Input size: 2 + 1 (Phase angle in radian)<br>Output size: 2 |- Name |
| ![image](library/icons/functionLib/alphabetatodq.svg)<br>**αβ to dq singular**<br>discontinuous | αβ to dq (singular)<br>Input size: 3 (Phase angle in radian)<br>Output size: 2 |- Name |
| ![image](library/icons/functionLib/dqtoalphabeta.svg)<br>**dq to αβ array**<br>discontinuous | dq to αβ (array)<br>Input size: 2 + 1 (Phase-angle in radian)<br>Output size: 2 |- Name |
| ![image](library/icons/functionLib/dqtoalphabeta.svg)<br>**dq to αβ singular**<br>discontinuous | dq to αβ (singular)<br>Input size: 3 (Phase-angle in radian)<br>Output size: 2 |- Name |

#### 3.5.5.7. Logic elements
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/not.svg)<br>**Inverter**<br>discontinuous | Inverts the input-signal (logical not) \< 0.5 is off \> 0.5 is on<br>Output is 0 or 1<br>(singular or array) | - Name|
| ![image](library/icons/functionLib/biggerthanconstant.svg)<br>**bigger than c**<br>discontinuous | If input value is bigger constant, output is high (1) otherwise it is low (0)<br>(singular or array) |- Name<br>- Constant to compare|
| ![image](library/icons/functionLib/biggerthan.svg)<br>**bigger than**<br>discontinuous | If input1 is bigger than input2, output is high (1), otherwise it is low (0)<br>Both inputs must either have the same size or one has to be a singular-input<br>(singular and array)|- Name |
| ![image](library/icons/functionLib/smallerthanconstant.svg)<br>**smaller than c**<br>discontinuous | If input value is smaller constant, output is high (1) otherwise it is low (0)<br>(singular or array)|- Name<br>- Constant to compare|
| ![image](library/icons/functionLib/smallerthan.svg)<br>**smaller than**<br>discontinuous | If input1 is smaller than input2, output is high (1), otherwise it is low (0)<br>Both inputs must either have the same size or one has to be a singular-input<br>(singular and array)|- Name|
| ![image](library/icons/functionLib/and.svg)<br>**AND**<br>discontinuous | Logical AND Input \>0.5 is high (1), otherwise it is low (0)<br>Both inputs must either have the same size or one has to be a singular-input<br>(singular and array) |- Name|
| ![image](library/icons/functionLib/or.svg)<br>**OR**<br>discontinuous | Logical OR Input \>0.5 is high (1), otherwise it is low (0)<br>Both inputs must either have the same size or one has to be a singular-input<br>(singular and array) |- Name|
| ![image](library/icons/functionLib/xor.svg)<br>**XOR**<br>discontinuous | Logical XOR Input \>0.5 is high (1), otherwise it is low (0)<br>Both inputs must either have the same size or one has to be a singular-input<br>(singular and array) |- Name|
| ![image](library/icons/functionLib/phasedetector.svg)<br>**Phase detector**<br>discontinuous | Phase-detector Input \>0.5 is high (1), otherwise it is low (0)(singular) |- Name|

#### 3.5.5.8. Time domain filter elements
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/phasedetector.svg)<br>**Periodic Average**<br>discontinuous | Averages the input over the set time period and updates the output at the end of the period.<br>(singular and array) |- Name<br>- Average time |

#### 3.5.5.9. Delays
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/turnondelay.svg)<br>**Turn On Delay**<br>discontinuous | Delays the turn on of the input signal.<br>(singular and array) |- Name<br>- Delay in s |

#### 3.5.5.10. Z domain
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/c2d.svg)<br>**Continuous to Discrete**<br>discrete | Samples the input-signal to a discrete output-signal<br>(singular or array |- Name |
| ![image](library/icons/functionLib/delay.svg)<br>**Delay**<br>discrete | Digital delay by one simulation-step<br>(singular or array) |- Name |
| ![image](library/icons/functionLib/discreteintegrator.svg)<br>**Discrete Integrator**<br>discrete | Discrete integrator with different integration methods.<br>(singular or array |- Name<br>- Integration method |
| ![image](library/icons/functionLib/discretederivator.svg)<br>**Discrete Derivator**<br>discrete | Discrete derivator(singular or array) |- Name |

#### 3.5.5.11. Analog-digital
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/quantizer.svg)<br>**Quantizer**<br>discontinuous | Quantizer<br>(singular or array) |- Name<br>- Smallest step |

#### 3.5.5.12. Graphical output
| **Name and Symbol** | **Functional description** | **Parameters** |
| --- | --- | --- |
| ![image](library/icons/functionLib/plot.svg)<br>**Plot Y**<br>discontinuous | Shows simulation values in simulation<br>By double-clicking on the element after a simulation the plot-window can be reopened |- Name<br>- Label for Y-axis<br>- Number of inputs|

# 4. Plot-window

The plot-window shows a visualisation of the given data.

|   |     |
| --- | --- |
| ![image](icons/actionicons/savefile.svg) | Saves the plot as a _.dtimg_-file |
| ![image](icons/actionicons/openfile.svg) | Opens a _.dtimg_-file |
| ![image](icons/actionicons/plus.svg) | Adds a saved plot in a new plot-surface |
| ![image](icons/actionicons/export.svg) | Exports the data as _.csv_ |
| ![image](icons/actionicons/import.svg) | Imports _.csv_-file |
| ![image](icons/actionicons/image.svg) | Creats a _.png_-file |

Figure 5 shows a plot-window with two empty plot-surfaces.

![image](_README_/plotwindow.png)

**Figure 5: Plot-window**

|    |    |
| --- | --- |
| ![image](Manual/Icons/leftclick.png) | By double left-clicking in the plot-surface the plot-property-window opens |


## 4.1. Plot-property-window

The following properties can be set in the plot-property-window

- Name of the plot
- xLabel
- yLabel
- Minimum value of the x-axis
- Maximum value of the x-axis
- Minimum value of the y-axis
- Maximum value of the y-axis
- Show-legend
- Data
  - Show-plot
  - Name of data
  - Colour of data
  - Remove data
- Remove plot-window

![image](_README_/plotproperty.png)

**Figure 6: Plot-property-window**

# 5. Background

## 5.1 Roadmap

Keep in mind this is a private project and therefore progress on the project might not go on as layed out here.

1. Add additional Elements in the library e.g. (2024)
    - Dead Zone Element(implemented with 0.11.1)
    - Discrete mean value(implemented with 0.11.1)
    - Diod-rectifier(implemented with 0.11.1)
    - Variable Resistor(implemented with 0.11.1)
    - Look-up-Table(1D version implemented with 0.11.1, 2D version with 0.12.0)
    - Source from csv.-file
2. Enhance Documentation (2024)
3. Thermal and magnetic-flux simulation (2025)
4. MicroPython and C-Code generation for [Arduino](https://www.arduino.cc/) (2025) (Standalone programm _3 comming soon)
    
## 5.2 How Δτ works
A little bit of math and a lot of magic. ;)

Δτ uses a space state model of the system and a solver for initial-value-problems to solve the following equation.

$x$ is the state vector

$u$ is the input vector to the model

$y$ is the output vector of the model

$$ {{\delta x} \over {\delta t}} = Ax + Bu $$
$$ y = Cx + Du $$

The matrixes $A$, $B$, $C$ and $D$ are the physical model of the system.
A solver is used to solve x for every time step. The solver determines also the size of every step. Those are the continuous sample points or minor-time-steps.

For every time step before and after calculating $y$ the model checks if it is still valid. This will be done by every element itself. If the model is no longer valid for the given inputs the model will be updated. The point will be recalculated if needed.
Δτ does not use a global event handler. Every element has its own event handlet and checks if validity. This reduces the performance, as more iterations could be needed to find a valid state. But otherwise it gives more freedome in desinging event handlers.
If after 10 iterations no valid state was found, the engine assumes the system has an ilegal state. This loop-detection can be deactivated. This can be usefull if the model is very complex or the mathematical problem is numerically badly conditioned.

As the solver determines the steps, it might not hit all descrete time-steps. Those so called major-time-steps will be calucated by the engine and not the solver.

All elements can be grouped into three categories:
1. continuous elements
    - output is determined by the solver
2. discontinuous elements
    - Input is a sink and output is a source for the continuous-model. The function of the element is calculated by the element itself outside the solver and the model
3. discrete elements
    - Those type of discontinuous elements will only update its output during major-time-steps

Every element can contain continous and discontinuous parts.
    
**Remark**: There are so called non-functional elements which have no effekt on the mthematical model of the system. Those are only relevant for the CAD-model.
