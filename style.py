# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 16:10:38 2022

@author: thocza
This is the name change Window for deltaTau
alpha 0.0.1
    extendet collection of action icons
alpha 0.0.2
    clock added
alpha 0.0.3
    color BACKGROUND added
"""

from PyQt5 import QtWidgets, QtCore, QtGui, QtOpenGL, Qt

BACKGROUND_COLOR = QtGui.QColor(0, 0, 0)
BACKGROUND = (0, 0, 0)
GRIDCOLOR = (150, 150, 150)
RED = (255, 0, 0)
WHITE = (255, 255, 255)
TURQUOISE = (0, 255, 255)
BLUE = (100, 178, 255)#50, 50, 255
GREEN = (100, 255, 100)
PURPLE = (215, 77, 255) #alternative: 173, 61, 204
PINK = (255, 53, 184)
ORANGE = (255, 177, 100)
COLORLIST = [GREEN, RED, BLUE, PURPLE, PINK, TURQUOISE]
ICONS = {
    "NewFile" : QtGui.QIcon("./icons/actionicons/newfile.svg"),
    "SaveFile" : QtGui.QIcon("./icons/actionicons/savefile.svg"),
    "OpenFile" : QtGui.QIcon("./icons/actionicons/openfile.svg"),
    "Tool" : QtGui.QIcon("./icons/actionicons/tool.svg"),
    "Info" : QtGui.QIcon("./icons/actionicons/info.svg"),
    "Help" : QtGui.QIcon("./icons/actionicons/help.svg"),
    "Play" : QtGui.QIcon("./icons/actionicons/play.svg"),
    "Stop" : QtGui.QIcon("./icons/actionicons/stop.svg"),
    "Cancel" : QtGui.QIcon("./icons/actionicons/cancel.svg"),
    "Apply" : QtGui.QIcon("./icons/actionicons/apply.svg"),
    "Discard" : QtGui.QIcon("./icons/actionicons/discard.svg"),
    "Delete" : QtGui.QIcon("./icons/actionicons/delete.svg"),
    "Undo" : QtGui.QIcon("./icons/actionicons/undo.svg"),
    "Redo" : QtGui.QIcon("./icons/actionicons/redo.svg"),
    "Plus" : QtGui.QIcon("./icons/actionicons/plus.svg"),
    "Minus" : QtGui.QIcon("./icons/actionicons/minus.svg"),
    "PlusWindow" : QtGui.QIcon("./icons/actionicons/pluswindow.svg"),
    "PlotWindow" : QtGui.QIcon("./icons/actionicons/plotwindow.svg"),
    "Export" : QtGui.QIcon("./icons/actionicons/export.svg"),
    "Import" : QtGui.QIcon("./icons/actionicons/import.svg"),
    "Image" : QtGui.QIcon("./icons/actionicons/image.svg"),
    "Clock" : QtGui.QIcon("./icons/actionicons/clock.svg")
    }