# -*- coding: utf-8 -*-
"""
Created on Tue Jul 26 15:27:04 2022

@author: czatho
This is the name change Window for deltaTau
alpha 0.0.1
    first UI
alpha 0.0.2
    new icons
alpha 0.0.3
    added undo and redo
beta 0.1.0
    language change to english
beta 0.1.1
    added new icons
beta 0.1.2
    changed the window positioning from absolute to relative
beta 0.1.3
    put some style constants to the style.py file
"""

# info variables
PROGRAM_NAME = "deltaTau name change window GUI"
FILE_NAME = "typeWindow.py"
FILE_CREATION = "2022-07-26"
FILE_UPDATE = "2022-11-08"
FILE_VERSION = "beta 0.1.3"
DT_VERSION = "0.12.0"
PRINT_INFO = True


# libraries
from PyQt5 import QtWidgets, QtCore, QtGui, QtOpenGL, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QStyle
import sys
import ctypes
import sys
import guigraphics as gg
from style import *

#static variables
FONTSIZE = 12

def printinfo():
    """shows all information about this file"""

    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)
    

class NameChangeWindow(QWidget):
    """name Window for edditing the name of the circuit
        """
        
    namechange = QtCore.pyqtSignal(str)
    closed = QtCore.pyqtSignal()
    
    def __init__(self, name, pos=None):
        """intits the Window
            also sets up the IOHandler for communication with the dt Framework
            sets name and icon
            """
        
        super().__init__()
        appicon = QtGui.QIcon()
        appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
        self.name = name
        self.setWindowIcon(appicon)
        self.setWindowTitle("deltaTau")
        self.errordialog = None
        if pos == None:
            pos = QtCore.QPoint(0, 0)
        self.initUI(pos)

    def initUI(self, pos):
        """inits the window"""
        
        self.setGeometry(pos.x() + 300, pos.y() + 300, 300, 100)
        self.setFixedSize(300, 100)
        self.setWindowTitle(f"deltaTau: {self.name}")
        
        self.hLayoutWindow = QtWidgets.QHBoxLayout(self)
        self.vLayoutWindow = QtWidgets.QVBoxLayout()
        
        self.hLayoutName = QtWidgets.QHBoxLayout()
        self.nameLabel = QtWidgets.QLabel()
        self.nameLabel.setMinimumSize(QtCore.QSize(75, 0))
        self.nameLabel.setMaximumSize(QtCore.QSize(75, 25))
        self.nameLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.nameLabel.setText("Name:")
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.nameEdit.setText(self.name)
        self.unitLabel = QtWidgets.QLabel()
        self.unitLabel.setMinimumSize(QtCore.QSize(30, 0))
        self.unitLabel.setMaximumSize(QtCore.QSize(30, 25))
        self.infoLabel = QtWidgets.QLabel()
        self.infoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
        self.infoLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.infoLabel.setMaximumSize(QtCore.QSize(25, 25))
        self.infoLabel.setToolTip("Circuit Name")
        
        self.hLayoutName.addWidget(self.nameLabel)
        self.hLayoutName.addWidget(self.nameEdit)
        self.hLayoutName.addWidget(self.unitLabel)
        self.hLayoutName.addWidget(self.infoLabel)
        
        self.hLayoutButtons = QtWidgets.QHBoxLayout()
        
        self.savebutton = QtWidgets.QPushButton("Save")
        self.savebutton.setIcon(ICONS["SaveFile"])
        self.savebutton.clicked.connect(self.save)
        self.okbutton = QtWidgets.QPushButton("OK")
        self.okbutton.setIcon(ICONS["Apply"])
        self.okbutton.clicked.connect(self.ok)
        self.cancelbutton = QtWidgets.QPushButton("Cancel")
        self.cancelbutton.setIcon(ICONS["Discard"])
        self.cancelbutton.clicked.connect(self.close)
        
        self.hLayoutButtons.addWidget(self.savebutton)
        self.hLayoutButtons.addWidget(self.okbutton)
        self.hLayoutButtons.addWidget(self.cancelbutton)
        
        self.vLayoutWindow.addLayout(self.hLayoutName)
        self.vLayoutWindow.addLayout(self.hLayoutButtons)
        self.hLayoutWindow.addLayout(self.vLayoutWindow)
        
    def closeEvent(self, event):
        """close Window"""
        
        self.name = self.nameEdit.text()
        self.closed.emit()
        self.close()
        
    def save(self):
        """save input values"""
        
        self.name = self.nameEdit.text()
        self.namechange.emit(self.name)
    
    def ok(self):
        """save input values and close window when values are valid"""
        
        self.name = self.nameEdit.text()
        self.namechange.emit(self.name)
        self.closed.emit()
        self.close()
         
    def keyPressEvent(self, event):
        """save event when enter key is pressed"""
        
        if event.key() == QtCore.Qt. Key_Return:
            self.name = self.nameEdit.text()
            self.namechange.emit(self.name)
            self.closed.emit()
            self.close()
    

"""prints the information about this file"""
if PRINT_INFO:
    printinfo()