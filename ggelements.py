# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 18:51:32 2022

@author: tomcz
This is the GUIgraphicsElements for deltaTau
alpha 0.0.1
    two elements for tests
alpha 0.0.2
    added inductance and capacitance
alpha 0.0.3
    added draw diod
alpha 0.0.4
    added voltage and current sources
    added current measurement
    minor cosmetical changes
alpha 0.0.5
    added subtractor, integrator and inverter and changed appeareance of adder
    to match appeareance of subtractor
alpha 0.0.6
    added bigger than, smaller than, and, or, xor and constant
alpha 0.0.7
    added the plot element
alpha 0.0.8
    added controled voltage source
alpha 0.0.9
    added the voltmeter
alpha 0.0.10
    added sawtooth function
alpha 0.0.11
    added sin smaller and bigger function
alpha 0.0.12
    added clock, multiplicator and divider
alpha 0.0.13
    added the switch
alpha 0.0.14
    added the coupler
alpha 0.0.15
    changes to the appeareance of the clock, plot and delay element due
    to the smaller function blocks
alpha 0.0.16
    added the moving mean element
alpha 0.0.17
    added clabels
alpha 0.0.18
    added flabels
alpha 0.0.19
    added arrayin, arrayout, timedelay, zero, one, clock
    clock is now time
alpha 0.0.20
    added thyristor and zenerdiod
alpha 0.0.21
    adjusted orientations
alpha 0.0.22
    added 3ph voltmeter, limiter, hysteresis and spwm
alpha 0.0.23
    added 3ph star voltmeter, 3ph ampermeter and 3ph voltage source
    added N and n to coupler
alpha 0.0.24
    removed labels and
    added alphabeta array
    alphabeta singular
    alphabeta reverse array
    alphabeta revers singular
    dq array
    dq singular
    dq reverse array
    dq reverse singular
    cartesian to polar
    polar to cartesian
alpha 0.0.25
    added
    sin
    cos
    tan
    asin
    acos
    atan
alpha 0.0.26
    added:
        square
        sqrt
        exp
        ln
        log2
        log10
        power
        mod
        rem
alpha 0.0.27
    added:
        PLL
        phasedetector
alpha 0.0.28
    added PID
alpha 0.0.29
    added ideal mosfet
    added Mosfet
    added periodical average
alpha 0.0.30
    added:
        Variable resistor
        Rectifier
        Dead-zone
        Discrete-mean-value
        1D-look-up-table
alpha 0.0.31
    added:
        2D look-up table
    changed:
        3 phase voltage source
        3 phase stare voltage probe
"""

# script variables
PROGRAM_NAME = "deltaTau GUI graphics elements"
FILE_NAME = "ggelements.py"
FILE_CREATION = "2022-03-23"
FILE_UPDATE = "2024-02-13"
FILE_VERSION = "alpha 0.0.30"
DT_VERSION = "0.12.0"
PRINT_INFO = True

# libraries
import numpy as np

def printinfo():
    """shows all information about this file"""
    
    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)

def drawResistor(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a resistor"""
    
    if orientation==0 or orientation==2:
        surface.drawrect(color, (pos[0]+1 + size[0]*3/8, pos[1]+1 + size[1]/4, size[0]/4, size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
    else:
        surface.drawrect(color, (pos[0]+1 + size[0]/4, pos[1]+1 + size[1]*3/8, size[0]/2, size[1]/4), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        
def drawVariableresistor(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a resistor"""
    
    if orientation==0 or orientation==2:
        surface.drawrect(color, (pos[0]+1 + size[0]*3/8, pos[1]+1 + size[1]/4, size[0]/4, size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
    else:
        surface.drawrect(color, (pos[0]+1 + size[0]/4, pos[1]+1 + size[1]*3/8, size[0]/2, size[1]/4), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
    if orientation == 0:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[1]/4 , pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]/4, pos[1] + size[1]/4), 135, width=1, fill=True)
    elif orientation == 1:
        surface.drawline(color, (pos[0] + size[1]/2, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0]  + size[1]/4 , pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), 45, width=1, fill=True)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]/2), (pos[0] + size[0]*5/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[1]/4 , pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), -45, width=1, fill=True)
    else:
        surface.drawline(color, (pos[0] + size[1]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0]  + size[1]/4 , pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]*3/4, pos[1] + size[1]/4), -135, width=1, fill=True)
        
def drawInductance(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws an inductance"""
    
    if orientation==0 or orientation==2:
        surface.drawrect(color, (pos[0]+1 + size[0]*3/8, pos[1]+1 + size[1]/4, size[0]/4, size[1]/2), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
    else:
        surface.drawrect(color, (pos[0]+1 + size[0]/4, pos[1]+1 + size[1]*3/8, size[0]/2, size[1]/4), width=1, fill=True)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        
def drawCapacitance(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a cpacitance"""
    
    if orientation==0 or orientation==2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/8), width=3)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*5/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*5/8), width=3)
    else:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), width=3)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/4), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), width=3)

def drawDiod(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a diod"""
    
    if orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), 180)
    if orientation == 3:
        surface.drawline(color, (pos[0], pos[1]  + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0]/4, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), 90)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), 0)
    if orientation == 1:
        surface.drawline(color, (pos[0], pos[1]  + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), -90)
        
def drawZenerdiod(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a diod"""
    
    if orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*3/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*1/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), 180)
    if orientation == 3:
        surface.drawline(color, (pos[0], pos[1]  + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0]/4, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/8, pos[1] + size[1]/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), 90)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*5/8), (pos[0] + size[0]/4, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), 0)
    if orientation == 1:
        surface.drawline(color, (pos[0], pos[1]  + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), -90)
        
def drawThyristor(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a diod"""
    
    if orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), 180)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]*3/8), (pos[0] + size[0], pos[1] + size[1]*5/8), width=1)
    if orientation == 3:
        surface.drawline(color, (pos[0], pos[1]  + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0]/4, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), 90)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1]), (pos[0] + size[0]*5/8, pos[1]), width=1)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), 0)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/8), (pos[0], pos[1] + size[1]*5/8), width=1)
    if orientation == 1:
        surface.drawline(color, (pos[0], pos[1]  + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/2, size[1]/2, (pos[0] + size[0]/2, pos[1] + size[1]/2), -90)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]), (pos[0] + size[0]*5/8, pos[1] + size[1]), width=1)

def drawSwitch(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a switch"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/8), (pos[0], pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]*3/16), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*13/16), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/8), (pos[0] + size[0]*15/32, pos[1] + size[1]*11/16), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*3/16, size[0]/8, size[1]/8), width=1, fill=False)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*11/16, size[0]/8, size[1]/8), width=1, fill=False)
    if orientation == 1:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]), (pos[0] + size[0]*5/8, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]*3/16, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*13/16, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*11/16, pos[1] + size[1]*17/32), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*3/16, pos[1] + size[1]*7/16, size[0]/8, size[1]/8), width=1, fill=False)
        surface.drawellipse(color, (pos[0] + size[0]*11/16, pos[1] + size[1]*7/16, size[0]/8, size[1]/8), width=1, fill=False)
    if orientation == 2:
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]*3/8), (pos[0] + size[0], pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]*3/16), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*13/16), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*17/32, pos[1] + size[1]*5/16), (pos[0] + size[0]*3/4, pos[1] + size[1]*5/8), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*3/16, size[0]/8, size[1]/8), width=1, fill=False)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*11/16, size[0]/8, size[1]/8), width=1, fill=False)
    if orientation == 3:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1]), (pos[0] + size[0]*5/8, pos[1]), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]*3/16, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*13/16, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]*15/32), (pos[0] + size[0]*5/8, pos[1] + size[1]/4), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*3/16, pos[1] + size[1]*7/16, size[0]/8, size[1]/8), width=1, fill=False)
        surface.drawellipse(color, (pos[0] + size[0]*11/16, pos[1] + size[1]*7/16, size[0]/8, size[1]/8), width=1, fill=False)

def drawConstantVoltageSource(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a constant voltage source"""
    
    drawSinusVoltageSource(surface, pos, size, orientation, color)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*13/16), (pos[0] + size[0]*3/8, pos[1] + size[1]*13/16), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*7/8), (pos[0] + size[0]*3/8, pos[1] + size[1]*7/8), width=1)

def drawSinusVoltageSource(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a voltage source"""
    
    surface.drawellipse(color, (pos[0] + size[0]/4, pos[1] + size[1]/4, size[0]/2, size[1]/2), width=1, fill=False)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*5/8), (pos[0] + size[0]*9/16, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*3/8), (pos[0] + size[0]*9/16, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*5/16), (pos[0] + size[0]/2, pos[1] + size[1]*7/16), width=1)
    elif orientation == 1:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*7/16), (pos[0] + size[0]*5/8, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*7/16), (pos[0] + size[0]*3/8, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]/2), (pos[0] + size[0]*7/16, pos[1] + size[1]/2), width=1)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*5/8), (pos[0] + size[0]*9/16, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*3/8), (pos[0] + size[0]*9/16, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*9/16), (pos[0] + size[0]/2, pos[1] + size[1]*11/16), width=1)
    elif orientation == 3:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*7/16), (pos[0] + size[0]*5/8, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*7/16), (pos[0] + size[0]*3/8, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*9/16, pos[1] + size[1]/2), (pos[0] + size[0]*11/16, pos[1] + size[1]/2), width=1)
        
def drawControlledVoltageSource(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a controlled voltage source"""
    
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*5/8), (pos[0] + size[0]*9/16, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*3/8), (pos[0] + size[0]*9/16, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*5/16), (pos[0] + size[0]/2, pos[1] + size[1]*7/16), width=1)
    elif orientation == 1:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*7/16), (pos[0] + size[0]*5/8, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*7/16), (pos[0] + size[0]*3/8, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]/2), (pos[0] + size[0]*7/16, pos[1] + size[1]/2), width=1)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*5/8), (pos[0] + size[0]*9/16, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*3/8), (pos[0] + size[0]*9/16, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*9/16), (pos[0] + size[0]/2, pos[1] + size[1]*11/16), width=1)
    elif orientation == 3:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*7/16), (pos[0] + size[0]*5/8, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*7/16), (pos[0] + size[0]*3/8, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*9/16, pos[1] + size[1]/2), (pos[0] + size[0]*11/16, pos[1] + size[1]/2), width=1)

def draw3phVoltagesource(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a constant voltage source"""
    
    # drawabc(surface, pos, size, orientation, color, version)
    if orientation == 0:
        surface.drawellipse(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4, size[0]/4, size[1]/2), width=1, fill=False)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]), (pos[0] + size[0]*(1/2 - np.sin(np.pi/4)/8), pos[1] + size[1]*(1/2 - np.cos(np.pi/4)/4)), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]), (pos[0] + size[0]*(1/2 + np.sin(np.pi/4)/8), pos[1] + size[1]*(1/2 - np.cos(np.pi/4)/4)), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        xcoordinates = np.linspace(0, 0.125, num=50)
        ycoordinate = (0.125*np.sin(xcoordinates*2*np.pi/0.125) + 0.5)*size[1] + pos[1]
        xcoordinates = (xcoordinates + 7/16)*size[0] + pos[0]
        coordinates = zip(list(xcoordinates), list(ycoordinate))
        surface.drawpolyline(color, coordinates)
        # surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] - size[1]*0.1), "a", size[0]*0.12, "Times New Roman", 0)
        # surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] - size[1]*0.1), "b", size[0]*0.12, "Times New Roman", 0)
        # surface.drawtext(color, (pos[0] + size[0]*0.65, pos[1] - size[1]*0.1), "c", size[0]*0.12, "Times New Roman", 0)
    elif orientation == 1:
        surface.drawellipse(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/8, size[0]/2, size[1]/4), width=1, fill=False)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/4), (pos[0] + size[0]*(1/2 - np.sin(np.pi/4)/4), pos[1] + size[1]*(1/2 - np.cos(np.pi/4)/8)), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/4), (pos[0] + size[0]*(1/2 - np.sin(np.pi/4)/4), pos[1] + size[1]*(1/2 + np.cos(np.pi/4)/8)), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        xcoordinates = np.linspace(0, 0.25, num=50)
        ycoordinate = (0.125/2*np.sin(xcoordinates*2*np.pi/0.25) + 0.5)*size[1] + pos[1]
        xcoordinates = (xcoordinates + 3/8)*size[0] + pos[0]
        coordinates = zip(list(xcoordinates), list(ycoordinate))
        surface.drawpolyline(color, coordinates)
        # surface.drawtext(color, (pos[0] - size[0]*0.1, pos[1] + size[1]*0.15), "a", size[1]*0.12, "Times New Roman", 0)
        # surface.drawtext(color, (pos[0] - size[0]*0.1, pos[1] + size[1]*0.4), "b", size[1]*0.12, "Times New Roman", 0)
        # surface.drawtext(color, (pos[0] - size[0]*0.1, pos[1] + size[1]*0.65), "c", size[1]*0.12, "Times New Roman", 0)
    elif orientation == 2:
        surface.drawellipse(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4, size[0]/4, size[1]/2), width=1, fill=False)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]), (pos[0] + size[0]*(1/2 - np.sin(np.pi/4)/8), pos[1] + size[1]*(1/2 + np.cos(np.pi/4)/4)), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]), (pos[0] + size[0]*(1/2 + np.sin(np.pi/4)/8), pos[1] + size[1]*(1/2 + np.cos(np.pi/4)/4)), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        xcoordinates = np.linspace(0, 0.125, num=50)
        ycoordinate = (0.125*np.sin(xcoordinates*2*np.pi/0.125) + 0.5)*size[1] + pos[1]
        xcoordinates = (xcoordinates + 7/16)*size[0] + pos[0]
        coordinates = zip(list(xcoordinates), list(ycoordinate))
        surface.drawpolyline(color, coordinates)
        # surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*1.1), "a", size[0]*0.12, "Times New Roman", 0)
        # surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*1.1), "b", size[0]*0.12, "Times New Roman", 0)
        # surface.drawtext(color, (pos[0] + size[0]*0.65, pos[1] + size[1]*1.1), "c", size[0]*0.12, "Times New Roman", 0)
    else:
        surface.drawellipse(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/8, size[0]/2, size[1]/4), width=1, fill=False)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]/4), (pos[0] + size[0]*(1/2 + np.sin(np.pi/4)/4), pos[1] + size[1]*(1/2 - np.cos(np.pi/4)/8)), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]*3/4), (pos[0] + size[0]*(1/2 + np.sin(np.pi/4)/4), pos[1] + size[1]*(1/2 + np.cos(np.pi/4)/8)), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        xcoordinates = np.linspace(0, 0.25, num=50)
        ycoordinate = (0.125/2*np.sin(xcoordinates*2*np.pi/0.25) + 0.5)*size[1] + pos[1]
        xcoordinates = (xcoordinates + 3/8)*size[0] + pos[0]
        coordinates = zip(list(xcoordinates), list(ycoordinate))
        surface.drawpolyline(color, coordinates)
        # surface.drawtext(color, (pos[0] + size[0]*1.1, pos[1] + size[1]*0.15), "a", size[1]*0.12, "Times New Roman", 0)
        # surface.drawtext(color, (pos[0] + size[0]*1.1, pos[1] + size[1]*0.4), "b", size[1]*0.12, "Times New Roman", 0)
        # surface.drawtext(color, (pos[0] + size[0]*1.1, pos[1] + size[1]*0.65), "c", size[1]*0.12, "Times New Roman", 0)

def drawabc(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws the abc next to the three phases"""
    
    if orientation == 0:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] - size[1]*0.1), "a", size[0]*0.12, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] - size[1]*0.1), "b", size[0]*0.12, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.65, pos[1] - size[1]*0.1), "c", size[0]*0.12, "Times New Roman", 0)
    elif orientation == 1:
        surface.drawtext(color, (pos[0] - size[0]*0.1, pos[1] + size[1]*0.15), "a", size[1]*0.12, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] - size[0]*0.1, pos[1] + size[1]*0.4), "b", size[1]*0.12, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] - size[0]*0.1, pos[1] + size[1]*0.65), "c", size[1]*0.12, "Times New Roman", 0)
    elif orientation == 2:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*1.1), "a", size[0]*0.12, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*1.1), "b", size[0]*0.12, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.65, pos[1] + size[1]*1.1), "c", size[0]*0.12, "Times New Roman", 0)
    else:
        surface.drawtext(color, (pos[0] + size[0]*1.1, pos[1] + size[1]*0.15), "a", size[1]*0.12, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*1.1, pos[1] + size[1]*0.4), "b", size[1]*0.12, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*1.1, pos[1] + size[1]*0.65), "c", size[1]*0.12, "Times New Roman", 0)

def drawVoltmeter(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a controlled voltage source"""
    
    surface.drawellipse(color, (pos[0] + size[0]/4, pos[1] + size[1]/4, size[0]/2, size[1]/2), width=1, fill=False)
    surface.drawtext(color, (pos[0] + size[0]*0.35, pos[1] + size[1]*0.45), "V", size[0]*0.4, "Times New Roman", 0)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*11/16, pos[1] + size[1]*3/4), (pos[0] + size[0]*13/16, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*11/16, pos[1] + size[1]/4), (pos[0] + size[0]*13/16, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*3/16), (pos[0] + size[0]*3/4, pos[1] + size[1]*5/16), width=1)
    elif orientation == 1:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/16, pos[1] + size[1]/4), (pos[0] + size[0]*5/16, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*11/16, pos[1] + size[1]/4), (pos[0] + size[0]*13/16, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/16), (pos[0] + size[0]/4, pos[1] + size[1]*5/16), width=1)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/16, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/16, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/16, pos[1] + size[1]/4), (pos[0] + size[0]*5/16, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*11/16), (pos[0] + size[0]/4, pos[1] + size[1]*13/16), width=1)
    elif orientation == 3:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/16, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/16, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*11/16, pos[1] + size[1]*3/4), (pos[0] + size[0]*13/16, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*11/16), (pos[0] + size[0]*3/4, pos[1] + size[1]*13/16), width=1)
        
def drawAmpermeter(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a controlled voltage source"""
    
    surface.drawellipse(color, (pos[0] + size[0]/4, pos[1] + size[1]/4, size[0]/2, size[1]/2), width=1, fill=False)
    surface.drawtext(color, (pos[0] + size[0]*0.37, pos[1] + size[1]*0.43), "A", size[0]*0.4, "Times New Roman", 0)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/8), (pos[0] + size[0]/8, pos[1] + size[1]*11/16), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]/8, pos[1] + size[1]*5/8), 0, width=1, fill=True)
    elif orientation == 1:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]*7/8), (pos[0] + size[0]*5/8, pos[1] + size[1]*7/8), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]*5/8, pos[1] + size[1]*7/8), -90, width=1, fill=True)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]*5/16), (pos[0] + size[0]*7/8, pos[1] + size[1]*5/8), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]*7/8, pos[1] + size[1]*3/8), 180, width=1, fill=True)
    elif orientation == 3:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/8), (pos[0] + size[0]*11/16, pos[1] + size[1]/8), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]*3/8, pos[1] + size[1]/8), 90, width=1, fill=True)

def draw3phVoltmeter(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a controlled voltage source"""
    
    # drawabc(surface, pos, size, orientation, color, version)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]), (pos[0] + size[0]/4, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]), (pos[0] + size[0]*3/4, pos[1] + size[1]), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*15/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*23/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]+ size[1]/2), (pos[0] + size[0]*7/16, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]*11/16, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]+ size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        # surface.drawline(color, (pos[0] + size[0]*27/32, pos[1]+ size[1]*3/8), (pos[0] + size[0]*29/32, pos[1] + size[1]*3/8), width=1)
        # surface.drawline(color, (pos[0] + size[0]*14/16, pos[1]+ size[1]*5/16), (pos[0] + size[0]*14/16, pos[1] + size[1]*7/16), width=1)
        # surface.drawline(color, (pos[0] + size[0]*27/32, pos[1]+ size[1]*5/8), (pos[0] + size[0]*29/32, pos[1] + size[1]*5/8), width=1)
    elif orientation == 1:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/4), (pos[0] + size[0], pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/4), (pos[0] + size[0], pos[1] + size[1]*3/4), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*15/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*7/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*23/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]*5/16), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*9/16), width=1)
        # surface.drawline(color, (pos[0] + size[0]*3/8, pos[1]+ size[1]*5/32), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/32), width=1)
        # surface.drawline(color, (pos[0] + size[0]*7/16, pos[1]+ size[1]*2/16), (pos[0] + size[0]*5/16, pos[1] + size[1]*2/16), width=1)
        # surface.drawline(color, (pos[0] + size[0]*5/8, pos[1]+ size[1]*5/32), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/32), width=1)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]), (pos[0] + size[0]/4, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]), (pos[0] + size[0]*3/4, pos[1] + size[1]), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*15/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*23/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawline(color, (pos[0], pos[1]+ size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]*5/16, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]+ size[1]/2), (pos[0] + size[0]*9/16, pos[1] + size[1]/2), width=1)
        # surface.drawline(color, (pos[0] + size[0]*5/32, pos[1]+ size[1]*3/8), (pos[0] + size[0]*3/32, pos[1] + size[1]*3/8), width=1)
        # surface.drawline(color, (pos[0] + size[0]*2/16, pos[1]+ size[1]*9/16), (pos[0] + size[0]*2/16, pos[1] + size[1]*11/16), width=1)
        # surface.drawline(color, (pos[0] + size[0]*5/32, pos[1]+ size[1]*5/8), (pos[0] + size[0]*3/32, pos[1] + size[1]*5/8), width=1)
    else:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/4), (pos[0] + size[0], pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/4), (pos[0] + size[0], pos[1] + size[1]*3/4), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*15/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*7/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*23/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]*7/16), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]*11/16), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        # surface.drawline(color, (pos[0] + size[0]*3/8, pos[1]+ size[1]*27/32), (pos[0] + size[0]*3/8, pos[1] + size[1]*29/32), width=1)
        # surface.drawline(color, (pos[0] + size[0]*9/16, pos[1]+ size[1]*14/16), (pos[0] + size[0]*11/16, pos[1] + size[1]*14/16), width=1)
        # surface.drawline(color, (pos[0] + size[0]*5/8, pos[1]+ size[1]*27/32), (pos[0] + size[0]*5/8, pos[1] + size[1]*29/32), width=1)

def draw3phVoltmeterstar(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a controlled voltage source"""
    
    # drawabc(surface, pos, size, orientation, color, version)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*7/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*15/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*23/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*7/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]+ size[1]/2), (pos[0] + size[0]*7/16, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]*11/16, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]+ size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
    elif orientation == 1:
        surface.drawline(color, (pos[0], pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*15/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*7/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*23/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*7/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]*5/16), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*9/16), width=1)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*15/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*23/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]/4), (pos[0] + size[0]*7/8, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0], pos[1]+ size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]*5/16, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]+ size[1]/2), (pos[0] + size[0]*9/16, pos[1] + size[1]/2), width=1)
    else:
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*15/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*7/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*23/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/8), (pos[0] + size[0]/4, pos[1] + size[1]*7/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]*7/16), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]*11/16), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)

def draw3phAmpermeter(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a controlled voltage source"""
    
    # drawabc(surface, pos, size, orientation, color, version)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]), (pos[0] + size[0]/4, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*5/8), (pos[0] + size[0]/4, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*5/8), (pos[0] + size[0]*3/4, pos[1] + size[1]), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*15/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*3/8, size[0]/8, size[1]/4), (90, 180), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*3/16, pos[1] + size[1]*3/8, size[0]/8, size[1]/4), (90, 180), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*23/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*11/16, pos[1] + size[1]*3/8, size[0]/8, size[1]/4), (90, 180), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]+ size[1]/2), (pos[0] + size[0]*6/16, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]*10/16, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]+ size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/16, size[1]/8, (pos[0] + size[0]/2, pos[1] + size[1]*6/8), 0, width=1, fill=True)
        surface.drawtriangle(color, size[0]/16, size[1]/8, (pos[0] + size[0]/4, pos[1] + size[1]*6/8), 0, width=1, fill=True)
        surface.drawtriangle(color, size[0]/16, size[1]/8, (pos[0] + size[0]*3/4, pos[1] + size[1]*6/8), 0, width=1, fill=True)
    elif orientation == 1:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/4), (pos[0] + size[0]*3/8, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/4), (pos[0] + size[0], pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/4), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), (pos[0] + size[0], pos[1] + size[1]*3/4), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*15/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*7/16, size[0]/4, size[1]/8), (0, -180), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*7/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/16, size[0]/4, size[1]/8), (0, -180), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*23/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*11/16, size[0]/4, size[1]/8), (0, -180), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]*6/16), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*10/16), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/16, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), -90, width=1, fill=True)
        surface.drawtriangle(color, size[0]/8, size[1]/16, (pos[0] + size[0]*3/4, pos[1] + size[1]/4), -90, width=1, fill=True)
        surface.drawtriangle(color, size[0]/8, size[1]/16, (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), -90, width=1, fill=True)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]), (pos[0] + size[0]/4, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*5/8), (pos[0] + size[0]/4, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*5/8), (pos[0] + size[0]*3/4, pos[1] + size[1]), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*15/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*3/8, size[0]/8, size[1]/4), (90, -180), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*3/16, pos[1] + size[1]*3/8, size[0]/8, size[1]/4), (90, -180), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*23/32, pos[1] + size[1]*7/16, size[0]/16, size[1]/8), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*11/16, pos[1] + size[1]*3/8, size[0]/8, size[1]/4), (90, -180), width=1, fill=True)
        surface.drawline(color, (pos[0], pos[1]+ size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]*6/16, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]+ size[1]/2), (pos[0] + size[0]*10/16, pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/16, size[1]/8, (pos[0] + size[0]/2, pos[1] + size[1]*2/8), 180, width=1, fill=True)
        surface.drawtriangle(color, size[0]/16, size[1]/8, (pos[0] + size[0]/4, pos[1] + size[1]*2/8), 180, width=1, fill=True)
        surface.drawtriangle(color, size[0]/16, size[1]/8, (pos[0] + size[0]*3/4, pos[1] + size[1]*2/8), 180, width=1, fill=True)
    else:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/4), (pos[0] + size[0]*3/8, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/4), (pos[0] + size[0], pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/4), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), (pos[0] + size[0], pos[1] + size[1]*3/4), width=1)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*15/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*7/16, size[0]/4, size[1]/8), (0, 180), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*7/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/16, size[0]/4, size[1]/8), (0, 180), width=1, fill=True)
        surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*23/32, size[0]/8, size[1]/16), width=1, fill=True)
        surface.drawarc(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*11/16, size[0]/4, size[1]/8), (0, 180), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]*6/16), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]*10/16), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]+ size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/16, (pos[0] + size[0]/4, pos[1] + size[1]/2), 90, width=1, fill=True)
        surface.drawtriangle(color, size[0]/8, size[1]/16, (pos[0] + size[0]/4, pos[1] + size[1]/4), 90, width=1, fill=True)
        surface.drawtriangle(color, size[0]/8, size[1]/16, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), 90, width=1, fill=True)

def drawControlledCurrentSource(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a controlled voltage source"""
    
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/8), (pos[0] + size[0]/2, pos[1] + size[1]*11/16), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), 0, width=1, fill=True)
    elif orientation == 1:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/2), (pos[0] + size[0]*11/16, pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), -90, width=1, fill=True)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*5/16), (pos[0] + size[0]/2, pos[1] + size[1]*5/8), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]/2, pos[1] + size[1]*3/8), 180, width=1, fill=True)
    elif orientation == 3:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]/2), (pos[0] + size[0]*5/8, pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]*3/8, pos[1] + size[1]/2), 90, width=1, fill=True)


def drawConstantCurrentSource(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a constant current source"""
    
    drawSinusCurrentSource(surface, pos, size, orientation, color)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*13/16), (pos[0] + size[0]*3/8, pos[1] + size[1]*13/16), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*7/8), (pos[0] + size[0]*3/8, pos[1] + size[1]*7/8), width=1)

def drawSinusCurrentSource(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a sinus current source"""
    
    surface.drawellipse(color, (pos[0] + size[0]/4, pos[1] + size[1]/4, size[0]/2, size[1]/2), width=1, fill=False)
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/8), (pos[0] + size[0]/2, pos[1] + size[1]*11/16), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), 0, width=1, fill=True)
    elif orientation == 1:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/2), (pos[0] + size[0]*11/16, pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), -90, width=1, fill=True)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*5/16), (pos[0] + size[0]/2, pos[1] + size[1]*5/8), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]/2, pos[1] + size[1]*3/8), 180, width=1, fill=True)
    elif orientation == 3:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]/2), (pos[0] + size[0]*5/8, pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/8, size[1]/8, (pos[0] + size[0]*3/8, pos[1] + size[1]/2), 90, width=1, fill=True)

# def drawCurrentMeter(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
#     """draws the currentmeter"""
    
#     surface.drawellipse(color, (pos[0] + size[0]/8, pos[1] + size[1]/8, size[0]*3/4, size[1]*3/4), width=1, fill=False)
#     if orientation == 0:
#         surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
#     elif orientation == 1:
#         surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/8), width=1)
#     elif orientation == 2:
#         surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/8, pos[1] + size[1]/2), width=1)
#     elif orientation == 3:
#         surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*7/8), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)

def drawCoupler(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws an coupler"""
    
    if orientation==0 or orientation==2:
        surface.drawrect(color, (pos[0]+1 + size[0]*13/48, pos[1]+1 + size[1]/4, size[0]/8, size[1]/2), width=1, fill=True)
        surface.drawrect(color, (pos[0]+1 + size[0]*29/48, pos[1]+1 + size[1]/4, size[0]/8, size[1]/2), width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]*1/2, pos[1]), (pos[0] + size[0]*1/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*1/3, pos[1]), (pos[0] + size[0]*1/3, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*1/3, pos[1] + size[1]*3/4), (pos[0] + size[0]*1/3, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*2/3, pos[1]), (pos[0] + size[0]*2/3, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*2/3, pos[1] + size[1]*3/4), (pos[0] + size[0]*2/3, pos[1] + size[1]), width=1)
        surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*1.1), "N", size[0]*0.125, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.53, pos[1] + size[1]*1.1), "n", size[0]*0.125, "Times New Roman", 0)
    else:
        surface.drawrect(color, (pos[0]+1 + size[0]/4, pos[1]+1 + size[1]*13/48, size[0]/2, size[1]/8), width=1, fill=True)
        surface.drawrect(color, (pos[0]+1 + size[0]/4, pos[1]+1 + size[1]*29/48, size[0]/2, size[1]/8), width=1, fill=True)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/3), (pos[0] + size[0]/4, pos[1] + size[1]/3), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/3), (pos[0] + size[0], pos[1] + size[1]/3), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*2/3), (pos[0] + size[0]/4, pos[1] + size[1]*2/3), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*2/3), (pos[0] + size[0], pos[1] + size[1]*2/3), width=1)
        surface.drawtext(color, (pos[0] - size[0]*0.1, pos[1] + size[1]*0.4), "N", size[1]*0.125, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] - size[0]*0.1, pos[1] + size[1]*0.53), "n", size[1]*0.125, "Times New Roman", 0)

def drawMosFET(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws an IGBT"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]/8, pos[1] + size[1]*3/16), (pos[0] + size[0]/8, pos[1] + size[1]*13/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]*3/16), (pos[0] + size[0]/4, pos[1] + size[1]*5/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]*7/16), (pos[0] + size[0]/4, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]*11/16), (pos[0] + size[0]/4, pos[1] + size[1]*13/16), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*3/8, pos[1] + size[1]/2), 90, width=1, fill=True)
    elif orientation == 1:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]), (pos[0] + size[0]/2, pos[1] + size[1]*7/8), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1]  + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]/2), (pos[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/16, pos[1] + size[1]*7/8), (pos[0] + size[0]*13/16, pos[1] + size[1]*7/8), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/16, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/16, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*7/16, pos[1] + size[1]*3/4), (pos[0] + size[0]*9/16, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*11/16, pos[1] + size[1]*3/4), (pos[0] + size[0]*13/16, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), 0, width=1, fill=True)
    elif orientation == 2:
        surface.drawline(color, (pos[0]  + size[0], pos[1] + size[1]/2), (pos[0] + size[0]*7/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*7/8, pos[1] + size[1]*3/16), (pos[0] + size[0]*7/8, pos[1] + size[1]*13/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]*3/16), (pos[0] + size[0]*3/4, pos[1] + size[1]*5/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]*7/16), (pos[0] + size[0]*3/4, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]*11/16), (pos[0] + size[0]*3/4, pos[1] + size[1]*13/16), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), -90, width=1, fill=True)
    else:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/8), width=1)
        surface.drawline(color, (pos[0], pos[1]  + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/16, pos[1] + size[1]/8), (pos[0] + size[0]*13/16, pos[1] + size[1]/8), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/16, pos[1] + size[1]/4), (pos[0] + size[0]*5/16, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*7/16, pos[1] + size[1]/4), (pos[0] + size[0]*9/16, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*11/16, pos[1] + size[1]/4), (pos[0] + size[0]*13/16, pos[1] + size[1]/4), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]/2, pos[1] + size[1]*3/8), 180, width=1, fill=True)
        
def drawRealMosFET(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws an IGBT"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/3, pos[1]), (pos[0] + size[0]/3, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*2/3, pos[1]), (pos[0] + size[0]*2/3, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]  + size[1]/4), (pos[0] + size[0]*2/3, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]/3, pos[1] + size[1]/4), (pos[0] + size[0]*11/30, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*11/30, pos[1] + size[1]*2/4), (pos[0] + size[0]*11/30, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]*2/4), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*19/30, pos[1] + size[1]*2/4), (pos[0] + size[0]*19/30, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*1/3, pos[1] + size[1]*5/8), (pos[0] + size[0]*2/3, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0]  + size[0]*5/15, pos[1] + size[1]*2/4), (pos[0] + size[0]*6/15, pos[1] + size[1]*2/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*7/15, pos[1] + size[1]*2/4), (pos[0] + size[0]*8/15, pos[1] + size[1]*2/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*9/15, pos[1] + size[1]*2/4), (pos[0] + size[0]*10/15, pos[1] + size[1]*2/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/3, pos[1]  + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/3, pos[1]  + size[1]*3/4), (pos[0] + size[0]/3, pos[1] + size[1]), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]/2, pos[1] + size[1]*3/8), 0, width=1, fill=True)
    elif orientation == 1:
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0]*5/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/3), (pos[0] + size[0]/4, pos[1] + size[1]/3), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*2/3), (pos[0] + size[0]/4, pos[1] + size[1]*2/3), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]  + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]*2/3), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]/3), (pos[0] + size[0]/4, pos[1] + size[1]*11/30), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]*11/30), (pos[0] + size[0]/4, pos[1] + size[1]*11/30), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]*19/30), (pos[0] + size[0]/4, pos[1] + size[1]*19/30), width=1)
        surface.drawline(color, (pos[0]  + size[0]*5/8, pos[1] + size[1]*1/3), (pos[0] + size[0]*5/8, pos[1] + size[1]*2/3), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]*5/15), (pos[0] + size[0]*2/4, pos[1] + size[1]*6/15), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]*7/15), (pos[0] + size[0]*2/4, pos[1] + size[1]*8/15), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]*9/15), (pos[0] + size[0]*2/4, pos[1] + size[1]*10/15), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]  + size[1]/3), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]  + size[1]/3), (pos[0] + size[0], pos[1] + size[1]/3), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*3/8, pos[1] + size[1]/2), -90, width=1, fill=True)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/3, pos[1] + size[1]), (pos[0] + size[0]/3, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*2/3, pos[1] + size[1]), (pos[0] + size[0]*2/3, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]  + size[1]*3/4), (pos[0] + size[0]*2/3, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]/3, pos[1] + size[1]*3/4), (pos[0] + size[0]*11/30, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*11/30, pos[1] + size[1]*2/4), (pos[0] + size[0]*11/30, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]*2/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*19/30, pos[1] + size[1]*2/4), (pos[0] + size[0]*19/30, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*1/3, pos[1] + size[1]*3/8), (pos[0] + size[0]*2/3, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0]  + size[0]*5/15, pos[1] + size[1]*2/4), (pos[0] + size[0]*6/15, pos[1] + size[1]*2/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*7/15, pos[1] + size[1]*2/4), (pos[0] + size[0]*8/15, pos[1] + size[1]*2/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*9/15, pos[1] + size[1]*2/4), (pos[0] + size[0]*10/15, pos[1] + size[1]*2/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/3, pos[1]  + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/3, pos[1]  + size[1]/4), (pos[0] + size[0]/3, pos[1]), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), 180, width=1, fill=True)
    else:
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]/3), (pos[0] + size[0]*3/4, pos[1] + size[1]/3), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]*2/3), (pos[0] + size[0]*3/4, pos[1] + size[1]*2/3), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1]  + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]*2/3), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]/3), (pos[0] + size[0]*3/4, pos[1] + size[1]*11/30), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]*11/30), (pos[0] + size[0]*3/4, pos[1] + size[1]*11/30), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]*19/30), (pos[0] + size[0]*3/4, pos[1] + size[1]*19/30), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/8, pos[1] + size[1]*1/3), (pos[0] + size[0]*3/8, pos[1] + size[1]*2/3), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]*5/15), (pos[0] + size[0]*2/4, pos[1] + size[1]*6/15), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]*7/15), (pos[0] + size[0]*2/4, pos[1] + size[1]*8/15), width=1)
        surface.drawline(color, (pos[0]  + size[0]*2/4, pos[1] + size[1]*9/15), (pos[0] + size[0]*2/4, pos[1] + size[1]*10/15), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]  + size[1]/3), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]  + size[1]/3), (pos[0], pos[1] + size[1]/3), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), 90, width=1, fill=True)

def drawMosFETwithdiod(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws an IGBT"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]/8, pos[1] + size[1]*3/16), (pos[0] + size[0]/8, pos[1] + size[1]*13/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]*3/16), (pos[0] + size[0]/4, pos[1] + size[1]*5/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]*7/16), (pos[0] + size[0]/4, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]*11/16), (pos[0] + size[0]/4, pos[1] + size[1]*13/16), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*3/8, pos[1] + size[1]/2), 90, width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*1/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*7/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*7/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*1/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*7/8), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), 180)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/8), (pos[0] + size[0]*7/8, pos[1] + size[1]*3/8), width=1)
    elif orientation == 1:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]), (pos[0] + size[0]/2, pos[1] + size[1]*7/8), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1]  + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]/2), (pos[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/16, pos[1] + size[1]*7/8), (pos[0] + size[0]*13/16, pos[1] + size[1]*7/8), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/16, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/16, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*7/16, pos[1] + size[1]*3/4), (pos[0] + size[0]*9/16, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*11/16, pos[1] + size[1]*3/4), (pos[0] + size[0]*13/16, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), 0, width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]/2), (pos[0] + size[0]/8, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/2), (pos[0] + size[0]*7/8, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]/4), (pos[0] + size[0]*7/8, pos[1] + size[1]/4), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]/4), 90)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/8), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/8), width=1)
    elif orientation == 2:
        surface.drawline(color, (pos[0]  + size[0], pos[1] + size[1]/2), (pos[0] + size[0]*7/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*7/8, pos[1] + size[1]*3/16), (pos[0] + size[0]*7/8, pos[1] + size[1]*13/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]*3/16), (pos[0] + size[0]*3/4, pos[1] + size[1]*5/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]*7/16), (pos[0] + size[0]*3/4, pos[1] + size[1]*9/16), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]*11/16), (pos[0] + size[0]*3/4, pos[1] + size[1]*13/16), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), -90, width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/8), (pos[0] + size[0]*1/4, pos[1] + size[1]*1/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*7/8), (pos[0] + size[0]*1/4, pos[1] + size[1]*7/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*1/4, pos[1] + size[1]*1/8), (pos[0] + size[0]*1/4, pos[1] + size[1]*7/8), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]*1/4, pos[1] + size[1]/2), 0)
        surface.drawline(color, (pos[0] + size[0]*1/8, pos[1] + size[1]*5/8), (pos[0] + size[0]*3/8, pos[1] + size[1]*5/8), width=1)
    else:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/8), width=1)
        surface.drawline(color, (pos[0], pos[1]  + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/16, pos[1] + size[1]/8), (pos[0] + size[0]*13/16, pos[1] + size[1]/8), width=1)
        surface.drawline(color, (pos[0]  + size[0]*3/16, pos[1] + size[1]/4), (pos[0] + size[0]*5/16, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*7/16, pos[1] + size[1]/4), (pos[0] + size[0]*9/16, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0]  + size[0]*11/16, pos[1] + size[1]/4), (pos[0] + size[0]*13/16, pos[1] + size[1]/4), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]/2, pos[1] + size[1]*3/8), 180, width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]/2), (pos[0] + size[0]/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/2), (pos[0] + size[0]*7/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*7/8, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), -90)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*5/8), (pos[0] + size[0]*5/8, pos[1] + size[1]*7/8), width=1)

def drawNIGBT(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws an IGBT"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/8), (pos[0], pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*5/8), (pos[0] + size[0]*5/16, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]*5/16), (pos[0] + size[0]*5/16, pos[1] + size[1]*11/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*1/4), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*7/16, pos[1] + size[1]*11/16), -45, width=1, fill=True)
    elif orientation == 1:
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]), (pos[0] + size[0]*5/8, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]), (pos[0] + size[0]*3/8, pos[1] + size[1]*11/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]*11/16), (pos[0] + size[0]*11/16, pos[1] + size[1]*11/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*1/4 , pos[1]+ size[1]*5/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*5/8), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*11/16, pos[1] + size[1]*9/16), -135, width=1, fill=True)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]*3/8), (pos[0] + size[0], pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]*3/8), (pos[0] + size[0]*11/16, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*11/16, pos[1] + size[1]*5/16), (pos[0] + size[0]*11/16, pos[1] + size[1]*11/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*1/4), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*9/16, pos[1] + size[1]*5/16), 135, width=1, fill=True)
    else:
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1]), (pos[0] + size[0]*5/8, pos[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1]), (pos[0] + size[0]*3/8, pos[1] + size[1]*5/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]*5/16), (pos[0] + size[0]*11/16, pos[1] + size[1]*5/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*1/4 , pos[1]+ size[1]*3/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/8), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*5/16, pos[1] + size[1]*7/16), 45, width=1, fill=True)
        
def drawNIGBTwithDiod(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws an IGBT"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/8), (pos[0], pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]*5/8), (pos[0] + size[0]*5/16, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]*5/16), (pos[0] + size[0]*5/16, pos[1] + size[1]*11/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*1/4), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*7/16, pos[1] + size[1]*11/16), -45, width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*1/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*7/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*7/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*1/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*7/8), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), 180)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/8), (pos[0] + size[0]*7/8, pos[1] + size[1]*3/8), width=1)
    elif orientation == 1:
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]), (pos[0] + size[0]*5/8, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]), (pos[0] + size[0]*3/8, pos[1] + size[1]*11/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]*11/16), (pos[0] + size[0]*11/16, pos[1] + size[1]*11/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*1/4 , pos[1]+ size[1]*5/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*5/8), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*11/16, pos[1] + size[1]*9/16), -135, width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]/2), (pos[0] + size[0]/8, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/2), (pos[0] + size[0]*7/8, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]/4), (pos[0] + size[0]*7/8, pos[1] + size[1]/4), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]/4), 90)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/8), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/8), width=1)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]*3/8), (pos[0] + size[0], pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0], pos[1] + size[1]*3/8), (pos[0] + size[0]*11/16, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*11/16, pos[1] + size[1]*5/16), (pos[0] + size[0]*11/16, pos[1] + size[1]*11/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*1/4), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*9/16, pos[1] + size[1]*5/16), 135, width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/8), (pos[0] + size[0]*1/4, pos[1] + size[1]*1/8), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*7/8), (pos[0] + size[0]*1/4, pos[1] + size[1]*7/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*1/4, pos[1] + size[1]*1/8), (pos[0] + size[0]*1/4, pos[1] + size[1]*7/8), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]*1/4, pos[1] + size[1]/2), 0)
        surface.drawline(color, (pos[0] + size[0]*1/8, pos[1] + size[1]*5/8), (pos[0] + size[0]*3/8, pos[1] + size[1]*5/8), width=1)
    else:
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1]), (pos[0] + size[0]*5/8, pos[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1]), (pos[0] + size[0]*3/8, pos[1] + size[1]*5/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/16, pos[1] + size[1]*5/16), (pos[0] + size[0]*11/16, pos[1] + size[1]*5/16), width=1)
        surface.drawline(color, (pos[0] + size[0]*1/4 , pos[1]+ size[1]*3/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/8), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/10, size[1]/10, (pos[0] + size[0]*5/16, pos[1] + size[1]*7/16), 45, width=1, fill=True)
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]/2), (pos[0] + size[0]/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/2), (pos[0] + size[0]*7/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*7/8, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), -90)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*5/8), (pos[0] + size[0]*5/8, pos[1] + size[1]*7/8), width=1)
    

def drawRectifier(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a rectifier element"""
    
    if orientation == 0:
        surface.drawrect(color, (pos[0] + size[0]*3/12, pos[1] + size[1]/8, size[0]/2, size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/3, pos[1]), (pos[0] + size[0]/3, pos[1] + size[1]/8))
        surface.drawline(color, (pos[0] + size[0]*2/3, pos[1]), (pos[0] + size[0]*2/3, pos[1] + size[1]/8))
        surface.drawline(color, (pos[0] + size[0]/3, pos[1] + size[1]*7/8), (pos[0] + size[0]/3, pos[1] + size[1]))
        surface.drawline(color, (pos[0] + size[0]*2/3, pos[1] + size[1]*7/8), (pos[0] + size[0]*2/3, pos[1] + size[1]))
        surface.drawtext(color, (pos[0] + size[0]*0.415, pos[1] + size[1]*0.65), "~", size[0]*0.2, "Times New Roman", orientation=90, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.675, pos[1] + size[1]*0.65), "~", size[0]*0.2, "Times New Roman", orientation=90, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.415, pos[1] + size[1]*0.2), "+", size[0]*0.2, "Times New Roman", orientation=90, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.675, pos[1] + size[1]*0.25), "-", size[0]*0.2, "Times New Roman", orientation=90, bold=True)
    elif orientation == 3:
        surface.drawrect(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/12, size[0]*3/4, size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/3), (pos[0] + size[0]/8, pos[1] + size[1]/3))
        surface.drawline(color, (pos[0], pos[1] + size[1]*2/3), (pos[0] + size[0]/8, pos[1] + size[1]*2/3))
        surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/3), (pos[0] + size[0], pos[1] + size[1]/3))
        surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]*2/3), (pos[0] + size[0], pos[1] + size[1]*2/3))
        surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.335), "~", size[0]*0.3, "Times New Roman", orientation=0, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.605), "~", size[0]*0.3, "Times New Roman", orientation=0, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.65, pos[1] + size[1]*0.335), "+", size[0]*0.2, "Times New Roman", orientation=0, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.65, pos[1] + size[1]*0.605), "-", size[0]*0.2, "Times New Roman", orientation=0, bold=True)
    elif orientation == 2:
        surface.drawrect(color, (pos[0] + size[0]*3/12, pos[1] + size[1]/8, size[0]/2, size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/3, pos[1]), (pos[0] + size[0]/3, pos[1] + size[1]/8))
        surface.drawline(color, (pos[0] + size[0]*2/3, pos[1]), (pos[0] + size[0]*2/3, pos[1] + size[1]/8))
        surface.drawline(color, (pos[0] + size[0]/3, pos[1] + size[1]*7/8), (pos[0] + size[0]/3, pos[1] + size[1]))
        surface.drawline(color, (pos[0] + size[0]*2/3, pos[1] + size[1]*7/8), (pos[0] + size[0]*2/3, pos[1] + size[1]))
        surface.drawtext(color, (pos[0] + size[0]*0.415, pos[1] + size[1]*0.2), "~", size[0]*0.2, "Times New Roman", orientation=90, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.675, pos[1] + size[1]*0.2), "~", size[0]*0.2, "Times New Roman", orientation=90, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.415, pos[1] + size[1]*0.65), "+", size[0]*0.2, "Times New Roman", orientation=90, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.675, pos[1] + size[1]*0.7), "-", size[0]*0.2, "Times New Roman", orientation=90, bold=True)
    else:
        surface.drawrect(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/12, size[0]*3/4, size[1]/2), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/3), (pos[0] + size[0]/8, pos[1] + size[1]/3))
        surface.drawline(color, (pos[0], pos[1] + size[1]*2/3), (pos[0] + size[0]/8, pos[1] + size[1]*2/3))
        surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/3), (pos[0] + size[0], pos[1] + size[1]/3))
        surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]*2/3), (pos[0] + size[0], pos[1] + size[1]*2/3))
        surface.drawtext(color, (pos[0] + size[0]*0.65, pos[1] + size[1]*0.335), "~", size[0]*0.3, "Times New Roman", orientation=0, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.65, pos[1] + size[1]*0.605), "~", size[0]*0.3, "Times New Roman", orientation=0, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.335), "+", size[0]*0.2, "Times New Roman", orientation=0, bold=True)
        surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.605), "-", size[0]*0.2, "Times New Roman", orientation=0, bold=True)

def drawGain(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a gain element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.3), "g", size[0]*0.6, "Times New Roman", 0)
    # if orientation == 0:
    #     surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.3), "g", size[0]*0.6, "Times New Roman", 90*orientation)
    # elif orientation == 1:
    #     surface.drawtext(color, (pos[0] + size[0]*0.7, pos[1] + size[1]*0.4), "g", size[0]*0.6, "Times New Roman", 90*orientation)
    # elif orientation == 2:
    #     surface.drawtext(color, (pos[0] + size[0]*0.625, pos[1] + size[1]*0.7), "g", size[0]*0.6, "Times New Roman", 90*orientation)
    # elif orientation == 3:
    #     surface.drawtext(color, (pos[0] + size[0]*0.3, pos[1] + size[1]*0.7), "g", size[0]*0.6, "Times New Roman", 90*orientation)
    # surface.drawrect(color, (pos[0], pos[1], size[0], size[1]), width=1) 

def drawDeadzone(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a limiter element"""
    
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/8, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
    
def drawLimiter(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a limiter element"""
    
    surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/4), (pos[0] + size[0]/4, pos[1] + size[1]*3/4), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]*7/8, pos[1] + size[1]/4), width=1)

def drawRateLimiter(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a ratelimiter element"""
    
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/2), (pos[0] + size[0]*5/8, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/8), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
    
def drawStep(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a limiter element"""
    
    surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]*7/8, pos[1] + size[1]/4), width=1)

def drawHysteresis(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a limiter element"""
    
    surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/8, pos[1] + size[1]/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/8, pos[1] + size[1]/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4), (pos[0] + size[0]*7/8, pos[1] + size[1]/4), width=1)

def drawSum(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a integrator element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.3, pos[1] + size[1]*0.4), "Σ", size[0]*0.6, "Times New Roman", 0)
    
def drawDiscretemean(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a integrator element"""
    
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4), (pos[0] + size[0]*5/8, pos[1] + size[1]/4), width=1)
    surface.drawtext(color, (pos[0] + size[0]*0.35, pos[1] + size[1]*0.4), "u", size[0]*0.55, "Times New Roman", 0)

def drawAdder(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw adder element"""
    
    # if orientation == 0 or orientation == 2:
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/8), (pos[0] + size[0]/2, pos[1] + size[1]*5/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/2), (pos[0] + size[0]*5/8, pos[1] + size[1]/2), width=1)
    # surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]*7/8), width=1)
    # surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), width=1)
    # else:
    #     surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]/2), width=1)
    #     surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/8), (pos[0] + size[0]/4, pos[1] + size[1]*5/8), width=1)
    #     surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), (pos[0] + size[0]*7/8, pos[1] + size[1]/2), width=1)
    #     surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*3/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*5/8), width=1)
        
def drawMultiplicator(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw adder element"""
    
    # if orientation == 0 or orientation == 2:
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/8), (pos[0] + size[0]*5/8, pos[1] + size[1]*5/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*5/8), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/8), width=1)
    # surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*5/8), (pos[0] + size[0]*5/8, pos[1] + size[1]*7/8), width=1)
    # surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*7/8), (pos[0] + size[0]*5/8, pos[1] + size[1]*5/8), width=1)
    # else:
    #     surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/8), (pos[0] + size[0]*3/8, pos[1] + size[1]*5/8), width=1)
    #     surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/8), (pos[0] + size[0]/8, pos[1] + size[1]*5/8), width=1)
    #     surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/8), (pos[0] + size[0]*7/8, pos[1] + size[1]*5/8), width=1)
    #     surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]*3/8), (pos[0] + size[0]*5/8, pos[1] + size[1]*5/8), width=1)

def drawDivider(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw adder element"""
    
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
    surface.drawtext(color, (pos[0] + size[0]*0.45, pos[1] + size[1]*0.3), "x", size[0]*0.25, "Times New Roman", 0)
    surface.drawtext(color, (pos[0] + size[0]*0.45, pos[1] + size[1]*0.6), "y", size[0]*0.25, "Times New Roman", 0)
    # surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*5/16, size[0]/8, size[1]/8), width=1, fill=True)
    # surface.drawellipse(color, (pos[0] + size[0]*7/16, pos[1] + size[1]*9/16, size[0]/8, size[1]/8), width=1, fill=True)
        
def drawSubtractor(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw subtractor element"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/8), (pos[0] + size[0]/4, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*1/8, pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), width=1)
    elif orientation == 3:
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]/2), (pos[0] + size[0]*3/8, pos[1] + size[1]/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4), (pos[0] + size[0]*5/8, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/8), (pos[0] + size[0]/2, pos[1] + size[1]*3/8), width=1)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4), (pos[0] + size[0]*5/8, pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*3/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*5/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), (pos[0] + size[0]*7/8, pos[1] + size[1]/2), width=1)
    else:
        surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]*7/8), width=1)
        surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), (pos[0] + size[0]*7/8, pos[1] + size[1]/2), width=1)
   
def drawIntegrator(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a integrator element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.45), "∫", size[0]*0.6, "Times New Roman", 0)
    
def drawTimedelay(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a integrator element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.37, pos[1] + size[1]*0.3), "τ", size[0]*0.7, "Times New Roman", 0)
    
def drawArrayin(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a array in element"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/8), (pos[0] + size[0]/2, pos[1] + size[1]*7/8), width=3)
    #     surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*1/4), (pos[0] + size[0]/2, pos[1] + size[1]*1/4), width=1)
    #     surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
    #     surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/2), (pos[0] + size[0]*3/4, pos[1] + size[1]*1/2), width=1)
    #     surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*7/16, pos[1] + size[1]*1/4), 90*orientation - 90, width=1, fill=True)
    #     surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*7/16, pos[1] + size[1]*3/4), 90*orientation - 90, width=1, fill=True)
    #     surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*11/16, pos[1] + size[1]*1/2), 90*orientation - 90, width=1, fill=True)
    elif orientation == 3:
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*1/2), (pos[0] + size[0]*7/8, pos[1] + size[1]*1/2), width=3)
        # surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*1/4), (pos[0] + size[0]/4, pos[1] + size[1]*1/2), width=1)
        # surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*1/2), width=1)
        # surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/2), (pos[0] + size[0]*1/2, pos[1] + size[1]*3/4), width=1)
        # surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*1/4, pos[1] + size[1]*7/16), -90*orientation - 90, width=1, fill=True)
        # surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*3/4, pos[1] + size[1]*7/16), -90*orientation - 90, width=1, fill=True)
        # surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*1/2, pos[1] + size[1]*11/16), -90*orientation - 90, width=1, fill=True)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/8), (pos[0] + size[0]/2, pos[1] + size[1]*7/8), width=3)
        # surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*1/4), width=1)
        # surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        # surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/2), (pos[0] + size[0]*1/4, pos[1] + size[1]*1/2), width=1)
        # surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*9/16, pos[1] + size[1]*1/4), 90*orientation - 90, width=1, fill=True)
        # surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*9/16, pos[1] + size[1]*3/4), 90*orientation - 90, width=1, fill=True)
        # surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*5/16, pos[1] + size[1]*1/2), 90*orientation - 90, width=1, fill=True)
    else:
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*1/2), (pos[0] + size[0]*7/8, pos[1] + size[1]/2), width=3)
        # surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*1/2), (pos[0] + size[0]*1/4, pos[1] + size[1]*3/4), width=1)
        # surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        # surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/2), (pos[0] + size[0]*1/2, pos[1] + size[1]*1/4), width=1)
        # surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*1/4, pos[1] + size[1]*9/16), -90*orientation - 90, width=1, fill=True)
        # surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*3/4, pos[1] + size[1]*9/16), -90*orientation - 90, width=1, fill=True)
        # surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*1/2, pos[1] + size[1]*5/16), -90*orientation - 90, width=1, fill=True)

def drawArrayout(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a array out element"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/8), (pos[0] + size[0]/2, pos[1] + size[1]*7/8), width=3)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*1/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/2), (pos[0] + size[0]*1/4, pos[1] + size[1]*1/2), width=1)
        surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*11/16, pos[1] + size[1]*1/4), 90*orientation - 90, width=1, fill=True)
        surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*11/16, pos[1] + size[1]*3/4), 90*orientation - 90, width=1, fill=True)
        surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*7/16, pos[1] + size[1]*1/2), 90*orientation - 90, width=1, fill=True)
    elif orientation == 3:
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*1/2), (pos[0] + size[0]*7/8, pos[1] + size[1]/2), width=3)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*1/2), (pos[0] + size[0]*1/4, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/2), (pos[0] + size[0]*1/2, pos[1] + size[1]*1/4), width=1)
        surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*1/4, pos[1] + size[1]*11/16), -90*orientation - 90, width=1, fill=True)
        surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*3/4, pos[1] + size[1]*11/16), -90*orientation - 90, width=1, fill=True)
        surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*1/2, pos[1] + size[1]*7/16), -90*orientation - 90, width=1, fill=True)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/8), (pos[0] + size[0]/2, pos[1] + size[1]*7/8), width=3)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*1/4), (pos[0] + size[0]/2, pos[1] + size[1]*1/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/2), (pos[0] + size[0]*3/4, pos[1] + size[1]*1/2), width=1)
        surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*5/16, pos[1] + size[1]*1/4), 90*orientation - 90, width=1, fill=True)
        surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*5/16, pos[1] + size[1]*3/4), 90*orientation - 90, width=1, fill=True)
        surface.drawtriangle(color, size[0]/8, size[0]/8,(pos[0] + size[0]*9/16, pos[1] + size[1]*1/2), 90*orientation - 90, width=1, fill=True)
    else:
        surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*1/2), (pos[0] + size[0]*7/8, pos[1] + size[1]*1/2), width=3)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*1/4), (pos[0] + size[0]/4, pos[1] + size[1]*1/2), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*1/2), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*1/2), (pos[0] + size[0]*1/2, pos[1] + size[1]*3/4), width=1)
        surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*1/4, pos[1] + size[1]*5/16), -90*orientation - 90, width=1, fill=True)
        surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*3/4, pos[1] + size[1]*5/16), -90*orientation - 90, width=1, fill=True)
        surface.drawtriangle(color, size[1]/8, size[1]/8,(pos[0] + size[0]*1/2, pos[1] + size[1]*9/16), -90*orientation - 90, width=1, fill=True)

def drawPWM(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a integrator element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.45), "PWM", size[0]*0.25, "Times New Roman", 0)
    
    
def drawNot(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a not element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.4), "!", size[0]*0.6, "Times New Roman", 0)
    
def drawBiggerThanConstant(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a bigger than constnat element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.4), ">c", size[0]*0.6, "Times New Roman", 0)
    
def drawBiggerThan(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a bigger than element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.4), ">", min(size)*0.6, "Times New Roman", 0)
    
def drawSmallerThan(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a bigger than element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.4), "<", min(size)*0.6, "Times New Roman", 0)
    
def drawSmallerThanConstant(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a smaller than constant element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.4), "<c", size[0]*0.6, "Times New Roman", 0)

def drawAnd(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws an and element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.35, pos[1] + size[1]*0.4), "&", min(size)*0.6, "Times New Roman", 0)
    
def drawOr(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws an or element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.25, pos[1] + size[1]*0.3), "or", min(size)*0.6, "Times New Roman", 0)
   
def drawXor(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a xor element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.25, pos[1] + size[1]*0.4), "=1", min(size)*0.6, "Times New Roman", 0)
    
def drawConstant(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a constant element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.35, pos[1] + size[1]*0.35), "c", size[0]*0.6, "Times New Roman", 0)
    
def drawZero(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a zero element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.35, pos[1] + size[1]*0.4), "0", size[0]*0.6, "Times New Roman", 0)
    
def drawOne(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a zero element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.35, pos[1] + size[1]*0.4), "1", size[0]*0.6, "Times New Roman", 0)
    
def drawPlot(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws the plot element"""
    
    surface.drawrect(color, (pos[0] + size[0]/4, pos[1] + size[1]/4, size[0]/2, size[1]*3/8), width=1)
    surface.drawellipse(color, (pos[0] + size[0]/2, pos[1] + size[1]*11/16, size[0]/16, size[1]/16), width=1, fill=True)
    surface.drawellipse(color, (pos[0] + size[0]*11/16, pos[1] + size[1]*11/16, size[0]/16, size[1]/16), width=1, fill=True)
    surface.drawline(color, (pos[0] + size[0]*3/12, pos[1] + size[1]*3/8), (pos[0] + size[0]*4/12, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]*4/12, pos[1] + size[1]/2), (pos[0] + size[0]*5/12, pos[1] + size[1]*3/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/12, pos[1] + size[1]*3/8), (pos[0] + size[0]*6/12, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]*6/12, pos[1] + size[1]/2), (pos[0] + size[0]*7/12, pos[1] + size[1]*3/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*7/12, pos[1] + size[1]*3/8), (pos[0] + size[0]*8/12, pos[1] + size[1]/2), width=1)
    
def drawSawTooth(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws the sawtooth element"""
    
    surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/8, pos[1] + size[1]/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/8, pos[1] + size[1]/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/8, pos[1] + size[1]/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*5/8, pos[1] + size[1]/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*7/8, pos[1] + size[1]/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*7/8, pos[1] + size[1]/4), width=1)
    
def drawTriangular(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws the sawtooth element"""
    
    surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/8, pos[1] + size[1]/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*7/8, pos[1] + size[1]/4), width=1)
    
def drawClock(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws the clock element"""
    
    pos = [pos[0] + size[0]/8, pos[1] + size[1]/8]
    size = [size[0]*3/4, size[1]*3/4]
    surface.drawline(color, (pos[0] + size[0]/8, pos[1] + size[1]/8), (pos[0] + size[0]/4, pos[1] + size[1]/8), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/8), (pos[0] + size[0]/4, pos[1] + size[1]*7/8), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*7/8), (pos[0] + size[0]*3/8, pos[1] + size[1]*7/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*7/8), (pos[0] + size[0]*3/8, pos[1] + size[1]/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/8), (pos[0] + size[0]/2, pos[1] + size[1]/8), width=1)
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/8), (pos[0] + size[0]/2, pos[1] + size[1]*7/8), width=1)
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*7/8), (pos[0] + size[0]*5/8, pos[1] + size[1]*7/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*7/8), (pos[0] + size[0]*5/8, pos[1] + size[1]/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]/8), (pos[0] + size[0]*3/4, pos[1] + size[1]/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*7/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*7/8), (pos[0] + size[0]*7/8, pos[1] + size[1]*7/8), width=1)
    
def drawTurnOnDelay(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws turnondelay element"""
    
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]*5/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*5/8), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/8), width=1)
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]*3/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/8), (pos[0] + size[0]*5/8, pos[1] + size[1]*3/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*3/8), (pos[0] + size[0]*5/8, pos[1] + size[1]*5/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/8, pos[1] + size[1]*5/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*5/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/2), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
    surface.drawtriangle(color, size[0]/16, size[1]/16, (pos[0] + size[0]*7/16, pos[1] + size[1]/2), -90, fill=True)
    
def drawPeriodicAverage(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws periodic average"""
    
    surface.drawline(color, (pos[0] + size[0]/5, pos[1] + size[1]*5/8), (pos[0] + size[0]*2/5, pos[1] + size[1]*5/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*2/5, pos[1] + size[1]*5/8), (pos[0] + size[0]*2/5, pos[1] + size[1]*3/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*2/5, pos[1] + size[1]*3/8), (pos[0] + size[0]*3/5, pos[1] + size[1]*3/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/5, pos[1] + size[1]*4/8), (pos[0] + size[0]*3/5, pos[1] + size[1]*3/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/5, pos[1] + size[1]*4/8), (pos[0] + size[0]*4/5, pos[1] + size[1]*4/8), width=1)

def drawSinFunction(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws the sin function element"""
    
    xcoordinates = np.linspace(0, 0.75, num=50)
    ycoordinate = (0.25*np.sin(xcoordinates*2*np.pi/0.75) + 0.5)*size[1] + pos[1]
    xcoordinates = (xcoordinates + 0.125)*size[0] + pos[0]
    coordinates = zip(list(xcoordinates), list(ycoordinate))
    surface.drawpolyline(color, coordinates)
    
def drawTime(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws the clock element"""
    
    surface.drawellipse(color, (pos[0] + size[0]/4, pos[1] + size[1]/4, size[0]/2, size[1]/2), width=1, fill=False)
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/8), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*5/8), (pos[0] + size[0]/2, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]/4), (pos[0] + size[0]/2, pos[1] + size[1]*5/16), width=1)
    surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*11/16), (pos[0] + size[0]/2, pos[1] + size[1]*3/4), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]*5/16, pos[1] + size[1]/2), width=1)
    surface.drawline(color, (pos[0] + size[0]*11/16, pos[1] + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)

def drawDelay(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a delay element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.4), "z", size[0]*0.5, "Times New Roman", 0)
    surface.drawtext(color, (pos[0] + size[0]*0.55, pos[1] + size[1]*0.25), "-1", size[0]*0.3, "Times New Roman", 0)
    
def drawContinuous2Discrete(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a delay element"""
    
    surface.drawline(color, (pos[0] + size[0]*19/128, pos[1] + size[1]*109/128), (pos[0] + size[0]*109/128, pos[1] + size[1]*19/128), width=1)
    surface.drawtext(color, (pos[0] + size[0]*0.27, pos[1] + size[1]*0.2), "τ", size[0]*0.4, "Times New Roman", 0)
    surface.drawtext(color, (pos[0] + size[0]*0.49, pos[1] + size[1]*0.57), "Δ", size[0]*0.4, "Times New Roman", 0)
    
def drawDiscreteIntegrator(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a ddiscrete integrator element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.3, pos[1] + size[1]*0.6), "z-1", size[0]*0.3, "Times New Roman", 0)
    surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.3), "T", size[0]*0.3, "Times New Roman", 0)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
    
def drawDiscreteDerivator(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a ddiscrete integrator element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.35, pos[1] + size[1]*0.6), "Tz", size[0]*0.3, "Times New Roman", 0)
    surface.drawtext(color, (pos[0] + size[0]*0.3, pos[1] + size[1]*0.3), "z-1", size[0]*0.3, "Times New Roman", 0)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/2), (pos[0] + size[0]*3/4, pos[1] + size[1]/2), width=1)
    
def drawQuantizer(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a ddiscrete integrator element"""
    
    surface.drawline(color, (pos[0] + size[0]*3/12, pos[1] + size[1]*9/12), (pos[0] + size[0]*3/12, pos[1] + size[1]*8/12), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/12, pos[1] + size[1]*8/12), (pos[0] + size[0]*4/12, pos[1] + size[1]*8/12), width=1)
    surface.drawline(color, (pos[0] + size[0]*4/12, pos[1] + size[1]*8/12), (pos[0] + size[0]*4/12, pos[1] + size[1]*7/12), width=1)
    surface.drawline(color, (pos[0] + size[0]*4/12, pos[1] + size[1]*7/12), (pos[0] + size[0]*5/12, pos[1] + size[1]*7/12), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/12, pos[1] + size[1]*7/12), (pos[0] + size[0]*5/12, pos[1] + size[1]*6/12), width=1)
    surface.drawline(color, (pos[0] + size[0]*5/12, pos[1] + size[1]*6/12), (pos[0] + size[0]*6/12, pos[1] + size[1]*6/12), width=1)
    surface.drawline(color, (pos[0] + size[0]*6/12, pos[1] + size[1]*6/12), (pos[0] + size[0]*6/12, pos[1] + size[1]*5/12), width=1)
    surface.drawline(color, (pos[0] + size[0]*6/12, pos[1] + size[1]*5/12), (pos[0] + size[0]*7/12, pos[1] + size[1]*5/12), width=1)
    surface.drawline(color, (pos[0] + size[0]*7/12, pos[1] + size[1]*5/12), (pos[0] + size[0]*7/12, pos[1] + size[1]*4/12), width=1)
    surface.drawline(color, (pos[0] + size[0]*7/12, pos[1] + size[1]*4/12), (pos[0] + size[0]*8/12, pos[1] + size[1]*4/12), width=1)
    surface.drawline(color, (pos[0] + size[0]*8/12, pos[1] + size[1]*4/12), (pos[0] + size[0]*8/12, pos[1] + size[1]*3/12), width=1)
    surface.drawline(color, (pos[0] + size[0]*8/12, pos[1] + size[1]*3/12), (pos[0] + size[0]*9/12, pos[1] + size[1]*3/12), width=1)

def drawDt(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a derivation element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.35, pos[1] + size[1]*0.4), "dt", size[0]*0.5, "Times New Roman", 0, bold=False, italic=True)
    
def drawPI(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a PI element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.3, pos[1] + size[1]*0.4), "PI", size[0]*0.5, "Times New Roman", 0)
    
def drawPID(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a PI element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.45), "PID", size[0]*0.35, "Times New Roman", 0)
    
def drawmm(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a moving mean element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.275, pos[1] + size[1]*0.425), "mm", size[0]*0.3, "Times New Roman", 0)

def drawCText(surface, pos, size, label, orientation=0, color=(255, 255, 255), version=0):
    """draws a Text Label for the circuit"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawtext(color, (pos[0], pos[1] + size[1]*7/8), label, size[1]/4, "Arial")
    elif orientation == 1:
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]), (pos[0] + size[0]/4, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawtext(color, (pos[0] + size[0]*1/8, pos[1]), label, size[1]/4, "Arial", orientation=90)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0], pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/4), width=1)
        surface.drawtext(color, (pos[0] + size[0]/4, pos[1]), label, size[1]/4, "Arial")
    else:
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
        surface.drawtext(color, (pos[0] + size[0], pos[1] + size[1]*1/4), label, size[1]/4, "Arial", orientation=90)
        
def drawCNode(surface, pos, size, label, orientation=0, color=(255, 255, 255), version=0):
    """draws a Text Label for the circuit"""
    
    if orientation == 0:
        surface.drawellipse(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/8, size[0]/2, size[1]/4), width=1, fill=False)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawtext(color, (pos[0], pos[1] + size[1]*7/8), label, size[1]/4, "Arial")
    elif orientation == 1:
        surface.drawellipse(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4, size[0]/4, size[1]/2), width=1, fill=False)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawtext(color, (pos[0] + size[0]*1/8, pos[1]), label, size[1]/4, "Arial", orientation=90)
    elif orientation == 2:
        surface.drawellipse(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/8, size[0]/2, size[1]/4), width=1, fill=False)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawtext(color, (pos[0] + size[0]/4, pos[1]), label, size[1]/4, "Arial")
    else:
        surface.drawellipse(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4, size[0]/4, size[1]/2), width=1, fill=False)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawtext(color, (pos[0] + size[0], pos[1] + size[1]*1/4), label, size[1]/4, "Arial", orientation=90)
        
def drawCio(surface, pos, size, label, orientation=0, color=(255, 255, 255), version=0):
    """draws a Text Label for the circuit"""
    
    if orientation == 0:
        surface.drawellipse(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/8, size[0]/4, size[1]/4), width=1, fill=False)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawtext(color, (pos[0], pos[1] + size[1]*7/8), label, size[1]/4, "Arial")
    elif orientation == 1:
        surface.drawellipse(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/2, size[0]/4, size[1]/4), width=1, fill=False)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawtext(color, (pos[0] + size[0]*1/8, pos[1]), label, size[1]/4, "Arial", orientation=90)
    elif orientation == 2:
        surface.drawellipse(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/8, size[0]/4, size[1]/4), width=1, fill=False)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawtext(color, (pos[0] + size[0]/4, pos[1]), label, size[1]/4, "Arial")
    else:
        surface.drawellipse(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4, size[0]/4, size[1]/4), width=1, fill=False)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawtext(color, (pos[0] + size[0], pos[1] + size[1]*1/4), label, size[1]/4, "Arial", orientation=90)

def drawFText(surface, pos, size, label, orientation=0, color=(255, 255, 255), version=0):
    """draws a Text Label for the functions"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]/4), (pos[0] + size[0], pos[1] + size[1]/4), width=1)
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/4), width=1)
        surface.drawtext(color, (pos[0] + size[0]/4, pos[1]), label, size[1]/4, "Arial")
    elif orientation == 3:
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/4), (pos[0] + size[0]*3/4, pos[1] + size[1]), width=1)
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
        surface.drawtext(color, (pos[0] + size[0], pos[1] + size[1]*1/4), label, size[1]/4, "Arial", orientation=90)
    elif orientation == 2:
        surface.drawline(color, (pos[0], pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawtext(color, (pos[0], pos[1] + size[1]*7/8), label, size[1]/4, "Arial")
    else:
        surface.drawline(color, (pos[0] + size[0]/4, pos[1]), (pos[0] + size[0]/4, pos[1] + size[1]*3/4), width=1)
        surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawtext(color, (pos[0] + size[0]*1/8, pos[1]), label, size[1]/4, "Arial", orientation=90)

def drawFArrow(surface, pos, size, label, orientation=0, color=(255, 255, 255), version=0):
        """draws the arrow Label for the functions"""
        
        if version == 0:
            if orientation == 0:
                surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
                surface.drawtriangle(color, size[0]/2, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]/2), -90)
                surface.drawtext(color, (pos[0], pos[1] + size[1]*7/8), label, size[1]/4, "Arial")
            elif orientation == 3:
                surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
                surface.drawtriangle(color, size[0]/2, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]/2), 0)
                surface.drawtext(color, (pos[0] + size[0]*1/8, pos[1]), label, size[1]/4, "Arial", orientation=90)
            elif orientation == 2:
                surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
                surface.drawtriangle(color, size[0]/2, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]/2), 90)
                surface.drawtext(color, (pos[0] + size[0]/4, pos[1]), label, size[1]/4, "Arial")
            else:
                surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[0]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
                surface.drawtriangle(color, size[0]/2, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]/2), 180)
                surface.drawtext(color, (pos[0] + size[0], pos[1] + size[1]*1/4), label, size[1]/4, "Arial", orientation=90)
        elif version == 1:
           if orientation == 0:
               surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
               surface.drawtriangle(color, size[0]/2, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]/2), -90)
               surface.drawtext(color, (pos[0], pos[1] + size[1]*7/8), label, size[1]/4, "Arial")
           elif orientation == 3:
               surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
               surface.drawtriangle(color, size[0]/2, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]/2), 0)
               surface.drawtext(color, (pos[0] + size[0]*1/8, pos[1]), label, size[1]/4, "Arial", orientation=90)
           elif orientation == 2:
               surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
               surface.drawtriangle(color, size[0]/2, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]/2), 90)
               surface.drawtext(color, (pos[0] + size[0]/4, pos[1]), label, size[1]/4, "Arial")
           else:
               surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
               surface.drawtriangle(color, size[0]/2, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]/2), 180)
               surface.drawtext(color, (pos[0] + size[0], pos[1] + size[1]*1/4), label, size[1]/4, "Arial", orientation=90)
               
def drawFin(surface, pos, size, label, orientation=0, color=(255, 255, 255), version=0):
    """draws the in function input"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), -90)
        surface.drawtext(color, (pos[0]  + size[0]/2, pos[1] + size[1]*7/8), label, size[1]/4, "Arial")
    elif orientation == 3:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[1]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), 0)
        surface.drawtext(color, (pos[0] + size[0]*1/8, pos[1] + size[1]/2), label, size[1]/4, "Arial", orientation=90)
    elif orientation == 2:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]*3/8, pos[1] + size[1]/2), 90)
        surface.drawtext(color, (pos[0] + size[0]/4, pos[1]), label, size[1]/4, "Arial")
    else:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]*3/8), 180)
        surface.drawtext(color, (pos[0] + size[0], pos[1] + size[1]*1/4), label, size[1]/4, "Arial", orientation=90)
        
def drawFout(surface, pos, size, label, orientation=0, color=(255, 255, 255), version=0):
    """draws the out function input"""
    
    if orientation == 0:
        surface.drawline(color, (pos[0], pos[1] + size[1]/2), (pos[0] + size[0]/4, pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]*3/8, pos[1] + size[1]/2), -90)
        surface.drawtext(color, (pos[0], pos[1] + size[1]*7/8), label, size[1]/4, "Arial")
    elif orientation == 3:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1]), (pos[0] + size[0]/2, pos[1] + size[1]/4), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]*3/8), 0)
        surface.drawtext(color, (pos[0] + size[0]*1/8, pos[1]), label, size[1]/4, "Arial", orientation=90)
    elif orientation == 2:
        surface.drawline(color, (pos[0] + size[0]*3/4, pos[1] + size[1]/2), (pos[0] + size[0], pos[1] + size[1]/2), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]*5/8, pos[1] + size[1]/2), 90)
        surface.drawtext(color, (pos[0] + size[0]/4, pos[1]), label, size[1]/4, "Arial")
    else:
        surface.drawline(color, (pos[0] + size[0]/2, pos[1] + size[0]*3/4), (pos[0] + size[0]/2, pos[1] + size[1]), width=1)
        surface.drawtriangle(color, size[0]/4, size[1]/4, (pos[0] + size[0]/2, pos[1] + size[1]*5/8), 180)
        surface.drawtext(color, (pos[0] + size[0], pos[1] + size[1]*1/4), label, size[1]/4, "Arial", orientation=90)

def drawAlphaBeta(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a alpha beta element"""
    
    if orientation == 0:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "abc", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "αβ", size[0]*0.3, "Times New Roman", 0)
    elif orientation == 3:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "abc", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "αβ", size[1]*0.3, "Times New Roman", 0)
    elif orientation == 2:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "αβ", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.65), "abc", size[0]*0.3, "Times New Roman", 0)
    else:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "αβ", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.65), "abc", size[1]*0.3, "Times New Roman", 0)
    surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/8), (pos[0] + size[0]/8, pos[1] + size[1]*7/8), width=1)
    
def drawAlphaBetareverse(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a alpha beta reverse element"""
    
    if orientation == 0:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "αβ", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.65), "abc", size[0]*0.3, "Times New Roman", 0)
    elif orientation == 3:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "αβ", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.65), "abc", size[1]*0.3, "Times New Roman", 0)
    elif orientation == 1:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "abc", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "αβ", size[1]*0.3, "Times New Roman", 0)
    else:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "abc", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "αβ", size[0]*0.3, "Times New Roman", 0)
    surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/8), (pos[0] + size[0]/8, pos[1] + size[1]*7/8), width=1)
    
def drawAlphaBetatodq(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a alpha beta element"""
    
    if orientation == 0:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "αβ", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "dq", size[0]*0.3, "Times New Roman", 0)
    elif orientation == 3:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "αβ", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "dq", size[1]*0.3, "Times New Roman", 0)
    elif orientation == 1:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "dq", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "αβ", size[1]*0.3, "Times New Roman", 0)
    else:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "dq", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "αβ", size[0]*0.3, "Times New Roman", 0)
    surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/8), (pos[0] + size[0]/8, pos[1] + size[1]*7/8), width=1)
    
def drawdqtoAlphaBeta(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a alpha beta element"""
    
    if orientation == 0:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "dq", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "αβ", size[0]*0.3, "Times New Roman", 0)
    elif orientation == 3:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "dq", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "αβ", size[1]*0.3, "Times New Roman", 0)
    elif orientation == 1:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "αβ", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "dq", size[1]*0.3, "Times New Roman", 0)
    else:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "αβ", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "dq", size[0]*0.3, "Times New Roman", 0)
    surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/8), (pos[0] + size[0]/8, pos[1] + size[1]*7/8), width=1)
    
def drawdq(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a dq element"""
    
    if orientation == 0:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "abc", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "dq", size[0]*0.3, "Times New Roman", 0)
    elif orientation == 3:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "abc", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "dq", size[1]*0.3, "Times New Roman", 0)
    elif orientation == 1:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "dq", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.65), "abc", size[1]*0.3, "Times New Roman", 0)
    else:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "dq", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.65), "abc", size[0]*0.3, "Times New Roman", 0)
    surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/8), (pos[0] + size[0]/8, pos[1] + size[1]*7/8), width=1)
    
def drawdqreverse(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a dq reverse element"""
    
    if orientation == 0:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "dq", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.65), "abc", size[0]*0.3, "Times New Roman", 0)
    elif orientation == 3:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "dq", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.4, pos[1] + size[1]*0.65), "abc", size[1]*0.3, "Times New Roman", 0)
    elif orientation == 1:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "abc", size[1]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "dq", size[1]*0.3, "Times New Roman", 0)
    else:
        surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.25), "abc", size[0]*0.3, "Times New Roman", 0)
        surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.6), "dq", size[0]*0.3, "Times New Roman", 0)
    surface.drawline(color, (pos[0] + size[0]*7/8, pos[1] + size[1]/8), (pos[0] + size[0]/8, pos[1] + size[1]*7/8), width=1)
    
def drawcartesian2polar(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws cartesian to polar"""
    
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]*3/4), width=1)
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
    surface.drawarc(color, (pos[0] - size[0]/8, pos[1] + size[1]*3/8, size[0]*3/4, size[1]*3/4), (0, 45), width=1, fill=True)
    
def drawpolar2cartesian(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws polar to cartesian"""
    
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*5/8), (pos[0] + size[0]*3/4, pos[1] + size[1]*5/8), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]/4), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), width=1)
    
def drawramp(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a ramp"""
    
    surface.drawline(color, (pos[0] + size[0]/4, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), width=1)
    surface.drawline(color, (pos[0] + size[0]*3/8, pos[1] + size[1]*3/4), (pos[0] + size[0]*3/4, pos[1] + size[1]/4), width=1)
    
def drawAbs(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a abs element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.23, pos[1] + size[1]*0.35), "|x|", size[0]*0.6, "Times New Roman", 0)
    
def drawNorm(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a abs element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.23, pos[1] + size[1]*0.37), "||x||", size[0]*0.5, "Times New Roman", 0)
    
def drawSignum(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a signum element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.35), "sig", size[0]*0.6, "Times New Roman", 0)
    
def drawMinimum(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a minimum element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.35), "min", size[0]*0.4, "Times New Roman", 0)

def drawMaximum(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a maximum element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.15, pos[1] + size[1]*0.35), "max", size[0]*0.4, "Times New Roman", 0)
    
def drawOffset(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws an offset"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.4), "+C", size[0]*0.5, "Times New Roman", 0)
    
def drawRound(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws round"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.19, pos[1] + size[1]*0.45), "round", size[0]*0.27, "Times New Roman", 0)
    
def drawSin(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a sine"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.225, pos[1] + size[1]*0.4), "sin", size[0]*0.5, "Times New Roman", 0)
    
def drawCos(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a cosine"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.4), "cos", size[0]*0.5, "Times New Roman", 0)
    
def drawTan(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a tangens"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.4), "tan", size[0]*0.5, "Times New Roman", 0)

def drawAsin(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a asine"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.195, pos[1] + size[1]*0.4), "asin", size[0]*0.4, "Times New Roman", 0)
    
def drawAcos(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a acos"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.155, pos[1] + size[1]*0.4), "acos", size[0]*0.4, "Times New Roman", 0)
    
def drawAtan(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a acos"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.17, pos[1] + size[1]*0.4), "atan", size[0]*0.4, "Times New Roman", 0)
    
def drawSquare(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a acos"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.35, pos[1] + size[1]*0.4), "x", size[0]*0.5, "Times New Roman", 0)
    surface.drawtext(color, (pos[0] + size[0]*0.6, pos[1] + size[1]*0.25), "2", size[0]*0.3, "Times New Roman", 0)
    
def drawSqrt(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a acos"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.4), "sqrt", size[0]*0.4, "Times New Roman", 0)
    
def drawExp(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a acos"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.22, pos[1] + size[1]*0.4), "exp", size[0]*0.4, "Times New Roman", 0)
    
def drawLn(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a acos"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.35, pos[1] + size[1]*0.4), "ln", size[0]*0.4, "Times New Roman", 0)
    
def drawLog2(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a acos"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.42), "log2", size[0]*0.35, "Times New Roman", 0)
    
def drawLog10(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a acos"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.45), "log10", size[0]*0.3, "Times New Roman", 0)

def drawPower(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a acos"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.35, pos[1] + size[1]*0.4), "x", size[0]*0.5, "Times New Roman", 0)
    surface.drawtext(color, (pos[0] + size[0]*0.6, pos[1] + size[1]*0.25), "v", size[0]*0.3, "Times New Roman", 0)

def drawMod(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a acos"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.4), "mod", size[0]*0.35, "Times New Roman", 0)

def drawRem(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a acos"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.23, pos[1] + size[1]*0.4), "rem", size[0]*0.35, "Times New Roman", 0)
    
def draw1d(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a 1d look-up table"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.225, pos[1] + size[1]*0.4), "1D", size[0]*0.45, "Times New Roman", 0)
    
def draw2d(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a 1d look-up table"""
    
    if orientation == 0 or orientation == 2:
        surface.drawtext(color, (pos[0] + size[0]*0.225, pos[1] + size[1]*0.45), "2D", min(size[0], size[1])*0.45, "Times New Roman", 0)
    else:
        surface.drawtext(color, (pos[0] + size[0]*0.3, pos[1] + size[1]*0.4), "2D", min(size[0], size[1])*0.45, "Times New Roman", 0)
    
def drawPhasedetector(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a phase detector"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.29, pos[1] + size[1]*0.52), "*", min(size)*0.8, "sans-serif", 0)
    
def drawPLL(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draws a PLL"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.21, pos[1] + size[1]*0.44), "PLL", size[0]*0.35, "Times New Roman", 0)
    
def drawADtime(surface, pos, size, orientation=0, color=(255, 255, 255), version=0):
    """draw a integrator element"""
    
    surface.drawtext(color, (pos[0] + size[0]*0.2, pos[1] + size[1]*0.35), "AD", size[0]*0.25, "Times New Roman", 0)
    surface.drawtext(color, (pos[0] + size[0]*0.5, pos[1] + size[1]*0.55), "time", size[0]*0.15, "Times New Roman", 0)

if PRINT_INFO:
    printinfo()