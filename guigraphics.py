# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 18:51:32 2022

@author: tomcz
This is the GUIgraphics for deltaTau
alpha 0.0.1
    first UI for tests
beta 0.1.0
    basic functionality is established
    comments added
beta 0.1.1
    property element is implemented with basic functionality
beta 0.1.2
    property element is implemented with full use
beta 0.1.3
    bugfix line and Vector collide function
beta 0.1.4
    minor bug fixes and implementation of function element with varing input
    and output number
beta 0.1.5
    added current measurement and minor bug fixes
beta 0.1.6
    added ctrl+R to rotate element
beta 0.1.7
    added the creatin of the dt model from cad
beta 0.1.8
    bug fixes
beta 0.1.9
    bug fixes
beta 0.1.10
    added class for saving circuits
beta 0.1.11
    added controloutput for CElement
beta 0.1.12
    added draw polyline function to Image Widget
beta 0.1.13
    added the feature that when closing a line or a vector to an open 
    node or arrow a new line or vector will be added automaticaly
    bug fix
beta 0.2.0
    changed the numbers of nodes for the the CElement
beta 0.2.1
    bug fixes
beta 0.2.2
    element connects automatically to existing node and arrows if placed
beta 0.2.3
    function elements are now smaller
beta 0.2.4
    nodes now connect to nodes
    nodes now show in deletemode
    arrows now connect to arrows
    arrows now show in deletemode
    added inputs and outputs on Functionblocks when floating
beta 0.2.5
    easier handling with arrows, nodes and vectors
beta 0.2.6
    input and output sizes if CElements and FElemens is now possible
    on right click the element will now be copied
beta 0.2.7
    circuit elements have now labels
beta 0.2.8
    bug fixes
beta 0.2.9
    function elements have now labels
beta 0.2.10
    removed show and added PURPLE as color
beta 0.2.11
    added savecheck
beta 0.2.12
    bugfix
beta 0.3.0
    added the subcircuit
beta 0.3.1
    error marker
beta 0.3.2
    added possibility to load and save subcircuits
beta 0.3.3
    added redo and undo
beta 0.3.4
    added the possibility to activate function elements during simulation
beta 0.3.5
    minor bug-fix
beta 0.3.6
    Nodes can now handle more elements
beta 0.3.7
    bug-fix
beta 0.3.8
    arrows are now more pointy
beta 0.3.9
    changes to comply with the new circuit element architecture
    and removed the current measurement
beta 0.3.10
    closed nodes can be opened by pressing shift
beta 0.3.11
    changed subwindow positioning
    and minor bug fix
beta 0.3.12
    bug fixes
    and put some style constants to the style.py file
beta 0.3.13
    arrows function now more like nodes
    minor bug-fixes
beta 0.3.14
    added drawimage
beta 0.3.15
    added labels to CElements and FElements
beta 0.3.16
    bugfix with the FLabel orientation
beta 0.3.17
    added rounded corners for rects
beta 0.3.18
    works now with QPointF
beta 0.3.19
    Bug-fixes
beta 0.3.20
    all elements can be marked by pressing CTRL_A
    all marked elements will be removed when pressing DEL
    copied FElements have no dtelement attached
    ellements can be marked by draging a selection space
    Functionelements andsubcircuits are now filled but opace
    bug-fixes
beta 0.3.21
    added Error-messages when element failed to creat
    Sub-circuits are now red when failure occured inside the subcircuit element
beta 0.4.0
    added UElement
"""

# script variables
PROGRAM_NAME = "deltaTau GUI graphics"
FILE_NAME = "guigraphics.py"
FILE_CREATION = "2022-03-23"
FILE_UPDATE = "2024-08-20"
FILE_VERSION = "beta 0.4.0"
DT_VERSION = "0.12.0"
PRINT_INFO = True


BACKGROUND_COLOR = None
BACKGROUND = None
GRIDCOLOR = None
RED = None
WHITE = None
TURQUOISE = None
BLUE = None
GREEN = None
PURPLE = None
PINK = None
ORANGE = None
COLORLIST = None

# libraries
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QWidget
import numpy as np
import math as m
import copy
import pickle as pc
import propertyWindow as pwindow
import typeWindow as twindow
# import namechangeWindow as nwindow
import subcircuittypeWindow as sctwindow
import dtengine as dtcore
from style import *


#static variables
DISTANCE = 30
FONTSIZE = 12
HISTORYLENGTH = 20


def printinfo():
    """shows all information about this file"""
    
    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)

class ImageWidget(QWidget):
    """image widged
        handels the circuit elements
        draws and updates them
        """
    changetounsaved = QtCore.pyqtSignal()
    changezoom = QtCore.pyqtSignal(int)
    opensubcircuit = QtCore.pyqtSignal(object)
    restoresignal = QtCore.pyqtSignal()
    setfocus = QtCore.pyqtSignal()
    
    def __init__(self, name="", mainWindowposition=None, parent=None):
        """initialises the image"""
        
        super(ImageWidget,self).__init__(parent)
        self.mainWindowposition = mainWindowposition
        self.name = name
        self.pix = QtGui.QPixmap(self.size())
        self.pix.fill(BACKGROUND_COLOR)
        self.qpainter = QtGui.QPainter()
        self.cadsurface = CadSurface(self)
        self.clickposx = None
        self.clickposy = None
        self.deltax = 0
        self.deltay = 0
        self.elements = []
        self.elementsreverse = []
        self.setMouseTracking(True)
        self.deletemode = False
        self.autoremove = False
        self.filepath = ""
        self.showinvisibelayer = 0 #0:not shown 1:allways shown 2:temporarly shown
        self.unsaved = False
        self.history = []
        self.historyposition = -1
        self.marker = [None, None, None, None]
        
    def paintEvent(self,event):
        """paint event
            redraws the image when update is called
            """
            
        self.cadsurface.update()
        self.qpainter = QtGui.QPainter()
        self.qpainter.begin(self)
        self.pix = self.pix.scaled(self.size())
        self.pix.fill(BACKGROUND_COLOR)
        self.qpainter.drawPixmap(QtCore.QPoint(), self.pix)
        self.cadsurface.draw()
        if len(self.elements) == len(self.elementsreverse) and len(self.elements) > 0:
            if  not self.elements[-1] == self.elementsreverse [0]:
                self.elements.sort(key=lambda x: x.level, reverse=False)
                self.elementsreverse = copy.copy(self.elements)
                self.elements.sort(key=lambda x: x.level, reverse=True)
        elif not len(self.elements) == len(self.elementsreverse):
            self.elements.sort(key=lambda x: x.level, reverse=False)
            self.elementsreverse = copy.copy(self.elements)
            self.elements.sort(key=lambda x: x.level, reverse=True)
        for e in self.elementsreverse:
            e.draw()
        self.drawmarker()
        self.qpainter.end()
        
    def sort(self):
        """sorts the elements after a level change"""
        
        self.elements.sort(key=lambda x: x.level, reverse=False)
        self.elementsreverse = copy.copy(self.elements)
        self.elements.sort(key=lambda x: x.level, reverse=True)
        
    def mousePressEvent(self, event):
        """activated when mouse press
            set the variabels for an drag event
            and updates all the circuit elements with the right event
            """
        
        self.setFocus()
        self.setfocus.emit()
        if event.buttons() == QtCore.Qt.LeftButton:
            self.clickposx = event.pos().x()
            self.clickposy = event.pos().y()
            devent =  DEvent("MOUSELEFTBUTTON", event.x(), event.y())
            if event.modifiers() & QtCore.Qt.ShiftModifier:
                devent.key = "SHIFT"
            for e in self.elements:
                if e.update(devent):
                    self.update()
                    break
        elif event.buttons() == QtCore.Qt.RightButton:
            devent =  DEvent("MOUSERIGHTBUTTON", event.x(), event.y())
            if event.modifiers() & QtCore.Qt.ShiftModifier:
                devent.key = "SHIFT"
            for e in self.elements:
                if e.update(devent):
                    self.update()
                    break
            
    def mouseReleaseEvent(self, event):
        """mouse release event called when drag operation is over"""
        
        self.deltax = 0
        self.deltay = 0
        if not self.marker[0] == None:
            for e in self.elements:
                x1 = self.marker[0]*self.cadsurface.zoom - self.cadsurface.posx
                x2 = self.marker[2]*self.cadsurface.zoom - self.cadsurface.posx
                y1 = self.marker[1]*self.cadsurface.zoom - self.cadsurface.posy
                y2 = self.marker[3]*self.cadsurface.zoom - self.cadsurface.posy
                if e.collide(((x1, y1), (x2, y1), (x2, y2), (x1, y2))):
                    e.setmarked()
            self.marker = [None, None, None, None]
            self.update()
        
    def mouseMoveEvent(self, event):
        """mouse move event
            updates the elements about the new mouse position
            if mouse button is held drag event
            """
            
        eventelement = False
        devent =  DEvent("MOUSEMOVE", event.x(), event.y())
        if event.modifiers() & QtCore.Qt.ShiftModifier:
            devent.key = "SHIFT"
        for e in self.elements:
            if e.update(devent):
                eventelement = True
                self.update()
                break
        if event.buttons() == QtCore.Qt.LeftButton and not eventelement == True:
            self.deltax = event.x() - self.clickposx
            self.deltay = event.y() - self.clickposy
            self.clickposx = event.x()
            self.clickposy = event.y()
            self.update()
        elif event.buttons() == QtCore.Qt.RightButton and not eventelement == True:
            if self.marker[0] == None:
                self.marker[0] = event.x()
                self.marker[1] = event.y()
                self.marker[2] = event.x()
                self.marker[3] = event.y()
            else:
                self.marker[2] = event.x()
                self.marker[3] = event.y()
            self.update()
    
    def wheelEvent(self, event):
        """zooms in or rotates an active element"""
        
        eventelement = False
        devent =  DEvent("MOUSEWHEEL", event.x(), event.y(), "", event.angleDelta().y()/120)
        for e in self.elements:
            if e.update(devent):
                eventelement = True
                self.update()
                break
        if eventelement == False:
            zoomdelta = event.angleDelta().y()/12000
            relposx = event.x() * self.cadsurface.zoom
            relposy = event.y() * self.cadsurface.zoom
            self.cadsurface.zoom = self.cadsurface.zoom + zoomdelta
            self.cadsurface.zoom = min(self.cadsurface.zoom, 1.5)
            self.cadsurface.zoom = max(self.cadsurface.zoom, 0.5)
            self.cadsurface.posx = self.cadsurface.posx - relposx + event.x() * self.cadsurface.zoom
            self.cadsurface.posy = self.cadsurface.posy - relposy + event.y() * self.cadsurface.zoom
            self.changezoom.emit(int(self.cadsurface.zoom*100))
            self.update()
            
    def mouseDoubleClickEvent(self, event):
        """mouse double click event
            to change properties of the elements
            """
            
        if event.buttons() == QtCore.Qt.LeftButton:
            devent =  DEvent("MOUSELEFTDOUBLE", event.x(), event.y())
            if event.modifiers() & QtCore.Qt.ShiftModifier:
                devent.key = "SHIFT"
            for e in self.elements:
                if e.update(devent):
                    self.update()
                    break
            for e in self.elements:
                e.marked = False
                self.update()
            
    def keyPressEvent(self, event):
        """delets an active element or activates the delete mode"""
        
        if event.key() == QtCore.Qt.Key_Delete:
            devent =  DEvent("KEY_P", 0, 0, "DEL")
            for e in self.elements:
                if e.update(devent):
                    self.update()
                    return
            if self.removemarkedelements():
                self.update()
                return
            if self.deletemode == False:
                self.deletemode = True
            else:
                self.deletemode = False
            self.update()
        if event.modifiers() & QtCore.Qt.ControlModifier:
            if event.key() == QtCore.Qt.Key_R:
                devent =  DEvent("KEY_P", 0, 0, "CTRL_R")
                for e in self.elements:
                    if e.update(devent):
                        self.update()
                        return
            if event.key() == QtCore.Qt.Key_L:
                devent =  DEvent("KEY_P", 0, 0, "CTRL_L")
                for e in self.elements:
                    if e.update(devent):
                        self.update()
                        return
            if event.key() == QtCore.Qt.Key_Z:
                self.historydown()
            if event.key() == QtCore.Qt.Key_Y:
                self.historyup()
            if event.key() == QtCore.Qt.Key_A:
                for e in self.elements:
                    e.setmarked()
                self.update()
                return
        if event.key() == QtCore.Qt.Key_Shift:
            devent =  DEvent("KEY_P", 0, 0, "SHIFT")
            for e in self.elements:
                if e.update(devent):
                    self.update()
                    return
    
    def keyReleaseEvent(self, event):
        if event.key() == QtCore.Qt.Key_Shift:
            devent =  DEvent("KEY_R", 0, 0, "SHIFT")
            for e in self.elements:
                if e.update(devent):
                    self.update()
                    return
    
    def removemarkedelements(self):
        """removes markedelements"""
        
        markedlist = []
        for e in self.elements:
            if e.marked == True:
                markedlist.append(e)
        if len(markedlist) == 0:
            return False
        
        def remove():
            for e in markedlist:
                e.remove()
                
        def add():
            for e in markedlist:
                e.add()
                
        remove()
        self.addhistory(add, remove)
        return True
    
    def addhistory(self, reversefunction, restorefunction):
        """add the current state to the history"""
        
        if not self.historyposition == len(self.history) - 1:
            self.history = self.history[0:self.historyposition + 1]
            self.historyposition = len(self.history) - 1
        self.history.append([reversefunction, restorefunction])
        self.historyposition = self.historyposition + 1
        if len(self.history) > HISTORYLENGTH:
            self.history.pop(0)
            self.historyposition = self.historyposition - 1
        # if not self.historyposition == len(self.history) - 1:
        #     self.history = self.history[0:self.historyposition + 1]
        #     self.historyposition = len(self.history) - 1
        # newentry = Savecircuit(self)
        # newentry.readyforsave()
        # newentry = copy.deepcopy(newentry)
        # newentry.removefloating()
        # self.history.append(newentry)
        # self.restore()
        # if len(self.history) > 20:
        #     self.history.pop(0)
        # self.historyposition = len(self.history) - 1
        # print(self.history)
        
    def historydown(self):
        """goes the list down"""
        
        if self.historyposition > -1:
            for e in self.elements:
                if type(e) == Line or type(e) == Vector:
                    if e.active == True:
                        self.elements.remove(e)
                        break
            self.history[self.historyposition][0]()
            self.historyposition = self.historyposition - 1
            self.setunsaved()
            self.update()
        
    def historyup(self):
        """goes the history up"""
        
        if self.historyposition + 1 < len(self.history):
            self.historyposition = self.historyposition + 1
            self.history[self.historyposition][1]()
            self.setunsaved()
            self.update()
    
    def drawmarker(self):
        """draws the marker"""
        
        if not self.marker[0] == None:
            self.drawrect(PINK, [self.marker[0], self.marker[1], self.marker[2] - self.marker[0], self.marker[3] - self.marker[1]], fill=True, alpha=50)
    
    def drawline(self, color, pos1, pos2, width=1, dashed=False):
        """draws a line"""
        
        pen = QtGui.QPen(QtGui.QColor(*color))
        pen.setWidth(width)
        if dashed:
            pen.setStyle(QtCore.Qt.DashLine)
        self.qpainter.setPen(pen)
        self.qpainter.drawLine(QtCore.QPointF(pos1[0], pos1[1]), QtCore.QPointF(pos2[0], pos2[1]))
        
    def drawrect(self, color, size, width=1, fill=False, radius=0, alpha=255):
        """draws a rect"""
        
        pen = QtGui.QPen(QtGui.QColor(*color))
        pen.setWidth(width)
        self.qpainter.setPen(pen)
        if fill==True:
            brush = QtGui.QBrush(QtGui.QColor(*color, alpha), QtCore.Qt.SolidPattern)
            self.qpainter.setBrush(brush)
        self.qpainter.drawRoundedRect(QtCore.QRectF(*size), radius, radius)
        self.qpainter.setBrush(QtCore.Qt.NoBrush)
        
    def drawellipse(self, color, size, width=1, fill=False):
        """draws an ellipse"""
        
        pen = QtGui.QPen(QtGui.QColor(*color))
        pen.setWidth(width)
        if fill==True:
            brush = QtGui.QBrush(QtGui.QColor(*color), QtCore.Qt.SolidPattern)
            self.qpainter.setBrush(brush)
        self.qpainter.setPen(pen)
        self.qpainter.drawEllipse(QtCore.QRectF(*size))
        self.qpainter.setBrush(QtCore.Qt.NoBrush)
        
    def drawarc(self, color, size, angles, width=1, fill=False):
        """draws an ellipse"""
        
        angles = list(angles)
        angles[0] = int(angles[0]*16)
        angles[1] = int(angles[1]*16)
        pen = QtGui.QPen(QtGui.QColor(*color))
        pen.setWidth(width)
        if fill==True:
            brush = QtGui.QBrush(QtGui.QColor(*color), QtCore.Qt.SolidPattern)
            self.qpainter.setBrush(brush)
        self.qpainter.setPen(pen)
        self.qpainter.drawArc(QtCore.QRectF(*size), *angles)
        self.qpainter.setBrush(QtCore.Qt.NoBrush)
    
    def drawpolygon(self, color, points, width=1, fill=False):
        """draws a polygon"""
        
        pen = QtGui.QPen(QtGui.QColor(*color))
        pen.setWidth(width)
        if fill==True:
            brush = QtGui.QBrush(QtGui.QColor(*color), QtCore.Qt.SolidPattern)
            self.qpainter.setBrush(brush)
        qpoints = []
        for p in points:
            qpoints.append(QtCore.QPointF(*p))
        self.qpainter.setPen(pen)
        self.qpainter.drawPolygon(QtGui.QPolygonF(qpoints))
        self.qpainter.setBrush(QtCore.Qt.NoBrush)
        
    def drawpolyline(self, color, points, width=1, fill=False):
        """draws a polyline"""
        
        pen = QtGui.QPen(QtGui.QColor(*color))
        pen.setWidth(width)
        if fill==True:
            brush = QtGui.QBrush(QtGui.QColor(*color), QtCore.Qt.SolidPattern)
            self.qpainter.setBrush(brush)
        qpoints = []
        for p in points:
            qpoints.append(QtCore.QPointF(*p))
        self.qpainter.setPen(pen)
        self.qpainter.drawPolyline(QtGui.QPolygonF(qpoints))
        self.qpainter.setBrush(QtCore.Qt.NoBrush)
        
    def drawtriangle(self, color, height, base, position, rotation, width=1, fill=False):
        """draws a triangle"""
        
        self.qpainter.translate(*position)
        self.qpainter.rotate(rotation)
        self.drawpolygon(color, ((0, height/2), (-base/2, -height/2), (base/2, -height/2)), width=width, fill=fill)
        self.qpainter.rotate(-rotation)
        self.qpainter.translate(-position[0], -position[1])
    
    def drawtext(self, color, position, text="", fontsize=10, fonttype="Arial", orientation=0, bold=False, italic=False):
        """draws text"""
        
        pen = QtGui.QPen(QtGui.QColor(*color))
        self.qpainter.setPen(pen)
        font = QtGui.QFont(fonttype)
        font.setPointSizeF(fontsize*0.8)
        font.setBold(bold)
        font.setItalic(italic)
        self.qpainter.setFont(font)
        self.qpainter.translate(*position)
        self.qpainter.rotate(orientation)
        self.qpainter.translate(0, fontsize/2)
        self.qpainter.drawText(0, 0, text)
        self.qpainter.translate(0, -fontsize/2)
        self.qpainter.rotate(-orientation)
        self.qpainter.translate(-position[0], -position[1])
    
    def drawimage(self, image, position, size, orientation=0):
        "draws an image"
        
        pen = QtGui.QPen(QtGui.QColor(*[255, 255, 255]))
        self.qpainter.setPen(pen)
        image = QtGui.QImage(image)
        rect = QtCore.QRect(*position, *size)
        self.qpainter.drawImage(rect, image)
    
    def setnodesid(self, maxid=0):
        """sets the ids of the nodes"""
        
        changes = True
        while changes:
            changes = False
            for e in self.elements:
                nodeid = e.setnodeid(maxid)
                if not nodeid == maxid:
                    changes = True
                maxid = max(maxid, nodeid)
        self.nodeslist = [dtcore.Node() for _ in range(maxid + 1)]
        return maxid
    
    def creatdtmodel(self, maxid=0, nodeslist=None):
        """creats CElement und Felement lists for the dt simulation"""
        self.setnodesid(maxid=maxid)
        if not nodeslist == None:
            self.nodeslist = nodeslist
        elements = copy.copy(self.elements)
        i = 0
        while len(elements) > 0:
            status = elements[i].creatdtelement(self.nodeslist)
            if status == False:
                return False
            elif status == True:
                elements.remove(elements[i])
            else:
                i = i + 1
            if i  >=len(elements):
                i = 0
        return True
    
            
    def getdtmodel(self):
        """returns two lists with CElements and FELements"""
        
        if self.creatdtmodel() == True:
            celist = []
            felist = []
            for e in self.elements:
                [c, f] = e.getdtelement(self.nodeslist)
                celist = celist + c
                felist = felist + f
            return [celist, felist]
        else:
            self.update()
            return False
    
    def cleardtmodel(self):
        """runs throught all elements and clears them"""
        
        for e in self.elements:
            e.clear()
        self.update()
        
    def restore(self):
        """restores the elements after save or load"""
        
        for e in self.elements:
            e.restore(self)
        self.cadsurface.qWidget = self
        self.restoresignal.emit()
        
    def load(self, saveobject):
        """loads the cad model from the save class"""
        
        self.name = saveobject.name
        self.elements = saveobject.elements
        self.cadsurface = saveobject.cadsurface
        self.restore()
        self.changezoom.emit(int(self.cadsurface.zoom*100))
        self.update()
    
    def loadelementsonly(self, saveobject):
        """loads only the saveobjects"""
        
        self.name = saveobject.name
        self.elements = saveobject.elements
        self.restore()
        self.update()
    
    def setunsaved(self):
        """set if changes appeared"""
        
        self.changetounsaved.emit()
        self.unsaved = True
    
    
class CadSurface:
    """background grid"""
    
    def __init__(self, qWidget):
        """inits the grid"""
        
        self.qWidget = qWidget
        self.posx = 0
        self.posy = 0
        self.width = self.qWidget.size().width()
        self.height = self.qWidget.size().height()
        self.zoom = 1
    
    def update(self):
        """sets the changed properties"""
        
        self.width = self.qWidget.size().width()
        self.height = self.qWidget.size().height()
        self.posx = self.posx + self.qWidget.deltax*self.zoom
        self.posy = self.posy + self.qWidget.deltay*self.zoom
    
    def draw(self):
        """draws the grid"""
        
        distance = DISTANCE / self.zoom
        length = 2
        for x in range(-1, m.ceil(self.width/distance)):
            for y in range(-1, m.ceil(self.height/distance)):
                posx = self.posx/self.zoom % distance + x*distance
                posy = self.posy/self.zoom % distance + y*distance
                if self.qWidget.deletemode == True:
                    self.qWidget.drawline(BLUE, (posx - length, posy), (posx + length, posy), width=1)
                    self.qWidget.drawline(BLUE, (posx, posy - length), (posx, posy + length), width=1)
                else:
                    self.qWidget.drawline(GRIDCOLOR, (posx - length, posy), (posx + length, posy), width=1)
                    self.qWidget.drawline(GRIDCOLOR, (posx, posy - length), (posx, posy + length), width=1)
    
    def clear(self):
        """clears the dt model nothing to do here"""
        
        pass

###############################################################################
# Update elements
###############################################################################

class UElement:
    """electrical circuit element part of the electric Lib"""
    
    def __init__(self, surface, propertyelement, level=1):
        """initialises
            2x2 by standard and floating
            """
        
        self.surface = surface
        self.propertyelement = propertyelement
        self.width = (1 + self.propertyelement.size[0])*DISTANCE
        self.height = (1 + self.propertyelement.size[1])*DISTANCE
        self.left = 0
        self.top = 0
        self.floating =  True
        self.justadded = True
        self.posx = 0
        self.posy = 0
        self.orientation = self.propertyelement.orientation
        self.collided = False
        self.node = []
        self.vectorin = []
        self.vectorout = []
        self.setnodes = None
        self.getnodes = None
        self.marked = False
        self.level = level
        self.dtelement = None
        self.ampermeter = None
        self.p = None
        self.failure = False
        self.errordialog = None
        
    def update(self, event):
        """updates in accordance with user input
            return True if changes
            """
        
        if event.type == "MOUSEMOVE":
            if self.floating:
                self.justadded = False
                zoom = self.surface.cadsurface.zoom
                surfaceposx = self.surface.cadsurface.posx
                surfaceposy = self.surface.cadsurface.posy
                self.posx = (round((event.x - (surfaceposx/zoom % (DISTANCE/zoom)) - self.width/2)/DISTANCE*zoom)*DISTANCE/zoom + surfaceposx/zoom % (DISTANCE/zoom))*zoom - surfaceposx
                self.posy = (round((event.y - (surfaceposy/zoom % (DISTANCE/zoom)) - self.height/2)/DISTANCE*zoom)*DISTANCE/zoom + surfaceposy/zoom % (DISTANCE/zoom))*zoom - surfaceposy
                self.collided = False
                for e in self.surface.elements:
                    if not e == self:
                        if e.collide(self._cornerpoints()):
                            self.collided = True
                            break
                return True
        elif event.type == "MOUSEWHEEL":
            if self.floating:
                oldorientation = self.orientation
                self.orientation = self.orientation + int(event.value)
                if self.orientation < 0:
                    self.orientation = 3
                elif self.orientation > 3:
                    self.orientation = 0
                if (self.orientation == 1 or self.orientation == 3) and (oldorientation == 0 or oldorientation == 2):
                    if self.propertyelement.size[1] > self.propertyelement.size[0]:
                        self.posx = self.posx - DISTANCE*int((self.propertyelement.size[1] - 1)/2)
                        self.posy = self.posy + DISTANCE*int((self.propertyelement.size[1] - 1)/2)
                    else:
                        self.posx = self.posx + DISTANCE*int((self.propertyelement.size[0] - 1)/2)
                        self.posy = self.posy - DISTANCE*int((self.propertyelement.size[0] - 1)/2)
                elif (self.orientation == 0 or self.orientation == 2) and (oldorientation == 1 or oldorientation == 3):
                    if self.propertyelement.size[1] > self.propertyelement.size[0]:
                        self.posx = self.posx + DISTANCE*int((self.propertyelement.size[1] - 1)/2)
                        self.posy = self.posy - DISTANCE*int((self.propertyelement.size[1] - 1)/2)
                    else:
                        self.posx = self.posx - DISTANCE*int((self.propertyelement.size[0] - 1)/2)
                        self.posy = self.posy + DISTANCE*int((self.propertyelement.size[0] - 1)/2)
                return True
        elif event.type == "MOUSELEFTBUTTON":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.floating and not self.collided:
                self.collided = True
                self.surface.elements.append(self.copy())
                self.add()
                self.surface.addhistory(self.remove, self.add)
                return True
            elif self.surface.deletemode and self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                self.remove()
                self.surface.addhistory(self.add, self.remove)
                return True
            elif self.floating == False and self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                if self.marked == False:
                    self.marked = True
                else:
                    self.marked = False
                return True
        elif event.type == "MOUSERIGHTBUTTON":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.floating:
                self.surface.elements.remove(self)
                return True
            else:
                if self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]) and not self.surface.deletemode:
                    floating = False
                    for e in self.surface.elements:
                        if type(e) == UElement or type(e) == CLabel or type(e) == FLabel or type(e) == SubCircuitElement:
                            if e.floating:
                                floating = True
                                break
                        elif type(e) == Line or type(e) == Vector:
                            if e.active:
                                floating = True
                                break
                    if not floating:
                        ecopy = self.copy()
                        ecopy.floating = True
                        ecopy.collided = True
                        self.surface.elements.append(ecopy)
                        if self.surface.autoremove:
                            self.remove()
                            self.surface.addhistory(self.add, self.remove)
                        return True
        if event.type == "KEY_P":
            if event.key == "DEL" and self.floating == True:
                self.surface.elements.remove(self)
                return True
            if event.key == "CTRL_R" and self.floating == True:
                self.orientation = self.orientation - 1
                if self.orientation < 0:
                    self.orientation = 3
                elif self.orientation > 3:
                    self.orientation = 0
                return True
            if event.key == "CTRL_L" and self.floating == True:
                self.orientation = self.orientation + 1
                if self.orientation < 0:
                    self.orientation = 3
                elif self.orientation > 3:
                    self.orientation = 0
                return True
        if event.type == "MOUSELEFTDOUBLE":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                self.marked = not self.marked
                self.p = pwindow.PropertyWindow(self.propertyelement, pos=self.surface.mainWindowposition())
                self.p.show()
                self.surface.setunsaved()
                return True
            
    def draw(self):
        """redraws the element"""
        
        if self.justadded == False:
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            distance8 = DISTANCE/zoom/4
            distance6 = DISTANCE/zoom/6
            distance4 = DISTANCE/zoom/2
            distance2 = DISTANCE/zoom
            if self.floating:
                if self.orientation == 0 or self.orientation == 2:
                    self.width = (1 + self.propertyelement.size[0])*DISTANCE/zoom
                    self.height = (1 + self.propertyelement.size[1])*DISTANCE/zoom
                else:
                    self.width = (1 + self.propertyelement.size[1])*DISTANCE/zoom
                    self.height = (1 + self.propertyelement.size[0])*DISTANCE/zoom
                self.left = (self.posx + surfaceposx)/zoom
                self.top = (self.posy + surfaceposy)/zoom
                if self.collided:
                    self.propertyelement.image(self.surface, (self.left, self.top), (self.width, self.height), orientation=self.orientation, color=RED)
                    if self.propertyelement.elementtype == "CE":
                        pass
                    else:
                        self.surface.drawrect(RED, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8, alpha=65, fill=True)
                        if self.orientation == 0:
                            self.surface.drawrect(RED, (self.left + distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                        elif self.orientation == 3:
                            self.surface.drawrect(RED, (self.left + self.width - distance6 - distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                        elif self.orientation == 2:
                            self.surface.drawrect(RED, (self.left + self.width - distance6 - distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                        else:
                            self.surface.drawrect(RED, (self.left + distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                    interfaceformat = [self.propertyelement.interface[: self.propertyelement.size[0]], self.propertyelement.interface[self.propertyelement.size[0] : self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[self.propertyelement.size[0] + self.propertyelement.size[1] : 2*self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[2*self.propertyelement.size[0] + self.propertyelement.size[1] :]]
                    if self.orientation == 0:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                if interfaceformat[0][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(RED, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                if interfaceformat[1][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation + 90, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(RED, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                if interfaceformat[2][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation +180, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                            self.surface.drawtext(RED, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                self.surface.drawline(RED, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                if interfaceformat[3][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation - 90, width=1, fill=True)
                    elif self.orientation == 1:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(RED, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(RED, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                if interfaceformat[0][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                if interfaceformat[1][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation + 90, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(RED, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                if interfaceformat[2][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation + 180, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                            self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                self.surface.drawline(RED, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                if interfaceformat[3][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation - 90, width=1, fill=True)
                    elif self.orientation == 2:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(RED, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                if interfaceformat[0][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(RED, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(RED, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                if interfaceformat[1][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation + 90, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                if interfaceformat[2][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation + 180, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                            self.surface.drawtext(RED, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                if interfaceformat[3][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation - 90, width=1, fill=True)
                    else:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(RED, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                if interfaceformat[0][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(RED, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                if interfaceformat[1][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation + 90, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(RED, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(RED, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                if interfaceformat[2][i] == "i":
                                    self.surface.drawtriangle(RED, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation + 180, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                           self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                           if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                               self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                               if interfaceformat[3][i] == "i":
                                   self.surface.drawtriangle(RED, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation - 90, width=1, fill=True)
                else:
                    self.propertyelement.image(self.surface, (self.left, self.top), (self.width, self.height), orientation=self.orientation, color=WHITE)
                    if self.propertyelement.elementtype == "CE":
                        pass
                    else:
                        self.surface.drawrect(WHITE, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8, alpha=65, fill=True)
                        if self.orientation == 0:
                            self.surface.drawrect(WHITE, (self.left + distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                        elif self.orientation == 3:
                            self.surface.drawrect(WHITE, (self.left + self.width - distance6 - distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                        elif self.orientation == 2:
                            self.surface.drawrect(WHITE, (self.left + self.width - distance6 - distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                        else:
                            self.surface.drawrect(WHITE, (self.left + distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                    interfaceformat = [self.propertyelement.interface[: self.propertyelement.size[0]], self.propertyelement.interface[self.propertyelement.size[0] : self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[self.propertyelement.size[0] + self.propertyelement.size[1] : 2*self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[2*self.propertyelement.size[0] + self.propertyelement.size[1] :]]
                    if self.orientation == 0:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(WHITE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(WHITE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                if interfaceformat[0][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(WHITE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(WHITE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                if interfaceformat[1][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation + 90, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(WHITE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(WHITE, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                if interfaceformat[2][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation +180, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                            self.surface.drawtext(WHITE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                self.surface.drawline(WHITE, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                if interfaceformat[3][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation - 90, width=1, fill=True)
                    elif self.orientation == 1:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(WHITE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(WHITE, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                if interfaceformat[0][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(WHITE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(WHITE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                if interfaceformat[1][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation + 90, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(WHITE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(WHITE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                if interfaceformat[2][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation + 180, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                            self.surface.drawtext(WHITE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                self.surface.drawline(WHITE, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                if interfaceformat[3][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation - 90, width=1, fill=True)
                    elif self.orientation == 2:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(WHITE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(WHITE, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                if interfaceformat[0][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(WHITE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(WHITE, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                if interfaceformat[1][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation + 90, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(WHITE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(WHITE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                if interfaceformat[2][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation + 180, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                            self.surface.drawtext(WHITE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                self.surface.drawline(WHITE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                if interfaceformat[3][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation - 90, width=1, fill=True)
                    else:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(WHITE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(WHITE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                if interfaceformat[0][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(WHITE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(WHITE, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                if interfaceformat[1][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation + 90, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(WHITE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(WHITE, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                if interfaceformat[2][i] == "i":
                                    self.surface.drawtriangle(WHITE, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation + 180, width=1, fill=True)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                           self.surface.drawtext(WHITE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                           if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                               self.surface.drawline(WHITE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                               if interfaceformat[3][i] == "i":
                                   self.surface.drawtriangle(WHITE, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation - 90, width=1, fill=True)
            elif not self.floating:
                zoom = self.surface.cadsurface.zoom
                surfaceposx = self.surface.cadsurface.posx
                surfaceposy = self.surface.cadsurface.posy
                if self.orientation == 0 or self.orientation == 2:
                    self.width = (1 + self.propertyelement.size[0])*DISTANCE/zoom
                    self.height = (1 + self.propertyelement.size[1])*DISTANCE/zoom
                else:
                    self.width = (1 + self.propertyelement.size[1])*DISTANCE/zoom
                    self.height = (1 + self.propertyelement.size[0])*DISTANCE/zoom
                self.left = (self.posx + surfaceposx)/zoom
                self.top = (self.posy + surfaceposy)/zoom
                if self.surface.deletemode == False:
                    if not self.dtelement == None:
                        if self.dtelement.fail == True:
                            self.failure = True
                    if self.failure == True:
                        self.propertyelement.image(self.surface, (self.left, self.top), (self.width, self.height), orientation=self.orientation, color=RED)
                        self.surface.drawtext(RED, (self.left + self.width*0.75, self.top + self.height*0.75), self.propertyelement.name, 16/zoom, "Arial")
                        if self.propertyelement.elementtype == "CE":
                            pass
                        else:
                            self.surface.drawrect(RED, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8, alpha=65, fill=True)
                            if self.orientation == 0:
                                self.surface.drawrect(RED, (self.left + distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                            elif self.orientation == 3:
                                self.surface.drawrect(RED, (self.left + self.width - distance6 - distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                            elif self.orientation == 2:
                                self.surface.drawrect(RED, (self.left + self.width - distance6 - distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                            else:
                                self.surface.drawrect(RED, (self.left + distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                        interfaceformat = [self.propertyelement.interface[: self.propertyelement.size[0]], self.propertyelement.interface[self.propertyelement.size[0] : self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[self.propertyelement.size[0] + self.propertyelement.size[1] : 2*self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[2*self.propertyelement.size[0] + self.propertyelement.size[1] :]]
                        if self.orientation == 0:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                self.surface.drawtext(RED, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawline(RED, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation +180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                                self.surface.drawtext(RED, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                                if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                    self.surface.drawline(RED, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[3][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation - 90, width=1, fill=True)
                        elif self.orientation == 1:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                self.surface.drawtext(RED, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                                if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawline(RED, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                self.surface.drawtext(RED, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation + 180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                                self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                    self.surface.drawline(RED, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[3][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation - 90, width=1, fill=True)
                        elif self.orientation == 2:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawline(RED, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                self.surface.drawtext(RED, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                                if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawline(RED, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation + 180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                                self.surface.drawtext(RED, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                    self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[3][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation - 90, width=1, fill=True)
                        else:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                self.surface.drawtext(RED, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawline(RED, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                self.surface.drawtext(RED, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                                if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawline(RED, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(RED, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation + 180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                               self.surface.drawtext(RED, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                               if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                   self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                   if interfaceformat[3][i] == "i":
                                       self.surface.drawtriangle(RED, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation - 90, width=1, fill=True)
                    elif self.marked == True:
                        self.propertyelement.image(self.surface, (self.left, self.top), (self.width, self.height), orientation=self.orientation, color=PINK)
                        self.surface.drawtext(PINK, (self.left + self.width, self.top + self.height), self.propertyelement.name, 16/zoom, "Arial")
                        if self.propertyelement.elementtype == "CE":
                            pass
                        else:
                            self.surface.drawrect(PINK, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8, alpha=65, fill=True)
                            if self.orientation == 0:
                                self.surface.drawrect(PINK, (self.left + distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                            elif self.orientation == 3:
                                self.surface.drawrect(PINK, (self.left + self.width - distance6 - distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                            elif self.orientation == 2:
                                self.surface.drawrect(PINK, (self.left + self.width - distance6 - distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                            else:
                                self.surface.drawrect(PINK, (self.left + distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                        interfaceformat = [self.propertyelement.interface[: self.propertyelement.size[0]], self.propertyelement.interface[self.propertyelement.size[0] : self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[self.propertyelement.size[0] + self.propertyelement.size[1] : 2*self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[2*self.propertyelement.size[0] + self.propertyelement.size[1] :]]
                        if self.orientation == 0:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                self.surface.drawtext(PINK, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawline(PINK, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                self.surface.drawtext(PINK, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawline(PINK, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                self.surface.drawtext(PINK, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawline(PINK, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation +180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                                self.surface.drawtext(PINK, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                                if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                    self.surface.drawline(PINK, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[3][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation - 90, width=1, fill=True)
                        elif self.orientation == 1:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                self.surface.drawtext(PINK, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                                if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawline(PINK, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                self.surface.drawtext(PINK, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawline(PINK, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                self.surface.drawtext(PINK, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawline(PINK, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation + 180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                                self.surface.drawtext(PINK, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                    self.surface.drawline(PINK, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[3][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation - 90, width=1, fill=True)
                        elif self.orientation == 2:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                self.surface.drawtext(PINK, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawline(PINK, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                self.surface.drawtext(PINK, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                                if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawline(PINK, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                self.surface.drawtext(PINK, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawline(PINK, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation + 180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                                self.surface.drawtext(PINK, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                    self.surface.drawline(PINK, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[3][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation - 90, width=1, fill=True)
                        else:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                self.surface.drawtext(PINK, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawline(PINK, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                self.surface.drawtext(PINK, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawline(PINK, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                self.surface.drawtext(PINK, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                                if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawline(PINK, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation + 180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                               self.surface.drawtext(PINK, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                               if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                   self.surface.drawline(PINK, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                   if interfaceformat[3][i] == "i":
                                       self.surface.drawtriangle(GREEN, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation - 90, width=1, fill=True)
                    else:
                        if self.propertyelement.elementtype == "CE":
                            self.propertyelement.image(self.surface, (self.left, self.top), (self.width, self.height), orientation=self.orientation, color=TURQUOISE)
                            self.surface.drawtext(TURQUOISE, (self.left + self.width, self.top + self.height), self.propertyelement.name, 16/zoom, "Arial")
                        else:
                            self.propertyelement.image(self.surface, (self.left, self.top), (self.width, self.height), orientation=self.orientation, color=GREEN)
                            self.surface.drawtext(GREEN, (self.left + self.width, self.top + self.height), self.propertyelement.name, 16/zoom, "Arial")
                            self.surface.drawrect(GREEN, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8, alpha=65, fill=True)
                            if self.orientation == 0:
                                self.surface.drawrect(GREEN, (self.left + distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                            elif self.orientation == 3:
                                self.surface.drawrect(GREEN, (self.left + self.width - distance6 - distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                            elif self.orientation == 2:
                                self.surface.drawrect(GREEN, (self.left + self.width - distance6 - distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                            else:
                                self.surface.drawrect(GREEN, (self.left + distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                        interfaceformat = [self.propertyelement.interface[: self.propertyelement.size[0]], self.propertyelement.interface[self.propertyelement.size[0] : self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[self.propertyelement.size[0] + self.propertyelement.size[1] : 2*self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[2*self.propertyelement.size[0] + self.propertyelement.size[1] :]]
                        if self.orientation == 0:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                if interfaceformat[0][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                elif interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                    self.surface.drawline(GREEN, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                if interfaceformat[1][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                elif interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                    self.surface.drawline(GREEN, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                if interfaceformat[2][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                elif interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                    self.surface.drawline(GREEN, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation +180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                                if interfaceformat[3][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                                elif interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                                    self.surface.drawline(GREEN, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[3][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation - 90, width=1, fill=True)
                        elif self.orientation == 1:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                if interfaceformat[0][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                                elif interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                                    self.surface.drawline(GREEN, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                if interfaceformat[1][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                elif interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                    self.surface.drawline(GREEN, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                if interfaceformat[2][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                elif interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                    self.surface.drawline(GREEN, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation + 180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                                if interfaceformat[3][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                elif interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                    self.surface.drawline(GREEN, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[3][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation - 90, width=1, fill=True)
                        elif self.orientation == 2:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                if interfaceformat[0][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                elif interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                    self.surface.drawline(GREEN, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                if interfaceformat[1][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                                elif interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                                    self.surface.drawline(GREEN, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                if interfaceformat[2][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                elif interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                    self.surface.drawline(GREEN, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation + 180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                                if interfaceformat[3][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                elif interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                    self.surface.drawline(GREEN, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[3][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation - 90, width=1, fill=True)
                        else:
                            for i, l in enumerate(self.propertyelement.labels[0]):
                                if interfaceformat[0][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                elif interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                                    self.surface.drawline(GREEN, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                    if interfaceformat[0][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width, self.top + distance2*(i + 1)), -90*self.orientation, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[1]):
                                if interfaceformat[1][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                elif interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                                    self.surface.drawline(GREEN, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                                    if interfaceformat[1][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + self.width - distance2*(i + 1), self.top + self.height), -90*self.orientation + 90, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[2]):
                                if interfaceformat[2][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                                elif interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                                    self.surface.drawline(GREEN, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                                    if interfaceformat[2][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left , self.top + self.height - distance2*(i + 1)), -90*self.orientation + 180, width=1, fill=True)
                            for i, l in enumerate(self.propertyelement.labels[3]):
                                if interfaceformat[3][i] == "n":
                                    self.surface.drawtext(TURQUOISE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                elif interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                    self.surface.drawtext(GREEN, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                                    self.surface.drawline(GREEN, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                    if interfaceformat[3][i] == "i":
                                        self.surface.drawtriangle(GREEN, 6, 6, (self.left + distance2*(i + 1), self.top), -90*self.orientation - 90, width=1, fill=True)
                elif self.surface.deletemode == True:
                    self.propertyelement.image(self.surface, (self.left, self.top), (self.width, self.height), orientation=self.orientation, color=BLUE)
                    self.surface.drawtext(BLUE, (self.left + self.width, self.top + self.height), self.propertyelement.name, 16/zoom, "Arial")
                    if self.propertyelement.elementtype == "CE":
                        pass
                    else:
                        self.surface.drawrect(BLUE, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8, alpha=65, fill=True)
                        if self.orientation == 0:
                            self.surface.drawrect(BLUE, (self.left + distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                        elif self.orientation == 3:
                            self.surface.drawrect(BLUE, (self.left + self.width - distance6 - distance8, self.top + distance8, distance6, distance6), width = 1, fill=True)
                        elif self.orientation == 2:
                            self.surface.drawrect(BLUE, (self.left + self.width - distance6 - distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                        else:
                            self.surface.drawrect(BLUE, (self.left + distance8, self.top + self.height - distance6 - distance8, distance6, distance6), width = 1, fill=True)
                    interfaceformat = [self.propertyelement.interface[: self.propertyelement.size[0]], self.propertyelement.interface[self.propertyelement.size[0] : self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[self.propertyelement.size[0] + self.propertyelement.size[1] : 2*self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[2*self.propertyelement.size[0] + self.propertyelement.size[1] :]]
                    if self.orientation == 0:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(BLUE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(BLUE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(BLUE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(BLUE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(BLUE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(BLUE, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                            self.surface.drawtext(BLUE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                self.surface.drawline(BLUE, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                    elif self.orientation == 1:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(BLUE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(BLUE, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(BLUE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(BLUE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(BLUE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(BLUE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                            self.surface.drawtext(BLUE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                self.surface.drawline(BLUE, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                    elif self.orientation == 2:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(BLUE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(BLUE, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(BLUE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(BLUE, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(BLUE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(BLUE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                            self.surface.drawtext(BLUE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                                self.surface.drawline(BLUE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                    else:
                        for i, l in enumerate(self.propertyelement.labels[0]):
                            self.surface.drawtext(BLUE, (self.left + self.width + DISTANCE/zoom*0.1, self.top + DISTANCE/zoom*(0.6 + i)), l, 12/zoom, "Arial")
                            if interfaceformat[0][i] == "i" or interfaceformat[0][i] == "o":
                                self.surface.drawline(BLUE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                        for i, l in enumerate(self.propertyelement.labels[1]):
                            self.surface.drawtext(BLUE, (self.left + DISTANCE/zoom*(0.4 + self.propertyelement.size[1] - i), self.top + self.height + DISTANCE/zoom*0.1 + len(l)*8/zoom), l, 12/zoom, "Arial", orientation=-90)
                            if interfaceformat[1][i] == "i" or interfaceformat[1][i] == "o":
                                self.surface.drawline(BLUE, (self.left + self.width - distance2*(i + 1), self.top + self.height), (self.left + self.width - distance2*(i + 1), self.top + self.height - distance8), width=1)
                        for i, l in enumerate(self.propertyelement.labels[2]):
                            self.surface.drawtext(BLUE, (self.left - DISTANCE/zoom*0.1 - len(l)*8/zoom, self.top + DISTANCE/zoom*(0.4 + self.propertyelement.size[0] - i)), l, 12/zoom, "Arial")
                            if interfaceformat[2][i] == "i" or interfaceformat[2][i] == "o":
                                self.surface.drawline(BLUE, (self.left + distance8, self.top + self.height - distance2*(i + 1)), (self.left , self.top + self.height - distance2*(i + 1)), width=1)
                        for i, l in enumerate(self.propertyelement.labels[3]):
                           self.surface.drawtext(BLUE, (self.left + DISTANCE/zoom*(0.6 + i), self.top - DISTANCE/zoom*0.1), l, 12/zoom, "Arial", orientation=-90)
                           if interfaceformat[3][i] == "i" or interfaceformat[3][i] == "o":
                               self.surface.drawline(BLUE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
    
    def copy(self):
        """copys the element"""
        
        ecopy = copy.copy(self)
        ecopy.node = []
        ecopy.vectorin = []
        ecopy.vectorout = []
        if not self.propertyelement.element == None:
            self.propertyelement.element = None
            ecopy.propertyelement = copy.deepcopy(self.propertyelement)
            self.propertyelement.element = self
        else:
            ecopy.propertyelement = copy.deepcopy(self.propertyelement)
        return ecopy
    
    def _cornerpoints(self):
        """returns the coner points of the element"""
        
        if self.orientation == 0 or self.orientation == 2:
            width = 1 + self.propertyelement.size[0]
            height = 1 + self.propertyelement.size[1]
        else:
            width = 1 + self.propertyelement.size[1]
            height = 1 + self.propertyelement.size[0]
        return ((self.posx + 1, self.posy + 1), (self.posx + width*DISTANCE - 1, self.posy + 1), (self.posx + width*DISTANCE - 1, self.posy + height*DISTANCE - 1), (self.posx + 1, self.posy + height*DISTANCE - 1))
    
    def add(self):
        """adds the element to the surface"""
        
        if not self in self.surface.elements:
            self.surface.elements.append(self)
        self.collided = False
        self.floating = False
        self.propertyelement.element = self
        interfaceformat = [self.propertyelement.interface[: self.propertyelement.size[0]], self.propertyelement.interface[self.propertyelement.size[0] : self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[self.propertyelement.size[0] + self.propertyelement.size[1] : 2*self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[2*self.propertyelement.size[0] + self.propertyelement.size[1] :]]
        for i, n in enumerate(interfaceformat[0]):
            if n == "n":
                if self.orientation==0:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(i + 1), self.posy)))
                elif self.orientation==1:
                    self.surface.elements.append(Node(self.surface, self, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[0] - i))))
                elif self.orientation==2:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(self.propertyelement.size[0] - i), self.posy + (self.propertyelement.size[1] + 1)*DISTANCE)))
                else:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + (self.propertyelement.size[1] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1))))
                self.node.append(self.surface.elements[-1])
            elif n == "i":
                if self.orientation==0:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(1 + i), self.posy), orientation=3))
                elif self.orientation==1:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[0] - i)), orientation=0))
                elif self.orientation==2:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(self.propertyelement.size[0] - i), self.posy + (self.propertyelement.size[1] + 1)*DISTANCE), orientation=1))
                else:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + (self.propertyelement.size[1] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1)), orientation=2))
                self.vectorin.append(self.surface.elements[-1])
            elif n == "o":
                if self.orientation==0:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(1 + i), self.posy), orientation=1))
                elif self.orientation==1:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[0] - i)), orientation=2))
                elif self.orientation==2:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(self.propertyelement.size[0] - i), self.posy + (self.propertyelement.size[1] + 1)*DISTANCE), orientation=3))
                else:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + (self.propertyelement.size[1] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1)), orientation=0))
                self.vectorout.append(self.surface.elements[-1])
        for i, n in enumerate(interfaceformat[1]):
            if n == "n":
                if self.orientation==0:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + (self.propertyelement.size[0] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1))))
                elif self.orientation==1:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(i + 1), self.posy)))
                elif self.orientation==2:
                    self.surface.elements.append(Node(self.surface, self, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[1] - i))))
                else:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(self.propertyelement.size[1] - i), self.posy + (self.propertyelement.size[0] + 1)*DISTANCE)))
                self.node.append(self.surface.elements[-1])
            elif n == "i":
                if self.orientation==0:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + (self.propertyelement.size[0] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1)), orientation=2))
                elif self.orientation==1:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(1 + i), self.posy), orientation=3))
                elif self.orientation==2:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[1] - i)), orientation=0))
                else:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(self.propertyelement.size[1] - i), self.posy + (self.propertyelement.size[0] + 1)*DISTANCE), orientation=1))
                self.vectorin.append(self.surface.elements[-1])
            elif n == "o":
                if self.orientation==0:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + (self.propertyelement.size[0] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1)), orientation=0))
                elif self.orientation==1:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(1 + i), self.posy), orientation=1))
                elif self.orientation==2:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[1] - i)), orientation=2))
                else:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(self.propertyelement.size[1] - i), self.posy + (self.propertyelement.size[0] + 1)*DISTANCE), orientation=3))
                self.vectorout.append(self.surface.elements[-1])
        for i, n in enumerate(interfaceformat[2]):
            if n == "n":
                if self.orientation==0:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(self.propertyelement.size[0] - i), self.posy + (self.propertyelement.size[1] + 1)*DISTANCE)))
                elif self.orientation==1:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + (self.propertyelement.size[1] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1))))
                elif self.orientation==2:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(i + 1), self.posy)))
                else:
                    self.surface.elements.append(Node(self.surface, self, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[0] - i))))
                self.node.append(self.surface.elements[-1])
            elif n == "i":
                if self.orientation==0:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(self.propertyelement.size[0] - i), self.posy + (self.propertyelement.size[1] + 1)*DISTANCE), orientation=1))
                elif self.orientation==1:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + (self.propertyelement.size[1] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1)), orientation=2))
                elif self.orientation==2:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(1 + i), self.posy), orientation=3))
                else:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[0] - i)), orientation=0))
                self.vectorin.append(self.surface.elements[-1])
            elif n == "o":
                if self.orientation==0:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(self.propertyelement.size[0] - i), self.posy + (self.propertyelement.size[1] + 1)*DISTANCE), orientation=3))
                elif self.orientation==1:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + (self.propertyelement.size[1] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1)), orientation=0))
                elif self.orientation==2:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(1 + i), self.posy), orientation=1))
                else:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[0] - i)), orientation=2))
                self.vectorout.append(self.surface.elements[-1])
        for i, n in enumerate(interfaceformat[3]):
            if n == "n":
                if self.orientation==0:
                    self.surface.elements.append(Node(self.surface, self, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[1] - i))))
                elif self.orientation==1:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(self.propertyelement.size[1] - i), self.posy + (self.propertyelement.size[0] + 1)*DISTANCE)))
                elif self.orientation==2:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + (self.propertyelement.size[0] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1))))
                else:
                    self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(i + 1), self.posy)))
                self.node.append(self.surface.elements[-1])
            elif n == "i":
                if self.orientation==0:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[1] - i)), orientation=0))
                elif self.orientation==1:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(self.propertyelement.size[1] - i), self.posy + (self.propertyelement.size[0] + 1)*DISTANCE), orientation=1))
                elif self.orientation==2:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + (self.propertyelement.size[0] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1)), orientation=2))
                else:
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(1 + i), self.posy), orientation=3))
                self.vectorin.append(self.surface.elements[-1])
            elif n == "o":
                if self.orientation==0:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx, self.posy + DISTANCE*(self.propertyelement.size[1] - i)), orientation=2))
                elif self.orientation==1:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(self.propertyelement.size[1] - i), self.posy + (self.propertyelement.size[0] + 1)*DISTANCE), orientation=3))
                elif self.orientation==2:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + (self.propertyelement.size[0] + 1)*DISTANCE, self.posy + DISTANCE*(i + 1)), orientation=0))
                else:
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(1 + i), self.posy), orientation=1))
                self.vectorout.append(self.surface.elements[-1])
        for j, n in enumerate(self.node):
            for i in self.surface.elements:
                if type(i) == Node:
                    if not i == n and abs(i.x1-n.x1)<2 and abs(i.y1-n.y1)<2:
                        self.surface.elements.remove(n)
                        i.addelement(self)
                        self.node[j] = i
                        break
            for i in self.surface.elements:
                if type(i) == Line:
                    if i.collide(((n.x1 - 5, n.y1 - 5), (n.x1 + 5, n.y1 + 5))):
                        n.addelement(i)
                        break
        for j, v in enumerate(self.vectorin):
            for i in self.surface.elements:
                if type(i) == Arrow:
                    if not i == v and abs(i.x1-v.x1)<2 and abs(i.y1-v.y1)<2:
                            self.surface.elements.remove(v)
                            i.addoutelement(self)
                            self.vectorin[j] = i
                            break
                if type(i) == Vector:
                    if i.collide(((v.x1 - 7, v.y1  - 7), (v.x1 + 7, v.y1 + 7))):
                        v.addinelement(i)
                        break
        for j, v in enumerate(self.vectorout):
            for i in self.surface.elements:
                if type(i) == Arrow:
                    if not i == v and abs(i.x1-v.x1)<2 and abs(i.y1-v.y1)<2:
                        if i.addinelement(self):
                            self.surface.elements.remove(v)
                            self.vectorout[j] = i
                            break
        self.surface.setunsaved()
    
    def remove(self):
        """removes the element and sets the states of the nodes properly"""
        
        self.surface.elements.remove(self)
        for n in self.node:
            n.removeelement(self)
        for v in self.vectorin:
            v.removeelement(self)
        for v in self.vectorout:
            v.removeelement(self)
        self.vectorin = []
        self.vectorout = []
        #self.surface.setunsaved()
        
    def creatdtelement(self, nodeslist):
        """creats the dt element returns True if created
            returns False if not possible
            None if some inputs are missing at the moment"""
        
        if self.dtelement == None:
            try:
                self.dtelement = self.propertyelement.creat()
            except Exception as e:
                self.failure = True
                self.errordialog = QtWidgets.QErrorMessage()
                self.errordialog.showMessage(f"{self.propertyelement.name} failed to creat: {e}")
                appicon = QtGui.QIcon()
                appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
                self.errordialog.setWindowIcon(appicon)
                self.errordialog.setWindowTitle("deltaTau")
                return False
        
            for i, _ in enumerate(self.propertyelement.outputs):
                self.propertyelement.outputs[i] = self.dtelement.getoutputs()[i]
        for i, v in enumerate(self.vectorin):
            self.propertyelement.inputs[i] = v.getsource(self)
        for i in self.propertyelement.inputs:
            if i == None or i == False:
                return i
        self.dtelement.setinputs(self.propertyelement.inputs)
        return True
            
        
    def getdtelement(self, nodeslist):
        """returns the dt element"""
        
        if not self.dtelement == None:
            nodes = []
            for n in self.node:
                nodes.append(nodeslist[n.nodeid])
            self.dtelement.setnodes(nodes)
        return [[self.dtelement], []]
        
    def getsource(self, toelement):
        """returns the output"""
        
        for i, v in enumerate(self.vectorout):
            if v == toelement:
                return self.propertyelement.outputs[i]
        
    def updatesize(self, inputsize, outputsize):
        
        self.remove()
        interfaceformat = [self.propertyelement.interface[: self.propertyelement.size[0]], self.propertyelement.interface[self.propertyelement.size[0] : self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[self.propertyelement.size[0] + self.propertyelement.size[1] : 2*self.propertyelement.size[0] + self.propertyelement.size[1]], self.propertyelement.interface[2*self.propertyelement.size[0] + self.propertyelement.size[1] :]]
        inputs = ["i"]*inputsize
        outputs = ["o"]*outputsize
        if inputsize > outputsize:
            outputs = outputs + [""]*(inputsize - outputsize)
        elif outputsize > inputsize:
            inputs = [""]*(outputsize - inputsize) + inputs
        self.propertyelement.interface = interfaceformat[0] + outputs + interfaceformat[2] + inputs
        self.propertyelement.size[1] = max(inputsize, outputsize)
        self.propertyelement.labels[1] = [""]*self.propertyelement.size[1]
        self.propertyelement.labels[3] = [""]*self.propertyelement.size[1]
        self.width = (1 + self.propertyelement.size[0])*DISTANCE
        self.height = (1 + self.propertyelement.size[1])*DISTANCE
        self.propertyelement.nodes = []
        self.propertyelement.inputs = []
        self.propertyelement.outputs = []
        for i in self.propertyelement.interface:
            if i == "n":
                self.propertyelement.nodes.append(None)
            elif i == "i":
                self.propertyelement.inputs.append(None)
            elif i == "o":
                self.propertyelement.outputs.append(None)
        self.add()
        
    def collide(self, points):
        """checks for collision with the given points
            and lines between the points
            """
        if self.orientation == 0 or self.orientation == 2:
            width = 1 + self.propertyelement.size[0]
            height = 1 + self.propertyelement.size[1]
        else:
            width = 1 + self.propertyelement.size[1]
            height = 1 + self.propertyelement.size[0]
        for p in points:
            if p[0] >= self.posx + 1 and p[0] <= self.posx + width*DISTANCE - 1 and p[1] >= self.posy + 1 and p[1] <= self.posy + height*DISTANCE - 1:
                return True
        for i, p in enumerate(points):
            if i > 0:
                if not points[i-1][0] == points[i][0]:
                    a = (points[i-1][1] - points[i][1])/(points[i-1][0] - points[i][0])
                    b = points[i-1][1] - a * points[i-1][0]
                    if points[i-1][0] >= self.posx + 1 and points[i][0] <= self.posx + 1 or points[i-1][0] <= self.posx + 1 and points[i][0] >= self.posx + 1:
                        y = a * (self.posx + 1) + b
                        if y >= self.posy + 1 and y <= self.posy + height*DISTANCE - 1:
                            return True
                    if points[i-1][0] >= self.posx + width*DISTANCE - 1 and points[i][0] <= self.posx + width*DISTANCE - 1 or points[i-1][0] <= self.posx + width*DISTANCE - 1 and points[i][0] >= self.posx + width*DISTANCE - 1:
                        y = a * (self.posx + width*DISTANCE - 1) + b
                        if y >= self.posy + 1 and y <= self.posy + height*DISTANCE - 1:
                            return True
                if not points[i-1][1] == points[i][1]:
                    a = (points[i-1][0] - points[i][0])/(points[i-1][1] - points[i][1])
                    b = points[i-1][0] - a * points[i-1][1]
                    if points[i-1][1] >= self.posy + 1 and points[i][1] <= self.posy + 1 or points[i-1][1] <= self.posy + 1 and points[i][1] >= self.posy + 1:
                        x = a * (self.posy + 1) + b
                        if x >= self.posx + 1 and x <= self.posx + width*DISTANCE - 1:
                            return True
                    if points[i-1][1] >= self.posy + height*DISTANCE - 1 and points[i][1] <= self.posy + height*DISTANCE - 1 or points[i-1][1] <= self.posy + height*DISTANCE - 1 and points[i][1] >= self.posy + height*DISTANCE - 1:
                        x = a * (self.posy + height*DISTANCE - 1) + b
                        if x >= self.posx + 1 and x <= self.posx + width*DISTANCE - 1:
                            return True
        xfree = True
        yfree = True
        x1 = self.posx + 1 
        x2 = self.posx + width*DISTANCE - 1
        y1 = self.posy + 1
        y2 = self.posy + height*DISTANCE - 1
        xcount = 0
        for p in points:
            if p[0] < x1 and p[0] < x2 :
                xcount = xcount + 1
            elif (p[0] < x1 and p[0] > x2) or (p[0] > x1 and p[0] < x2):
                xfree = False
        if xcount > 0 and not len(points) == xcount:
            xfree = False
        ycount = 0
        for p in points:
            if p[1] < y1 and p[1] < y2:
                ycount = ycount + 1
            elif (p[1] < y1 and p[1] > y2) or (p[1] > y1 and p[1] < y2):
                yfree = False
        if ycount > 0 and not len(points) == ycount:
            yfree = False
        if yfree == False and xfree == False:
            return True
        return False
    
    def clear(self):
        """clear the dtmodel"""
        
        self.errordialog = None
        if not self.dtelement == None:
            self.dtelement.clear()
        self.dtelement = None
        self.setnodes = None
        self.getnodes = None
        self.propertyelement.clear()
        #self.ampermeter = None

    def setnodeid(self, maxid):
        """sets the nodeids of the nodes"""
        
        for n in self.node:
            if n.nodeid == -1:
                n.nodeid = maxid + 1
                maxid = maxid + 1
        return maxid
    
    def readyforsave(self):
        """makes the element ready for save"""
        
        self.surface = None
        
    def restore(self, surface):
        """restores the object after save"""
        
        self.surface = surface
        
    def setmarked(self):
        """marks the element"""
        
        self.marked = True


###############################################################################
# electrical elements
###############################################################################

class Node:
    """an electrical Node"""
    
    def __init__(self, surface, element1, position, level=3):
        """inits the node with one elemnt given
            nodes without elements do not exist
            """
        self.surface = surface
        self.x1 = position[0]
        self.y1 = position[1]
        self.over = False
        self.modifierover = False
        self.closed = 0
        self.elements = [element1]
        self.marked = False
        self.level = level
        self.nodeid = -1
        
    def update(self, event):
        """updates the node in accordance to the user input"""
        
        if event.type == "MOUSEMOVE":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            actualover = self.over
            self.over = False
            self.modifierover = False
            if ((event.x - (self.x1 + surfaceposx)/zoom)**2 + (event.y - (self.y1 + surfaceposy)/zoom)**2)<=16 and self.closed == 0:
                self.over = True
                return True
            if ((event.x - (self.x1 + surfaceposx)/zoom)**2 + (event.y - (self.y1 + surfaceposy)/zoom)**2)<=16 and event.key == "SHIFT":
                self.over = True
                self.modifierover = True
                return True
            if not actualover == self.over:
                return True
        if event.type == "MOUSELEFTBUTTON":
            if (self.closed == False or self.modifierover == True) and self.surface.deletemode == False:
                if self.over == True:
                    self.over = False
                    self.modifierover = False
                    self.surface.elements.append(Line(self.surface, self, (self.x1, self.y1)))
                    return True
            
    def addelement(self, element):
        """adds an element to the node"""
        
        if not element in self.elements:
            self.elements.append(element)
        if len(self.elements) == 1:
            self.closed = 0
        if len(self.elements) == 2:
            self.closed = 2
        if len(self.elements) > 2:
            self.closed = 1
        if self.closed == 2:
            for e in self.elements:
                if type(e) == Line:
                    if not e.node1 == self and not e.node2 == self:
                        self.closed = 1
                        break
            
    def removeelement(self, element):
        """removes the element from the node"""
        
        if element in self.elements:
            self.elements.remove(element)
        if len(self.elements) == 1:
            self.closed = 0
        if len(self.elements) == 2:
            self.closed = 2
        if len(self.elements) > 2:
            self.closed = 1
        if len(self.elements) == 0:
            self.surface.elements.remove(self)
        elif self.closed == 2:
            for e in self.elements:
                if type(e) == Line:
                    if not e.node1 == self and not e.node2 == self:
                        self.closed = 1
                        break
        elif len(self.elements) == 1:
            if type(self.elements[0]) == Line:
                if not self.elements[0].node1 == self and not self.elements[0].node2 == self:
                    self.surface.elements.remove(self)
            
    def draw(self):
        """draws the node"""
        
        if self.surface.deletemode == False:
            self.marked = False
            for e in self.elements:
                self.marked = self.marked or e.marked
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.closed == 0 or self.modifierover == True:
                if self.over:
                    self.surface.drawellipse(WHITE, [(self.x1 + surfaceposx)/zoom - 4, (self.y1 + surfaceposy)/zoom - 4, 8, 8], 1, True)
                    self.surface.drawellipse(WHITE, [(self.x1 + surfaceposx)/zoom - 6, (self.y1 + surfaceposy)/zoom - 6, 12, 12])
                else:
                    if self.marked:
                        self.surface.drawellipse(PINK, [(self.x1 + surfaceposx)/zoom - 4, (self.y1 + surfaceposy)/zoom - 4, 8, 8], 1, True)
                    else:
                        self.surface.drawellipse(TURQUOISE, [(self.x1 + surfaceposx)/zoom - 4, (self.y1 + surfaceposy)/zoom - 4, 8, 8], 1, True)
            elif self.closed == 1:
                if self.marked:
                    self.surface.drawellipse(PINK, [(self.x1 + surfaceposx)/zoom - 3, (self.y1 + surfaceposy)/zoom - 3, 6, 6], 1, True)
                else:
                    self.surface.drawellipse(TURQUOISE, [(self.x1 + surfaceposx)/zoom - 3, (self.y1 + surfaceposy)/zoom - 3, 6, 6], 1, True)
        else:
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            self.surface.drawrect(BLUE, ((self.x1 + surfaceposx)/zoom - 4, (self.y1 + surfaceposy)/zoom - 4, 8, 8), width=1, fill=True)
            
    def collide(self, points):
        """collide function not used"""
        
        return False
    
    def clear(self):
        """clears the dt model"""
        
        self.nodeid = -1

    def setnodeid(self, maxid):
        """sets the nodeid"""
        
        nodeid = -1
        changes = False
        for e in self.elements:
            if type(e) == Node or type(e) == Line:
                nodeid = max(e.nodeid, nodeid, self.nodeid)
                if not nodeid == e.nodeid or not nodeid == self.nodeid:
                    changes = True
        if not changes:
            return maxid
        if nodeid == -1:
            nodeid = maxid + 1
        for e in self.elements:
            if type(e) == Node or type(e) == Line:
                e.nodeid = nodeid
        self.nodeid = nodeid
        if nodeid == maxid:
            return -1
        return nodeid
    
    def remove(self):
        """does nothing"""
        
        pass
    
    def creatdtelement(self, nodeslist):
        """does nothing"""
        
        return True
    
    def getdtelement(self, nodeslist):
        """returns two emtpy lists"""
        
        return [[], []]
    
    def readyforsave(self):
        """makes the element ready for save"""
        
        self.surface = None
        
    def restore(self, surface):
        """restores the object after save"""
        
        self.surface = surface
        
    def setmarked(self):
        """marks the element"""
        
        pass
        

class Line:
    """a line is a connection between two nodes"""
    
    def __init__(self, surface, node, position, level=4):
        """inits the line
            is floating when created
            """
            
        self.surface = surface
        self.x1 = position[0]
        self.y1 =  position[1]
        self.x2 =  position[0]
        self.y2 = position[1]
        self.active = True
        self.collided = False
        self.node1 = node
        self.node2 = None
        self.marked = False
        self.level = level
        self.nodeid = -1
        
    def update(self, event):
        """updates the Line in accordance with user input"""
        
        if event.type == "MOUSELEFTBUTTON":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.active==True:
                if self.collided==False:
                    if not (self.x1 == self.x2 and self.y1 == self.y2):
                        # self.add(half=True)
                        self.surface.addhistory(self.add(half=True), self.add)
                        self.surface.setunsaved()
                        # self.surface.addhistory(self.remove, self.add)
                        return True
            if self.surface.deletemode and self.collide(((event.x*zoom - surfaceposx - 5, event.y*zoom -surfaceposy - 5), (event.x*zoom - surfaceposx + 5, event.y*zoom - surfaceposy - 5), (event.x*zoom - surfaceposx + 5, event.y*zoom -surfaceposy + 5), (event.x*zoom - surfaceposx - 5, event.y*zoom - surfaceposy + 5))):
                self.remove()
                self.surface.setunsaved()
                self.surface.addhistory(self.add, self.remove)
                return True
            if self.collide(((event.x*zoom - surfaceposx - 5, event.y*zoom -surfaceposy - 5), (event.x*zoom - surfaceposx + 5, event.y*zoom - surfaceposy - 5), (event.x*zoom - surfaceposx + 5, event.y*zoom -surfaceposy + 5), (event.x*zoom - surfaceposx - 5, event.y*zoom - surfaceposy + 5))):
                self.marked = not self.marked
                return True
        if event.type == "MOUSERIGHTBUTTON":
            if self.active==True:
                self.surface.elements.remove(self)
                self.node1.element2 = None
                return True
        if event.type == "MOUSEMOVE":
            if self.active == True:
                zoom = self.surface.cadsurface.zoom
                surfaceposx = self.surface.cadsurface.posx
                surfaceposy = self.surface.cadsurface.posy
                x2 = (round((event.x - (surfaceposx/zoom % (DISTANCE/zoom)))/DISTANCE*zoom)*DISTANCE/zoom + surfaceposx/zoom % (DISTANCE/zoom))*zoom - surfaceposx
                y2 = (round((event.y - (surfaceposy/zoom % (DISTANCE/zoom)))/DISTANCE*zoom)*DISTANCE/zoom + surfaceposy/zoom % (DISTANCE/zoom))*zoom - surfaceposy
                if abs(x2 - self.x1)>abs(y2 - self.y1)*2:
                    self.x2 = x2
                    self.y2 = self.y1
                elif 2*abs(x2 - self.x1)<abs(y2 - self.y1):
                    self.x2 = self.x1
                    self.y2 = y2
                else:
                    deltax = self.x1 - x2
                    deltay = self.y1 - y2
                    delta = round((abs(deltax) + abs(deltay))/2/DISTANCE)*DISTANCE
                    self.x2 = self.x1 - np.sign(deltax)*delta
                    self.y2 = self.y1 - np.sign(deltay)*delta
                self.collided = False
                for e in self.surface.elements:
                    if not e == self and (type(e) == UElement  or type(e) == CLabel or type(e) == FLabel or type(e) == SubCircuitElement):
                        if e.collide(((self.x1, self.y1), (self.x2, self.y2))):
                            self.collided = True
                            break
                return True
        if event.type == "KEY_P":
            if event.key == "DEL" and self.active == True:
                self.surface.elements.remove(self)
                return True
                   
    def draw(self):
        """draws the line"""
        
        zoom = self.surface.cadsurface.zoom
        surfaceposx = self.surface.cadsurface.posx
        surfaceposy = self.surface.cadsurface.posy
        if self.active==True:
            if self.collided==True:
                self.surface.drawline(RED, ((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), ((self.x2 + surfaceposx)/zoom, (self.y2 + surfaceposy)/zoom), width=1)
            else:
                self.surface.drawline(WHITE, ((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), ((self.x2 + surfaceposx)/zoom, (self.y2 + surfaceposy)/zoom), width=1)
        elif self.surface.deletemode:
            self.surface.drawline(BLUE, ((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), ((self.x2 + surfaceposx)/zoom, (self.y2 + surfaceposy)/zoom), width=1)
        else:
            if self.marked:
                self.surface.drawline(PINK, ((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), ((self.x2 + surfaceposx)/zoom, (self.y2 + surfaceposy)/zoom), width=1)
            else:
                self.surface.drawline(TURQUOISE, ((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), ((self.x2 + surfaceposx)/zoom, (self.y2 + surfaceposy)/zoom), width=1)
                
    def remove(self):
        """removes the line"""
        
        self.surface.elements.remove(self)
        for e in copy.copy(self.surface.elements):
            if type(e) == Node:
                e.removeelement(self)
            
    
    def add(self, half=False):
        """adds the line"""
        
        addwhenremove = []
        self.active =  False
        self.level = 2
        self.surface.sort()
                
        if half == True:
            self.node1.addelement(self)
        else:
            self.surface.elements.append(self)
            for i in self.surface.elementsreverse:
                if type(i) == Line:
                    if not i == self:
                        line = i.lineover(self)
                        if not line == None:
                            addwhenremove.append(line)
            self.node1 = Node(self.surface, self, (self.x1, self.y1))
            self.surface.elements.append(self.node1)
            noded = False
            for i in self.surface.elements:
                if type(i) == Node:
                    if not i == self.node1 and not i == self.node2 and abs(i.x1-self.x1)<2 and abs(i.y1-self.y1)<2:
                        self.surface.elements.remove(self.node1)
                        self.node1 = i
                        i.addelement(self)
                        noded = True
                        break
            for i in self.surface.elements:
                if type(i) == Node:
                    if not i == self.node1 and not i == self.node2  and self.collide([[i.x1, i.y1]]) and i.closed == 0:
                        i.addelement(self)
            if not noded:
                for i in self.surface.elements:
                    if type(i) == Line:
                        if i.collide(((self.x1 - 5, self.y1 - 5), (self.x1 + 5, self.y1 + 5))) and not i==self:
                            self.node1.addelement(i)
                            break
        for i in self.surface.elementsreverse:
            if type(i) == Line:
                if not i == self:
                    # i.lineover(self)
                    line = i.lineover(self)
                    if not line == None and not line in addwhenremove:
                        addwhenremove.append(line)
        self.node2 = Node(self.surface, self, (self.x2, self.y2))
        self.surface.elements.append(self.node2)
        noded = False
        for i in self.surface.elements:
            if type(i) == Node:
                if not i == self.node1 and not i == self.node2 and abs(i.x1-self.x2)<2 and abs(i.y1-self.y2)<2:
                    self.surface.elements.remove(self.node2)
                    self.node2 = i
                    i.addelement(self)
                    noded = True
                    break
        for i in self.surface.elements:
            if type(i) == Node:
                if not i == self.node1 and not i == self.node2  and self.collide([[i.x1, i.y1]]) and i.closed == 0:
                    i.addelement(self)
        def remove():
            self.remove()
            for e in addwhenremove:
                e.add()
        
        if not noded:
            for i in self.surface.elements:
                if type(i) == Line:
                    if i.collide(((self.x2 - 5, self.y2 - 5), (self.x2 + 5, self.y2 + 5))) and not i==self:
                        self.node2.addelement(i)
                        return remove
        else:
            return remove
        if half == True:
            self.surface.elements.append(Line(self.surface, self.node2, (self.x2, self.y2)))
        return remove
    
    def collide(self, points):
        """checks if points collide with the line
            or the lines between the points collide with the line
            """  
            
        if len(points) == 1:
            if not self.x1 == self.x2:
                c = (self.y1 - self.y2)/(self.x1 - self.x2)
                d = self.y1 - c * self.x1
                if (c*points[0][0] + d) == points[0][1] and ((self.x1 < points[0][0] and self.x2 > points[0][0]) or (self.x1 > points[0][0] and self.x2 < points[0][0])):
                    return True
            if not self.y1 == self.y2:
                c = (self.x1 - self.x2)/(self.y1 - self.y2)
                d = self.x1 - c * self.y1
                if (c*points[0][1] + d) == points[0][0] and ((self.y1 < points[0][1] and self.y2 > points[0][1]) or (self.y1 > points[0][1] and self.y2 < points[0][1])):
                    return True
        for i, p in enumerate(points):
            j = i - 1
            if not points[j][0] == points[i][0]:
                a = (points[j][1] - points[i][1])/(points[j][0] - points[i][0])
                b = points[j][1] - a * points[j][0]
                if not self.x1 == self.x2:
                    c = (self.y1 - self.y2)/(self.x1 - self.x2)
                    d = self.y1 - c * self.x1
                    if not a == c:
                        x = (d - b)/(a - c)
                        xlist = [self.x1, self.x2, points[j][0], points[i][0]]
                        xlist.sort()
                        if x >= xlist[1] and x <= xlist[2] and not min(points[j][0], points[i][0]) >= max(self.x1, self.x2) and not min(self.x1, self.x2) >= max(points[j][0], points[i][0]):
                            return True
                if not self.y1 == self.y2:
                    c = (self.x1 - self.x2)/(self.y1 - self.y2)
                    d = self.x1 - c * self.y1
                    if not (1 - c * a) == 0:
                        x = (c * b + d)/(1 - c * a)
                        xlist = [self.x1, self.x2, points[j][0], points[i][0]]
                        xlist.sort()
                        if x >= xlist[1] and x <= xlist[2] and not min(points[j][0], points[i][0]) >= max(self.x1, self.x2) and not min(self.x1, self.x2) >= max(points[j][0], points[i][0]) and not min(points[j][1], points[i][1]) >= max(self.y1, self.y2) and not min(self.y1, self.y2) >= max(points[j][1], points[i][1]):
                            return True
            if not points[j][1] == points[i][1]:
                a = (points[j][0] - points[i][0])/(points[j][1] - points[i][1])
                b = points[j][0] - a * points[j][1]
                if not self.y1 == self.y2:
                    c = (self.x1 - self.x2)/(self.y1 - self.y2)
                    d = self.x1 - c * self.y1
                    if not a == c:
                        y = (d - b)/(a - c)
                        ylist = [self.y1, self.y2, points[j][1], points[i][1]]
                        ylist.sort()
                        if y >= ylist[1] and y <= ylist[2] and not min(points[j][1], points[i][1]) >= max(self.y1, self.y2) and not min(self.y1, self.y2) >= max(points[j][1], points[i][1]):
                            return True
                if not self.x1 == self.x2:
                    c = (self.y1 - self.y2)/(self.x1 - self.x2)
                    d = self.y1 - c * self.x1
                    if not (1 - c * a) == 0:
                        y = (c * b + d)/(1 - c * a)
                        ylist = [self.y1, self.y2, points[j][1], points[i][1]]
                        ylist.sort()
                        if y >= ylist[1] and y <= ylist[2] and not min(points[j][1], points[i][1]) >= max(self.y1, self.y2) and not min(self.y1, self.y2) >= max(points[j][1], points[i][1]) and not min(points[j][0], points[i][0]) >= max(self.x1, self.x2) and not min(self.x1, self.x2) >= max(points[j][0], points[i][0]):
                            return True
        if len(points) == 4:
            count = 0
            for p in points:
                if p[0] < self.x1 and p[0] < self.x2 :
                    count = count + 1
            if count == len(points):
                return False
            count = 0
            for p in points:
                if p[0] > self.x1 and p[0] > self.x2 :
                    count = count + 1
            if count == len(points):
                return False
            count = 0
            for p in points:
                if p[1] < self.y1 and p[1] < self.y2 :
                    count = count + 1
            if count == len(points):
                return False
            count = 0
            for p in points:
                if p[1] > self.y1 and p[1] > self.y2 :
                    count = count + 1
            if count == len(points):
                return False
            return True
        return False
    
    def lineover(self, line):
        """checks if lines are over each other and if so changes the lines"""
        
        if not self.y1 == self.y2 and not line.y1 == line.y2:
            a = (line.x1 - line.x2)/(line.y1 - line.y2)
            b = line.x1 - a * line.y1
            c = (self.x1 - self.x2)/(self.y1 - self.y2)
            d = self.x1 - c * self.y1
            if a == c and b == d:
                if (line.y1 < self.y1 and line.y2 > self.y2 and line.y1 < line.y2 and self.y1 < self.y2) or (line.y1 < self.y2 and line.y2 > self.y1 and line.y1 < line.y2 and self.y2 < self.y1) or (line.y1 > self.y1 and line.y2 < self.y2 and line.y1 > line.y2 and self.y2 < self.y1) or (line.y1 > self.y2 and line.y2 < self.y1 and line.y1 > line.y2 and self.y1 < self.y2):
                    for i in self.surface.elements:
                        if type(i) == Node:
                            if self in i.elements:
                                i.addelement(line)
                                i.removeelement(self)
                    if self in self.surface.elements:
                        self.surface.elements.remove(self)
                        return self
                    return None
                elif (line.y1 > self.y1 and line.y1 < self.y2 and self.y2 > self.y1) or (line.y1 < self.y1 and line.y1 > self.y2 and self.y2 < self.y1):
                    if line.y2 > line.y1 and self.y1 < self.y2 or line.y2 < line.y1 and self.y1 > self.y2:
                        line.y2 = self.y2
                        line.x2 = self.x2
                        return None
                    elif line.y2 > line.y1 and self.y1 > self.y2 or line.y2 < line.y1 and self.y1 < self.y2:
                        line.y2 = self.y1
                        line.x2 = self.x1
                        return None
                elif (line.y2 >= self.y1 and line.y2 <= self.y2 and self.y2 > self.y1) or (line.y2 <= self.y1 and line.y2 >= self.y2 and self.y2 < self.y1):
                    if line.y2 > line.y1 and self.y1 < self.y2 or line.y2 < line.y1 and self.y1 > self.y2:
                        line.y2 = self.y1
                        line.x2 = self.x1
                        return None
                    elif line.y2 > line.y1 and self.y1 > self.y2 or line.y2 < line.y1 and self.y1 < self.y2:
                        line.y2 = self.y2
                        line.x2 = self.x2
                        return None
        elif not self.x1 == self.x2 and not line.x1 == line.x2:
            a = (line.y1 - line.y2)/(line.x1 - line.x2)
            b = line.y1 - a * line.x1
            c = (self.y1 - self.y2)/(self.x1 - self.x2)
            d = self.y1 - c * self.x1
            if a == c and b == d:
                if (line.x1 < self.x1 and line.x2 > self.x2 and line.x1 < line.x2 and self.x1 < self.x2) or (line.x1 < self.x2 and line.x2 > self.x1 and line.x1 < line.x2 and self.x2 < self.x1) or (line.x1 > self.x1 and line.x2 < self.x2 and line.x1 > line.x2 and self.x1 > self.x2) or (line.x1 > self.x2 and line.x2 < self.x1 and line.x1 > line.x2 and self.x2 > self.x1):
                    for i in self.surface.elements:
                        if type(i) == Node:
                            if self in i.elements:
                                i.addelement(line)
                                i.removeelement(self)
                    if self in self.surface.elements:
                        self.surface.elements.remove(self)
                        return self
                    return None
                elif (line.x1 > self.x1 and line.x1 < self.x2 and self.x2 > self.x1) or (line.x1 < self.x1 and line.x1 > self.x2 and self.x2 < self.x1):
                    if line.x2 > line.x1 and self.x1 < self.x2 or line.x2 < line.x1 and self.x1 > self.x2:
                        line.y2 = self.y2
                        line.x2 = self.x2
                        return None
                    elif line.x2 > line.x1 and self.x1 > self.x2 or line.x2 < line.x1 and self.x1 < self.x2:
                        line.y2 = self.y1
                        line.x2 = self.x1
                        return None
                elif (line.x2 >= self.x1 and line.x2 <= self.x2 and self.x2 > self.x1) or (line.x2 <= self.x1 and line.x2 >= self.x2 and self.x2 < self.x1):
                    if line.x2 > line.x1 and self.x1 < self.x2 or line.x2 < line.x1 and self.x1 > self.x2:
                        line.y2 = self.y1
                        line.x2 = self.x1
                        return None
                    elif line.x2 > line.x1 and self.x1 > self.x2 or line.x2 < line.x1 and self.x1 < self.x2:
                        line.y2 = self.y2
                        line.x2 = self.x2
                        return None
    
    def clear(self):
        """clears the dt model"""
        
        self.nodeid = -1
    
    
    def setnodeid(self, maxid):
        """sets the nodeid"""
        
        nodeid1 = -2
        nodeid2 = -2
        if type(self.node1) == Node or type(self.node1) == Line:
            nodeid1 = self.node1.nodeid
        if type(self.node2) == Node or type(self.node2) == Line:
            nodeid2 = self.node2.nodeid
        nodeid = max(nodeid1, nodeid2, self.nodeid)
        if (nodeid1 == nodeid or nodeid1 == -2) and (nodeid2 == nodeid or nodeid2 == -2) and self.nodeid == nodeid:
            return maxid
        if nodeid == -1:
            nodeid = maxid + 1
        if type(self.node1) == Node or type(self.node1) == Line:
            self.node1.nodeid = nodeid
        if type(self.node2) == Node or type(self.node2) == Line:
            self.node2.nodeid = nodeid
        self.nodeid = nodeid
        if nodeid == maxid:
            return -1
        return nodeid
    
    def creatdtelement(self, nodeslist):
        """does nothing"""
        
        return True
    
    def getdtelement(self, nodeslist):
        """returns two emtpy lists"""
        
        return [[], []]
    
    def readyforsave(self):
        """makes the element ready for save"""
        
        self.surface = None
        
    def restore(self, surface):
        """restores the object after save"""
        
        self.surface = surface
        
    def setmarked(self):
        """marks the element"""
        
        self.marked = True
    

class CLabel():
    """Circuit Label element"""
    
    def __init__(self, surface, type_, level=1):
        """inits the Label element"""
        
        self.surface = surface
        self.width = 2*DISTANCE
        self.height = 2*DISTANCE
        self.left = 0 
        self.top = 0
        self.type = type_
        self.floating = True
        self.justadded = True
        self.posx = 0
        self.posy = 0
        self.orientation = 0
        self.collided = False
        self.node = None
        self.level = level
        self.marked = False
        self.p = None
        self.nodeid = -1
        
    def update(self, event):
        """updates the label element in accordacne with the user input"""
        
        if event.type == "MOUSEMOVE":
            if self.floating:
                self.justadded = False
                zoom = self.surface.cadsurface.zoom
                surfaceposx = self.surface.cadsurface.posx
                surfaceposy = self.surface.cadsurface.posy
                self.posx = (round((event.x - (surfaceposx/zoom % (DISTANCE/zoom)) - self.width/2)/DISTANCE*zoom)*DISTANCE/zoom + surfaceposx/zoom % (DISTANCE/zoom))*zoom - surfaceposx
                self.posy = (round((event.y - (surfaceposy/zoom % (DISTANCE/zoom)) - self.height/2)/DISTANCE*zoom)*DISTANCE/zoom + surfaceposy/zoom % (DISTANCE/zoom))*zoom - surfaceposy
                self.collided = False
                for e in self.surface.elements:
                    if not e == self and not type(e) == Line:
                        if e.collide(self._cornerpoints()):
                            self.collided = True
                            break
                return True
        elif event.type == "MOUSELEFTBUTTON":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.floating and not self.collided:
                self.collided = True
                if event.key == "SHIFT":
                    typeelement = self.type.element
                    self.type.element = None
                    type_ = copy.deepcopy(self.type)
                    self.type.element = typeelement
                    self.type = type_
                self.add()
                self.surface.setunsaved()
                self.surface.addhistory(self.remove, self.add)
                return True
            elif self.surface.deletemode and self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                self.remove()
                self.surface.setunsaved()
                self.surface.addhistory(self.add, self.remove)
                return True
            elif self.floating == False and self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                if self.marked == False:
                    self.marked = True
                else:
                    self.marked = False
                return True
        elif event.type == "MOUSEWHEEL":
            if self.floating:
                self.orientation = self.orientation - int(event.value)
                if self.orientation < 0:
                    self.orientation = 3
                elif self.orientation > 3:
                    self.orientation = 0
                return True
        elif event.type == "MOUSERIGHTBUTTON":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.floating:
                self.surface.elements.remove(self)
                return True
            else:
                if self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]) and not self.surface.deletemode:
                    floating = False
                    for e in self.surface.elements:
                        if type(e) == UElement or type(e) == CLabel or type(e) == FLabel or type(e) == SubCircuitElement:
                            if e.floating:
                                floating = True
                                break
                        elif type(e) == Line or type(e) == Vector:
                            if e.active:
                                floating = True
                                break
                    if not floating:
                        if event.key == "SHIFT":
                            ecopy = self.copy(fullcopy=True)
                        else:
                            ecopy = self.copy()
                        ecopy.floating = True
                        ecopy.collided = True
                        self.surface.elements.append(ecopy)
                        if self.surface.autoremove:
                            self.surface.setunsaved()
                            self.surface.addhistory()
                            self.remove()
                        return True
        if event.type == "KEY_P":
            if event.key == "DEL" and self.floating == True:
                self.surface.elements.remove(self)
                return True
            if event.key == "CTRL_R" and self.floating == True:
                self.orientation = self.orientation - 1
                if self.orientation < 0:
                    self.orientation = 3
                elif self.orientation > 3:
                    self.orientation = 0
                return True
        if event.type == "MOUSELEFTDOUBLE":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                self.marked = not self.marked
                self.p = twindow.TypeWindow(self.type, pos=self.surface.mainWindowposition())
                self.p.show()
                self.surface.setunsaved()
                return True
            
    def draw(self):
        """draws the label"""
        
        if self.justadded == False:
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.floating:
                self.width = 2*DISTANCE/zoom
                self.height = 2*DISTANCE/zoom
                self.left = (self.posx + surfaceposx)/zoom
                self.top = (self.posy + surfaceposy)/zoom
                if self.collided:
                    self.type.draw(self.surface, (self.left, self.top), (self.width, self.height), self.type.label, orientation=self.orientation, color=RED)
                else:
                    self.type.draw(self.surface, (self.left, self.top), (self.width, self.height), self.type.label, orientation=self.orientation, color=WHITE)
            elif not self.floating:
                zoom = self.surface.cadsurface.zoom
                surfaceposx = self.surface.cadsurface.posx
                surfaceposy = self.surface.cadsurface.posy
                self.width = 2*DISTANCE/zoom
                self.height = 2*DISTANCE/zoom
                self.left = (self.posx + surfaceposx)/zoom
                self.top = (self.posy + surfaceposy)/zoom
                if not self.surface.deletemode:
                    if self.marked == False:
                        self.type.draw(self.surface, (self.left, self.top), (self.width, self.height), self.type.label, orientation=self.orientation, color=TURQUOISE)
                    else:
                        if self.type.connectedtype == 0:
                            for i in self.type.element:
                                if not i == self:
                                    self.surface.drawline(PINK, (self.left + self.width/2, self.top + self.height/2), ((i.posx + surfaceposx)/zoom + self.width/2, (i.posy + surfaceposy)/zoom + self.height/2))
                        self.type.draw(self.surface, (self.left, self.top), (self.width, self.height), self.type.label, orientation=self.orientation, color=PINK)
                else:
                    self.type.draw(self.surface, (self.left, self.top), (self.width, self.height), self.type.label, orientation=self.orientation, color=BLUE)
            
    def _cornerpoints(self):
        """returns the corner points of the element"""
        
        return ((self.posx + 1, self.posy + 1), (self.posx + 2*DISTANCE - 1, self.posy + 1), (self.posx + 2*DISTANCE - 1, self.posy + 2*DISTANCE - 1), (self.posx + 1, self.posy + 2*DISTANCE - 1))
    
    def remove(self):
        self.surface.elements.remove(self)
        if not self.type.connectedtype == -1:
            self.type.element.remove(self)
        self.node.removeelement(self)
    
    def add(self):
        """adds the element"""
        
        self.surface.elements.append(self.copy())
        self.collided = False
        self.floating = False
        self.level = 2
        if self.type.connectedtype == -1:
            self.type.element = self
        else:
            if self.type.element == None:
                self.type.element = [self]
            else:
                self.type.element.append(self)
        if self.orientation == 0:
            self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*2, self.posy + DISTANCE)))
        elif self.orientation == 1:
            self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE, self.posy + DISTANCE*2)))
        elif self.orientation == 2:
            self.surface.elements.append(Node(self.surface, self, (self.posx, self.posy + DISTANCE)))
        else:
            self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE, self.posy)))
        self.node = self.surface.elements[-1]
        for i in self.surface.elements:
            if type(i) == Node:
                if not i == self.node and abs(i.x1-self.node.x1)<2 and abs(i.y1-self.node.y1)<2:
                    self.surface.elements.remove(self.node)
                    i.addelement(self)
                    self.node = i
                    return
        for i in self.surface.elements:
            if type(i) == Line:
                if i.collide(((self.node.x1 - 5, self.node.y1 - 5), (self.node.x1 + 5, self.node.y1 + 5))) and not i==self:
                    self.node.addelement(i)
                    return
    
    def copy(self, fullcopy=False):
        """copys the element"""
        
        ecopy = copy.copy(self)
        if self.type.connectedtype == -1 or fullcopy == True:
            if not self.type.element == None:
                element = self.type.element
                self.type.element = None
                ecopy.type = copy.deepcopy(self.type)
                self.type.element = element
            else:
                ecopy.type = copy.deepcopy(self.type)
            return ecopy
        return ecopy
            
        
    def collide(self, points):
        """checks for collision with the given points
            and lines between the points
            """
        height = 2
        width = 2
        for p in points:
            if p[0] >= self.posx + 1 and p[0] <= self.posx + width*DISTANCE - 1 and p[1] >= self.posy + 1 and p[1] <= self.posy + height*DISTANCE - 1:
                return True
        for i, p in enumerate(points):
            if i > 0:
                if not points[i-1][0] == points[i][0]:
                    a = (points[i-1][1] - points[i][1])/(points[i-1][0] - points[i][0])
                    b = points[i-1][1] - a * points[i-1][0]
                    if points[i-1][0] >= self.posx + 1 and points[i][0] <= self.posx + 1 or points[i-1][0] <= self.posx + 1 and points[i][0] >= self.posx + 1:
                        y = a * (self.posx + 1) + b
                        if y >= self.posy + 1 and y <= self.posy + height*DISTANCE - 1:
                            return True
                    if points[i-1][0] >= self.posx + width*DISTANCE - 1 and points[i][0] <= self.posx + width*DISTANCE - 1 or points[i-1][0] <= self.posx + width*DISTANCE - 1 and points[i][0] >= self.posx + width*DISTANCE - 1:
                        y = a * (self.posx + width*DISTANCE - 1) + b
                        if y >= self.posy + 1 and y <= self.posy + height*DISTANCE - 1:
                            return True
                if not points[i-1][1] == points[i][1]:
                    a = (points[i-1][0] - points[i][0])/(points[i-1][1] - points[i][1])
                    b = points[i-1][0] - a * points[i-1][1]
                    if points[i-1][1] >= self.posy + 1 and points[i][1] <= self.posy + 1 or points[i-1][1] <= self.posy + 1 and points[i][1] >= self.posy + 1:
                        x = a * (self.posy + 1) + b
                        if x >= self.posx + 1 and x <= self.posx + width*DISTANCE - 1:
                            return True
                    if points[i-1][1] >= self.posy + height*DISTANCE - 1 and points[i][1] <= self.posy + height*DISTANCE - 1 or points[i-1][1] <= self.posy + height*DISTANCE - 1 and points[i][1] >= self.posy + height*DISTANCE - 1:
                        x = a * (self.posy + height*DISTANCE - 1) + b
                        if x >= self.posx + 1 and x <= self.posx + width*DISTANCE - 1:
                            return True
        xfree = True
        yfree = True
        x1 = self.posx + 1
        x2 = self.posx + width*DISTANCE - 1
        y1 = self.posy + 1
        y2 = self.posy + height*DISTANCE - 1
        xcount = 0
        for p in points:
            if p[0] < x1 and p[0] < x2 :
                xcount = xcount + 1
            elif (p[0] < x1 and p[0] > x2) or (p[0] > x1 and p[0] < x2):
                xfree = False
        if xcount > 0 and not len(points) == xcount:
            xfree = False
        ycount = 0
        for p in points:
            if p[1] < y1 and p[1] < y2:
                ycount = ycount + 1
            elif (p[1] < y1 and p[1] > y2) or (p[1] > y1 and p[1] < y2):
                yfree = False
        if ycount > 0 and not len(points) == ycount:
            yfree = False
        if yfree == False and xfree == False:
            return True
        return False
    
    def setnodeid(self, maxid):
        """sets the nodeid of all connected elements"""
        
        if type(self.type.element) == list:
            nodeid = -1
            for e in self.type.element:
                if not e.node == None:
                    e.nodeid = max(e.nodeid, e.node.nodeid)
            for e in self.type.element:
                nodeid = max(nodeid, e.nodeid)
            if nodeid == -1:
                nodeid = maxid + 1
            maxidreturn = maxid
            for e in self.type.element:
                if not e.nodeid == nodeid:
                    e.nodeid = nodeid
                    e.node.nodeid = nodeid
                    if nodeid == maxid:
                        maxidreturn = -1
                    else:
                        maxidreturn = nodeid
            return maxidreturn
        else:
            if self.nodeid == -1 or not self.node.nodeid == self.nodeid:
                self.nodeid = max(self.node.nodeid, self.nodeid)
                self.node.nodeid = max(self.node.nodeid, self.nodeid)
                if self.nodeid == -1:
                    self.nodeid = maxid + 1
                    self.node.nodeid = maxid + 1
                    return maxid + 1
                return -1
            return maxid
    
    def clear(self):
        """clears the dt model"""
        
        self.nodeid = -1
        
    def creatdtelement(self, nodeslist):
        """does nothing"""
        
        return True
    
    def getdtelement(self, nodeslist):
        """returns two emtpy lists"""
        
        return [[], []]
    
    def readyforsave(self):
        """makes the element ready for save"""
        
        self.surface = None
        
    def restore(self, surface):
        """restores the object after save"""
        
        self.surface = surface
        
    def setmarked(self):
        """marks the element"""
        
        self.marked = True

###############################################################################
# function elements
###############################################################################
    
    
class Arrow:
    """arrow is a node for the function elements but has a direction"""
    
    def __init__(self, surface, elementin, elementout, position, orientation=0, level=3):
        """inits the arrow with an input and an output element"""
        
        self.surface = surface
        self.x1 = position[0]
        self.y1 = position[1]
        self.over = False
        self.closed = 0
        self.elements = []
        self.elementin = elementin
        self.marked = False
        if not elementout == None:
            self.elementout = [elementout]
        else:
            self.elementout = []
        self.orientation = orientation
        self.level = level
        self.loopcheck = False
        self.inconsistent = False
        
    def update(self, event):
        """updates the arrow in accordance with the user input"""
        
        if event.type == "MOUSEMOVE":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            actualover = self.over
            self.over = False
            if ((event.x - (self.x1 + surfaceposx)/zoom)**2 + (event.y - (self.y1 + surfaceposy)/zoom)**2)<=16 and self.closed == 0:
                self.over = True
                return True
            if not actualover == self.over:
                return True
        if event.type == "MOUSELEFTBUTTON":
            if self.closed == False and self.surface.deletemode == False:
                if self.over == True:
                    if self.elementout == []:
                        self.surface.elements.append(Vector(self.surface, self, None, (self.x1, self.y1)))
                    else:
                        self.surface.elements.append(Vector(self.surface, None, self, (self.x1, self.y1)))
                    self.over = False
                    return True
            
    def draw(self):
        """draws the arrow"""
        if self.surface.deletemode == False:
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            self.marked = False
            if not self.elementin == None:
                self.marked = self.elementin.marked
            for e in self.elementout:
                self.marked = self.marked or e.marked
            if self.closed == 0:
                if self.over:
                    self.surface.drawtriangle(WHITE, 10, 10,((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), -90*self.orientation - 90, width=1, fill=True)
                elif self.inconsistent:
                    self.surface.drawtriangle(RED, 8, 8,((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), -90*self.orientation - 90, width=1, fill=True)
                else:
                    if self.marked:
                        self.surface.drawtriangle(PINK, 8, 8,((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), -90*self.orientation - 90, width=1, fill=True)
                    else:
                        self.surface.drawtriangle(GREEN, 8, 8,((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), -90*self.orientation - 90, width=1, fill=True)
            elif self.closed == 3:
                if self.inconsistent:
                    self.surface.drawtriangle(RED, 6, 6,((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), -90*self.orientation - 90, width=1, fill=True)
                else:
                    if self.inconsistent:
                        self.surface.drawtriangle(RED, 6, 6,((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), -90*self.orientation - 90, width=1, fill=True)
                    else:
                        if self.marked:
                            self.surface.drawtriangle(PINK, 6, 6,((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), -90*self.orientation - 90, width=1, fill=True)
                        else:
                            self.surface.drawtriangle(GREEN, 6, 6,((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), -90*self.orientation - 90, width=1, fill=True)
            elif self.closed == 1:
                if self.inconsistent:
                    self.surface.drawellipse(RED, [(self.x1 + surfaceposx)/zoom - 3, (self.y1 + surfaceposy)/zoom - 3, 6, 6], 1, True)
                else:
                    if self.marked:
                        self.surface.drawellipse(PINK, [(self.x1 + surfaceposx)/zoom - 3, (self.y1 + surfaceposy)/zoom - 3, 6, 6], 1, True)
                    else:
                        self.surface.drawellipse(GREEN, [(self.x1 + surfaceposx)/zoom - 3, (self.y1 + surfaceposy)/zoom - 3, 6, 6], 1, True)
        else:
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            self.surface.drawrect(BLUE, ((self.x1 + surfaceposx)/zoom - 4, (self.y1 + surfaceposy)/zoom - 4, 8, 8), width=1, fill=True)
    
    def addinelement(self, element):
        """adds the inelement if possible
            returns true if sucessfull
            false if not possible"""
            
        if self.elementin == None and not element in self.elementout:
            self.elementin = element
            if self.elementout == []:
                self.orientation = self.elementin.orientation
                if type(self.elementin) == SubCircuitElement:
                    if self.orientation == 0:
                        self.orientation = 2
                    elif self.orientation == 1:
                        self.orientation = 3
                    elif self.orientation == 2:
                        self.orientation = 0
                    else:
                        self.orientation = 1
            else:
                if len(self.elementout) == 1:
                    self.closed = 2
                elif len(self.elementout) > 1:
                    self.closed = 1
                for e in self.elementout:
                    if type(e) == SubCircuitElement:
                        if self.closed == 3:
                            self.closed = 1
                        else:
                            self.closed = 3
                            self.orientation = e.orientation
                if not self.closed == 3:
                    if type(self.elementin) == Vector:
                        if not self.elementin.elementout == self:
                            self.closed = 1
            return True
        else:
            return False
        
    def remove(self):
        """does nothing"""
        
        pass
        
    def addoutelement(self, element):
        """adds the element to the elementout list"""
        
        if not element in self.elementout and not element == self.elementin:
            self.elementout.append(element)
            if not self.elementin == None:
                if len(self.elementout) == 1:
                    self.closed = 2
                elif len(self.elementout) > 1:
                    self.closed = 1
                for e in self.elementout:
                    if type(e) == UElement or type(e) == SubCircuitElement:
                        self.closed = 3
    
    def removeelement(self, element):
        """removes the element either from elementin or element out"""
        
        if element == self.elementin:
            self.elementin = None
            self.closed = 0
            if self.elementout == []:
                self.surface.elements.remove(self)
                return
            else:
                for e in self.elementout:
                    if type(e) == UElement or type(e) == SubCircuitElement:
                        # self.orientation = e.orientation
                        return
                self.orientation = self.elementout[0].orientation
        if element in self.elementout:
            self.elementout.remove(element)
            if self.elementout ==[]:
                self.closed = 0
                if self.elementin == None:
                    self.surface.elements.remove(self)
                    return
                else:
                    self.orientation = self.elementin.orientation
                    if type(self.elementin) == SubCircuitElement:
                        if self.orientation == 0:
                            self.orientation = 2
                        elif self.orientation == 1:
                            self.orientation = 3
                        elif self.orientation == 2:
                            self.orientation = 0
                        else:
                            self.orientation = 1
                if type(self.elementin) == Vector:
                    if not self.elementin.elementout == self:
                        self.surface.elements.remove(self)
                        return
            elif self.elementin == None:
                self.closed = 0
            else:
                if len(self.elementout) == 1:
                    self.closed = 2
                elif len(self.elementout) > 1:
                    self.closed = 1
                for e in self.elementout:
                    if type(e) == FLabel or type(e) == UElement or type(e) == SubCircuitElement:
                        self.closed = 3
                for e in self.elementout:
                    if type(e) == FLabel or type(e) == UElement or type(e) == SubCircuitElement:
                        self.orientation = e.orientation
                        return
                    self.orientation = self.elementout[0].orientation
            
                
    def collide(self, rect):
        """not used for arrows"""
        
        return False
    
    def getsource(self, toelement):
        """returns the output"""
        
        if self.loopcheck == True:
            self.inconsistent = True
            self.loopcheck = False
            return False
        else:
            self.loopcheck = True
        if self.elementin == None:
            self.inconsistent = True
            return False
        source = self.elementin.getsource(self)
        self.loopcheck = False
        if source == False:
            self.inconsistent = True
            return False
        else:
            self.inconsistent = False
            return source
    
    def clear(self):
        """clear the dtmodel"""
        
        self.loopcheck = False
        self.inconsistent = False
        
    def setnodeid(self, maxid):
        """sets the nodeid"""
        
        return maxid
    
    def creatdtelement(self, nodeslist):
        """does nothing"""
        
        return True
    
    def getdtelement(self, nodeslist):
        """returns two emtpy lists"""
        
        return [[], []]
    
    def readyforsave(self):
        """makes the element ready for save"""
        
        self.surface = None
        
    def restore(self, surface):
        """restores the object after save"""
        
        self.surface = surface
        
    def setmarked(self):
        """marks the element"""
        
        pass
        

class Vector:
    """vector is the line for function elements but has a direction"""
    
    def __init__(self, surface, arrowin, arrowout, position, level=4):
        """inits the vector with an inputs and an output element"""
        
        self.surface = surface
        self.x1 = position[0]
        self.y1 =  position[1]
        self.x2 =  position[0]
        self.y2 = position[1]
        self.active = True
        self.collided = False
        self.elementin = arrowin
        self.elementout = arrowout
        self.orientation = 0
        self.level = level
        self.marked = False
        self.loopcheck = False
        self.inconsistent = False
        
    def update(self, event):
        """updates the vector in accordance with user input"""
        
        if event.type == "MOUSELEFTBUTTON":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.active==True:
                if self.collided==False:
                    if not (self.x1 == self.x2 and self.y1 == self.y2):
                        self.add()
                        self.surface.setunsaved()
                        self.surface.addhistory(self.remove, self.add)
                    return True
            if self.surface.deletemode and self.collide(((event.x*zoom - surfaceposx - 5, event.y*zoom -surfaceposy - 5), (event.x*zoom - surfaceposx + 5, event.y*zoom - surfaceposy - 5), (event.x*zoom - surfaceposx + 5, event.y*zoom -surfaceposy + 5), (event.x*zoom - surfaceposx - 5, event.y*zoom - surfaceposy + 5))):
                self.remove()
                self.surface.setunsaved()
                self.surface.addhistory(self.add, self.remove)
                return True
            if self.collide(((event.x*zoom - surfaceposx - 5, event.y*zoom -surfaceposy - 5), (event.x*zoom - surfaceposx + 5, event.y*zoom - surfaceposy - 5), (event.x*zoom - surfaceposx + 5, event.y*zoom -surfaceposy + 5), (event.x*zoom - surfaceposx - 5, event.y*zoom - surfaceposy + 5))):
                self.marked = not self.marked
                return True
        if event.type == "MOUSERIGHTBUTTON":
            if self.active==True:
                self.surface.elements.remove(self)
                return True
        if event.type == "MOUSEMOVE":
            if self.active == True:
                zoom = self.surface.cadsurface.zoom
                surfaceposx = self.surface.cadsurface.posx
                surfaceposy = self.surface.cadsurface.posy
                x2 = (round((event.x - (surfaceposx/zoom % (DISTANCE/zoom)))/DISTANCE*zoom)*DISTANCE/zoom + surfaceposx/zoom % (DISTANCE/zoom))*zoom - surfaceposx
                y2 = (round((event.y - (surfaceposy/zoom % (DISTANCE/zoom)))/DISTANCE*zoom)*DISTANCE/zoom + surfaceposy/zoom % (DISTANCE/zoom))*zoom - surfaceposy
                if abs(x2 - self.x1)>abs(y2 - self.y1)*2:
                    self.x2 = x2
                    self.y2 = self.y1
                    if (self.x1 > self.x2 and self.elementin == None) or (self.x1 < self.x2 and self.elementout == None):
                        self.orientation = 0
                    else:
                        self.orientation = 2
                elif 2*abs(x2 - self.x1)<abs(y2 - self.y1):
                    self.x2 = self.x1
                    self.y2 = y2
                    if (self.y1 > self.y2 and self.elementin == None) or (self.y1 < self.y2 and self.elementout == None):
                        self.orientation = 3
                    else:
                        self.orientation = 1
                else:
                    deltax = self.x1 - x2
                    deltay = self.y1 - y2
                    delta = round((abs(deltax) + abs(deltay))/2/DISTANCE)*DISTANCE
                    self.x2 = self.x1 - np.sign(deltax)*delta
                    self.y2 = self.y1 - np.sign(deltay)*delta
                    if (self.x2 > self.x1 and self.y2 > self.y1 and self.elementout == None) or (self.x2 < self.x1 and self.y2 < self.y1 and self.elementin == None):
                        self.orientation = 3.5
                    elif (self.x2 > self.x1 and self.y2 < self.y1 and self.elementin == None) or (self.x2 < self.x1 and self.y2 > self.y1 and self.elementout == None):
                        self.orientation = 2.5
                    elif (self.x2 > self.x1 and self.y2 > self.y1 and self.elementin == None) or (self.x2 < self.x1 and self.y2 < self.y1 and self.elementout == None):
                        self.orientation = 1.5
                    elif (self.x2 > self.x1 and self.y2 < self.y1 and self.elementout == None) or (self.x2 < self.x1 and self.y2 > self.y1 and self.elementin == None):
                        self.orientation = 0.5
                self.collided = False
                for e in self.surface.elements:
                    if not e == self and (type(e) == UElement or type(e) == CLabel or type(e) == FLabel or type(e) == SubCircuitElement):
                        if e.collide(((self.x1, self.y1), (self.x2, self.y2))):
                            self.collided = True
                            break
                    elif not e == self and type(e) == Vector and self.elementout == None:
                        if e.vectorovercheck(self):
                            self.collided = True
                            break
                return True
        if event.type == "KEY_P":
            if event.key == "DEL" and self.active == True:
                self.surface.elements.remove(self)
                return True
            
    def draw(self):
        """draws the vector"""
        
        zoom = self.surface.cadsurface.zoom
        surfaceposx = self.surface.cadsurface.posx
        surfaceposy = self.surface.cadsurface.posy
        if self.active==True:
            if self.collided==True:
                self.surface.drawline(RED, ((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), ((self.x2 + surfaceposx)/zoom, (self.y2 + surfaceposy)/zoom), width=1)
            else:
                self.surface.drawline(WHITE, ((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), ((self.x2 + surfaceposx)/zoom, (self.y2 + surfaceposy)/zoom), width=1)
        elif self.surface.deletemode:
            self.surface.drawline(BLUE, ((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), ((self.x2 + surfaceposx)/zoom, (self.y2 + surfaceposy)/zoom), width=1)
        else:
            if self.inconsistent:
                self.surface.drawline(RED, ((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), ((self.x2 + surfaceposx)/zoom, (self.y2 + surfaceposy)/zoom), width=1)
            elif self.marked:
                self.surface.drawline(PINK, ((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), ((self.x2 + surfaceposx)/zoom, (self.y2 + surfaceposy)/zoom), width=1)
            else:
                self.surface.drawline(GREEN, ((self.x1 + surfaceposx)/zoom, (self.y1 + surfaceposy)/zoom), ((self.x2 + surfaceposx)/zoom, (self.y2 + surfaceposy)/zoom), width=1)

    def remove(self):
        """remove the element"""
        
        self.surface.elements.remove(self)
        for e in copy.copy(self.surface.elements):
            if type(e) == Arrow:
                e.removeelement(self)
                    
    def add(self):
        """adds the element"""
        
        if not self.elementin == None and not self.elementout == None:
            if self.elementin.x1 == self.x1 and self.elementin.y1 == self.y1:
                self.elementin = Arrow(self.surface, None, self, (self.x1, self.y1), orientation=self.orientation)
            else:
                self.elementin = Arrow(self.surface, None, self, (self.x2, self.y2), orientation=self.orientation)
            if self.elementout.x1 == self.x1 and self.elementout.y1 == self.y1:
                self.elementout = Arrow(self.surface, self, None, (self.x1, self.y1), orientation=self.orientation)
            else:
                self.elementout = Arrow(self.surface, self, None, (self.x2, self.y2), orientation=self.orientation)
            self.surface.elements.append(self.elementin)
            self.surface.elements.append(self.elementout)
            noded = False
            for i in self.surface.elements:
                if type(i) == Arrow:
                    if not i == self.elementin and not i == self.elementout and abs(i.x1-self.elementin.x1)<2 and abs(i.y1-self.elementin.y1)<2:
                        i.addoutelement(self)
                        self.surface.elements.remove(self.elementin)
                        self.elementin = i
                        self.level = 2
                        noded = True
                        break
            for i in self.surface.elements:
                if type(i) == Arrow:
                    if not i == self.elementin and not i == self.elementout and abs(i.x1-self.elementout.x1)<2 and abs(i.y1-self.elementout.y1)<2:
                        if i.addinelement(self):
                            self.level = 2
                            self.surface.elements.remove(self.elementout)
                            self.elementout = i
                            self.level = 2
                            self.elementout = i
                            i.removeelement(self)
                            i.addinelement(self)
                            break
            if noded == False:
                if self.elementin.elementin == None:
                    for i in self.surface.elements:
                        if type(i) == Vector:
                            if i.collide(((self.elementin.x1 - 7, self.elementin.y1  - 7), (self.elementin.x1 + 7, self.elementin.y1 + 7))) and not i==self:
                                self.level = 2
                                self.elementin.addinelement(i)
                                noded = True
                                break
            for e in self.surface.elements:
                if type(e) == Arrow:
                    if self.collide(((e.x1 - 7, e.y1 - 7),(e.x1 + 7 , e.y1 + 7))) and not e == self.elementin and not e == self.elementout:
                        e.addinelement(self)
        else:
            self.active =  False
            self.level = 2
            if self.elementin == None:
                for i in self.surface.elements:
                    if type(i) == Vector:
                        if not i == self:
                            i.vectorover(self)
            if not self.elementout == None:
                self.elementout.addinelement(self)
            elif not self.elementin == None:
                self.elementin.addoutelement(self)
            noded = False
            if self.elementin == None:
                for i in self.surface.elements:
                    if type(i) == Arrow:
                        if not i == self.elementin and not i == self.elementout and abs(i.x1-self.x2)<2 and abs(i.y1-self.y2)<2:
                            i.addoutelement(self)
                            self.level = 2
                            self.elementin = i
                            noded = True
                            break
            if self.elementout == None:
                for i in self.surface.elements:
                    if type(i) == Arrow:
                        if not i == self.elementin and not i == self.elementout and abs(i.x1-self.x2)<2 and abs(i.y1-self.y2)<2:
                            if i.addinelement(self):
                                self.level = 2
                                self.elementout = i
                                i.removeelement(self)
                                i.addinelement(self)
                                break
            if self.elementin == None:
                self.elementin = Arrow(self.surface, None, self, (self.x2, self.y2), orientation=self.orientation)
                self.surface.elements.append(self.elementin)
            elif self.elementout == None:
                self.elementout = Arrow(self.surface, self, None, (self.x2, self.y2), orientation=self.orientation)
                self.surface.elements.append(self.elementout)
            if noded == False:
                if self.elementin.elementin == None:
                    for i in self.surface.elements:
                        if type(i) == Vector:
                            if i.collide(((self.x2 - 7, self.y2  - 7), (self.x2 + 7, self.y2 + 7))) and not i==self:
                                self.level = 2
                                self.elementin.addinelement(i)
                                noded = True
                                break
            for e in self.surface.elements:
                if type(e) == Arrow:
                    if self.collide(((e.x1 - 7, e.y1 - 7),(e.x1 + 7 , e.y1 + 7))) and not e == self.elementin and not e == self.elementout:
                        e.addinelement(self)
        if not self in self.surface.elements:
            self.surface.elements.append(self)
        else:
            if noded == False:
                if self.elementout.elementout == []:
                    self.surface.elements.append(Vector(self.surface, self.elementout, None, (self.x2, self.y2)))
                elif self.elementin.elementin == None:
                    self.surface.elements.append(Vector(self.surface, None, self.elementin, (self.x2, self.y2)))
        
    def collide(self, points):
        """checks for collision with the points and with the lines between the points"""
        
        if len(points) == 1:
            if not self.x1 == self.x2:
                c = (self.y1 - self.y2)/(self.x1 - self.x2)
                d = self.y1 - c * self.x1
                if (c*points[0][0] + d) == points[0][1] and ((self.x1 < points[0][0] and self.x2 > points[0][0]) or (self.x1 > points[0][0] and self.x2 < points[0][0])):
                    return True
            if not self.y1 == self.y2:
                c = (self.x1 - self.x2)/(self.y1 - self.y2)
                d = self.x1 - c * self.y1
                if (c*points[0][1] + d) == points[0][0] and ((self.y1 < points[0][0] and self.y2 > points[0][0]) or (self.y1 > points[0][0] and self.y2 < points[0][0])):
                    return True
        for i, p in enumerate(points):
            j = i - 1
            if not points[j][0] == points[i][0]:
                a = (points[j][1] - points[i][1])/(points[j][0] - points[i][0])
                b = points[j][1] - a * points[j][0]
                if not self.x1 == self.x2:
                    c = (self.y1 - self.y2)/(self.x1 - self.x2)
                    d = self.y1 - c * self.x1
                    if not a == c:
                        x = (d - b)/(a - c)
                        xlist = [self.x1, self.x2, points[j][0], points[i][0]]
                        xlist.sort()
                        if x >= xlist[1] and x <= xlist[2] and not min(points[j][0], points[i][0]) >= max(self.x1, self.x2) and not min(self.x1, self.x2) >= max(points[j][0], points[i][0]):
                            return True
                if not self.y1 == self.y2:
                    c = (self.x1 - self.x2)/(self.y1 - self.y2)
                    d = self.x1 - c * self.y1
                    if not (1 - c * a) == 0:
                        x = (c * b + d)/(1 - c * a)
                        xlist = [self.x1, self.x2, points[j][0], points[i][0]]
                        xlist.sort()
                        if x >= xlist[1] and x <= xlist[2] and not min(points[j][0], points[i][0]) >= max(self.x1, self.x2) and not min(self.x1, self.x2) >= max(points[j][0], points[i][0]) and not min(points[j][1], points[i][1]) >= max(self.y1, self.y2) and not min(self.y1, self.y2) >= max(points[j][1], points[i][1]):
                            return True
            if not points[j][1] == points[i][1]:
                a = (points[j][0] - points[i][0])/(points[j][1] - points[i][1])
                b = points[j][0] - a * points[j][1]
                if not self.y1 == self.y2:
                    c = (self.x1 - self.x2)/(self.y1 - self.y2)
                    d = self.x1 - c * self.y1
                    if not a == c:
                        y = (d - b)/(a - c)
                        ylist = [self.y1, self.y2, points[j][1], points[i][1]]
                        ylist.sort()
                        if y >= ylist[1] and y <= ylist[2] and not min(points[j][1], points[i][1]) >= max(self.y1, self.y2) and not min(self.y1, self.y2) >= max(points[j][1], points[i][1]):
                            return True
                if not self.x1 == self.x2:
                    c = (self.y1 - self.y2)/(self.x1 - self.x2)
                    d = self.y1 - c * self.x1
                    if not (1 - c * a) == 0:
                        y = (c * b + d)/(1 - c * a)
                        ylist = [self.y1, self.y2, points[j][1], points[i][1]]
                        ylist.sort()
                        if y >= ylist[1] and y <= ylist[2] and not min(points[j][1], points[i][1]) >= max(self.y1, self.y2) and not min(self.y1, self.y2) >= max(points[j][1], points[i][1]) and not min(points[j][0], points[i][0]) >= max(self.x1, self.x2) and not min(self.x1, self.x2) >= max(points[j][0], points[i][0]):
                            return True
        if len(points) == 4:
            count = 0
            for p in points:
                if p[0] < self.x1 and p[0] < self.x2 :
                    count = count + 1
            if count == len(points):
                return False
            count = 0
            for p in points:
                if p[0] > self.x1 and p[0] > self.x2 :
                    count = count + 1
            if count == len(points):
                return False
            count = 0
            for p in points:
                if p[1] < self.y1 and p[1] < self.y2 :
                    count = count + 1
            if count == len(points):
                return False
            count = 0
            for p in points:
                if p[1] > self.y1 and p[1] > self.y2 :
                    count = count + 1
            if count == len(points):
                return False
            return True
        return False
    
    def vectorover(self, vector):
        """checks if lines are over each other and if so changes the lines"""
        
        if not self.y1 == self.y2 and not vector.y1 == vector.y2:
            a = (vector.x1 - vector.x2)/(vector.y1 - vector.y2)
            b = vector.x1 - a * vector.y1
            c = (self.x1 - self.x2)/(self.y1 - self.y2)
            d = self.x1 - c * self.y1
            if a == c and b == d:
                if (vector.y1 < self.y1 and vector.y2 > self.y2 and vector.y1 < vector.y2 and self.y1 < self.y2) or (vector.y1 > self.y1 and vector.y2 < self.y2 and vector.y1 > vector.y2 and self.y1 > self.y2):
                    vector.y2 = self.y1
                    vector.x2 = self.x1
                    return
                elif (vector.y1 > self.y1 and vector.y2 < self.y2 and vector.y1 > vector.y2 and self.y1 < self.y2) or (vector.y1 < self.y1 and vector.y2 > self.y2 and vector.y1 < vector.y2 and self.y1 > self.y2):
                    vector.y2 = self.y2
                    vector.x2 = self.x2
                    return
                elif (vector.y1 > self.y1 and vector.y1 < self.y2 and self.y2 > self.y1) or (vector.y1 < self.y1 and vector.y1 > self.y2 and self.y2 < self.y1):
                    if vector.y2 > vector.y1 and self.y1 < self.y2 or vector.y2 < vector.y1 and self.y1 > self.y2:
                        vector.y2 = self.y2
                        vector.x2 = self.x2
                        return
                    elif vector.y2 > vector.y1 and self.y1 > self.y2 or vector.y2 < vector.y1 and self.y1 < self.y2:
                        vector.y2 = self.y1
                        vector.x2 = self.x1
                        return
                elif (vector.y2 > self.y1 and vector.y2 < self.y2 and self.y2 > self.y1) or (vector.y2 < self.y1 and vector.y2 > self.y2 and self.y2 < self.y1):
                    if vector.y2 > vector.y1 and self.y1 < self.y2 or vector.y2 < vector.y1 and self.y1 > self.y2:
                        vector.y2 = self.y1
                        vector.x2 = self.x1
                        return
                    elif vector.y2 > vector.y1 and self.y1 > self.y2 or vector.y2 < vector.y1 and self.y1 < self.y2:
                        vector.y2 = self.y2
                        vector.x2 = self.x2
                        return
        elif not self.x1 == self.x2 and not vector.x1 == vector.x2:
            a = (vector.y1 - vector.y2)/(vector.x1 - vector.x2)
            b = vector.y1 - a * vector.x1
            c = (self.y1 - self.y2)/(self.x1 - self.x2)
            d = self.y1 - c * self.x1
            if a == c and b == d:
                if (vector.x1 < self.x1 and vector.x2 > self.x2 and vector.x1 < vector.x2 and self.x1 < self.x2) or (vector.x1 > self.x1 and vector.x2 < self.x2 and vector.x1 > vector.x2 and self.x1 > self.x2):
                    vector.y2 = self.y1
                    vector.x2 = self.x1
                    return
                elif (vector.x1 > self.x1 and vector.x2 < self.x2 and vector.x1 > vector.x2 and self.x1 < self.x2) or (vector.x1 < self.x1 and vector.x2 > self.x2 and vector.x1 < vector.x2 and self.x1 > self.x2):
                    vector.y2 = self.y2
                    vector.x2 = self.x2
                    return
                elif (vector.x1 > self.x1 and vector.x1 < self.x2 and self.x2 > self.x1) or (vector.x1 < self.x1 and vector.x1 > self.x2 and self.x2 < self.x1):
                    if vector.x2 > vector.x1 and self.x1 < self.x2 or vector.x2 < vector.x1 and self.x1 > self.x2:
                        vector.y2 = self.y2
                        vector.x2 = self.x2
                        return
                    elif vector.x2 > vector.x1 and self.x1 > self.x2 or vector.x2 < vector.x1 and self.x1 < self.x2:
                        vector.y2 = self.y1
                        vector.x2 = self.x1
                        return
                elif (vector.x2 > self.x1 and vector.x2 < self.x2 and self.x2 > self.x1) or (vector.x2 < self.x1 and vector.x2 > self.x2 and self.x2 < self.x1):
                    if vector.x2 > vector.x1 and self.x1 < self.x2 or vector.x2 < vector.x1 and self.x1 > self.x2:
                        vector.y2 = self.y1
                        vector.x2 = self.x1
                        return
                    elif vector.x2 > vector.x1 and self.x1 > self.x2 or vector.x2 < vector.x1 and self.x1 < self.x2:
                        vector.y2 = self.y2
                        vector.x2 = self.x2
                        return
                
    def vectorovercheck(self, vector):
        """checks if lines are over each other and if so changes the lines"""
        
        if not self.y1 == self.y2 and not vector.y1 == vector.y2:
            a = (vector.x1 - vector.x2)/(vector.y1 - vector.y2)
            b = vector.x1 - a * vector.y1
            c = (self.x1 - self.x2)/(self.y1 - self.y2)
            d = self.x1 - c * self.y1
            if a == c and b == d:
                if (vector.y1 < self.y1 and vector.y2 > self.y2 and vector.y1 < vector.y2 and self.y1 < self.y2) or (vector.y1 > self.y1 and vector.y2 < self.y2 and vector.y1 > vector.y2 and self.y1 > self.y2):
                    return True
                elif (vector.y1 > self.y1 and vector.y2 < self.y2 and vector.y1 > vector.y2 and self.y1 < self.y2) or (vector.y1 < self.y1 and vector.y2 > self.y2 and vector.y1 < vector.y2 and self.y1 > self.y2):
                    return True
                elif (vector.y1 > self.y1 and vector.y1 < self.y2 and self.y2 > self.y1) or (vector.y1 < self.y1 and vector.y1 > self.y2 and self.y2 < self.y1):
                    if vector.y2 > vector.y1 and self.y1 < self.y2 or vector.y2 < vector.y1 and self.y1 > self.y2:
                        return True
                    elif vector.y2 > vector.y1 and self.y1 > self.y2 or vector.y2 < vector.y1 and self.y1 < self.y2:
                        return True
                elif (vector.y2 > self.y1 and vector.y2 < self.y2 and self.y2 > self.y1) or (vector.y2 < self.y1 and vector.y2 > self.y2 and self.y2 < self.y1):
                    if vector.y2 > vector.y1 and self.y1 < self.y2 or vector.y2 < vector.y1 and self.y1 > self.y2:
                        return True
                    elif vector.y2 > vector.y1 and self.y1 > self.y2 or vector.y2 < vector.y1 and self.y1 < self.y2:
                        return True
        elif not self.x1 == self.x2 and not vector.x1 == vector.x2:
            a = (vector.y1 - vector.y2)/(vector.x1 - vector.x2)
            b = vector.y1 - a * vector.x1
            c = (self.y1 - self.y2)/(self.x1 - self.x2)
            d = self.y1 - c * self.x1
            if a == c and b == d:
                if (vector.x1 < self.x1 and vector.x2 > self.x2 and vector.x1 < vector.x2 and self.x1 < self.x2) or (vector.x1 > self.x1 and vector.x2 < self.x2 and vector.x1 > vector.x2 and self.x1 > self.x2):
                    return True
                elif (vector.x1 > self.x1 and vector.x2 < self.x2 and vector.x1 > vector.x2 and self.x1 < self.x2) or (vector.x1 < self.x1 and vector.x2 > self.x2 and vector.x1 < vector.x2 and self.x1 > self.x2):
                    return True
                elif (vector.x1 > self.x1 and vector.x1 < self.x2 and self.x2 > self.x1) or (vector.x1 < self.x1 and vector.x1 > self.x2 and self.x2 < self.x1):
                    if vector.x2 > vector.x1 and self.x1 < self.x2 or vector.x2 < vector.x1 and self.x1 > self.x2:
                        return True
                    elif vector.x2 > vector.x1 and self.x1 > self.x2 or vector.x2 < vector.x1 and self.x1 < self.x2:
                        return True
                elif (vector.x2 > self.x1 and vector.x2 < self.x2 and self.x2 > self.x1) or (vector.x2 < self.x1 and vector.x2 > self.x2 and self.x2 < self.x1):
                    if vector.x2 > vector.x1 and self.x1 < self.x2 or vector.x2 < vector.x1 and self.x1 > self.x2:
                        return True
                    elif vector.x2 > vector.x1 and self.x1 > self.x2 or vector.x2 < vector.x1 and self.x1 < self.x2:
                        return True
        return False
    
    def getsource(self, toelement):
        """returns the output"""
        
        if self.loopcheck == True:
            self.inconsistent = True
            self.loopcheck = False
            return False
        else:
            self.loopcheck = True
        if self.elementin == None:
            self.inconsistent = True
            return False
        source = self.elementin.getsource(self)
        self.loopcheck = False
        if source == False:
            self.inconsistent = True
            return False
        else:
            self.inconsistent = False
            return source
    
    def clear(self):
        """clear the dtmodel"""
        
        self.loopcheck = False
        self.inconsistent = False   
        
    def setnodeid(self, maxid):
        """sets the nodeid"""
        
        return maxid
    
    def creatdtelement(self, nodeslist):
        """does nothing"""
        
        return True
    
    def getdtelement(self, nodeslist):
        """returns two emtpy lists"""
        
        return [[], []]
    
    def readyforsave(self):
        """makes the element ready for save"""
        
        self.surface = None
        
    def restore(self, surface):
        """restores the object after save"""
        
        self.surface = surface
        
    def setmarked(self):
        """marks the element"""
        
        self.marked = True
        

class FLabel:
    """Circuit Label element"""
    
    def __init__(self, surface, type_, level=1):
        """inits the Label element"""
        
        self.surface = surface
        self.width = 2*DISTANCE
        self.height = 2*DISTANCE
        self.left = 0 
        self.top = 0
        self.type = type_
        self.floating = True
        self.justadded = True
        self.posx = 0
        self.posy = 0
        self.orientation = 0
        self.collided = False
        self.vector = None
        self.level = level
        self.marked = False
        self.p = None
        self.shift = False
        self.loopcheck = False
        self.inconsistent = False
        
    def update(self, event):
        """updates the label element in accordacne with the user input"""
        
        if event.type == "MOUSEMOVE":
            if self.floating:
                self.justadded = False
                zoom = self.surface.cadsurface.zoom
                surfaceposx = self.surface.cadsurface.posx
                surfaceposy = self.surface.cadsurface.posy
                self.posx = (round((event.x - (surfaceposx/zoom % (DISTANCE/zoom)) - self.width/2)/DISTANCE*zoom)*DISTANCE/zoom + surfaceposx/zoom % (DISTANCE/zoom))*zoom - surfaceposx
                self.posy = (round((event.y - (surfaceposy/zoom % (DISTANCE/zoom)) - self.height/2)/DISTANCE*zoom)*DISTANCE/zoom + surfaceposy/zoom % (DISTANCE/zoom))*zoom - surfaceposy
                self.collided = False
                for e in self.surface.elements:
                    if not e == self:
                        if e.collide(self._cornerpoints()):
                            self.collided = True
                            break
                return True
        elif event.type == "MOUSELEFTBUTTON":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.floating and not self.collided:
                self.collided = True
                if event.key == "SHIFT":
                    typeelement = self.type.element
                    typeelementout = self.type.elementin
                    self.type.element = None
                    self.type.elementin = None
                    type_ = copy.deepcopy(self.type)
                    self.type.element = typeelement
                    self.type.elementin = typeelementout
                    self.type = type_
                self.surface.elements.append(self.copy())
                self.add()
                self.surface.addhistory(self.remove, self.add)
                self.surface.setunsaved()
                return True
            elif self.surface.deletemode and self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                self.remove()
                self.surface.addhistory(self.add, self.remove)
                self.surface.setunsaved()
                return True
            elif self.floating == False and self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                if self.marked == False:
                    self.marked = True
                else:
                    self.marked = False
                return True
        elif event.type == "MOUSEWHEEL":
            if self.floating:
                self.orientation = self.orientation + int(event.value)
                if self.orientation < 0:
                    self.orientation = 3
                elif self.orientation > 3:
                    self.orientation = 0
                return True
        elif event.type == "MOUSERIGHTBUTTON":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.floating:
                self.surface.elements.remove(self)
                return True
            else:
                if self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]) and not self.surface.deletemode:
                    floating = False
                    for e in self.surface.elements:
                        if type(e) == UElement or type(e) == CLabel or type(e) == FLabel or type(e) == SubCircuitElement:
                            if e.floating:
                                floating = True
                                break
                        elif type(e) == Line or type(e) == Vector:
                            if e.active:
                                floating = True
                                break
                    if not floating:
                        if event.key == "SHIFT":
                            ecopy = self.copy(fullcopy=True)
                        else:
                            ecopy = self.copy()
                        ecopy.floating = True
                        ecopy.collided = True
                        self.surface.elements.append(ecopy)
                        if self.surface.autoremove:
                            self.surface.addhistory()
                            self.surface.setunsaved()
                            self.remove()
                        return True
        if event.type == "KEY_P":
            if event.key == "DEL" and self.floating == True:
                self.surface.elements.remove(self)
                return True
            if event.key == "CTRL_R" and self.floating == True:
                self.orientation = self.orientation - 1
                if self.orientation < 0:
                    self.orientation = 3
                elif self.orientation > 3:
                    self.orientation = 0
                return True
            if event.key == "SHIFT" and self.floating == True:
                self.shift = True
                return True
        if event.type == "KEY_R":
            if event.key == "SHIFT" and self.floating == True:
                self.shift = False
                return True
        if event.type == "MOUSELEFTDOUBLE":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                self.marked = not self.marked
                self.p = twindow.TypeWindow(self.type, pos=self.surface.mainWindowposition())
                self.p.show()
                self.surface.setunsaved()
                return True
            
    def draw(self):
        """draws the label"""
        
        if self.justadded == False:
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            version = 0
            if self.type.connectedtype == 0:
                version = 1
                if self.type.elementin == self or self.floating and self.type.elementin == None or self.shift:
                    version = 0
            if self.floating:
                self.width = 2*DISTANCE/zoom
                self.height = 2*DISTANCE/zoom
                self.left = (self.posx + surfaceposx)/zoom
                self.top = (self.posy + surfaceposy)/zoom
                if self.collided:
                    self.type.draw(self.surface, (self.left, self.top), (self.width, self.height), self.type.label, orientation=self.orientation, color=RED, version=version)
                else:
                    self.type.draw(self.surface, (self.left, self.top), (self.width, self.height), self.type.label, orientation=self.orientation, color=WHITE, version=version)
            elif not self.floating:
                zoom = self.surface.cadsurface.zoom
                surfaceposx = self.surface.cadsurface.posx
                surfaceposy = self.surface.cadsurface.posy
                self.width = 2*DISTANCE/zoom
                self.height = 2*DISTANCE/zoom
                self.left = (self.posx + surfaceposx)/zoom
                self.top = (self.posy + surfaceposy)/zoom
                if not self.surface.deletemode:
                    if self.inconsistent:
                        self.type.draw(self.surface, (self.left, self.top), (self.width, self.height), self.type.label, orientation=self.orientation, color=RED, version=version)
                    if self.marked == False:
                        self.type.draw(self.surface, (self.left, self.top), (self.width, self.height), self.type.label, orientation=self.orientation, color=GREEN, version=version)
                    else:
                        if self.type.connectedtype == 0:
                            if not self.type.elementin == None and not self.type.element == None:
                                for i in self.type.element:
                                    self.surface.drawline(PINK, ((self.type.elementin.posx + surfaceposx)/zoom + self.width/2, (self.type.elementin.posy + surfaceposy)/zoom + self.height/2), ((i.posx + surfaceposx)/zoom + self.width/2, (i.posy + surfaceposy)/zoom + self.height/2))
                        self.type.draw(self.surface, (self.left, self.top), (self.width, self.height), self.type.label, orientation=self.orientation, color=PINK, version=version)
                else:
                    self.type.draw(self.surface, (self.left, self.top), (self.width, self.height), self.type.label, orientation=self.orientation, color=BLUE, version=version)
            
    def _cornerpoints(self):
        """returns the corner points of the element"""
        
        return ((self.posx + 2, self.posy + 2), (self.posx + 2*DISTANCE - 2, self.posy + 2), (self.posx + 2*DISTANCE - 2, self.posy + 2*DISTANCE - 2), (self.posx + 2, self.posy + 2*DISTANCE - 2))
    
    def remove(self):
        self.surface.elements.remove(self)
        for e in self.surface.elements:
            if type(e) == Arrow:
                e.removeelement(self)
        if self.type.elementin == self:
            self.type.elementin = None
        elif self in self.type.element:
            self.type.element.remove(self)
    
    def add(self):
        """adds the element"""
        
        if not self in self.surface.elements:
            self.surface.elements.append(self)
        self.collided = False
        self.floating = False
        if self.type.connectedtype == -1 or self.type.connectedtype == 1 or self.type.connectedtype == 2:
            self.type.element = self
        elif self.type.connectedtype == 0:
            if self.type.elementin == None:
                self.type.elementin = self
            elif self.type.element == None:
                self.type.element = [self]
            else:
                self.type.element.append(self)
        if (not self.type.elementin == None and not self.type.elementin == self) or self.type.connectedtype == 1:
            if self.orientation == 2:
                self.surface.elements.append(Arrow(self.surface, self, None, (self.posx, self.posy + DISTANCE), orientation=self.orientation))
            elif self.orientation == 1:
                self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE, self.posy), orientation=self.orientation))
            elif self.orientation == 0:
                self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*2, self.posy + DISTANCE), orientation=self.orientation))
            else:
                self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE, self.posy + DISTANCE*2), orientation=self.orientation))
        else:
            if self.orientation == 0:
                self.surface.elements.append(Arrow(self.surface, None, self, (self.posx, self.posy + DISTANCE), orientation=self.orientation))
            elif self.orientation == 3:
                self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE, self.posy), orientation=self.orientation))
            elif self.orientation == 2:
                self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*2, self.posy + DISTANCE), orientation=self.orientation))
            else:
                self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE, self.posy + DISTANCE*2), orientation=self.orientation))
        self.vector = self.surface.elements[-1]
        if self.vector.elementin == None:
            for i in self.surface.elements:
                if type(i) == Arrow:
                    if not i == self.vector and abs(i.x1-self.vector.x1)<2 and abs(i.y1-self.vector.y1)<2:
                            self.surface.elements.remove(self.vector)
                            i.addoutelement(self)
                            self.vector = i
                            break
                if type(i) == Vector:
                    if i.collide(((self.vector.x1 - 7, self.vector.y1  - 7), (self.vector.x1 + 7, self.vector.y1 + 7))):
                        self.vector.addinelement(i)
                        break
        elif self.vector.elementout == []:
            for i in self.surface.elements:
                if type(i) == Arrow:
                    if not i == self.vector and abs(i.x1-self.vector.x1)<2 and abs(i.y1-self.vector.y1)<2:
                        if i.addinelement(self):
                            self.surface.elements.remove(self.vector)
                            self.vector = i
                            break
    
    def copy(self, fullcopy=False):
        """copys the element"""
        
        ecopy = copy.copy(self)
        if self.type.connectedtype == -1 or self.type.connectedtype == 1 or self.type.connectedtype == 2 or fullcopy == True:
            if not self.type.element == None or not self.type.elementin == None:
                typeelement = self.type.element
                typeelementout = self.type.elementin
                self.type.element = None
                self.type.elementin = None
                ecopy.type = copy.deepcopy(self.type)
                self.type.element = typeelement
                self.type.elementin = typeelementout
            else:
                ecopy.type = copy.deepcopy(self.type)
            return ecopy
        return ecopy
            
        
    def collide(self, points):
        """checks for collision with the given points
            and lines between the points
            """
        height = 2
        width = 2
        for p in points:
            if p[0] >= self.posx + 1 and p[0] <= self.posx + width*DISTANCE - 1 and p[1] >= self.posy + 1 and p[1] <= self.posy + height*DISTANCE - 1:
                return True
        for i, p in enumerate(points):
            if i > 0:
                if not points[i-1][0] == points[i][0]:
                    a = (points[i-1][1] - points[i][1])/(points[i-1][0] - points[i][0])
                    b = points[i-1][1] - a * points[i-1][0]
                    if points[i-1][0] >= self.posx + 1 and points[i][0] <= self.posx + 1 or points[i-1][0] <= self.posx + 1 and points[i][0] >= self.posx + 1:
                        y = a * (self.posx + 1) + b
                        if y >= self.posy + 1 and y <= self.posy + height*DISTANCE - 1:
                            return True
                    if points[i-1][0] >= self.posx + width*DISTANCE - 1 and points[i][0] <= self.posx + width*DISTANCE - 1 or points[i-1][0] <= self.posx + width*DISTANCE - 1 and points[i][0] >= self.posx + width*DISTANCE - 1:
                        y = a * (self.posx + width*DISTANCE - 1) + b
                        if y >= self.posy + 1 and y <= self.posy + height*DISTANCE - 1:
                            return True
                if not points[i-1][1] == points[i][1]:
                    a = (points[i-1][0] - points[i][0])/(points[i-1][1] - points[i][1])
                    b = points[i-1][0] - a * points[i-1][1]
                    if points[i-1][1] >= self.posy + 1 and points[i][1] <= self.posy + 1 or points[i-1][1] <= self.posy + 1 and points[i][1] >= self.posy + 1:
                        x = a * (self.posy + 1) + b
                        if x >= self.posx + 1 and x <= self.posx + width*DISTANCE - 1:
                            return True
                    if points[i-1][1] >= self.posy + height*DISTANCE - 1 and points[i][1] <= self.posy + height*DISTANCE - 1 or points[i-1][1] <= self.posy + height*DISTANCE - 1 and points[i][1] >= self.posy + height*DISTANCE - 1:
                        x = a * (self.posy + height*DISTANCE - 1) + b
                        if x >= self.posx + 1 and x <= self.posx + width*DISTANCE - 1:
                            return True
        xfree = True
        yfree = True
        x1 = self.posx + 1
        x2 = self.posx + width*DISTANCE - 1
        y1 = self.posy + 1
        y2 = self.posy + height*DISTANCE - 1
        xcount = 0
        for p in points:
            if p[0] < x1 and p[0] < x2 :
                xcount = xcount + 1
            elif (p[0] < x1 and p[0] > x2) or (p[0] > x1 and p[0] < x2):
                xfree = False
        if xcount > 0 and not len(points) == xcount:
            xfree = False
        ycount = 0
        for p in points:
            if p[1] < y1 and p[1] < y2:
                ycount = ycount + 1
            elif (p[1] < y1 and p[1] > y2) or (p[1] > y1 and p[1] < y2):
                yfree = False
        if ycount > 0 and not len(points) == ycount:
            yfree = False
        if yfree == False and xfree == False:
            return True
        return False

    def getsource(self, toelement):
        """returns the output"""
        
        if self.loopcheck == True:
            self.inconsistent = True
            self.loopcheck = False
            return False
        if self.type.connectedin == True and self.type.connectedtype == 2:
            if self.vector == None:
                self.inconsistent = True
                return False
            source = self.vector.getsource(self)
        elif self.type.connectedin == True and self.type.connectedtype == 1:
            if self.type.elementin == None:
                self.inconsistent = True
                return False
            source = self.type.elementin.getsource(self)
        else:
            if self.type.elementin == None:
                self.inconsistent = True
                return False
            if self.type.elementin.vector == None:
                self.inconsistent = True
                return False
            source = self.type.elementin.vector.getsource(self)
        if source == False:
            self.inconsistent = True
            return False
        else:
            self.inconsistent = False
            return source
    
    def clear(self):
        """clear the dtmodel"""
        
        self.loopcheck = False
        self.inconsistent = False
        
    def setnodeid(self, maxid):
        """sets the nodeid"""
        
        return maxid
    
    def creatdtelement(self, nodeslist):
        """does nothing"""
        
        return True
    
    def getdtelement(self, nodeslist):
        """returns two emtpy lists"""
        
        return [[], []]
    
    def readyforsave(self):
        """makes the element ready for save"""
        
        self.surface = None
        # if self.type.connectedin == True and self.type.connectedtype == 1 and not self.type.elementin == None:
            # self.type.elementin.surface = None
        self.type.elementin = None
        
    def restore(self, surface):
        """restores the object after save"""
        
        self.surface = surface
        
    def setmarked(self):
        """marks the element"""
        
        self.marked = True
    

###############################################################################
# subcircuit elements
###############################################################################

class SubCircuitElement:
    
    def __init__(self, surface, type_, level=1):
        """inits the subcircuit"""
        
        self.surface = surface
        self.width = DISTANCE
        self.height = DISTANCE
        self.left = 0 
        self.top = 0
        self.type = type_
        self.cadcircuit = None
        self.floating = True
        self.justadded = True
        self.posx = 0
        self.posy = 0
        self.orientation = 0
        self.collided = False
        self.nodes = []
        self.vectorin = []
        self.vectorout = []
        self.level = level
        self.marked = False
        self.p = None
        self.shift = False
        self.loopcheck = False
        self.inconsistent = False
        self.failure = False
        if not self.type.sub == None:
            if type(self.type.sub) == Savecircuit:
                self.cadcircuit = self.type.sub
            elif type(self.type.sub) == str:
                try:
                    circuitobject = open(self.type.sub, "rb")
                    loadobject = pc.load(circuitobject)
                    circuitobject.close()
                    if type(loadobject) == Savecircuit:
                        self.cadcircuit = loadobject
                        self.cadcircuit.name = self.type.name
                except:
                    self.errordialog = QtWidgets.QErrorMessage()
                    self.errordialog.showMessage("unable to load element")
                    return
            for e in self.cadcircuit.elements:
                if type(e) == FLabel:
                    if e.type.connectedin == True:
                        if e.type.connectedtype == 1:
                            self.type.vectorin.append(e)
                            e.type.elementin = self
                        elif e.type.connectedtype == 2:
                            self.type.vectorout.append(e)
                if type(e) == CLabel:
                    if e.type.connectedin == True:
                        self.type.nodes.append(e)
        else:
            self.cadcircuit = Savecircuit(ImageWidget(name=self.type.name))
            
        
    def update(self, event):
        """updates the label element in accordacne with the user input"""
        
        if event.type == "MOUSEMOVE":
            if self.floating:
                self.justadded = False
                zoom = self.surface.cadsurface.zoom
                surfaceposx = self.surface.cadsurface.posx
                surfaceposy = self.surface.cadsurface.posy
                self.posx = (round((event.x - (surfaceposx/zoom % (DISTANCE/zoom)) - self.width/2)/DISTANCE*zoom)*DISTANCE/zoom + surfaceposx/zoom % (DISTANCE/zoom))*zoom - surfaceposx
                self.posy = (round((event.y - (surfaceposy/zoom % (DISTANCE/zoom)) - self.height/2)/DISTANCE*zoom)*DISTANCE/zoom + surfaceposy/zoom % (DISTANCE/zoom))*zoom - surfaceposy
                self.collided = False
                for e in self.surface.elements:
                    if not e == self:
                        if e.collide(self._cornerpoints()):
                            self.collided = True
                            break
                return True
        elif event.type == "MOUSELEFTBUTTON":
            if self.floating and not self.collided:
                self.collided = True
                self.surface.elements.append(self.copy())
                self.add()
                self.surface.setunsaved()
                self.surface.addhistory(self.remove, self.add)
                return True
            if self.floating == False:
                zoom = self.surface.cadsurface.zoom
                surfaceposx = self.surface.cadsurface.posx
                surfaceposy = self.surface.cadsurface.posy
                if not self.surface.deletemode and self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                    self.marked = not self.marked
                    return True
                elif self.surface.deletemode and self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                    self.remove()
                    self.surface.setunsaved()
                    self.surface.addhistory(self.add, self.remove)
                    return True
        elif event.type == "MOUSERIGHTBUTTON":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.floating:
                self.surface.elements.remove(self)
                return True
            else:
                if self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]) and not self.surface.deletemode:
                    floating = False
                    for e in self.surface.elements:
                        if type(e) == UElement or type(e) == CLabel or type(e) == FLabel or type(e) == SubCircuitElement:
                            if e.floating:
                                floating = True
                                break
                        elif type(e) == Line or type(e) == Vector:
                            if e.active:
                                floating = True
                                break
                    if not floating:
                        ecopy = self.copy()
                        ecopy.floating = True
                        ecopy.collided = True
                        self.surface.elements.append(ecopy)
                        self.surface.elementsreverse.append(ecopy)
                        if self.surface.autoremove:
                            self.remove()
                            self.surface.setunsaved()
                            self.surface.addhistory(self.add, self.remove)
                        return True
        elif event.type == "MOUSEWHEEL":
            if self.floating:
                oldorientation = self.orientation
                self.orientation = self.orientation + int(event.value)
                if self.orientation < 0:
                    self.orientation = 3
                elif self.orientation > 3:
                    self.orientation = 0
                if (self.orientation == 1 or self.orientation == 3) and (oldorientation == 0 or oldorientation == 2):
                    self.posx = self.posx - DISTANCE*int((max(len(self.type.vectorin), len(self.type.vectorout)) - len(self.type.nodes) - 1)/2)
                    self.posy = self.posy + DISTANCE*int((max(len(self.type.vectorin), len(self.type.vectorout)) - len(self.type.nodes) - 1)/2)
                elif (self.orientation == 0 or self.orientation == 2) and (oldorientation == 1 or oldorientation == 3):
                    self.posx = self.posx + DISTANCE*int((max(len(self.type.vectorin), len(self.type.vectorout)) - len(self.type.nodes) - 1)/2)
                    self.posy = self.posy - DISTANCE*int((max(len(self.type.vectorin), len(self.type.vectorout)) - len(self.type.nodes) - 1)/2)
                return True
        if event.type == "KEY_P":
            if event.key == "DEL" and self.floating == True:
                self.surface.elements.remove(self)
                self.surface.setunsaved()
                return True
            if event.key == "CTRL_R" and self.floating == True:
                oldorientation = self.orientation
                self.orientation = self.orientation + 1
                if self.orientation < 0:
                    self.orientation = 3
                elif self.orientation > 3:
                    self.orientation = 0
                if (self.orientation == 1 or self.orientation == 3) and (oldorientation == 0 or oldorientation == 2):
                    self.posx = self.posx - DISTANCE*int((max(len(self.type.vectorin), len(self.type.vectorout)) - len(self.type.nodes) - 1)/2)
                    self.posy = self.posy + DISTANCE*int((max(len(self.type.vectorin), len(self.type.vectorout)) - len(self.type.nodes) - 1)/2)
                elif (self.orientation == 0 or self.orientation == 2) and (oldorientation == 1 or oldorientation == 3):
                    self.posx = self.posx + DISTANCE*int((max(len(self.type.vectorin), len(self.type.vectorout)) - len(self.type.nodes) - 1)/2)
                    self.posy = self.posy - DISTANCE*int((max(len(self.type.vectorin), len(self.type.vectorout)) - len(self.type.nodes) - 1)/2)
                return True
        if event.type == "MOUSELEFTDOUBLE":
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            if self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                if event.key == "SHIFT" and self.type.lock == False or self.type.lock == True:
                    self.marked = not self.marked
                    self.namechangewindow = sctwindow.SubCircuitTypeWindow(self.type.name, self.type.lock, pos=self.surface.mainWindowposition())
                    self.namechangewindow.show()
                    self.namechangewindow.namechange.connect(self.namechange)
                    self.namechangewindow.lockchange.connect(self.lockchange)
                    self.namechangewindow.closed.connect(self.namechangewindowclose)
                    self.namechangewindow.load.connect(self.loadcircuit)
                    self.namechangewindow.savesignal.connect(self.saveciruit)
                else:
                    zoom = self.surface.cadsurface.zoom
                    surfaceposx = self.surface.cadsurface.posx
                    surfaceposy = self.surface.cadsurface.posy
                    if self.collide([(event.x*zoom - surfaceposx, event.y*zoom -surfaceposy)]):
                        self.marked = not self.marked
                        self.surface.opensubcircuit.emit(self)
          
    def draw(self):
        """draws the element"""
        
        if self.justadded == False:
            zoom = self.surface.cadsurface.zoom
            surfaceposx = self.surface.cadsurface.posx
            surfaceposy = self.surface.cadsurface.posy
            orientationhorizontal = False
            orientationvertical = False
            if self.orientation == 0 or self.orientation == 2:
                orientationhorizontal = True
            else:
                orientationvertical = True
            if self.floating:
                distance8 = DISTANCE/zoom/4
                distance4 = DISTANCE/zoom/2
                distance2 = DISTANCE/zoom
                self.width = DISTANCE/zoom + ((len(self.type.vectorin) + len(self.type.vectorout))*DISTANCE/zoom)*orientationvertical + len(self.type.nodes)*DISTANCE/zoom*orientationhorizontal
                self.height = DISTANCE/zoom + ((len(self.type.vectorin) + len(self.type.vectorout))*DISTANCE/zoom)*orientationhorizontal + len(self.type.nodes)*DISTANCE/zoom*orientationvertical
                self.left = (self.posx + surfaceposx)/zoom
                self.top = (self.posy + surfaceposy)/zoom
                if self.collided:
                    if not self.type.draw == None:
                        self.propertyelement.image(self.surface, (self.left + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationvertical, self.top + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationhorizontal), (2*DISTANCE/zoom, 2*DISTANCE/zoom), orientation=self.orientation, color=RED)
                    self.surface.drawrect(RED, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8)
                    for i, _ in enumerate(self.type.nodes):
                        if self.orientation == 0:
                            self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                        elif self.orientation == 3:
                            self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                        elif self.orientation == 2:
                            self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                        else:
                            self.surface.drawline(RED, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                    for i, _ in enumerate(self.type.vectorin):
                        if self.orientation == 0:
                            self.surface.drawline(RED, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                        elif self.orientation == 3:
                            self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                        elif self.orientation == 2:
                            self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                        else:
                            self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                    for i, _ in enumerate(self.type.vectorout):
                        if self.orientation == 0:
                            self.surface.drawline(RED, (self.left, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                        elif self.orientation == 3:
                            self.surface.drawline(RED, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + distance8), width=1)
                        elif self.orientation == 2:
                            self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + self.width, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                        else:
                            self.surface.drawline(RED, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height - distance8), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height), width=1)
                    if self.orientation == 0:
                        self.surface.drawrect(RED, (self.left + distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    elif self.orientation == 3:
                        self.surface.drawrect(RED, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    elif self.orientation == 2:
                        self.surface.drawrect(RED, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    else:
                        self.surface.drawrect(RED, (self.left + distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    if self.type.lock == True:
                        self.surface.drawrect(RED, (self.left + self.width - distance8/2, self.top + self.height - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1)
                        self.surface.drawarc(RED, (self.left + self.width - distance8/2, self.top + self.height - distance8 - DISTANCE/zoom/8, DISTANCE/zoom/6, DISTANCE/zoom/4), (0, 180), width = 1)
                else:
                    if not self.type.draw == None:
                        self.propertyelement.image(self.surface, (self.left + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationvertical, self.top + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationhorizontal), (2*DISTANCE/zoom, 2*DISTANCE/zoom), orientation=self.orientation, color=WHITE)
                    self.surface.drawrect(WHITE, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8)
                    for i, _ in enumerate(self.type.nodes):
                        if self.orientation == 0:
                            self.surface.drawline(WHITE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                        elif self.orientation == 3:
                            self.surface.drawline(WHITE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                        elif self.orientation == 2:
                            self.surface.drawline(WHITE, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                        else:
                            self.surface.drawline(WHITE, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                    for i, _ in enumerate(self.type.vectorin):
                        if self.orientation == 0:
                            self.surface.drawline(WHITE, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                        elif self.orientation == 3:
                            self.surface.drawline(WHITE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                        elif self.orientation == 2:
                            self.surface.drawline(WHITE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                        else:
                            self.surface.drawline(WHITE, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                    for i, _ in enumerate(self.type.vectorout):
                        if self.orientation == 0:
                            self.surface.drawline(WHITE, (self.left, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                        elif self.orientation == 3:
                            self.surface.drawline(WHITE, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + distance8), width=1)
                        elif self.orientation == 2:
                            self.surface.drawline(WHITE, (self.left + self.width - distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + self.width, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                        else:
                            self.surface.drawline(WHITE, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height - distance8), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height), width=1)
                    if self.orientation == 0:
                        self.surface.drawrect(WHITE, (self.left + distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    elif self.orientation == 3:
                        self.surface.drawrect(WHITE, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    elif self.orientation == 2:
                        self.surface.drawrect(WHITE, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    else:
                        self.surface.drawrect(WHITE, (self.left + distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    if self.type.lock == True:
                        self.surface.drawrect(WHITE, (self.left + self.width - distance8/2, self.top + self.height - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1)
                        self.surface.drawarc(WHITE, (self.left + self.width - distance8/2, self.top + self.height - distance8 - DISTANCE/zoom/8, DISTANCE/zoom/6, DISTANCE/zoom/4), (0, 180), width = 1)
            elif not self.floating:
                zoom = self.surface.cadsurface.zoom
                surfaceposx = self.surface.cadsurface.posx
                surfaceposy = self.surface.cadsurface.posy
                distance8 = DISTANCE/zoom/4
                distance4 = DISTANCE/zoom/2
                distance2 = DISTANCE/zoom
                self.width = DISTANCE/zoom + ((len(self.type.vectorin) + len(self.type.vectorout))*DISTANCE/zoom)*orientationvertical + len(self.type.nodes)*DISTANCE/zoom*orientationhorizontal
                self.height = DISTANCE/zoom + ((len(self.type.vectorin) + len(self.type.vectorout))*DISTANCE/zoom)*orientationhorizontal + len(self.type.nodes)*DISTANCE/zoom*orientationvertical
                self.left = (self.posx + surfaceposx)/zoom
                self.top = (self.posy + surfaceposy)/zoom
                if self.surface.deletemode == False:
                    if self.failure == True or self.inconsistent == True:
                        if not self.type.draw == None:
                            self.propertyelement.image(self.surface, (self.left + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationvertical, self.top + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationhorizontal), (2*DISTANCE/zoom, 2*DISTANCE/zoom), orientation=self.orientation, color=RED)
                        self.surface.drawrect(RED, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8, alpha=65, fill=True)
                        self.surface.drawtext(RED, (self.left + self.width, self.top + self.height + 8/zoom), self.type.name, 16/zoom, "Arial")
                        for i, n in enumerate(self.type.nodes):
                            if self.orientation == 0:
                                self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                self.surface.drawtext(RED, (self.left + distance2*(i + 1) + distance4, self.top), n.type.label, 10/zoom, "Arial", orientation=-90)
                            elif self.orientation == 3:
                                self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(RED, (self.left + self.width, self.top + distance2*(i + 1) + distance4), n.type.label, 10/zoom, "Arial")
                            elif self.orientation == 2:
                                self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                                self.surface.drawtext(RED, (self.left + distance2*(len(self.type.nodes) - i) + distance4, self.top + self.height), n.type.label, 10/zoom, "Arial", orientation=90)
                            else:
                                self.surface.drawline(RED, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(RED, (self.left + distance4, self.top + distance2*(len(self.type.nodes) - i)), n.type.label, 10/zoom, "Arial")
                        for i, v in enumerate(self.type.vectorin):
                            if self.orientation == 0:
                                self.surface.drawline(RED, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(RED, (self.left + distance4, self.top + distance2*(i + 1)), v.type.label, 10/zoom, "Arial")
                            elif self.orientation == 3:
                                self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                self.surface.drawtext(RED, (self.left + distance2*(len(self.type.vectorin) - i) + distance4 + distance2*len(self.type.vectorout), self.top), v.type.label, 10/zoom, "Arial", orientation=-90)
                            elif self.orientation == 2:
                                self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(RED, (self.left + self.width, self.top + distance2*(len(self.type.vectorin) - i) + distance4 + distance2*len(self.type.vectorout)), v.type.label, 10/zoom, "Arial")
                            else:
                                self.surface.drawline(RED, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                                self.surface.drawtext(RED, (self.left + distance2*(i + 1) + distance4, self.top + self.height), v.type.label, 10/zoom, "Arial", orientation=90)
                        for i, v in enumerate(self.type.vectorout):
                            if self.orientation == 0:
                                self.surface.drawline(RED, (self.left, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                                self.surface.drawtext(RED, (self.left + distance4, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), v.type.label, 10/zoom, "Arial")
                            elif self.orientation == 3:
                                self.surface.drawline(RED, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + distance8), width=1)
                                self.surface.drawtext(RED, (self.left + distance2*(len(self.type.vectorout) - i) + distance4, self.top), v.type.label, 10/zoom, "Arial", orientation=-90)
                            elif self.orientation == 2:
                                self.surface.drawline(RED, (self.left + self.width - distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + self.width, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                                self.surface.drawtext(RED, (self.left + self.width, self.top + distance2*(len(self.type.vectorout) - i) + distance4), v.type.label, 10/zoom, "Arial")
                            else:
                                self.surface.drawline(RED, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height - distance8), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height), width=1)
                                self.surface.drawtext(RED, (self.left + distance2*(i + 1) + distance4 + distance2*len(self.type.vectorin), self.top + self.height), v.type.label, 10/zoom, "Arial", orientation=90)
                        if self.orientation == 0:
                            self.surface.drawrect(RED, (self.left + distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        elif self.orientation == 3:
                            self.surface.drawrect(RED, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        elif self.orientation == 2:
                            self.surface.drawrect(RED, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        else:
                            self.surface.drawrect(RED, (self.left + distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        if self.type.lock == True:
                            self.surface.drawrect(RED, (self.left + self.width - distance8/2, self.top + self.height - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1)
                            self.surface.drawarc(RED, (self.left + self.width - distance8/2, self.top + self.height - distance8 - DISTANCE/zoom/8, DISTANCE/zoom/6, DISTANCE/zoom/4), (0, 180), width = 1)
                    elif self.marked == True:
                        if not self.type.draw == None:
                            self.propertyelement.image(self.surface, (self.left + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationvertical, self.top + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationhorizontal), (2*DISTANCE/zoom, 2*DISTANCE/zoom), orientation=self.orientation, color=PINK)
                        self.surface.drawrect(PINK, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8, alpha=65, fill=True)
                        self.surface.drawtext(PINK, (self.left + self.width, self.top + self.height + 8/zoom), self.type.name, 16/zoom, "Arial")
                        for i, n in enumerate(self.type.nodes):
                            if self.orientation == 0:
                                self.surface.drawline(PINK, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                self.surface.drawtext(PINK, (self.left + distance2*(i + 1) + distance4, self.top), n.type.label, 10/zoom, "Arial", orientation=-90)
                            elif self.orientation == 3:
                                self.surface.drawline(PINK, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(PINK, (self.left + self.width, self.top + distance2*(i + 1) + distance4), n.type.label, 10/zoom, "Arial")
                            elif self.orientation == 2:
                                self.surface.drawline(PINK, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                                self.surface.drawtext(PINK, (self.left + distance2*(len(self.type.nodes) - i) + distance4, self.top + self.height), n.type.label, 10/zoom, "Arial", orientation=90)
                            else:
                                self.surface.drawline(PINK, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(PINK, (self.left + distance4, self.top + distance2*(len(self.type.nodes) - i)), n.type.label, 10/zoom, "Arial")
                        for i, v in enumerate(self.type.vectorin):
                            if self.orientation == 0:
                                self.surface.drawline(PINK, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(PINK, (self.left + distance4, self.top + distance2*(i + 1)), v.type.label, 10/zoom, "Arial")
                            elif self.orientation == 3:
                                self.surface.drawline(PINK, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                self.surface.drawtext(PINK, (self.left + distance2*(len(self.type.vectorin) - i) + distance4 + distance2*len(self.type.vectorout), self.top), v.type.label, 10/zoom, "Arial", orientation=-90)
                            elif self.orientation == 2:
                                self.surface.drawline(PINK, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(PINK, (self.left + self.width, self.top + distance2*(len(self.type.vectorin) - i) + distance4 + distance2*len(self.type.vectorout)), v.type.label, 10/zoom, "Arial")
                            else:
                                self.surface.drawline(PINK, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                                self.surface.drawtext(PINK, (self.left + distance2*(i + 1) + distance4, self.top + self.height), v.type.label, 10/zoom, "Arial", orientation=90)
                        for i, v in enumerate(self.type.vectorout):
                            if self.orientation == 0:
                                self.surface.drawline(PINK, (self.left, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                                self.surface.drawtext(PINK, (self.left + distance4, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), v.type.label, 10/zoom, "Arial")
                            elif self.orientation == 3:
                                self.surface.drawline(PINK, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + distance8), width=1)
                                self.surface.drawtext(PINK, (self.left + distance2*(len(self.type.vectorout) - i) + distance4, self.top), v.type.label, 10/zoom, "Arial", orientation=-90)
                            elif self.orientation == 2:
                                self.surface.drawline(PINK, (self.left + self.width - distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + self.width, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                                self.surface.drawtext(PINK, (self.left + self.width, self.top + distance2*(len(self.type.vectorout) - i) + distance4), v.type.label, 10/zoom, "Arial")
                            else:
                                self.surface.drawline(PINK, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height - distance8), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height), width=1)
                                self.surface.drawtext(PINK, (self.left + distance2*(i + 1) + distance4 + distance2*len(self.type.vectorin), self.top + self.height), v.type.label, 10/zoom, "Arial", orientation=90)
                        if self.orientation == 0:
                            self.surface.drawrect(PINK, (self.left + distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        elif self.orientation == 3:
                            self.surface.drawrect(PINK, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        elif self.orientation == 2:
                            self.surface.drawrect(PINK, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        else:
                            self.surface.drawrect(PINK, (self.left + distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        if self.type.lock == True:
                            self.surface.drawrect(PINK, (self.left + self.width - distance8/2, self.top + self.height - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1)
                            self.surface.drawarc(PINK, (self.left + self.width - distance8/2, self.top + self.height - distance8 - DISTANCE/zoom/8, DISTANCE/zoom/6, DISTANCE/zoom/4), (0, 180), width = 1)
                    else:
                        if not self.type.draw == None:
                            self.propertyelement.image(self.surface, (self.left + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationvertical, self.top + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationhorizontal), (2*DISTANCE/zoom, 2*DISTANCE/zoom), orientation=self.orientation, color=ORANGE)
                        self.surface.drawrect(ORANGE, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8, alpha=65, fill=True)
                        self.surface.drawtext(ORANGE, (self.left + self.width, self.top + self.height + 8/zoom), self.type.name, 16/zoom, "Arial")
                        for i, n in enumerate(self.type.nodes):
                            if self.orientation == 0:
                                self.surface.drawline(ORANGE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                self.surface.drawtext(ORANGE, (self.left + distance2*(i + 1) + distance4, self.top), n.type.label, 10/zoom, "Arial", orientation=-90)
                            elif self.orientation == 3:
                                self.surface.drawline(ORANGE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(ORANGE, (self.left + self.width, self.top + distance2*(i + 1) + distance4), n.type.label, 10/zoom, "Arial")
                            elif self.orientation == 2:
                                self.surface.drawline(ORANGE, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                                self.surface.drawtext(ORANGE, (self.left + distance2*(len(self.type.nodes) - i) + distance4, self.top + self.height), n.type.label, 10/zoom, "Arial", orientation=90)
                            else:
                                self.surface.drawline(ORANGE, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(ORANGE, (self.left + distance4, self.top + distance2*(len(self.type.nodes) - i)), n.type.label, 10/zoom, "Arial")
                        for i, v in enumerate(self.type.vectorin):
                            if self.orientation == 0:
                                self.surface.drawline(ORANGE, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(ORANGE, (self.left + distance4, self.top + distance2*(i + 1)), v.type.label, 10/zoom, "Arial")
                            elif self.orientation == 3:
                                self.surface.drawline(ORANGE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                                self.surface.drawtext(ORANGE, (self.left + distance2*(len(self.type.vectorin) - i) + distance4 + distance2*len(self.type.vectorout), self.top), v.type.label, 10/zoom, "Arial", orientation=-90)
                            elif self.orientation == 2:
                                self.surface.drawline(ORANGE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                                self.surface.drawtext(ORANGE, (self.left + self.width, self.top + distance2*(len(self.type.vectorin) - i) + distance4 + distance2*len(self.type.vectorout)), v.type.label, 10/zoom, "Arial")
                            else:
                                self.surface.drawline(ORANGE, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                                self.surface.drawtext(ORANGE, (self.left + distance2*(i + 1) + distance4, self.top + self.height), v.type.label, 10/zoom, "Arial", orientation=90)
                        for i, v in enumerate(self.type.vectorout):
                            if self.orientation == 0:
                                self.surface.drawline(ORANGE, (self.left, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                                self.surface.drawtext(ORANGE, (self.left + distance4, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), v.type.label, 10/zoom, "Arial")
                            elif self.orientation == 3:
                                self.surface.drawline(ORANGE, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + distance8), width=1)
                                self.surface.drawtext(ORANGE, (self.left + distance2*(len(self.type.vectorout) - i) + distance4, self.top), v.type.label, 10/zoom, "Arial", orientation=-90)
                            elif self.orientation == 2:
                                self.surface.drawline(ORANGE, (self.left + self.width - distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + self.width, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                                self.surface.drawtext(ORANGE, (self.left + self.width, self.top + distance2*(len(self.type.vectorout) - i) + distance4), v.type.label, 10/zoom, "Arial")
                            else:
                                self.surface.drawline(ORANGE, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height - distance8), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height), width=1)
                                self.surface.drawtext(ORANGE, (self.left + distance2*(i + 1) + distance4 + distance2*len(self.type.vectorin), self.top + self.height), v.type.label, 10/zoom, "Arial", orientation=90)
                        if self.orientation == 0:
                            self.surface.drawrect(ORANGE, (self.left + distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        elif self.orientation == 3:
                            self.surface.drawrect(ORANGE, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        elif self.orientation == 2:
                            self.surface.drawrect(ORANGE, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        else:
                            self.surface.drawrect(ORANGE, (self.left + distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                        if self.type.lock == True:
                            self.surface.drawrect(ORANGE, (self.left + self.width - distance8/2, self.top + self.height - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1)
                            self.surface.drawarc(ORANGE, (self.left + self.width - distance8/2, self.top + self.height - distance8 - DISTANCE/zoom/8, DISTANCE/zoom/6, DISTANCE/zoom/4), (0, 180), width = 1)
                else:
                    if not self.type.draw == None:
                        self.propertyelement.image(self.surface, (self.left + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationvertical, self.top + (max(len(self.type.vectorin), len(self.type.vectorout)) - 1)*DISTANCE/zoom/2*orientationhorizontal), (2*DISTANCE/zoom, 2*DISTANCE/zoom), orientation=self.orientation, color=BLUE)
                    self.surface.drawrect(BLUE, (self.left + distance8, self.top + distance8, self.width - distance4, self.height - distance4), width=1, radius=DISTANCE/zoom/8, alpha=65, fill=True)
                    self.surface.drawtext(BLUE, (self.left + self.width, self.top + self.height + 8/zoom), self.type.name, 16/zoom, "Arial")
                    for i, n in enumerate(self.type.nodes):
                        if self.orientation == 0:
                            self.surface.drawline(BLUE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                            self.surface.drawtext(BLUE, (self.left + distance2*(i + 1) + distance4, self.top), n.type.label, 10/zoom, "Arial", orientation=-90)
                        elif self.orientation == 3:
                            self.surface.drawline(BLUE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                            self.surface.drawtext(BLUE, (self.left + self.width, self.top + distance2*(i + 1) + distance4), n.type.label, 10/zoom, "Arial")
                        elif self.orientation == 2:
                            self.surface.drawline(BLUE, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                            self.surface.drawtext(BLUE, (self.left + distance2*(len(self.type.nodes) - i) + distance4, self.top + self.height), n.type.label, 10/zoom, "Arial", orientation=90)
                        else:
                            self.surface.drawline(BLUE, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                            self.surface.drawtext(BLUE, (self.left + distance4, self.top + distance2*(len(self.type.nodes) - i)), n.type.label, 10/zoom, "Arial")
                    for i, v in enumerate(self.type.vectorin):
                        if self.orientation == 0:
                            self.surface.drawline(BLUE, (self.left, self.top + distance2*(i + 1)), (self.left + distance8, self.top + distance2*(i + 1)), width=1)
                            self.surface.drawtext(BLUE, (self.left + distance4, self.top + distance2*(i + 1)), v.type.label, 10/zoom, "Arial")
                        elif self.orientation == 3:
                            self.surface.drawline(BLUE, (self.left + distance2*(i + 1), self.top), (self.left + distance2*(i + 1), self.top + distance8), width=1)
                            self.surface.drawtext(BLUE, (self.left + distance2*(len(self.type.vectorin) - i) + distance4 + distance2*len(self.type.vectorout), self.top), v.type.label, 10/zoom, "Arial", orientation=-90)
                        elif self.orientation == 2:
                            self.surface.drawline(BLUE, (self.left + self.width - distance8, self.top + distance2*(i + 1)), (self.left + self.width, self.top + distance2*(i + 1)), width=1)
                            self.surface.drawtext(BLUE, (self.left + self.width, self.top + distance2*(len(self.type.vectorin) - i) + distance4 + distance2*len(self.type.vectorout)), v.type.label, 10/zoom, "Arial")
                        else:
                            self.surface.drawline(BLUE, (self.left + distance2*(i + 1), self.top + self.height - distance8), (self.left + distance2*(i + 1), self.top + self.height), width=1)
                            self.surface.drawtext(BLUE, (self.left + distance2*(i + 1) + distance4, self.top + self.height), v.type.label, 10/zoom, "Arial", orientation=90)
                    for i, v in enumerate(self.type.vectorout):
                        if self.orientation == 0:
                            self.surface.drawline(BLUE, (self.left, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                            self.surface.drawtext(BLUE, (self.left + distance4, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), v.type.label, 10/zoom, "Arial")
                        elif self.orientation == 3:
                            self.surface.drawline(BLUE, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + distance8), width=1)
                            self.surface.drawtext(BLUE, (self.left + distance2*(len(self.type.vectorout) - i) + distance4, self.top), v.type.label, 10/zoom, "Arial", orientation=-90)
                        elif self.orientation == 2:
                            self.surface.drawline(BLUE, (self.left + self.width - distance8, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), (self.left + self.width, self.top + distance2*(i + 1) + distance2*len(self.type.vectorin)), width=1)
                            self.surface.drawtext(BLUE, (self.left + self.width, self.top + distance2*(len(self.type.vectorout) - i) + distance4), v.type.label, 10/zoom, "Arial")
                        else:
                            self.surface.drawline(BLUE, (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height - distance8), (self.left + distance2*(i + 1) + distance2*len(self.type.vectorin), self.top + self.height), width=1)
                            self.surface.drawtext(BLUE, (self.left + distance2*(i + 1) + distance4 + distance2*len(self.type.vectorin), self.top + self.height), v.type.label, 10/zoom, "Arial", orientation=90)
                    if self.orientation == 0:
                        self.surface.drawrect(BLUE, (self.left + distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    elif self.orientation == 3:
                        self.surface.drawrect(BLUE, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    elif self.orientation == 2:
                        self.surface.drawrect(BLUE, (self.left + self.width - DISTANCE/zoom/6 - distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    else:
                        self.surface.drawrect(BLUE, (self.left + distance8, self.top + self.height - DISTANCE/zoom/6 - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1, fill=True)
                    if self.type.lock == True:
                        self.surface.drawrect(BLUE, (self.left + self.width - distance8/2, self.top + self.height - distance8, DISTANCE/zoom/6, DISTANCE/zoom/6), width = 1)
                        self.surface.drawarc(BLUE, (self.left + self.width - distance8/2, self.top + self.height - distance8 - DISTANCE/zoom/8, DISTANCE/zoom/6, DISTANCE/zoom/4), (0, 180), width = 1)
    
    def collide(self, points):
        """checks for collision with the points and
            lines between the points
            """
            
        if self.orientation == 0 or self.orientation == 2:
            width = 1 + len(self.nodes)
            height = 1 + (len(self.type.vectorin) + len(self.type.vectorout))
            width = 1 + len(self.type.nodes)
        else:
            width = 1 + (len(self.type.vectorin) + len(self.type.vectorout))
            height = 1 + len(self.type.nodes)
        for p in points:
            if p[0] >= self.posx + 1 and p[0] <= self.posx + width*DISTANCE - 1 and p[1] >= self.posy + 1 and p[1] <= self.posy + height*DISTANCE - 1:
                return True
        for i, p in enumerate(points):
            if i > 0:
                if not points[i-1][0] == points[i][0]:
                    a = (points[i-1][1] - points[i][1])/(points[i-1][0] - points[i][0])
                    b = points[i-1][1] - a * points[i-1][0]
                    if points[i-1][0] >= self.posx + 1 and points[i][0] <= self.posx + 1 or points[i-1][0] <= self.posx + 1 and points[i][0] >= self.posx + 1:
                        y = a * (self.posx + 1) + b
                        if y >= self.posy + 1 and y <= self.posy + height*DISTANCE - 1:
                            return True
                    if points[i-1][0] >= self.posx + width*DISTANCE - 1 and points[i][0] <= self.posx + width*DISTANCE - 1 or points[i-1][0] <= self.posx + width*DISTANCE - 1 and points[i][0] >= self.posx + width*DISTANCE - 1:
                        y = a * (self.posx + width*DISTANCE - 1) + b
                        if y >= self.posy + 1 and y <= self.posy + height*DISTANCE - 1:
                            return True
                if not points[i-1][1] == points[i][1]:
                    a = (points[i-1][0] - points[i][0])/(points[i-1][1] - points[i][1])
                    b = points[i-1][0] - a * points[i-1][1]
                    if points[i-1][1] >= self.posy + 1 and points[i][1] <= self.posy + 1 or points[i-1][1] <= self.posy + 1 and points[i][1] >= self.posy + 1:
                        x = a * (self.posy + 1) + b
                        if x >= self.posx + 1 and x <= self.posx + width*DISTANCE - 1:
                            return True
                    if points[i-1][1] >= self.posy + height*DISTANCE - 1 and points[i][1] <= self.posy + height*DISTANCE - 1 or points[i-1][1] <= self.posy + height*DISTANCE - 1 and points[i][1] >= self.posy + height*DISTANCE - 1:
                        x = a * (self.posy + height*DISTANCE - 1) + b
                        if x >= self.posx + 1 and x <= self.posx + width*DISTANCE - 1:
                            return True
        xfree = True
        yfree = True
        x1 = self.posx + 1
        x2 = self.posx + width*DISTANCE - 1
        y1 = self.posy + 1
        y2 = self.posy + height*DISTANCE - 1
        xcount = 0
        for p in points:
            if p[0] < x1 and p[0] < x2 :
                xcount = xcount + 1
            elif (p[0] < x1 and p[0] > x2) or (p[0] > x1 and p[0] < x2):
                xfree = False
        if xcount > 0 and not len(points) == xcount:
            xfree = False
        ycount = 0
        for p in points:
            if p[1] < y1 and p[1] < y2:
                ycount = ycount + 1
            elif (p[1] < y1 and p[1] > y2) or (p[1] > y1 and p[1] < y2):
                yfree = False
        if ycount > 0 and not len(points) == ycount:
            yfree = False
        if yfree == False and xfree == False:
            return True
        return False
    
    def remove(self):
        """removes the element and sets the states of the ins and outs properly"""
        
        self.surface.elements.remove(self)
        for v in self.vectorin:
            v.removeelement(self)
        for v in self.vectorout:
            v.removeelement(self)
        for n in self.nodes:
            n.removeelement(self)
    
    def add(self):
        """adds the element"""
        
        self.type.nodes = []
        self.type.vectorin = []
        self.type.vectorout = []
        for e in self.cadcircuit.elements:
            if type(e) == FLabel:
                if e.floating == False:
                    if e.type.connectedin == True:
                        if e.type.connectedtype == 1:
                            self.type.vectorin.append(e)
                            e.type.elementin = self
                        elif e.type.connectedtype == 2:
                            self.type.vectorout.append(e)
            if type(e) == CLabel:
                if e.floating == False:
                    if e.type.connectedin == True:
                        self.type.nodes.append(e)
        if not self in self.surface.elements:
            self.surface.elements.append(self)
        self.collided = False
        self.floating = False
        self.type.element = self
        self.nodes = []
        self.vectorin= []
        self.vectorout = []
        if self.orientation==0:
            for i, n in enumerate(self.type.nodes):
                self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(i + 1), self.posy)))
                self.nodes.append(self.surface.elements[-1])
        elif self.orientation==3:
            for i, n in enumerate(self.type.nodes):
                self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(1 + (len(self.type.vectorin) + len(self.type.vectorout))), self.posy + DISTANCE*(i + 1))))
                self.nodes.append(self.surface.elements[-1])
        elif self.orientation==2:
            for i, n in enumerate(self.type.nodes):
                self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(len(self.type.nodes) - i), self.posy + DISTANCE*(1 + (len(self.type.vectorin) + len(self.type.vectorout))))))
                self.nodes.append(self.surface.elements[-1])
        else:
            for i, n in enumerate(self.type.nodes):
                self.surface.elements.append(Node(self.surface, self, (self.posx, self.posy + DISTANCE*(len(self.type.nodes) - i))))
                self.nodes.append(self.surface.elements[-1])
        for j, n in enumerate(self.nodes):
            for i in self.surface.elements:
                if type(i) == Node:
                    if not i == n and abs(i.x1-n.x1)<2 and abs(i.y1-n.y1)<2:
                        self.surface.elements.remove(n)
                        i.addelement(self)
                        self.nodes[j] = i
                        break
            for i in self.surface.elements:
                if type(i) == Line:
                    if i.collide(((n.x1 - 5, n.y1 - 5), (n.x1 + 5, n.y1 + 5))):
                        n.addelement(i)
                        break
        if self.orientation==0:
            for i, _ in enumerate(self.type.vectorin):
                self.surface.elements.append(Arrow(self.surface, None, self, (self.posx, self.posy + DISTANCE*(1 + i)), orientation=self.orientation))
                self.vectorin.append(self.surface.elements[-1])
            for i, _ in enumerate(self.type.vectorout):
                self.surface.elements.append(Arrow(self.surface, self, None, (self.posx, self.posy + DISTANCE*(1 + i + len(self.type.vectorin))), orientation=2))
                self.vectorout.append(self.surface.elements[-1])
        elif self.orientation==3:
            for i, _ in enumerate(self.type.vectorin):
                self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(len(self.type.vectorin) - i + len(self.type.vectorout)), self.posy), orientation=self.orientation))
                self.vectorin.append(self.surface.elements[-1])
            for i, _ in enumerate(self.type.vectorout):
                self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(len(self.type.vectorout) - i), self.posy), orientation=1))
                self.vectorout.append(self.surface.elements[-1])
        elif self.orientation==2:
            for i, _ in enumerate(self.type.vectorin):
                self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(1 + len(self.type.nodes)), self.posy + DISTANCE*(len(self.type.vectorin) - i + len(self.type.vectorout))), orientation=self.orientation))
                self.vectorin.append(self.surface.elements[-1])
            for i, _ in enumerate(self.type.vectorout):
                self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(1 + len(self.type.nodes)), self.posy + DISTANCE*(len(self.type.vectorout) - i)), orientation=0))
                self.vectorout.append(self.surface.elements[-1])
        else:
            for i, _ in enumerate(self.type.vectorin):
                self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(1 + i), self.posy + DISTANCE*(1 + len(self.type.nodes))), orientation=self.orientation))
                self.vectorin.append(self.surface.elements[-1])
            for i, _ in enumerate(self.type.vectorout):
                self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(1 + i + len(self.type.vectorin)), self.posy + DISTANCE*(1 + len(self.type.nodes))), orientation=3))
                self.vectorout.append(self.surface.elements[-1])
        for j, v in enumerate(self.vectorin):
            for i in self.surface.elements:
                if type(i) == Arrow:
                    if not i == v and abs(i.x1-v.x1)<2 and abs(i.y1-v.y1)<2:
                            self.surface.elements.remove(v)
                            i.addoutelement(self)
                            self.vectorin[j] = i
                            break
                if type(i) == Vector:
                    if i.collide(((v.x1 - 7, v.y1  - 7), (v.x1 + 7, v.y1 + 7))):
                        v.addinelement(i)
                        break
        for j, v in enumerate(self.vectorout):
            for i in self.surface.elements:
                if type(i) == Arrow:
                    if not i == v and abs(i.x1-v.x1)<2 and abs(i.y1-v.y1)<2:
                        if i.addinelement(self):
                            self.surface.elements.remove(v)
                            self.vectorout[j] = i
                            break
    
    def changecheck(self, addtohistory=True):
        """checks for changes"""
        
        self.type.name = self.cadcircuit.cadsurface.qWidget.name
        self.type.nodes = []
        self.type.vectorin = []
        self.type.vectorout = []
        for e in self.cadcircuit.elements:
            if type(e) == FLabel:
                if e.floating == False:
                    if e.type.connectedin == True:
                        if e.type.connectedtype == 1:
                            self.type.vectorin.append(e)
                            e.type.elementin = self
                        elif e.type.connectedtype == 2:
                            self.type.vectorout.append(e)
            if type(e) == CLabel:
                if e.floating == False:
                    if e.type.connectedin == True:
                        self.type.nodes.append(e)
        inputsize = len(self.type.vectorin)
        outputsize = len(self.type.vectorout)
        if self.orientation == 3:
            self.posx = self.posx - DISTANCE*((inputsize + outputsize) - (len(self.vectorin) + len(self.vectorout)))
        elif self.orientation == 2:
            self.posy = self.posy - DISTANCE*((inputsize + outputsize) - (len(self.vectorin) + len(self.vectorout)))
            self.posx = self.posx - DISTANCE*(len(self.type.nodes) - len(self.nodes))
        elif self.orientation == 1:
            self.posy = self.posy - DISTANCE*(len(self.type.nodes) - len(self.nodes))
        if not inputsize == len(self.vectorin) or not  outputsize == len(self.vectorout):
            for v in self.vectorin:
                v.removeelement(self)
            for v in self.vectorout:
                v.removeelement(self)
            for n in self.nodes:
                n.removeelement(self)
            self.nodes = []
            self.vectorout = []
            self.vectorin = []
            if self.orientation==0:
                for i, _ in enumerate(self.type.vectorin):
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx, self.posy + DISTANCE*(1 + i)), orientation=self.orientation))
                    self.vectorin.append(self.surface.elements[-1])
                for i, _ in enumerate(self.type.vectorout):
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx, self.posy + DISTANCE*(1 + i + len(self.type.vectorin))), orientation=2))
                    self.vectorout.append(self.surface.elements[-1])
            elif self.orientation==3:
                for i, _ in enumerate(self.type.vectorin):
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(len(self.type.vectorin) - i + len(self.type.vectorout)), self.posy), orientation=self.orientation))
                    self.vectorin.append(self.surface.elements[-1])
                for i, _ in enumerate(self.type.vectorout):
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(len(self.type.vectorout) - i), self.posy), orientation=1))
                    self.vectorout.append(self.surface.elements[-1])
            elif self.orientation==2:
                for i, _ in enumerate(self.type.vectorin):
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(1 + len(self.type.nodes)), self.posy + DISTANCE*(len(self.type.vectorin) - i + len(self.type.vectorout))), orientation=self.orientation))
                    self.vectorin.append(self.surface.elements[-1])
                for i, _ in enumerate(self.type.vectorout):
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(1 + len(self.type.nodes)), self.posy + DISTANCE*(len(self.type.vectorout) - i)), orientation=0))
                    self.vectorout.append(self.surface.elements[-1])
            else:
                for i, _ in enumerate(self.type.vectorin):
                    self.surface.elements.append(Arrow(self.surface, None, self, (self.posx + DISTANCE*(1 + i), self.posy + DISTANCE*(1 + len(self.type.nodes))), orientation=self.orientation))
                    self.vectorin.append(self.surface.elements[-1])
                for i, _ in enumerate(self.type.vectorout):
                    self.surface.elements.append(Arrow(self.surface, self, None, (self.posx + DISTANCE*(1 + i + len(self.type.vectorin)), self.posy + DISTANCE*(1 + len(self.type.nodes))), orientation=3))
                    self.vectorout.append(self.surface.elements[-1])
            for j, v in enumerate(self.vectorin):
                for i in self.surface.elements:
                    if type(i) == Arrow:
                        if not i == v and abs(i.x1-v.x1)<2 and abs(i.y1-v.y1)<2:
                                self.surface.elements.remove(v)
                                i.addoutelement(self)
                                self.vectorin[j] = i
                                break
                    if type(i) == Vector:
                        if i.collide(((v.x1 - 7, v.y1  - 7), (v.x1 + 7, v.y1 + 7))):
                            v.addinelement(i)
                            break
            for j, v in enumerate(self.vectorout):
                for i in self.surface.elements:
                    if type(i) == Arrow:
                        if not i == v and abs(i.x1-v.x1)<2 and abs(i.y1-v.y1)<2:
                            if i.addinelement(self):
                                self.surface.elements.remove(v)
                                self.vectorout[j] = i
                                break
        if not len(self.type.nodes) == len(self.nodes):
            for n in self.nodes:
                n.removeelement(self)
            self.nodes = []
            if self.orientation==0:
                for i, n in enumerate(self.type.nodes):
                    self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(i + 1), self.posy)))
                    self.nodes.append(self.surface.elements[-1])
            elif self.orientation==3:
                for i, n in enumerate(self.type.nodes):
                    self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(1 + (len(self.type.vectorin) + len(self.type.vectorout))), self.posy + DISTANCE*(i + 1))))
                    self.nodes.append(self.surface.elements[-1])
            elif self.orientation==2:
                for i, n in enumerate(self.type.nodes):
                    self.surface.elements.append(Node(self.surface, self, (self.posx + DISTANCE*(len(self.type.nodes) - i), self.posy + DISTANCE*(1 + (len(self.type.vectorin) + len(self.type.vectorout))))))
                    self.nodes.append(self.surface.elements[-1])
            else:
                for i, n in enumerate(self.type.nodes):
                    self.surface.elements.append(Node(self.surface, self, (self.posx, self.posy + DISTANCE*(len(self.type.nodes) - i))))
                    self.nodes.append(self.surface.elements[-1])
            for j, n in enumerate(self.nodes):
                for i in self.surface.elements:
                    if type(i) == Node:
                        if not i == n and abs(i.x1-n.x1)<2 and abs(i.y1-n.y1)<2:
                            self.surface.elements.remove(n)
                            i.addelement(self)
                            self.nodes[j] = i
                            break
                for i in self.surface.elements:
                    if type(i) == Line:
                        if i.collide(((n.x1 - 5, n.y1 - 5), (n.x1 + 5, n.y1 + 5))):
                            n.addelement(i)
                            break
        if addtohistory:
            self.surface.addhistory(self.cadcircuit.cadsurface.qWidget.historydown, self.cadcircuit.cadsurface.qWidget.historyup)
        self.surface.setunsaved()
    
    def subrevers(self):
        """reverses the subcircuit"""
        
        self.cadcircuit.cadsurface.qWidget.historydown()
        self.changecheck(addtohistory=False)
        
    def subrestore(self):
        """restores the subcircuit"""
        
        self.cadcircuit.cadsurface.qWidget.historyup()
        self.changecheck(addtohistory=False)
    
    def _cornerpoints(self):
        """returns the corner points of the element"""
        
        orientationhorizontal = False
        orientationvertical = False
        if self.orientation == 0 or self.orientation == 2:
            orientationhorizontal = True
        else:
            orientationvertical = True
        width = DISTANCE + ((len(self.type.vectorin) + len(self.type.vectorout))*DISTANCE)*orientationvertical + orientationhorizontal*len(self.type.nodes)*DISTANCE
        height = DISTANCE + ((len(self.type.vectorin) + len(self.type.vectorout))*DISTANCE)*orientationhorizontal + orientationvertical*len(self.type.nodes)*DISTANCE
        return ((self.posx + 1, self.posy + 1), (self.posx + width - 1, self.posy + 1), (self.posx + width - 1, self.posy + height - 1), (self.posx + 1, self.posy + height - 1))
    
    def copy(self):
        """copys the element"""
        
        ecopy = copy.copy(self)
        ecopy.vectorin = []
        ecopy.vectorout = []
        ecopy.nodes = []
        subsurface = self.cadcircuit.cadsurface.qWidget
        surface = self.surface
        self.surface = None
        self.cadcircuit.readyforsave()
        if not self.type.element == None:
            self.type.element = None
            ecopy.type = copy.deepcopy(self.type)
            self.type.element = self
        else:
            ecopy.type = copy.deepcopy(self.type)
        ecopy.cadcircuit = copy.deepcopy(self.cadcircuit)
        for e in self.cadcircuit.elements:
            e.surface = subsurface
        self.cadcircuit.cadsurface.qWidget = subsurface
        self.surface = surface
        return ecopy
    
    def setnodeid(self, maxid):
        """sets the nodeid"""
        
        for i, e in enumerate(self.nodes):
            if e.nodeid >-1:
                self.type.nodes[i].nodeid = e.nodeid
            else:
                maxid = maxid + 1
                self.type.nodes[i].nodeid = maxid
                e.nodeid = maxid
        maxid = self.cadcircuit.setnodesid(maxid=maxid)
        for i, e in enumerate(self.type.nodes):
            self.nodes[i].nodeid = e.nodeid
        return maxid
    
    def creatdtelement(self, nodeslist):
        """does nothing"""
        
        for e in self.cadcircuit.elements:
            if type(e) == FLabel:
                if e.floating == False:
                    if e.type.connectedin == True:
                        if e.type.connectedtype == 1:
                            e.type.elementin = self
        if self.cadcircuit.creatdtmodel(maxid=0, nodeslist=nodeslist) == True:
            return True
        else:
            self.failure = True
            return False
    
    def getdtelement(self, nodeslist):
        """returns two emtpy lists"""
        
        return self.cadcircuit.getdtmodel(nodeslist=nodeslist)
    
    def namechange(self, name):
        """changes the name of the element"""
        
        reverse = self.type.funksetname()
        self.type.name = name
        self.cadcircuit.cadsurface.qWidget.name = name
        self.cadcircuit.name = name
        self.cadcircuit.cadsurface.qWidget.changetounsaved.emit()
        restore = self.type.funksetname()
        self.surface.addhistory(reverse, restore)
        self.surface.update()
        
    def lockchange(self, lock):
        """sets the lock"""
        
        reverse = self.type.funksetlock()
        self.type.lock = lock
        restore = self.type.funksetlock()
        self.surface.addhistory(reverse, restore)
        self.surface.update()
        
    def namechangewindowclose(self):
        """closes the namechangewindow"""
        
        self.namechangewindow = None
        self.surface.setunsaved()
    
    def clear(self):
        """clear the dtmodel"""
        
        self.failure = False
        self.cadcircuit.cleardtmodel()
        self.loopcheck = False
        self.inconsistent = False
        
    def getsource(self, toelement):
        """returns the output"""
        
        if self.loopcheck == True:
            self.inconsistent = True
            self.loopcheck = False
            return False
        for i, e in enumerate(self.vectorout):
            if toelement == e:
                source = self.type.vectorout[i].getsource(self)
                break
        for i, e in enumerate(self.type.vectorin):
            if toelement == e:
                source = self.vectorin[i].getsource(self)
                break
        if source == False:
            self.inconsistent = True
            return False
        else:
            self.inconsistent = False
            return source
        
    def loadcircuit(self, file):
        """loads a circuit into the subcircuit"""
        
        circuitobject = open(file[0], "rb")
        loadobject = pc.load(circuitobject)
        circuitobject.close()
        surface = self.cadcircuit.cadsurface.qWidget
        if type(loadobject) == Savecircuit:
            reverse = self.funksetcircuit()
            self.cadcircuit = loadobject
            self.changecheck(addtohistory=False)
            self.type.name = self.cadcircuit.name
            surface.load(self.cadcircuit)
            self.surface.changezoom.emit(int(self.surface.cadsurface.zoom*100))
            restore = self.funksetcircuit()
            self.surface.addhistory(reverse, restore)
            
    def funksetcircuit(self):
        """returns a funktion that resets the circuit"""
        
        surface = self.cadcircuit.cadsurface.qWidget
        self.cadcircuit.readyforsave()
        loadobject = copy.deepcopy(self.cadcircuit)
        self.cadcircuit.restore(surface)
        def setcircuit():
            self.cadcircuit = loadobject
            surface.load(self.cadcircuit)
            self.surface.changezoom.emit(int(self.surface.cadsurface.zoom*100))
            self.type.name = self.cadcircuit.name
            self.changecheck(addtohistory=False)
        return setcircuit
            
    def saveciruit(self, file):
        """saves the subcircuit"""
        
        circuitobject = open(file, "wb")
        surface = self.cadcircuit.cadsurface.qWidget
        self.cadcircuit.readyforsave()
        pc.dump(self.cadcircuit, circuitobject, protocol=pc.HIGHEST_PROTOCOL)
        circuitobject.close()
        self.cadcircuit.restore(surface)
        
    def readyforsave(self):
        """makes the element ready for save"""
        
        self.surface = None
        self.cadcircuit.readyforsave()
        
    def restore(self, surface):
        """restores the object after save"""
        
        self.surface = surface
        # self.cadcircuit.restore(self.cadcircuit.getsurface())
        
    def setmarked(self):
        """marks the element"""
        
        self.marked = True

###############################################################################
# additional classes
###############################################################################

class DEvent:
    """event class for communication of the user input with the cad elements"""
    
    def __init__(self, Type, x=0, y=0, key="", value=0):
        """inits the event
            type[string]:
            MOUSEDRAG
            MOUSELEFTBUTTON
            MOUSELEFTDOUBLE
            MOUSERIGHTBUTTON
            MOUSEWHEEL
            KEY_P
            KEY_R
            MOUSEMOVE"""
            
        self.type = Type
        self.x = x
        self.y = y
        self.key = key
        self.value = value
        

class Savecircuit:
    """class to save the circuit"""
    
    def __init__(self, imageWidget):
        """inits the save circuit"""
        
        self.name = imageWidget.name
        self.elements =  imageWidget.elements
        self.cadsurface = imageWidget.cadsurface
        # self.getsurface = getsurface(self.cadsurface.qWidget)
        
    def readyforsave(self):
        """makes the object ready to save it"""
        
        self.cleardtmodel()
        for e in self.elements:
            e.readyforsave()
        self.cadsurface.qWidget = None
        
    def setnodesid(self, maxid=0):
        """sets the ids of the nodes"""
        
        changes = True
        changed = False
        oldmaxid = maxid
        while changes:
            changes = False
            for e in self.elements:
                nodeid = e.setnodeid(maxid)
                if not nodeid == maxid:
                    changes = True
                    changed = True
                maxid = max(maxid, nodeid)
        self.nodeslist = [dtcore.Node() for _ in range(maxid + 1)]
        if oldmaxid == maxid and changed == True:
            return -1
        return maxid
    
    def creatdtmodel(self, maxid=0, nodeslist=None):
        """creats CElement und Felement lists for the dt simulation"""
        
        if nodeslist == None:
            self.setnodesid(maxid=maxid)
        if not nodeslist == None:
            self.nodeslist = nodeslist
        for e in self.elements:
            status = e.creatdtelement(self.nodeslist)
            if status == False:
                return False
            if status == None:
                return None
        return True
    
            
    def getdtmodel(self, nodeslist=None):
        """returns two lists with CElements and FELements"""
        
        if not nodeslist == None:
            self.nodeslist = nodeslist
        checkcreat = True
        if nodeslist == None:
            checkcreat = self.creatdtmodel()
        if checkcreat:
            celist = []
            felist = []
            for e in self.elements:
                [c, f] = e.getdtelement(self.nodeslist)
                celist = celist + c
                felist = felist + f
            return [celist, felist]
        else:
            self.update()
            return False
    
    def cleardtmodel(self):
        """runs throught all elements and clears them"""
        
        for e in self.elements:
            e.clear()
        
    def restore(self, surface):
        """restores the elements after save or load"""
        
        surface.restore()
        # for e in self.elements:
        #     e.restore(surface)
        # self.cadsurface.qWidget = surface
        
    def removefloating(self):
        """removes floating elements"""
        
        floatingelements = []
        for e in self.elements:
            if type(e) == UElement or type(e) == CLabel or type(e) == FLabel or type(e) == SubCircuitElement:
                if e.floating == True:
                    floatingelements.append(e)
        for e in floatingelements:
            self.elements.remove(e)
        
# def getsurface(surface):
#     def _getsurface():
#         return surface
#     return _getsurface

if PRINT_INFO:
    printinfo()