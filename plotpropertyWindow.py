# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 21:33:47 2022

@author: thocza
This is the name change Window for deltaTau
alpha 0.0.1
    first UI
beta 0.1.0
    basic functionality
beta 0.1.1
    added the possibility to add and remove graphs
beta 0.1.2
    changed the window positioning from absolute to relative
beta 0.1.3
    added the remove function
    and put some style constants to the style.py file
"""

# info variables
PROGRAM_NAME = "deltaTau plot property window GUI"
FILE_NAME = "typeWindow.py"
FILE_CREATION = "2022-08-17"
FILE_UPDATE = "2022-11-08"
FILE_VERSION = "beta 0.1.3"
DT_VERSION = "0.12.0"
PRINT_INFO = True


# libraries
from PyQt5 import QtWidgets, QtCore, QtGui, QtOpenGL, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QStyle
import sys
import ctypes
import sys
import guigraphics as gg
from style import *

#static variables
FONTSIZE = 12

def printinfo():
    """shows all information about this file"""

    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)
    

class PlotPropertyWindow(QWidget):
    """name Window for edditing the name of the circuit
        """
        
    namechange = QtCore.pyqtSignal(str)
    xlabelchange = QtCore.pyqtSignal(str)
    ylabelchange = QtCore.pyqtSignal(str)
    framechange = QtCore.pyqtSignal(object)
    legendchange = QtCore.pyqtSignal(bool)
    yinfoget = QtCore.pyqtSignal()
    yinfochange = QtCore.pyqtSignal(object)
    closed = QtCore.pyqtSignal()
    removeplots = QtCore.pyqtSignal(object)
    
    def __init__(self, name, xlabel, ylabel, frame, legend, yinfo, fremove, pos=None):
        """intits the Window
            also sets up the IOHandler for communication with the dt Framework
            sets name and icon
            """
        
        super().__init__()
        appicon = QtGui.QIcon()
        appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
        self.name = name
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.frame = frame
        self.yinfo = yinfo
        self.legend = legend
        self.setWindowIcon(appicon)
        self.setWindowTitle("deltaTau")
        self.errordialog = None
        self.setremove = False
        self.fremove = fremove
        if pos == None:
            pos = QtCore.QPoint(0, 0)
        self.initUI(pos)

    def initUI(self, pos):
        """inits the window"""
        
        self.setGeometry(pos.x() + 150, pos.y() + 150, 600, 300)
        self.setFixedSize(600, 300)
        self.setWindowTitle(f"deltaTau: {self.name}")
        
        self.hLayoutWindow = QtWidgets.QHBoxLayout(self)
        self.vLayoutWindow = QtWidgets.QVBoxLayout()
        
        self.hLayoutName = QtWidgets.QHBoxLayout()
        self.nameLabel = QtWidgets.QLabel()
        self.nameLabel.setMinimumSize(QtCore.QSize(75, 0))
        self.nameLabel.setMaximumSize(QtCore.QSize(75, 25))
        self.nameLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.nameLabel.setText("Name:")
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.nameEdit.setText(self.name)
        self.unitLabel = QtWidgets.QLabel()
        self.unitLabel.setMinimumSize(QtCore.QSize(30, 0))
        self.unitLabel.setMaximumSize(QtCore.QSize(30, 25))
        self.infoLabel = QtWidgets.QLabel()
        self.infoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
        self.infoLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.infoLabel.setMaximumSize(QtCore.QSize(25, 25))
        self.infoLabel.setToolTip("Plot Name")
        
        self.hLayoutName.addWidget(self.nameLabel)
        self.hLayoutName.addWidget(self.nameEdit)
        self.hLayoutName.addWidget(self.unitLabel)
        self.hLayoutName.addWidget(self.infoLabel)
        
        self.hLayoutxlabel = QtWidgets.QHBoxLayout()
        self.xlabelLabel = QtWidgets.QLabel()
        self.xlabelLabel.setMinimumSize(QtCore.QSize(75, 0))
        self.xlabelLabel.setMaximumSize(QtCore.QSize(75, 25))
        self.xlabelLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.xlabelLabel.setText("xLabel:")
        self.xlabelEdit = QtWidgets.QLineEdit()
        self.xlabelEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.xlabelEdit.setText(self.xlabel)
        self.xlabelunitLabel = QtWidgets.QLabel()
        self.xlabelunitLabel.setMinimumSize(QtCore.QSize(30, 0))
        self.xlabelunitLabel.setMaximumSize(QtCore.QSize(30, 25))
        self.xlabelinfoLabel = QtWidgets.QLabel()
        self.xlabelinfoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
        self.xlabelinfoLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.xlabelinfoLabel.setMaximumSize(QtCore.QSize(25, 25))
        self.xlabelinfoLabel.setToolTip("xLabel in the plotpane")
        
        self.hLayoutxlabel.addWidget(self.xlabelLabel)
        self.hLayoutxlabel.addWidget(self.xlabelEdit)
        self.hLayoutxlabel.addWidget(self.xlabelunitLabel)
        self.hLayoutxlabel.addWidget(self.xlabelinfoLabel)
        
        self.hLayoutylabel = QtWidgets.QHBoxLayout()
        self.ylabelLabel = QtWidgets.QLabel()
        self.ylabelLabel.setMinimumSize(QtCore.QSize(75, 0))
        self.ylabelLabel.setMaximumSize(QtCore.QSize(75, 25))
        self.ylabelLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.ylabelLabel.setText("yLabel:")
        self.ylabelEdit = QtWidgets.QLineEdit()
        self.ylabelEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.ylabelEdit.setText(self.ylabel)
        self.ylabelunitLabel = QtWidgets.QLabel()
        self.ylabelunitLabel.setMinimumSize(QtCore.QSize(30, 0))
        self.ylabelunitLabel.setMaximumSize(QtCore.QSize(30, 25))
        self.ylabelinfoLabel = QtWidgets.QLabel()
        self.ylabelinfoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
        self.ylabelinfoLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.ylabelinfoLabel.setMaximumSize(QtCore.QSize(25, 25))
        self.ylabelinfoLabel.setToolTip("yLabel in the plotpane")
        
        self.hLayoutylabel.addWidget(self.ylabelLabel)
        self.hLayoutylabel.addWidget(self.ylabelEdit)
        self.hLayoutylabel.addWidget(self.ylabelunitLabel)
        self.hLayoutylabel.addWidget(self.ylabelinfoLabel)
        
        self.hLayoutxmin = QtWidgets.QHBoxLayout()
        self.xminLabel = QtWidgets.QLabel()
        self.xminLabel.setMinimumSize(QtCore.QSize(75, 0))
        self.xminLabel.setMaximumSize(QtCore.QSize(75, 25))
        self.xminLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.xminLabel.setText("x-min:")
        self.xminEdit = QtWidgets.QLineEdit()
        self.xminEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.xminEdit.setText(str(float('%.4g' % self.frame[0])))
        self.xminunitLabel = QtWidgets.QLabel()
        self.xminunitLabel.setMinimumSize(QtCore.QSize(30, 0))
        self.xminunitLabel.setMaximumSize(QtCore.QSize(30, 25))
        self.xmininfoLabel = QtWidgets.QLabel()
        self.xmininfoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
        self.xmininfoLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.xmininfoLabel.setMaximumSize(QtCore.QSize(25, 25))
        self.xmininfoLabel.setToolTip("show graph from x min")
        
        self.hLayoutxmin.addWidget(self.xminLabel)
        self.hLayoutxmin.addWidget(self.xminEdit)
        self.hLayoutxmin.addWidget(self.xminunitLabel)
        self.hLayoutxmin.addWidget(self.xmininfoLabel)
        
        self.hLayoutxmax = QtWidgets.QHBoxLayout()
        self.xmaxLabel = QtWidgets.QLabel()
        self.xmaxLabel.setMinimumSize(QtCore.QSize(75, 0))
        self.xmaxLabel.setMaximumSize(QtCore.QSize(75, 25))
        self.xmaxLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.xmaxLabel.setText("x-max:")
        self.xmaxEdit = QtWidgets.QLineEdit()
        self.xmaxEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.xmaxEdit.setText(str(float('%.4g' % self.frame[1])))
        self.xmaxunitLabel = QtWidgets.QLabel()
        self.xmaxunitLabel.setMinimumSize(QtCore.QSize(30, 0))
        self.xmaxunitLabel.setMaximumSize(QtCore.QSize(30, 25))
        self.xmaxinfoLabel = QtWidgets.QLabel()
        self.xmaxinfoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
        self.xmaxinfoLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.xmaxinfoLabel.setMaximumSize(QtCore.QSize(25, 25))
        self.xmaxinfoLabel.setToolTip("show graph to x max")
        
        self.hLayoutxmax.addWidget(self.xmaxLabel)
        self.hLayoutxmax.addWidget(self.xmaxEdit)
        self.hLayoutxmax.addWidget(self.xmaxunitLabel)
        self.hLayoutxmax.addWidget(self.xmaxinfoLabel)
        
        self.hLayoutymin = QtWidgets.QHBoxLayout()
        self.yminLabel = QtWidgets.QLabel()
        self.yminLabel.setMinimumSize(QtCore.QSize(75, 0))
        self.yminLabel.setMaximumSize(QtCore.QSize(75, 25))
        self.yminLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.yminLabel.setText("y-min:")
        self.yminEdit = QtWidgets.QLineEdit()
        self.yminEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.yminEdit.setText(str(float('%.4g' % self.frame[2])))
        self.yminunitLabel = QtWidgets.QLabel()
        self.yminunitLabel.setMinimumSize(QtCore.QSize(30, 0))
        self.yminunitLabel.setMaximumSize(QtCore.QSize(30, 25))
        self.ymininfoLabel = QtWidgets.QLabel()
        self.ymininfoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
        self.ymininfoLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.ymininfoLabel.setMaximumSize(QtCore.QSize(25, 25))
        self.ymininfoLabel.setToolTip("show graph from y min")
        
        self.hLayoutymin.addWidget(self.yminLabel)
        self.hLayoutymin.addWidget(self.yminEdit)
        self.hLayoutymin.addWidget(self.yminunitLabel)
        self.hLayoutymin.addWidget(self.ymininfoLabel)
        
        self.hLayoutymax = QtWidgets.QHBoxLayout()
        self.ymaxLabel = QtWidgets.QLabel()
        self.ymaxLabel.setMinimumSize(QtCore.QSize(75, 0))
        self.ymaxLabel.setMaximumSize(QtCore.QSize(75, 25))
        self.ymaxLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.ymaxLabel.setText("y-max:")
        self.ymaxEdit = QtWidgets.QLineEdit()
        self.ymaxEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.ymaxEdit.setText(str(float('%.4g' % self.frame[3])))
        self.ymaxunitLabel = QtWidgets.QLabel()
        self.ymaxunitLabel.setMinimumSize(QtCore.QSize(30, 0))
        self.ymaxunitLabel.setMaximumSize(QtCore.QSize(30, 25))
        self.ymaxinfoLabel = QtWidgets.QLabel()
        self.ymaxinfoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
        self.ymaxinfoLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.ymaxinfoLabel.setMaximumSize(QtCore.QSize(25, 25))
        self.ymaxinfoLabel.setToolTip("show graph to y max")
        
        self.hLayoutymax.addWidget(self.ymaxLabel)
        self.hLayoutymax.addWidget(self.ymaxEdit)
        self.hLayoutymax.addWidget(self.ymaxunitLabel)
        self.hLayoutymax.addWidget(self.ymaxinfoLabel)
        
        self.hLayoutButtons = QtWidgets.QHBoxLayout()
        
        self.savebutton = QtWidgets.QPushButton("Save")
        self.savebutton.setIcon(ICONS["SaveFile"])
        self.savebutton.clicked.connect(self.save)
        self.okbutton = QtWidgets.QPushButton("OK")
        self.okbutton.setIcon(ICONS["Apply"])
        self.okbutton.clicked.connect(self.ok)
        self.cancelbutton = QtWidgets.QPushButton("Cancel")
        self.cancelbutton.setIcon(ICONS["Discard"])
        self.cancelbutton.clicked.connect(self.close)
        
        self.hLayoutButtons.addWidget(self.savebutton)
        self.hLayoutButtons.addWidget(self.okbutton)
        self.hLayoutButtons.addWidget(self.cancelbutton)
        
        self.vLayoutWindow.addLayout(self.hLayoutName)
        self.vLayoutWindow.addLayout(self.hLayoutxlabel)
        self.vLayoutWindow.addLayout(self.hLayoutylabel)
        self.vLayoutWindow.addLayout(self.hLayoutxmin)
        self.vLayoutWindow.addLayout(self.hLayoutxmax)
        self.vLayoutWindow.addLayout(self.hLayoutymin)
        self.vLayoutWindow.addLayout(self.hLayoutymax)
        self.vLayoutWindow.addLayout(self.hLayoutButtons)
        
        self.vLayoutPlots = QtWidgets.QVBoxLayout()
        
        self.hLayoutlegendcheck = QtWidgets.QHBoxLayout()
        self.legendlabel = QtWidgets.QLabel()
        self.legendlabel.setText("Show legend")
        self.legendlabel.setMinimumSize(QtCore.QSize(150, 25))
        self.legendlabel.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.legendcheck = QtWidgets.QCheckBox()
        self.legendcheck.setChecked(self.legend)
        self.hlegendSpacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.removebutton = QtWidgets.QPushButton("remove")
        self.removebutton.setIcon(ICONS["Discard"])
        self.removebutton.clicked.connect(self.toggleremove)
        
        self.hLayoutlegendcheck.addWidget(self.legendcheck)
        self.hLayoutlegendcheck.addWidget(self.legendlabel)
        self.hLayoutlegendcheck.addItem(self.hlegendSpacer)
        self.hLayoutlegendcheck.addWidget(self.removebutton)
        
        self.plotlist = QtWidgets.QScrollArea()
        self.plotlist.setMaximumSize(QtCore.QSize(320, 230))
        self.plotlist.setMinimumSize(QtCore.QSize(320 ,230))
        
        self.scrolllWidget = QtWidgets.QWidget()
        vLayoutPlots = QtWidgets.QVBoxLayout()
        
        def getset(y, name, color, show):
            def _set():
                y[0] = name.text()
                y[1] = COLORLIST[color.currentIndex()]
                y[2] = show.isChecked()
            return _set
        
        def getremove(layout, item):
            def remove():
                self.removey(layout, item)
            return remove
        
        self.remove = []
        
        for j, y in enumerate(self.yinfo):
            hLayoutplots = QtWidgets.QHBoxLayout()
            
            plotcheck = QtWidgets.QCheckBox()
            plotcheck.setChecked(y[2])
            plotEdit = QtWidgets.QLineEdit()
            plotEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            plotEdit.setText(y[0])
            plotcolorcombo = QtWidgets.QComboBox()
            plotcolormodel = plotcolorcombo.model()
            currentindex = 0
            for i, c in enumerate(COLORLIST):
                if c == y[1]:
                    currentindex = i
                item = QtGui.QStandardItem(str(c))
                brush = QtGui.QBrush(QtGui.QColor(*c), QtCore.Qt.SolidPattern)
                item.setBackground(brush)
                brush = QtGui.QBrush(QtGui.QColor(255, 255, 255), QtCore.Qt.SolidPattern)
                item.setForeground(brush)
                plotcolormodel.appendRow(item)
            
            self.remove.append(plotcheck.isEnabled)
            
            deletebutton = QtWidgets.QPushButton("")
            deletebutton.setIcon(ICONS["Discard"])
            deletebutton.setToolTip("Removes curve")
            deletebutton.clicked.connect(getremove(vLayoutPlots, hLayoutplots))
            
            self.yinfoget.connect(getset(self.yinfo[j], plotEdit, plotcolorcombo, plotcheck))
                
            plotcolorcombo.setCurrentIndex(currentindex)
            hLayoutplots.addWidget(plotcheck)
            hLayoutplots.addWidget(plotEdit)
            hLayoutplots.addWidget(plotcolorcombo)
            hLayoutplots.addWidget(deletebutton)
            
            vLayoutPlots.addLayout(hLayoutplots)
        
        self.scrolllWidget.setLayout(vLayoutPlots)
        self.plotlist.setWidget(self.scrolllWidget)
        
        self.vLayoutPlots.addLayout(self.hLayoutlegendcheck)
        self.vLayoutPlots.addWidget(self.plotlist)
        
        self.hLayoutWindow.addLayout(self.vLayoutWindow)
        self.hLayoutWindow.addLayout(self.vLayoutPlots)
        
    def closeEvent(self, event):
        """close Window"""
        
        self.name = self.nameEdit.text()
        self.closed.emit()
        self.close()
        
    def save(self):
        """save input values"""
        
        if self.setremove:
            self.fremove()
            self.closed.emit()
            self.close()
            return
        self.yinfoget.emit()
        self.name = self.nameEdit.text()
        self.xlabel = self.xlabelEdit.text()
        self.ylabel = self.ylabelEdit.text()
        self.legend = self.legendcheck.isChecked()
        check = True
        try:
            self.frame[0] = float(self.xminEdit.text())
        except:
            check = False
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage("x-min has to be a number")
        try:
            self.frame[1] = float(self.xmaxEdit.text())
        except:
            check = False
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage("x-max has to be a number")
        try:
            self.frame[2] = float(self.yminEdit.text())
        except:
            check = False
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage("y-min has to be a number")
        try:
            self.frame[3] = float(self.ymaxEdit.text())
        except:
            check = False
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage("y-max has to be a number")
        if check:
            yinforemove = []
            yindex = []
            for i, r in enumerate(self.remove):
                if not r():
                    yinforemove.append(self.yinfo[i])
                    yindex.append(i)
            for y in yinforemove:
                self.yinfo.remove(y)
            self.namechange.emit(self.name)
            self.xlabelchange.emit(self.xlabel)
            self.ylabelchange.emit(self.ylabel)
            self.framechange.emit(self.frame)
            self.legendchange.emit(self.legend)
            self.removeplots.emit(yindex)
            self.yinfochange.emit(self.yinfo)
    
    def ok(self):
        """save input values and close window when values are valid"""
        
        if self.setremove:
            self.fremove()
            self.closed.emit()
            self.close()
            return
        self.yinfoget.emit()
        self.name = self.nameEdit.text()
        self.xlabel = self.xlabelEdit.text()
        self.ylabel = self.ylabelEdit.text()
        self.legend = self.legendcheck.isChecked()
        check = True
        try:
            self.frame[0] = float(self.xminEdit.text())
        except:
            check = False
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage("x-min has to be a number")
        try:
            self.frame[1] = float(self.xmaxEdit.text())
        except:
            check = False
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage("x-max has to be a number")
        try:
            self.frame[2] = float(self.yminEdit.text())
        except:
            check = False
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage("y-min has to be a number")
        try:
            self.frame[3] = float(self.ymaxEdit.text())
        except:
            check = False
            self.errordialog = QtWidgets.QErrorMessage()
            self.errordialog.showMessage("y-max has to be a number")
        if check:
            yinforemove = []
            yindex = []
            for i, r in enumerate(self.remove):
                if not r():
                    yinforemove.append(self.yinfo[i])
                    yindex.append(i)
            for y in yinforemove:
                self.yinfo.remove(y)
            self.namechange.emit(self.name)
            self.xlabelchange.emit(self.xlabel)
            self.ylabelchange.emit(self.ylabel)
            self.framechange.emit(self.frame)
            self.legendchange.emit(self.legend)
            self.removeplots.emit(yindex)
            self.yinfochange.emit(self.yinfo)
        self.closed.emit()
        self.close()
         
    def keyPressEvent(self, event):
        """save event when enter key is pressed"""
        
        self.yinfoget.emit()
        if event.key() == QtCore.Qt. Key_Return:
            self.name = self.nameEdit.text()
            self.xlabel = self.xlabelEdit.text()
            self.ylabel = self.ylabelEdit.text()
            self.legend = self.legendcheck.isChecked()
            check = True
            try:
                self.frame[0] = float(self.xminEdit.text())
            except:
                check = False
                self.errordialog = QtWidgets.QErrorMessage()
                self.errordialog.showMessage("x-min has to be a number")
            try:
                self.frame[1] = float(self.xmaxEdit.text())
            except:
                check = False
                self.errordialog = QtWidgets.QErrorMessage()
                self.errordialog.showMessage("x-max has to be a number")
            try:
                self.frame[2] = float(self.yminEdit.text())
            except:
                check = False
                self.errordialog = QtWidgets.QErrorMessage()
                self.errordialog.showMessage("y-min has to be a number")
            try:
                self.frame[3] = float(self.ymaxEdit.text())
            except:
                check = False
                self.errordialog = QtWidgets.QErrorMessage()
                self.errordialog.showMessage("y-max has to be a number")
            if check:
                yinforemove = []
                yindex = []
                for i, r in enumerate(self.remove):
                    if not r():
                        yinforemove.append(self.yinfo[i])
                        yindex.append(i)
                for y in yinforemove:
                    self.yinfo.remove(y)
                self.namechange.emit(self.name)
                self.xlabelchange.emit(self.xlabel)
                self.ylabelchange.emit(self.ylabel)
                self.framechange.emit(self.frame)
                self.legendchange.emit(self.legend)
                self.removeplots.emit(yindex)
                self.yinfochange.emit(self.yinfo)
            self.closed.emit()
            self.close()
    
    def toggleremove(self):
        """toggles the remove bool"""
        
        self.setremove = not self.setremove
        if self.setremove:
            self.removebutton.setIcon(ICONS["Plus"])
            self.removebutton.setText("add")
        else:
            self.removebutton.setIcon(ICONS["Discard"])
            self.removebutton.setText("remove")
    
    def removey(self, layout, item):
        state = False
        for i in range(item.count()):
            item.itemAt(i).widget().setEnabled(not item.itemAt(i).widget().isEnabled())
            if type(item.itemAt(i).widget()) == QtWidgets.QPushButton:
                item.itemAt(i).widget().setEnabled(True)
                if state == True:
                    item.itemAt(i).widget().setIcon(ICONS["Discard"])
                else:
                    item.itemAt(i).widget().setIcon(ICONS["Plus"])
            else:
                state = item.itemAt(i).widget().isEnabled()
        # for i in range(layout.count()):
        #     layout_item = layout.itemAt(i)
        #     if layout_item.layout() == item:
        #         layout.removeItem(layout_item)
        #         print("done")

"""prints the information about this file"""
if PRINT_INFO:
    printinfo()

