# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 10:23:59 2022

@author: czatho

This is a library of all different function elements
It contains of the folowing elements
constant source
zero one
sin source
sawtooth
clock
time
ramp
abs
adder
subtractor
multiplicator
divider
gain
sig
square
sqrt
exp
ln
log2
log10
power
mod
rem
sine
cos
tan
asin
acos
atan
maximum
minimum
integrator
limiter
hysteresis
ploty
PI
PID
PLL
bigger than constant
smaller than constant
bigger
smaller
And 
Or
Xor
Not
phasedetector
dt
delay
discrete integrator
discrete derivator
analog to descrete
periodical average
arrayin
arrayout
symetrical pwm
alpha beta array
alpha beta single
alpha beta reverse array
alpha beta reverse single
dq array
dq singular
dq reverse array
dq reverse singular
cartesian to polar
polar to cartesian
timedelay
ADtime
alpha 0.0.1:
    first draft not working
    under work
beta 0.1.0
    working version with some basic elements
beta 0.1.1
    added the sin source
beta 0.1.2
    added the first logical block 
    bigger than constant
beta 0.1.3
    added 6 new logical blocks and the derivative block
beta 0.1.4
    added new blocks
beta 0.1.5
    added deltaTau Version numbers to Info
beta 0.1.6
    library is added to LIBRARY constant for easy integration to GUI
beta 0.1.7
    added adder to LIBRARY
beta 0.1.8
    subtractor, integrator and inverter added to LIBRARY
    and minor bug fixes
beta 0.1.9
    added bigger than, smaller than, and , or xor and constant to LIBRARY
beta 0.1.10
    added the plot element to the LIBRARY
    and changes to the basLib
beta 0.1.11
    added the sawtooth function
beta 0.1.12
    added sin smaller and bigger function to LIBRARY
    bugfixed the sawtooth function
beta 0.1.13
    added the clock, multiplikator and divider function to LIBRARY
beta 0.1.14
    added the delay, dt and PI Element and some cosmetical changes
beta 0.1.15
    added the moving mean function
    bug fixes
beta 0.1.16
    language change to englisch
    bug fixes with the plot element
beta 0.1.17
    save library as .lib
beta 0.1.18
    added the stop and action functions
beta 0.1.19
    bug fix with the offset of the sawtooth
beta 0.1.20
    added the arrayin, arrayout, timedelay, clock elements
    clock is not time
beta 0.1.20a
    time is now time_
beta 0.1.21
    added limiter hysteresis and symetrical pwm
beta 0.1.22
    added alphabeta array
    alphabeta singular
    alphabeta reverse array
    alphabeta revers singular
    dq array
    dq singular
    dq reverse array
    dq reverse singular
    cartesian to polar
    polar to cartesian
beta 0.2.0
    all elements have now the new function architecture
beta 0.2.1
    added
    sin
    cos
    tan
    asin
    acos
    atan
beta 0.2.2
    added:
        square
        sqrt
        exp
        ln
        log2
        log10
        power
        mod
        rem
beta 0.2.3
    changes to PI Controller    
    added:
        PLL
        phasedetector
beta 0.2.4
    added PID
beta 0.2.5
    Bug-fixes
beta 0.2.6
    added:
        descrete integrator
        discrete derivator
        sawtooth PWM
        periodical average
    changed:
        integrator
        delay
        symetrical PWM
        triangular wave
        sawtooth wave
    variables can now be lists
beta 0.2.7
    added:
        Dead-zone
        Discrete-mean-value
        1D-look-up-table
beta 0.2.8
    updated to new baseLib functions
    added:
        2D-look-up table
"""

# info variables
PROGRAM_NAME = "deltaTau Function Libraray"
FILE_NAME = "functionLib.py"
FILE_CREATION = "2022-01-19"
FILE_UPDATE = "2024-08-09"
FILE_VERSION = "beta 0.2.8"
DT_VERSION = "0.12.0"
PRINT_INFO = True


# libraries
import dtengine as deltaTau
import math as m
import cmath as cm
import time
import baseLib as bl
import ggelements as baseimages
import numpy as np
import scipy.interpolate as interp
import pickle as pc
import plotWindow as pw
import copy
import time as utime


def printinfo():
    """shows all information about this file"""
    
    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)

#constant source##############################################################
constantsourceargs = [[1],
                       ["Constant"],
                       [""],
                       [float],
                       [bl.PropertyElement.noconstrains],
                       ["Constant value"]]

def constantsource(name, value):
    """this returns a constant source signal"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, value):
            super().__init__(name)
            
            self.value = value
            self.felement = deltaTau.FunctionELement(name, "Q", value)
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """set outputsignals"""
            
            return[self.felement]
        
        def setarray(self):
            """gets the output atomics"""
            
            self.outputs[0].setarray([self.felement.getoutput()])
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.felement.output.data, self.felement.output.t]]

    return UpdateCE(name, value)

#zero##########################################################################

zeroargs = [[], [], [], [], [], []]

def zero(name):
    """this returns a constant source signal"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.felement = deltaTau.FunctionELement(name, "Q", 0)
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setarray(self):
            """gets the output atomics"""
            
            self.outputs[0].setarray([self.felement.getoutput()])
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.felement.output.data, self.felement.output.t]]

    return UpdateCE(name)

#one###########################################################################

oneargs = [[], [], [], [], [], []]

def one(name):
    """this returns a constant source signal"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.felement = deltaTau.FunctionELement(name, "Q", 1)
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setarray(self):
            """set outputsignals"""
            
            self.outputs[0].setarray([self.felement.getoutput()])
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.felement.output.data, self.felement.output.t]]

    return UpdateCE(name)

#sin###########################################################################

sinargs = [[1, 50, 0],
           ["Amplitude", "Frequency", "Phase"],
           ["", "Hz", "°"],
           [float, float, float],
           [bl.PropertyElement.biggerzero, bl.PropertyElement.biggerzero, bl.PropertyElement.noconstrains],
           ["Amplitude of the sine signal", "Frequency of the sine signal", "Phase of the signal"]]

def sin(name, peakvalue, frequency, phi):
    """returns a sinus source signal"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, peakvalue, frequency, phi):
            super().__init__(name)
            
            self.peakvalue = peakvalue
            self.frequency = frequency
            self.phi = phi
            self.felement = deltaTau.FunctionELement(name, "Q", self.peakvalue*m.sin(2*m.pi*self.frequency*0 + self.phi/180*m.pi))
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setarray(self):
            """set outputsignals"""
            
            self.outputs[0].setarray([self.felement.getoutput()])
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            self.felement.setvalue(self.peakvalue * m.sin(2 * m.pi * self.frequency * time + self.phi * m.pi / 180))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.felement.output.data, self.felement.output.t]]

    return UpdateCE(name, peakvalue, frequency, phi)

#saw tooth#####################################################################

sawtoothargs = [[[1, -1], 1, 0],
                ["Range", "Periode", "Offset"],
                ["", "s", "p.u"],
                [list, float, float],
                [bl.PropertyElement.listoftwofloats, bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains],
                ["TRange of wave; Top and bottom limits", "Periode of the signal", "Time-offset of the signal in p.u. of period"]]

def sawtooth(name, _range, period, offset):
    """returns a sawtooth source signal"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, _range, period, offset):
            super().__init__(name)
            
            self.grade = (_range[0] - _range[1])/period
            self.offset = offset*period
            self.period = period
            self.bottomvalue = _range[1]
            self.felement = deltaTau.FunctionELement(name, "Q", self.grade*self.offset - self.grade*self.period*int(self.offset/self.period) + self.bottomvalue)
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setarray(self):
            """set outputsignals"""
            
            self.outputs[0].setarray([self.felement.getoutput()])
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            self.felement.setvalue(self.grade*(self.offset + time) - self.grade*self.period*int((time + self.offset)/self.period) + self.bottomvalue)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.felement.output.data, self.felement.output.t]]

    return UpdateCE(name, _range, period, offset)

#triangular####################################################################

triangularargs = [[[1, -1], 1, 0],
                  ["Range", "Periode", "Offset"],
                  ["", "s", "p.u"],
                  [list, float, float],
                  [bl.PropertyElement.listoftwofloats, bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains],
                  ["Range of wave; Top and bottom limits", "Periode of the signal", "Time-offset of the signal in p.u. of period"]]

def triangular(name, _range, period, offset):
    """returns a sawtooth source signal"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, _range, period, offset):
            super().__init__(name)
            
            self.grade = 2*(_range[0] - _range[1])/period
            self.offset = offset*period
            while self.offset < 0:
                self.offset = self.offset + period
            self.period = period
            if _range[1] < _range[0]:
                self.bottomvalue = _range[1]
                self.peakvalue = _range[0]
            else:
                self.bottomvalue = _range[0]
                self.peakvalue = _range[1]
                self.grade = -self.grade
                self.offset = self.offset + self.period/2
            value = self.grade*self.offset - self.grade*self.period*int(self.offset/self.period) + self.bottomvalue
            if value > self.peakvalue or value < self.bottomvalue:
                value = -self.grade*(self.offset - self.period/2) + self.grade*self.period*int((self.offset - self.period/2)/self.period) + self.peakvalue
            self.felement = deltaTau.FunctionELement(name, "Q", value)
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setarray(self):
            """set outputsignals"""
            
            self.outputs[0].setarray([self.felement.getoutput()])
        
        def update(self, major, time):
            """updates the voltagesource"""
            value = self.grade*(self.offset + time) - self.grade*self.period*int((time + self.offset)/self.period) + self.bottomvalue
            if value > self.peakvalue or value < self.bottomvalue:
                value = -self.grade*(self.offset + time - self.period/2) + self.grade*self.period*int((time + self.offset - self.period/2)/self.period) + self.peakvalue
            self.felement.setvalue(value)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.felement.output.data, self.felement.output.t]]

    return UpdateCE(name, _range, period, offset)

#step##########################################################################

stepargs = [[0, 1, 1],
            ["Start", "End", "step-time"],
            ["", "", "s"],
            [float, float, float],
            [bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains],
            ["Start value", "End value", "time of the step"]]

def step(name, start, end, time_):
    """this returns the a stept"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, start, end, time_):
            super().__init__(name)
            
            self.start = start
            self.end = end
            self.time = time_
            self.felement = deltaTau.FunctionELement(name, "Q", start)
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setarray(self):
            """set outputsignals"""
            
            self.outputs[0].setarray([self.felement.getoutput()])
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            if time > self.time and self.felement.value == self.start:
                self.felement.setvalue(self.end)
                return 1
            elif time < self.time and self.felement.value == self.end:
                self.felement.setvalue(self.start)
                return 1
            return 0
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.felement.output.data, self.felement.output.t]]

    return UpdateCE(name, start, end, time_)

#clock#########################################################################

clockargs = [[50],
             ["Frequency"],
             ["Hz"],
             [float],
             [bl.PropertyElement.biggerzero],
             ["Frequency of the clock"]]

def clock(name, frequency):
    """returns a sinus source signal"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, frequency):
            super().__init__(name)
            
            self.frequency = frequency
            self.felement = deltaTau.FunctionELement(name, "Q", 0)
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setarray(self):
            """set outputsignals"""
            
            self.outputs[0].setarray([self.felement.getoutput()])
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            self.felement.setvalue(int(m.sin(2*m.pi*self.frequency*time)>0))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.felement.output.data, self.felement.output.t]]

    return UpdateCE(name, frequency)

#time##########################################################################

timeargs = [[], [], [], [], [], []]

def time_(name):
    """this returns the simulation time"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.felement = deltaTau.FunctionELement(name, "Q", 0)
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setarray(self):
            """set outputsignals"""
            
            self.outputs[0].setarray([self.felement.getoutput()])
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            self.felement.setvalue(time)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.felement.output.data, self.felement.output.t]]

    return UpdateCE(name)

#ramp##########################################################################

rampargs = [[1, 0, 1, 0],
            ["slope", "initial value", "end value", "start time"],
            ["1/s", "", "", "s"],
            [float, float, float, float],
            [bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains, bl.PropertyElement.notnegative],
            ["slope of the ramp", "initial value of the ramp", "end value of the ramp\n if inf the ramp has no end", "start of the ramp"]]

def ramp(name, slope, start, end, starttime):
    """returns a ramp"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, slope, start, end, starttime):
            super().__init__(name)
            
            self.grade = slope
            self.start = start
            self.end = end
            self.starttime = starttime
            if starttime < 0:
                initvalue = self.start + self.grade*-self.starttime
            else:
                initvalue = self.start
            if initvalue > self.end and self.grade > 0 or initvalue < self.end and self.grade < 0:
                initvalue = self.end
            self.felement = deltaTau.FunctionELement(name, "Q", initvalue)
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return[self.felement]
        
        def setarray(self):
            """set outputsignals"""
            
            self.outputs[0].setarray([self.felement.getoutput()])
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            if time > self.starttime:
                value = self.start + self.grade*(time - self.starttime)
            else:
                value = self.start
            if value > self.end and self.grade > 0 or value < self.end and self.grade < 0:
                value = self.end
            self.felement.setvalue(value)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.felement.output.data, self.felement.output.t]]

    return UpdateCE(name, slope, start, end, starttime)

#abs###########################################################################

absargs = [[], [], [], [], [], []]

def abs_(name):
    """returns the abs value"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.gain = gain
            self.baseelement = deltaTau.FunctionELement(name, "G", 1)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felements = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                e = self.baseelement.copy()
                e.setinput(s)
                e.setoutput(outputs[i])
                self.felements.append(e)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            update = 0
            for e in self.felements:
                if e.output.data[-1] < 0:
                    e.setvalue(-e.value)
                    update = 2
            return update
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#norm##########################################################################

normargs = [[], [], [], [], [], []]

def norm(name):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.out = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + [self.out]
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray([self.out.getoutput()])
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                ein.setinput(s)
                self.felementsin.append(ein)
            self.out.setoutput(outputs[0])
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            norm = 0
            for i, _ in enumerate(self.felementsin):
                norm = norm + self.felementsin[i].input.data[-1]**2
            self.out.setvalue(np.sqrt(norm))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#adder#########################################################################

adderargs = [[], [], [], [], [], []]

def adder(name):
    """this adds both inputs"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.baseelement = deltaTau.FunctionELement(name, "A", [])
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felements = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2): 
                for i, s in enumerate(inputs1):
                    e = self.baseelement.copy()
                    e.setinput(s)
                    e.setvalue([inputs2[i]])
                    e.setoutput(outputs[i])
                    self.felements.append(e)
            elif len(inputs2) == 1:
                for i, s in enumerate(inputs1):
                    e = self.baseelement.copy()
                    e.setinput(s)
                    e.setvalue([inputs2[0]])
                    e.setoutput(outputs[i])
                    self.felements.append(e)
            elif len(inputs1) == 1:
                for i, s in enumerate(inputs2):
                    e = self.baseelement.copy()
                    e.setinput(s)
                    e.setvalue([inputs1[0]])
                    e.setoutput(outputs[i])
                    self.felements.append(e)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            # print(self.felements)
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#sum###########################################################################

sumargs = [[], [], [], [], [], []]

def summ(name):
    """this adds both inputs"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.adder = deltaTau.FunctionELement(name, "A", [])
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return [self.adder]
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray([self.adder.getoutput()])
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs) > 1:
                self.adder.setinput(inputs[0])
                inputs.pop(0)
                self.adder.setvalue(inputs)
                self.adder.setoutput(outputs[0])
            else:
                self.adder = deltaTau.FunctionELement(name, "G", 1)
                self.adder.setinput(inputs[0])
                self.adder.setoutput(outputs[0])
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#subtractorargs################################################################

subtractorargs = [[], [], [], [], [], []]

def subtractor(name):
    """this function subtracts input2 from inputs1"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.baseadder = deltaTau.FunctionELement(name, "A", [])
            self.basegain = deltaTau.FunctionELement(name, "G", -1)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felements = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[1].getsignals()
            inputs2 = self.inputs[0].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[1].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[1].getsignals()
            inputs2 = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2): 
                for i, s in enumerate(inputs1):
                    a = self.baseadder.copy()
                    g = self.basegain.copy()
                    a.setinput(s)
                    g.setinput(inputs2[i])
                    a.setvalue([g.getoutput()])
                    a.setoutput(outputs[i])
                    self.felements.append(a)
                    self.felements.append(g)
            elif len(inputs2) == 1:
                g = self.basegain.copy()
                self.felements.append(g)
                g.setinput(inputs2[0])
                for i, s in enumerate(inputs1):
                    a = self.baseadder.copy()
                    a.setinput(s)
                    a.setvalue([g.getoutput()])
                    a.setoutput(outputs[i])
                    self.felements.append(a)
            elif len(inputs1) == 1:
                for i, s in enumerate(inputs2):
                    a = self.baseadder.copy()
                    g = self.basegain.copy()
                    a.setinput(inputs1[0])
                    g.setinput(s)
                    a.setvalue([g.getoutput()])
                    a.setoutput(outputs[i])
                    self.felements.append(a)
                    self.felements.append(g)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            # print(self.felements)
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#multiplicator#################################################################

multiplicatorargs = [[], [], [], [], [], []]

def multiplicator(name):
    """this multiplicates both inputs"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.basesource = deltaTau.FunctionELement(name, "Q", 0,)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[1].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2):
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs2) == 1:
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[0])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs1) == 1:
                for i, _ in enumerate(inputs2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[0])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                # print(self.felementsin1[i].input.data[-1])
                self.felementsout[i].setvalue(self.felementsin1[i].input.data[-1]*self.felementsin2[i].input.data[-1])
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#divider#######################################################################

dividerargs = [[], [], [], [], [], []]

def divider(name):
    """this divides both inputs"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.basesource = deltaTau.FunctionELement(name, "Q", 0,)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[1].getsignals()
            inputs2 = self.inputs[0].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[1].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[1].getsignals()
            inputs2 = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2):
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs2) == 1:
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[0])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs1) == 1:
                for i, _ in enumerate(inputs2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[0])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                if not self.felementsin2[i].input.data[-1] == 0:
                    self.felementsout[i].setvalue(self.felementsin1[i].input.data[-1]/self.felementsin2[i].input.data[-1])
                else:
                    self.felementsout[i].setvalue(0)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#gain##########################################################################

gainargs = [[1],
            ["Factor"],
            [""],
            [float],
            [bl.PropertyElement.noconstrains],
            ["Gain-factor"]]

def gain(name, gain):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, gain):
            super().__init__(name)
            
            self.gain = gain
            self.baseelement = deltaTau.FunctionELement(name, "G", gain)
            self.felements = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
            
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                e = self.baseelement.copy()
                e.setinput(s)
                e.setoutput(outputs[i])
                self.felements.append(e)
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, gain)

#integrator####################################################################

integratorargs = [[["Continuous", "discrete"]],
                  ["Type"],
                  [""],
                  [str],
                  [bl.PropertyElement.noconstrains],
                  ["Use continuous space state method or discrete space state method (Trapizoidal integration)"]]

def integrator(name, method):
    """this integrates the inputs"""
    
    class UpdateCEcontinious(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.gain = gain
            self.baseelement = deltaTau.FunctionELement(name, "S", 1)
            self.felements = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
            
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                e = self.baseelement.copy()
                e.setinput(s)
                e.setoutput(outputs[i])
                self.felements.append(e)
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
            
    
    class UpdateCEdiscrete(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if len(self.felementsin[i].input.data) > 1:
                    self.felementsout[i].setvalue((self.felementsin[i].input.data[-1] + self.felementsin[i].input.data[-2])*(time - self.felementsout[i].output.t[-2])/2 + self.felementsout[i].output.data[-2])
                else:
                    self.felementsout[i].setvalue(self.felementsin[i].input.data[-1])
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
    
    if method == "discrete":
        return UpdateCEdiscrete(name)
    return UpdateCEcontinious(name)

#signum########################################################################

signumargs = [[], [], [], [], [], []]

def signum(name):
    """returns the abs value"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                # print("signum-anfang")
                # print(np.sign(self.felementsin[i].input.data[-1]))
                self.felementsout[i].setvalue(np.sign(self.felementsin[i].input.data[-1]))
                # print(self.felementsout[i].value)
                # print(self.felementsout[i].output.data[-1])
                # print("signum-ende")
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#maximum#######################################################################

maximumargs = [[["global", "per input"]],
               ["type"],
               [""],
               [str],
               [bl.PropertyElement.noconstrains],
               ["choses how the the maximum will be calculated"]]

def maximum(name, globalmax, numberinputs):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
    
        def __init__(self, name, globalmax, numberinputs):
            super().__init__(name)
            
            self.globalmax = globalmax == "global"
            self.felement = deltaTau.FunctionELement(name, "Q", 0,)
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.felements = []
            self.felementsout = [self.felement]
            if self.globalmax == False:
                self.felementsout = []
                for _ in range(numberinputs):
                    self.felementsout.append(self.felement.copy())
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements + self.felementsout
        
        def setarray(self):
            """gets the output atomics"""
            
            if self.globalmax:
                self.outputs[0].setarray([self.felement.getoutput()])
            else:
                out = []
                for e in self.felementsout:
                    out.append(e.getoutput())
                self.outputs[0].setarray(out)
            
        def connect(self):
            """sets the outsignals"""
            
            for i in self.inputs:
                inputs = i.getsignals()
                for i, s in enumerate(inputs):
                    e = self.basein.copy()
                    e.setinput(s)
                    self.felements.append(e)
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            if self.globalmax == True:
                minvalue = []
                for i in self.inputs:
                    inputs = i.getsignals()
                    for i in inputs:
                        minvalue.append(i.data[-1])
                self.felement.setvalue(max(minvalue))
            else:
                for j, i in enumerate(self.inputs):
                    minvalue = []
                    inputs = i.getsignals()
                    for i in inputs:
                        minvalue.append(i.data[-1])
                    self.felementsout[j].setvalue(max(minvalue))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
    
    return UpdateCE(name, globalmax, numberinputs)

#minimum#######################################################################

minimumargs = [[["global", "per input"]],
               ["type"],
               [""],
               [str],
               [bl.PropertyElement.noconstrains],
               ["choses how the the minimum will be calculated"]]

def minimum(name, globalmin, numberinputs):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
    
        def __init__(self, name, globalmin, numberinputs):
            super().__init__(name)
            
            self.globalmin = globalmin == "global"
            self.felement = deltaTau.FunctionELement(name, "Q", 0,)
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.felements = []
            self.felementsout = [self.felement]
            if self.globalmin == False:
                self.felementsout = []
                for _ in range(numberinputs):
                    self.felementsout.append(self.felement.copy())
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements + self.felementsout
        
        def setarray(self):
            """gets the output atomics"""
            
            if self.globalmin:
                self.outputs[0].setarray([self.felement.getoutput()])
            else:
                out = []
                for e in self.felementsout:
                    out.append(e.getoutput())
                self.outputs[0].setarray(out)
            
        def connect(self):
            """sets the outsignals"""
            
            for i in self.inputs:
                inputs = i.getsignals()
                for i, s in enumerate(inputs):
                    e = self.basein.copy()
                    e.setinput(s)
                    self.felements.append(e)
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            if self.globalmin == True:
                minvalue = []
                for i in self.inputs:
                    inputs = i.getsignals()
                    for i in inputs:
                        minvalue.append(i.data[-1])
                self.felement.setvalue(min(minvalue))
            else:
                for j, i in enumerate(self.inputs):
                    minvalue = []
                    inputs = i.getsignals()
                    for i in inputs:
                        minvalue.append(i.data[-1])
                    self.felementsout[j].setvalue(min(minvalue))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
    
    return UpdateCE(name, globalmin, numberinputs)

#offset########################################################################

offsetargs = [[0],
              ["Offset"],
              [""],
              [float],
              [bl.PropertyElement.noconstrains],
              ["Offset value"]]

def offset(name, offset):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, offset):
            super().__init__(name)
            
            self.basesource = deltaTau.FunctionELement(name, "Q", offset)
            self.baseadder = deltaTau.FunctionELement(name, "A", [])
            self.felements = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                q = self.basesource.copy()
                a = self.baseadder.copy()
                a.setvalue([q.getoutput()])
                a.setinput(s)
                a.setoutput(outputs[i])
                self.felements.append(q)
                self.felements.append(a)
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, offset)

#square########################################################################

squareargs = [[], [], [], [], [], []]

def square(name):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                self.felementsout[i].setvalue(self.felementsin[i].input.data[-1]*self.felementsin[i].input.data[-1])
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#sqrt#########################################################################

sqrtargs = [[], [], [], [], [], []]

def sqrt(name):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                self.felementsout[i].setvalue(np.sqrt(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#exp###########################################################################

expargs = [[], [], [], [], [], []]

def exp(name):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                self.felementsout[i].setvalue(np.exp(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#ln############################################################################

lnargs = [[], [], [], [], [], []]

def ln(name):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                self.felementsout[i].setvalue(np.log(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#log2##########################################################################

log2args = [[], [], [], [], [], []]

def log2(name):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                self.felementsout[i].setvalue(np.log2(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#log10#########################################################################

log10args = [[], [], [], [], [], []]

def log10(name):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                self.felementsout[i].setvalue(np.log10(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#power#########################################################################

powerargs = [[], [], [], [], [], []]

def power(name):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.basesource = deltaTau.FunctionELement(name, "Q", 0,)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[1].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2):
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs2) == 1:
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[0])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs1) == 1:
                for i, _ in enumerate(inputs2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[0])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                self.felementsout[i].setvalue(np.power(self.felementsin1[i].input.data[-1], self.felementsin2[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#round#########################################################################

roundargs = [[["floor", "ceil", "round", "fixed"]],
             ["rounding function"],
             [""],
             [str],
             [bl.PropertyElement.noconstrains],
             ["rounding function can either be floor, ceil. round or fixed"]]

def rounding(name, roundingfunction):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, degree):
            super().__init__(name)
            
            self.roundingfunction = roundingfunction
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.roundingfunction == "round":
                    self.felementsout[i].setvalue(np.round(self.felementsin[i].input.data[-1]))
                elif self.roundingfunction == "floor":
                    self.felementsout[i].setvalue(np.floor(self.felementsin[i].input.data[-1]))
                elif self.roundingfunction == "fixed":
                    self.felementsout[i].setvalue(np.fix(self.felementsin[i].input.data[-1]))
                else:
                    self.felementsout[i].setvalue(np.ceil(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, roundingfunction)

#modulo########################################################################

modargs = [[], [], [], [], [], []]

def mod(name):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.basesource = deltaTau.FunctionELement(name, "Q", 0,)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[1].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2):
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs2) == 1:
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[0])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs1) == 1:
                for i, _ in enumerate(inputs2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[0])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                self.felementsout[i].setvalue(np.mod(self.felementsin1[i].input.data[-1], self.felementsin2[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#remainder#####################################################################

remargs = [[], [], [], [], [], []]

def rem(name):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.basesource = deltaTau.FunctionELement(name, "Q", 0,)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[1].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2):
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs2) == 1:
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[0])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs1) == 1:
                for i, _ in enumerate(inputs2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[0])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                self.felementsout[i].setvalue(np.remainder(self.felementsin1[i].input.data[-1], self.felementsin2[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#sine##########################################################################

sineargs = [[["degree", "radian"]],
            ["input unit"],
            [""],
            [str],
            [bl.PropertyElement.noconstrains],
            ["input unit can be degree or radian"]]

def sine(name, degree):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, degree):
            super().__init__(name)
            
            self.degree = degree == "degree"
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.degree:
                    self.felementsout[i].setvalue(np.sin(self.felementsin[i].input.data[-1]*180/np.pi))
                else:
                    self.felementsout[i].setvalue(np.sin(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, degree)

#cos###########################################################################

cosargs = [[["degree", "radian"]],
            ["input unit"],
            [""],
            [str],
            [bl.PropertyElement.noconstrains],
            ["input unit can be degree or radian"]]

def cos(name, degree):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, degree):
            super().__init__(name)
            
            self.degree = degree == "degree"
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.degree:
                    self.felementsout[i].setvalue(np.cos(self.felementsin[i].input.data[-1]*180/np.pi))
                else:
                    self.felementsout[i].setvalue(np.cos(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, degree)

#tangens#######################################################################

tanargs = [[["degree", "radian"]],
            ["input unit"],
            [""],
            [str],
            [bl.PropertyElement.noconstrains],
            ["input unit can be degree or radian"]]

def tan(name, degree):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, degree):
            super().__init__(name)
            
            self.degree = degree == "degree"
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.degree:
                    self.felementsout[i].setvalue(np.tan(self.felementsin[i].input.data[-1]*180/np.pi))
                else:
                    self.felementsout[i].setvalue(np.tan(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, degree)

#arcsin########################################################################

arcsinargs = [[["degree", "radian"]],
              ["input unit"],
              [""],
              [str],
              [bl.PropertyElement.noconstrains],
              ["input unit can be degree or radian"]]

def arcsin(name, degree):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, degree):
            super().__init__(name)
            
            self.degree = degree == "degree"
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.degree:
                    self.felementsout[i].setvalue(np.arcsin(self.felementsin[i].input.data[-1]*180/np.pi))
                else:
                    self.felementsout[i].setvalue(np.arcsin(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, degree)

#arccos########################################################################

arccosargs = [[["degree", "radian"]],
              ["input unit"],
              [""],
              [str],
              [bl.PropertyElement.noconstrains],
              ["input unit can be degree or radian"]]

def arccos(name, degree):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, degree):
            super().__init__(name)
            
            self.degree = degree == "degree"
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.degree:
                    self.felementsout[i].setvalue(np.arccos(self.felementsin[i].input.data[-1]*180/np.pi))
                else:
                    self.felementsout[i].setvalue(np.arccos(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, degree)

#arctan########################################################################

arctanargs = [[["degree", "radian"]],
              ["input unit"],
              [""],
              [str],
              [bl.PropertyElement.noconstrains],
              ["input unit can be degree or radian"]]

def arctan(name, degree):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, degree):
            super().__init__(name)
            
            self.degree = degree == "degree"
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.degree:
                    self.felementsout[i].setvalue(np.arctan(self.felementsin[i].input.data[-1]*180/np.pi))
                else:
                    self.felementsout[i].setvalue(np.arctan(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, degree)

#limiter#######################################################################

limiterargs = [[1, -1],
               ["Top-limit", "Bottom-limit"],
               ["", ""],
               [float, float],
               [bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains],
               ["Top-limit", "Bottom-limit"]]

def limiter(name, toplimit, bottomlimit):
    """limits the input"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, toplimit, bottomlimit):
            super().__init__(name)
            
            self.felement1 = deltaTau.FunctionELement(name, "D", 0,)
            self.felement2 = deltaTau.FunctionELement(name, "Q", 1)
            self.toplimit = max(toplimit, bottomlimit)
            self.bottomlimit = min(toplimit, bottomlimit)
            self.base = [self.felement1, self.felement2]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                model = copy.deepcopy(self.base)
                model[0].setinput(s)
                model[1].setoutput(outputs[i])
                self.felements = self.felements + model
                self.models.append(model)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                    if model[0].input.data[-1] > self.toplimit:
                        model[1].setvalue(self.toplimit)
                    elif model[0].input.data[-1] < self.bottomlimit:
                        model[1].setvalue(self.bottomlimit)
                    else:
                        model[1].setvalue(model[0].input.data[-1])
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, toplimit, bottomlimit)

#ratelimiter###################################################################

ratelimiterargs = [[1, -1],
                   ["Rising-rate-limit", "Falling-rate-limit"],
                   ["", ""],
                   [float, float],
                   [bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains],
                   ["Rising-rate-limit", "Falling-rate-limit"]]

def ratelimiter(name, risinglimit, fallinglimit):
    """this function multiplies with a set gain"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, risinglimit, fallinglimit):
            super().__init__(name)
            
            self.felement1 = deltaTau.FunctionELement(name, "D", 0,)
            self.felement2 = deltaTau.FunctionELement(name, "Q", 1)
            self.risinglimit = risinglimit
            self.fallinglimit = fallinglimit
            self.base = [self.felement1, self.felement2]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                model = copy.deepcopy(self.base)
                model[0].setinput(s)
                model[1].setoutput(outputs[i])
                self.felements = self.felements + model
                self.models.append(model)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                if len(model[0].input.data) > 1:
                    if model[0].input.data[-1] > model[1].output.data[-2] + self.risinglimit*(model[0].input.t[-1] - model[0].input.t[-2]):
                        model[1].setvalue(model[1].output.data[-2] + self.risinglimit*(model[0].input.t[-1] - model[0].input.t[-2]))
                    elif model[0].input.data[-1] < model[1].output.data[-2] + self.fallinglimit*(model[0].input.t[-1] - model[0].input.t[-2]):
                        model[1].setvalue(model[1].output.data[-2] + self.fallinglimit*(model[0].input.t[-1] - model[0].input.t[-2]))
                    else:
                        model[1].setvalue(model[0].input.data[-1])
                else:
                    model[1].setvalue(model[0].input.data[-1])
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
            
    return UpdateCE(name, risinglimit, fallinglimit)

#lookup1d######################################################################

lookup1dargs = [[[0, 1], [0, 1]],
                ["x", "y"],
                ["", ""],
                [list, list],
                [bl.PropertyElement.steadyincreasinglist, bl.PropertyElement.noconstrains],
                ["x-vector of look-up table", "y-vector of look-up table"]]

def lookup1d(name, x, y):
    """1d look-up table"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, x, y):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.lookupf = interp.interp1d(x, y)
            self.x = x
            self.y = y
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.felementsin[i].input.data[-1] <= self.x[0]:
                    self.felementsout[i].setvalue(self.y[0])
                elif self.felementsin[i].input.data[-1] >= self.x[-1]:
                    self.felementsout[i].setvalue(self.y[-1])
                else:
                    self.felementsout[i].setvalue(self.lookupf(self.felementsin[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, x, y)

#lookup2d######################################################################

lookup2dargs = [[[0, 1], [0, 1], [[0, 1], [0, 1]]],
                ["x", "y", "z"],
                ["", "", ""],
                [list, list, list],
                [bl.PropertyElement.steadyincreasinglist, bl.PropertyElement.steadyincreasinglist, bl.PropertyElement.noconstrains],
                ["x-vector of look-up table", "y-vector of look-up table", "z-matrix of look-up table"]]

def lookup2d(name, x, y, z):
    """1d look-up table"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, x, y, z):
            super().__init__(name)
            
            self.baseinx = deltaTau.FunctionELement(name, "D", 0,)
            self.baseiny = deltaTau.FunctionELement(name, "D", 0)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            #self.lookupf = interp.interp1d(x, y)
            self.x = x
            self.y = y
            self.z = np.array(z)
            self.xx , self.yy = np.meshgrid(self.x, self.y)
            if not self.z.shape == self.xx.shape or not self.z.shape == self.yy.shape:
                raise TypeError(f"{name} sizes of x, y or z do not match")
            self.lookupf = interp.interp2d(self.xx, self.yy, self.z)
            #self.lookupf = interp.LinearNDInterpolator(list(zip(x, y)), z)
            self.felementsinx = []
            self.felementsiny = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsinx + self.felementsiny + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputsx = self.inputs[1].getsignals()
            inputsy = self.inputs[0].getsignals()
            if not len(inputsx) == len(inputsy):
                raise IndexError("Array length of input x does not macht input array length of input y")
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputsx):
                einx = self.baseinx.copy()
                einy = self.baseiny.copy()
                eout = self.baseout.copy()
                einx.setinput(s)
                einy.setinput(inputsy[i])
                eout.setoutput(outputs[i])
                self.felementsinx.append(einx)
                self.felementsiny.append(einy)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsinx):
                x = self.felementsinx[i].input.data[-1]
                y = self.felementsiny[i].input.data[-1]
                if x < self.x[0]:
                    x = self.x[0]
                elif x > self.x[-1]:
                    x = self.x[-1]
                if y < self.y[0]:
                    y = self.y[0]
                elif y > self.y[-1]:
                    y = self.y[-1]
                self.felementsout[i].setvalue(self.lookupf(x, y)[0])
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, x, y, z)

#deadzone######################################################################

deadzoneargs = [[1, -1],
                ["High-limit", "Low-limit"],
                ["", ""],
                [float, float],
                [bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains],
                ["High-limit for deadzone", "Low-limit for deadzone"]]

def deadzone(name, toplimit, bottomlimit):
    """deadzone of input"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, toplimit, bottomlimit):
            super().__init__(name)
            
            self.felement1 = deltaTau.FunctionELement(name, "D", 0,)
            self.felement2 = deltaTau.FunctionELement(name, "Q", 1)
            self.toplimit = max(toplimit, bottomlimit)
            self.bottomlimit = min(toplimit, bottomlimit)
            self.base = [self.felement1, self.felement2]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                model = copy.deepcopy(self.base)
                model[0].setinput(s)
                model[1].setoutput(outputs[i])
                self.felements = self.felements + model
                self.models.append(model)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                    if model[0].input.data[-1] > self.toplimit:
                        model[1].setvalue(model[0].input.data[-1] - self.toplimit)
                    elif model[0].input.data[-1] < self.bottomlimit:
                        model[1].setvalue(model[0].input.data[-1] - self.bottomlimit)
                    else:
                        model[1].setvalue(0)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, toplimit, bottomlimit)

#relay#########################################################################

relayargs = [[[1, -1], [1, -1]],
             ["Threshold", "Output-state"],
             ["", ""],
             [list, list],
             [bl.PropertyElement.listoftwofloats, bl.PropertyElement.listoftwofloats],
             ["Turn high/low input value", "High/Low output value"]]

def relay(name, threshold, outputstate):
    """hysteresis"""
            
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, threshold, outputstate):
            super().__init__(name)
            
            self.felement1 = deltaTau.FunctionELement(name, "D", 0,)
            self.felement2 = deltaTau.FunctionELement(name, "Q", max(outputstate))
            self.onvalue = max(outputstate)
            self.offvalue = min(outputstate)
            self.base = [self.felement1, self.felement2]
            self.onswitching = max(threshold)
            self.offswitching = min(threshold)
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                model = copy.deepcopy(self.base)
                model[0].setinput(s)
                model[1].setoutput(outputs[i])
                self.felements = self.felements + model
                self.models.append(model)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                if model[0].input.data[-1] > self.onswitching and model[1].output.data[-1] == self.offvalue:
                    model[1].setvalue(self.onvalue)
                elif model[0].input.data[-1] < self.offswitching and model[1].output.data[-1] == self.onvalue:
                    model[1].setvalue(self.offvalue)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name, threshold, outputstate)

#ploty#########################################################################

plotyargs = [[""],
             ["Y-Axis"],
             [""],
             [str],
             [bl.PropertyElement.noconstrains],
             ["Text at the Y-axis"]]

def ploty(name, ylabel, numerinputs):
    """plots the inputs"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, ylabel):
            super().__init__(name, ylabel)
            
            self.felement1 = deltaTau.FunctionELement(name, "D", 0,)
            self.felements = []
            # self.y = []
            # self.x = []
            self.window = pw.PlotWindow(name)
            self.data = self.window.addstream(name, "time", name)
            self.window.show()
            self.lastupdate = 0
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            for i, s in enumerate(inputs):
                model = copy.deepcopy(self.felement1)
                model.setinput(s)
                self.felements.append(model)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            y = []
            inputs = self.inputs[0].getsignals()
            if len(inputs[0].time()) < 10 or utime.time() < self.lastupdate + .04:
                return 0
            for i in inputs:
                y.append(i.getdata()[0:-1])
            _time = inputs[0].time()[0:-1]
            self.lastupdate = utime.time()
            self.data._set([_time, y])
            return 0
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            y = []
            inputs = self.inputs[0].getsignals()
            for i in inputs:
                y.append(i.getdata())
            time = inputs[0].time()
            time= copy.deepcopy(time)
            y = copy.deepcopy(y)
            self.data._set([time, y])
            self.data.close()
            self.publicvariables = []
            
        def action(self):
            """user actived action"""
            
            self.window.show()
            
        def clear(self):
            """closes the window"""
            
            self.window.close()
                
    return UpdateCE(name, ylabel)

#PI############################################################################

PIargs = [[1, 1],
          ["kI", "kP"],
          ["", ""],
          [float, float],
          [bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains],
          ["Integration factor", "Proportional factor"]]

def PI(name, gainI, gainP):
    """PI controller"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, gainI, gainP):
            super().__init__(name)
            
            self.basegain1 = deltaTau.FunctionELement(name, "G", gainI)
            self.basegain2 = deltaTau.FunctionELement(name, "G", gainP)
            self.baseintegrator = deltaTau.FunctionELement(name, "S", 1)
            self.baseadder = deltaTau.FunctionELement(name, "A", [self.baseintegrator.getoutput()])
            self.baseintegrator.setinput(self.basegain1.getoutput())
            self.baseadder.setinput(self.basegain2.getoutput())
            self.basemodel = [self.basegain1, self.basegain2, self.baseintegrator, self.baseadder]
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felements = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                m = copy.deepcopy(self.basemodel)
                m[0].setinput(s)
                m[1].setinput(s)
                m[3].setoutput(outputs[i])
                self.felements = self.felements + m
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, gainI, gainP)

#PID###########################################################################

PIDargs = [[1, 1, 1, 1000],
           ["kI", "kP", "kd", "kf"],
           ["", "", "", ""],
           [float, float, float, float],
           [bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains],
           ["Integration gain", "Proportional gain", "Derivativ gain", "filter coefficient"]]

def PID(name, gainI, gainP, gaind, gainf):
    """PID controller"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, gainI, gainP, gaind, gainf):
            super().__init__(name)
            
            self.gI = deltaTau.FunctionELement(name, "G", gainI)
            self.gP = deltaTau.FunctionELement(name, "G", gainP)
            self.gd = deltaTau.FunctionELement(name, "G", gaind)
            self.gf = deltaTau.FunctionELement(name, "G", gainf)
            self.gn = deltaTau.FunctionELement(name, "G", -1)
            self.sI = deltaTau.FunctionELement(name, "S", 1)
            self.sd = deltaTau.FunctionELement(name, "S", 1)
            self.ad = deltaTau.FunctionELement(name, "A", [self.gd.getoutput()])
            self.ae = deltaTau.FunctionELement(name, "A", [self.sI.getoutput(), self.gf.getoutput()])
            self.sI.setinput(self.gI.getoutput())
            self.ae.setinput(self.gP.getoutput())
            self.ad.setinput(self.gn.getoutput())
            self.gn.setinput(self.sd.getoutput())
            self.gf.setinput(self.ad.getoutput())
            self.sd.setinput(self.gf.getoutput())
            self.basemodel = [self.gI, self.gP, self.gd, self.gf, self.gn, self.sI, self.sd, self.ad, self.ae]
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felements = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                m = copy.deepcopy(self.basemodel)
                m[0].setinput(s)
                m[1].setinput(s)
                m[2].setinput(s)
                m[8].setoutput(outputs[i])
                self.felements = self.felements + m
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, gainI, gainP, gaind, gainf)

#PLL###########################################################################

PLLargs = [[100, 100, 100, ["frequency", "sin-signal", "rect-signal", "phase-angle"]],
           ["kI", "kP", "f_offset", "output"],
           ["", "", "Hz", ""],
           [float, float, float, str],
           [bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains],
           ["gain integrator PI-controller", "gain proportional part PI-controller", "start frequency", "output of the PLL"]]

def PLL(name, gainI, gainP, w0, output):
    """PLL"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, gainI, gainP, w0, output):
            super().__init__(name)
            
            self.output = output
            self.phdetecin1 = deltaTau.FunctionELement(name, "D", 0,)
            self.phdetecin2 = deltaTau.FunctionELement(name, "D", 0,)
            self.phdetecout = deltaTau.FunctionELement(name, "Q", 0)
            self.PIGI = deltaTau.FunctionELement(name, "G", gainI)
            self.PIGP = deltaTau.FunctionELement(name, "G", gainP)
            self.PII = deltaTau.FunctionELement(name, "S", 1)
            self.PIA = deltaTau.FunctionELement(name, "A", [self.PII.getoutput()])
            self.WQ = deltaTau.FunctionELement(name, "Q", 2*np.pi*w0)
            self.ADDW = deltaTau.FunctionELement(name, "A", [self.WQ.getoutput()])
            self.IW = deltaTau.FunctionELement(name, "S", 1)
            self.cosin = deltaTau.FunctionELement(name, "D", 0,)
            self.cosout = deltaTau.FunctionELement(name, "Q", 0)
            self.rect = deltaTau.FunctionELement(name, "Q", 0)
            self.fg = deltaTau.FunctionELement(name, "G", 1/2/np.pi)
            self.phdetecin2.setinput(self.cosout.getoutput())
            self.PIGI.setinput(self.phdetecout.getoutput())
            self.PIGP.setinput(self.phdetecout.getoutput())
            self.PII.setinput(self.PIGI.getoutput())
            self.PIA.setinput(self.PIGP.getoutput())
            self.ADDW.setinput(self.PIA.getoutput())
            self.IW.setinput(self.ADDW.getoutput())
            self.cosin.setinput(self.IW.getoutput())
            self.fg.setinput(self.ADDW.getoutput())
            self.base = [self.phdetecin1, self.phdetecin2, self.phdetecout, self.PIGI, self.PIGP, self.PII, self.PIA, self.WQ, self.ADDW, self.cosin, self.cosout, self.IW, self.fg, self.rect]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                model = copy.deepcopy(self.base)
                model[0].setinput(s)
                if self.output == "frequency":
                    model[12].setoutput(outputs[i])
                elif self.output == "sin-signal":
                    model[10].setoutput(outputs[i])
                    model[1].setinput(outputs[i])
                elif self.output == "rect-signal":
                    model[13].setoutput(outputs[i])
                else:
                    model[8].setoutput(outputs[i])
                    model[11].setinput(outputs[i])
                    model[12].setinput(outputs[i])
                self.felements = self.felements + model
                self.models.append(model)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                model[10].setvalue(np.sin(model[9].input.data[-1]))
                model[13].setvalue(int(0 <= np.sin(model[9].input.data[-1])))
                if len(model[0].input.data) > 1:
                    if model[2].output.data[-1] == 1 and model[1].input.data[-1] > 0:
                        model[2].setvalue(0)
                    elif model[2].output.data[-1] == -1 and model[0].input.data[-1] > 0:
                        model[2].setvalue(0)
                    if model[2].output.data[-1] == 0:
                        if model[0].input.data[-1] > 0 and model[0].input.data[-2] <= 0 and model[1].input.data[-1] <= 0:
                            model[2].setvalue(1)
                        elif model[1].input.data[-1] > 0 and model[1].input.data[-2] <= 0 and model[0].input.data[-1] <= 0:
                            model[2].setvalue(-1)
                else:
                    if model[0].input.data[-1] > 0 and model[1].input.data[-1] <= 0:
                        model[2].setvalue(1)
                    elif model[1].input.data[-1] > 0 and model[0].input.data[-1] <= 0:
                        model[2].setvalue(-1)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name, gainI, gainP, w0, output)

#PLL3ph########################################################################

PLL3phargs = [[100, 100, 1, 100, ["frequency", "sin-signal", "rect-signal", "phase-angle", "amplitude"]],
              ["kI", "kP", "kd", "f_offset", "output"],
              ["", "", "", "Hz", ""],
              [float, float, float, float, str],
              [bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains, bl.PropertyElement.noconstrains],
              ["gain integrator PID-controller", "gain proportional part PID-controller", "gain diferential part PID-controller", "start frequency", "output of the PLL"]]

def PLL3ph(name, gainI, gainP, gaind, w0, output):
    """PLL"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, gainI, gainP, gaind, w0, output):
            super().__init__(name)
            
            self.output = output
            self.phain = deltaTau.FunctionELement(name+".phain", "D", 0)
            self.phbin = deltaTau.FunctionELement(name+".phbin", "D", 0)
            self.phcin = deltaTau.FunctionELement(name+".phcin", "D", 0)
            self.phi = deltaTau.FunctionELement(name+".phi", "D", 0)
            
            self.detectd = deltaTau.FunctionELement(name+".detectd", "Q", 0)
            self.detectq = deltaTau.FunctionELement(name+".detectq", "Q", 0)
            
            self.PIGI = deltaTau.FunctionELement(name+".PIGI", "G", gainI)
            self.PIGP = deltaTau.FunctionELement(name+".PIGP", "G", gainP)
            self.PIGd = deltaTau.FunctionELement(name+".PIGd", "G", gaind)
            self.PIGk = deltaTau.FunctionELement(name+".PIGk", "G", 100000)
            self.PII = deltaTau.FunctionELement(name+".PII", "S", 1)
            self.PId = deltaTau.FunctionELement(name+".PId", "S", 1)
            self.PIGn = deltaTau.FunctionELement(name+".PIGn", "G", -1)
            self.PIAd = deltaTau.FunctionELement(name+".PIAd", "A", [self.PIGd.getoutput()])
            self.PIA = deltaTau.FunctionELement(name+".PIA", "A", [self.PII.getoutput(), self.PIGk.getoutput()])
            
            self.WQ = deltaTau.FunctionELement(name+".WQ", "Q", 2*np.pi*w0)
            self.ADDW = deltaTau.FunctionELement(name+".ADDW", "A", [self.WQ.getoutput()])
            self.IW = deltaTau.FunctionELement(name+".IW", "S", 1)
            self.cosin = deltaTau.FunctionELement(name+".cosin", "D", 0,)
            self.cosout = deltaTau.FunctionELement(name+".cosout", "Q", 0)
            self.rec = deltaTau.FunctionELement(name+".rec", "Q", 0)
            
            self.fg = deltaTau.FunctionELement(name+".fg", "G", 1/2/np.pi)
            
            self.PIGI.setinput(self.detectq.getoutput())
            self.PIGP.setinput(self.detectq.getoutput())
            self.PIGd.setinput(self.detectq.getoutput())
            self.PIGk.setinput(self.PIAd.getoutput())
            self.PII.setinput(self.PIGI.getoutput())
            self.PId.setinput(self.PIGn.getoutput())
            self.PIGn.setinput(self.PIGk.getoutput())
            self.PIAd.setinput(self.PId.getoutput())
            self.PIA.setinput(self.PIGP.getoutput())
            self.ADDW.setinput(self.PIA.getoutput())
            self.IW.setinput(self.ADDW.getoutput())
            self.phi.setinput(self.IW.getoutput())
            self.cosin.setinput(self.IW.getoutput())
            self.fg.setinput(self.ADDW.getoutput())
            self.base = [self.phain, self.phbin, self.phcin, self.phi, self.detectd, self.detectq, self.PIGI, self.PIGP, self.PIGd, self.PIGk, self.PII, self.PId, self.PIA, self.WQ, self.ADDW, self.cosin, self.cosout, self.IW, self.fg, self.rec, self.PIGn, self.PIAd]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            inputs = self.inputs[0].copysignals()
            if len(inputs) > 0:
                self.outputs[0].setarray([copy.deepcopy(inputs[0])])
            else:
                self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            if len(self.inputs[0].signals) == 3:
                inputs = self.inputs[0].getsignals()
                outputs = self.outputs[0].getsignals()
                if len(inputs)/3 == len(outputs):
                    for i in range(int(len(inputs)/3)):
                        model = copy.deepcopy(self.base)
                        model[0].setinput(inputs[i])
                        model[1].setinput(inputs[i + int(len(inputs)/3)])
                        model[2].setinput(inputs[i + int(2*len(inputs)/3)])
                        if self.output == "frequency":
                            model[18].setoutput(outputs[i])
                        elif self.output == "sin-signal":
                            model[16].setoutput(outputs[i])
                        elif self.output == "rect-signal":
                            model[19].setoutput(outputs[i])
                        elif self.output == "amplitude":
                            model[4].setoutput(outputs[i])
                        else:
                            model[17].setoutput(outputs[i])
                            model[3].setinput(outputs[i])
                            model[15].setinput(outputs[i])
                        self.felements = self.felements + model
                        self.models.append(model)
                else:
                    raise TypeError("TypeError: input is of wrong size")
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                model[16].setvalue(np.sin(model[15].input.data[-1]))
                model[19].setvalue(int(0 >= np.sin(model[15].input.data[-1])))
                model[4].setvalue(2/3*(m.cos(model[3].input.data[-1])*model[0].input.data[-1] + m.cos(model[3].input.data[-1] + 2/3*m.pi)*model[1].input.data[-1] + m.cos(model[3].input.data[-1] - 2/3*m.pi)*model[2].input.data[-1]))
                model[5].setvalue(2/3*(m.sin(model[3].input.data[-1])*model[0].input.data[-1] + m.sin(model[3].input.data[-1] + 2/3*m.pi)*model[1].input.data[-1] + m.sin(model[3].input.data[-1] - 2/3*m.pi)*model[2].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name, gainI, gainP, gaind, w0, output)

#biggerthanconstant############################################################

biggerthanconstantargs = [[0],
                          ["Constant c"],
                          [""],
                          [float],
                          [bl.PropertyElement.noconstrains],
                          ["Constant value for comparison"]]

def biggerthanconstant(name, constant):
    """compares the inputs to a constant
    if True output is 1 if False output is 0"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, constant):
            super().__init__(name)
            
            self.constant = constant
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.felementsin[i].input.data[-1] > self.constant:
                    self.felementsout[i].setvalue(1)
                else:
                    self.felementsout[i].setvalue(0)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, constant)

#smallerthanconstantargs#######################################################

smallerthanconstantargs = [[0],
                           ["Constant c"],
                           [""],
                           [float],
                           [bl.PropertyElement.noconstrains],
                           ["Constant value for comparison"]]

def smallerthanconstant(name, constant):
    """compares the inputs to a constant
    if True output is 1 if False output is 0"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, constant):
            super().__init__(name)
            
            self.constant = constant
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.felementsin[i].input.data[-1] < self.constant:
                    self.felementsout[i].setvalue(1)
                else:
                    self.felementsout[i].setvalue(0)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, constant)

#bigger########################################################################

biggerargs = [[], [], [], [], [], []]

def bigger(name):
    """compares to inputs
    1 if input0 > input1"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.basesource = deltaTau.FunctionELement(name, "Q", 0,)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[1].getsignals()
            inputs2 = self.inputs[0].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[1].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[1].getsignals()
            inputs2 = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2):
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs2) == 1:
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[0])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs1) == 1:
                for i, _ in enumerate(inputs2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[0])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                self.felementsout[i].setvalue(int(self.felementsin1[i].input.data[-1]>self.felementsin2[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#smaller#######################################################################

smallerargs = [[], [], [], [], [], []]

def smaller(name):
    """compares to inputs
    1 if input0 > input1"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.basesource = deltaTau.FunctionELement(name, "Q", 0,)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[1].getsignals()
            inputs2 = self.inputs[0].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[1].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[1].getsignals()
            inputs2 = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2):
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs2) == 1:
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[0])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs1) == 1:
                for i, _ in enumerate(inputs2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[0])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                self.felementsout[i].setvalue(int(self.felementsin1[i].input.data[-1]<self.felementsin2[i].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#and###########################################################################

andargs = [[], [], [], [], [], []]

def And(name):
    """if both inputs are bigger than 0.5 1 will be returned"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.basesource = deltaTau.FunctionELement(name, "Q", 0,)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[1].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2):
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs2) == 1:
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[0])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs1) == 1:
                for i, _ in enumerate(inputs2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[0])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                self.felementsout[i].setvalue(int(self.felementsin1[i].input.data[-1] > 0.5 and self.felementsin2[i].input.data[-1] > 0.5))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#or############################################################################

orargs = [[], [], [], [], [], []]

def Or(name):
    """if one or both inputs are bigger than 0.5 1 will be returned"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.basesource = deltaTau.FunctionELement(name, "Q", 0,)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[1].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2):
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs2) == 1:
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[0])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs1) == 1:
                for i, _ in enumerate(inputs2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[0])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                self.felementsout[i].setvalue(int(self.felementsin1[i].input.data[-1] > 0.5 or self.felementsin2[i].input.data[-1] > 0.5))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#xor###########################################################################

xorargs = [[], [], [], [], [], []]

def Xor(name):
    """if one of both is greater 0.5 1 will be returned"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.basesource = deltaTau.FunctionELement(name, "Q", 0,)
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            if len(inputs1) > len(inputs2):
                self.outputs[0].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[1].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[0].getsignals()
            inputs2 = self.inputs[1].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2):
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs2) == 1:
                for i, _ in enumerate(inputs1):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[i])
                    in2.setinput(inputs2[0])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            elif len(inputs1) == 1:
                for i, _ in enumerate(inputs2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out = self.basesource.copy()
                    in1.setinput(inputs1[0])
                    in2.setinput(inputs2[i])
                    out.setoutput(outputs[i])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout.append(out)
            else:
                raise TypeError("TypeError: Input Arrays are not compatible")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                self.felementsout[i].setvalue(int((int(self.felementsin1[i].input.data[-1] > 0.5) + int(self.felementsin2[i].input.data[-1] > 0.5)) == 1))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#not###########################################################################

notargs = [[], [], [], [], [], []]

def Not(name):
    """inverts the logic value"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.felementsin[i].input.data[-1] < 0.5:
                    self.felementsout[i].setvalue(1)
                else:
                    self.felementsout[i].setvalue(0)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name)

#phasedetector#################################################################

phasedetectorargs = [[], [], [], [], [], []]

def phasedetector(name):
    """digital phase detector"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.felement1 = deltaTau.FunctionELement(name, "D", 0,)
            self.felement2 = deltaTau.FunctionELement(name, "D", 0,)
            self.felement3 = deltaTau.FunctionELement(name, "Q", 0)
            self.base = [self.felement1, self.felement2, self.felement3]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs1 = self.inputs[1].getsignals()
            inputs2 = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            if len(inputs1) == len(inputs2):
                for i, s in enumerate(inputs1):
                    model = copy.deepcopy(self.base)
                    model[0].setinput(s)
                    model[1].setinput(inputs2[i])
                    model[2].setoutput(outputs[i])
                    self.felements = self.felements + model
                    self.models.append(model)
            else:
                raise TypeError ("TypeError: Inputs do not have the same size")
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                if len(model[0].input.data) > 1:
                    if model[2].output.data[-1] == 1 and model[1].input.data[-1] > 0:
                        model[2].setvalue(0)
                    elif model[2].output.data[-1] == -1 and model[0].input.data[-1] > 0:
                        model[2].setvalue(0)
                    if model[2].output.data[-1] == 0:
                        if model[0].input.data[-1] > 0 and model[0].input.data[-2] <= 0 and model[1].input.data[-1] <= 0:
                            model[2].setvalue(1)
                        elif model[1].input.data[-1] > 0 and model[1].input.data[-2] <= 0 and model[0].input.data[-1] <= 0:
                            model[2].setvalue(-1)
                else:
                    if model[0].input.data[-1] > 0 and model[1].input.data[-1] <= 0:
                        model[2].setvalue(1)
                    elif model[1].input.data[-1] > 0 and model[0].input.data[-1] <= 0:
                        model[2].setvalue(-1)
                return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#derivator#####################################################################

dtargs = [ [], [], [], [], [], []]

def dt(name):
    """derivation"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.felement2 = deltaTau.FunctionELement(name, "G", -1)
            self.felement3 = deltaTau.FunctionELement(name, "G", 100000)
            self.felement4 = deltaTau.FunctionELement(name, "S", 1)
            self.felement1 = deltaTau.FunctionELement(name, "A", [self.felement2.getoutput()])
            self.felement3.setinput(self.felement1.getoutput())
            self.felement4.setinput(self.felement3.getoutput())
            self.felement2.setinput(self.felement4.getoutput())
            self.basemodel = [self.felement1, self.felement2, self.felement3, self.felement4]
            self.outputs = [deltaTau.FunctionArray(self)]
            self.felements = []
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                m = copy.deepcopy(self.basemodel)
                m[0].setinput(s)
                m[2].setoutput(outputs[i])
                m[3].setinput(outputs[i])
                self.felements = self.felements + m
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#turnondelay###################################################################

turnondelayargs = [[0.01],
                   ["Delay"],
                   ["s"],
                   [float],
                   [bl.PropertyElement.biggerzero],
                   ["Turn on delay in s"]]

def turnondelay(name, delta):
    """analog to digital time discretization"""
    
    class UpdateCE(deltaTau.UpdateCE):
    
        def __init__(self, name, delta):
            super().__init__(name)
            
            self.delta = delta
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if self.felementsin[i].input.data[-1] < 0.5:
                    self.felementsout[i].setvalue(0)
                if self.felementsout[i].value == 0 and self.felementsin[i].input.data[-1] >= 0.5:
                    for j, d in enumerate(reversed(self.felementsin[i].input.data)):
                        if d < 0.5:
                            if time - self.felementsin[i].input.t[-j] >= self.delta:
                                self.felementsout[i].setvalue(1)
                            break
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]   
            
    return UpdateCE(name, delta)

#delay#########################################################################

delayargs = [[], [], [], [], [], []]

def delay(name):
    """delay by one timestep"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 0)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            if major == True:
                for i, _ in enumerate(self.felementsin):
                    if len(self.felementsin[i].input.datamajor) > 1:
                        self.felementsout[i].setvalue(self.felementsin[i].input.datamajor[-2])
                    else:
                        self.felementsout[i].setvalue(0)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#continuous2discrete###########################################################

continuous2discreteargs = [[], [], [], [], [], []]

def continuous2discrete(name):
    """continuous to descrete"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 0)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            if major == True:
                for i, _ in enumerate(self.felementsin):
                    if len(self.felementsin[i].input.datamajor) > 0:
                        self.felementsout[i].setvalue(self.felementsin[i].input.datamajor[-1])
                    else:
                        self.felementsout[i].setvalue(0)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#discreteintegrator############################################################

discreteintegratorargs = [[["Forward Euler", "Backward Euler", "Trapezoidal"]],
                          ["Method"],
                          [""],
                          [str],
                          [bl.PropertyElement.noconstrains],
                          ["Integration method"]]

def discreteintegrator(name, method):
    """discrete integrator by one timestep"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, method):
            super().__init__(name)
            
            self.method = method
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 0)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            if major:
                for i, _ in enumerate(self.felementsin):
                    if len(self.felementsin[i].input.datamajor) > 1:
                        if self.method == "Backward Euler":
                            self.felementsout[i].setvalue(self.felementsin[i].input.datamajor[-1]*(self.majordelta) + self.felementsout[i].output.datamajor[-2])
                        elif self.method == "Trapezoidal":
                            self.felementsout[i].setvalue((self.felementsin[i].input.datamajor[-1] + self.felementsin[i].input.datamajor[-2])*(self.majordelta)/2 + self.felementsout[i].output.datamajor[-2])
                        else:
                            self.felementsout[i].setvalue(self.felementsin[i].input.datamajor[-2]*(self.majordelta) + self.felementsout[i].output.datamajor[-2])
                    else:
                        self.felementsout[i].setvalue(self.felementsin[i].input.datamajor[-1])
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name, method)

#discretederivatorargs#########################################################

discretederivatorargs = [[], [], [], [], [], []]

def discretederivator(name):
    """delay by one timestep"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            if major:
                for i, _ in enumerate(self.felementsin):
                    if len(self.felementsin[i].input.data) > 1:
                        if (time - self.felementsout[i].output.t[-2]) > 0:
                            self.felementsout[i].setvalue((self.felementsin[i].input.datamajor[-1] - self.felementsin[i].input.datamajor[-2])/(self.majordelta))
                        else:
                            self.felementsout[i].setvalue(self.felementsout[i].output.datamajor[-1])
                    else:
                        self.felementsout[i].setvalue(0)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#discretemeanvalue#############################################################

discretemeanvalueargs = [[0, 100],
                         ["Initial-value", "Samples"],
                         ["", ""],
                         [float, int],
                         [bl.PropertyElement.noconstrains, bl.PropertyElement.biggerzeroint],
                         ["Initial value output", "Number of Samples"]]

def discretemeanvalue(name, initialvalue, samples):
    """mean of mejor time steps"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, initialvalue, samples):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.lasttmajor = 0
            self.basearray = np.array([initialvalue]*samples)
            self.register = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                self.register = self.register + [self.basearray.copy()]
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            if major:
                if len(self.felementsin[0].input.data) > 1:
                    if self.lasttmajor < self.felementsin[0].input.tmajor[-1]:
                        self.lasttmajor = self.felementsin[0].input.tmajor[-1]
                        for i, _ in enumerate(self.felementsin):
                            self.register[i] = np.roll(self.register[i], 1)
                    for i, _ in enumerate(self.felementsin):
                        self.register[i][0] = self.felementsin[i].input.datamajor[-1]
                        self.felementsout[i].setvalue(self.register[i].mean())
                else:
                    for i, _ in enumerate(self.felementsin):
                        self.felementsout[i].setvalue(self.register[i].mean())
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name, initialvalue, samples)

#quantizer####################################################################

quantizerargs = [[1],
                 ["q"],
                 [""],
                 [float],
                 [bl.PropertyElement.biggerzero],
                 ["Quantum step q"]]

def quantizer(name, quantum):
    """delay by one timestep"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, quantum):
            super().__init__(name)
            
            self.quantum = quantum
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if len(self.felementsin[i].input.data) > 0:
                    self.felementsout[i].setvalue(self.quantum*np.round(self.felementsin[i].input.data[-1]/self.quantum))
                else:
                    self.felementsout[i].setvalue(0)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name, quantum)

def ADtime(name, delta):
    """analog to digital time discretization"""
    
    class UpdateCE(deltaTau.UpdateCE):
    
        def __init__(self, name, delta):
            super().__init__(name)
            
            self.delta = delta
            self.dtime = -1
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if delta > self.delta:
                    raise ZeroDivisionError("Error: Simulation delta bigger than {self.name} delta")
                if time > self.dtime + self.delta:
                    if self.dtime == -1:
                        self.dtime = 0
                    self.dtime = self.dtime + self.delta
                    self.felementsout[i].setvalue(self.felementsin[i].input.data[-1])
                else:
                    self.felementsout[i].setvalue(self.felementsout[i].output.data[-1])
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]   
            
    return UpdateCE(name, delta)

def movingmean(name, period):
    """this means the over a set period the inputs"""
    
    class Function(deltaTau.FunctionBlock):
        """function object"""
        
        def __init__(self, inputlen, outputlen, source, name, period):
            """saves the inputs over the period in a list and then means them"""
            
            super().__init__(inputlen, outputlen, source, name)
            self.period = period
            
        def function(self):
            """returns the mean value"""
            
            return[np.mean(np.array(self.inputs[0])[np.array(self.time) > self.time[-1] - self.period])]
            
        # def setinputs(self, inputs, time_, delta):
        #     """calculates the mean over the period"""
            
        #     self.N = int(self.period/delta)
        #     while not len(self.inputovertime) == self.N:
        #         if len(self.inputovertime) > self.N:
        #             self.inputovertime.pop(0)
        #         else:
        #             self.inputovertime = [0]*(self.N - len(self.inputovertime)) + self.inputovertime
        #         self.mean = np.mean(self.inputovertime)
        #     self.mean = self.mean + (inputs[0] - self.inputovertime[0])/self.N
        #     self.inputovertime.pop(0)
        #     self.inputovertime.append(inputs[0])
            
    return Function(1, 1, False, name, period)

#periodicaverage###############################################################

periodicaverageargs = [[0.1],
                       ["Average time"],
                       ["s"],
                       [float],
                       [bl.PropertyElement.biggerzero],
                       ["Timespan of to average in s"]]

def periodicaverage(name, timedelta):
    """analog to digital time discretization"""
    
    class UpdateCE(deltaTau.UpdateCE):
    
        def __init__(self, name, timedelta):
            super().__init__(name)
            
            self.timedelta = timedelta
            self.starttime = 0
            self.endtime = timedelta
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 0)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            if time > self.endtime:
                for i, _ in enumerate(self.felementsin):
                    average = 0
                    for j, d in enumerate(reversed(self.felementsin[i].input.data)):
                        if j == 0:
                            average = average + d*(self.endtime - self.felementsin[i].input.t[-j-1])
                        elif self.felementsin[i].input.t[-j-1] <= self.starttime:
                            average = average + d*(self.felementsin[i].input.t[-j] - self.starttime)
                            break
                        else:
                            average = average + d*(self.felementsin[i].input.t[-j] - self.felementsin[i].input.t[-j-1])
                    average = average/self.timedelta
                    self.felementsout[i].setvalue(average)
                self.starttime = self.endtime
                self.endtime = self.endtime + self.timedelta
            if time < self.starttime:
                self.endtime = self.starttime
                self.starttime = self.starttime - self.timedelta
                for i, _ in enumerate(self.felementsin):
                    self.felementsout[i].setvalue(self.felementsout[i].output.data[-1])
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]   
            
    return UpdateCE(name, timedelta)

#arrayin#######################################################################

arrayinargs = [[], [], [], [], [], []]

def arrayin(name, numberinputs):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.gains = []
            # for _ in range(numberinputs):
            #     self.gains.append(deltaTau.FunctionELement(name, "G", 1))
            self.outputs = [deltaTau.FunctionArray(self)]
        
        # def getFE(self):
        #     """returns the FElement"""
            
        #     return []
        
        def setarray(self):
            """sets the format"""
            
            inputs = self.inputs.copy()
            inputs.reverse()
            self.outputs[0].setarray(inputs)
            for e in self.outputs[0].outputs:
                e.setarray()
        
        # def setinputs(self, inputs):
        #     """sets the inputs"""
            
        #     for i, g in enumerate(self.gains):
        #         g.setinput(inputs[i])
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            return 0
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#arrayout######################################################################

arrayoutargs = [[], [], [], [], [], []]

def arrayout(name, numberoutputs):
    """resolves the array into multiple vectors"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, numberoutputs):
            super().__init__(name)
            
            self.gains = [] 
            self.outputs = []
            for i in range(numberoutputs):
                self.outputs.append(deltaTau.FunctionArray(self))
        
        
        def setarray(self):
            """sets the format"""
            
            if not len(self.outputs) == len(self.inputs[0].signals):
                self.failed = False
                raise TypeError("TypeError: Arrays do not match")
            for i, o in enumerate(self.outputs):
                if type(self.inputs[0].signals[i]) == deltaTau.FunctionSignal:
                    o.setarray([self.inputs[0].signals[i]])
                else:
                    o.setarray(self.inputs[0].signals[i].signals)
                for e in o.outputs:
                    e.setarray()
            # self.outputs[0].setarray(self.inputs)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            return 0
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.inputs[0].getdata(), self.inputs[0].time()]]
        
    return UpdateCE(name, numberoutputs)

#sawtoothPWM###################################################################

sawtoothPWMargs = [[["natural", "regular"], 100, [1, -1], [1, -1], 0],
                   ["sampling", "switching frequency", "carrier-limits", "output-values", "offset"],
                   ["", "Hz", "", "", "p.u."],
                   [str, float, list, list, float],
                   [bl.PropertyElement.noconstrains, bl.PropertyElement.biggerzero, bl.PropertyElement.listoftwofloats, bl.PropertyElement.listoftwofloats, bl.PropertyElement.noconstrains],
                   ["sampling type", "switching frequency in Hz", "values of carrier", "output values", "offset of carrier in p.u."]]

def sawtoothPWM(name, sampling, frequency, carrierlimits, outputvalues, offset):
    """generates a symetrical PWM """
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, sampling, frequency, carrierlimits, outputvalues, offset):
            super().__init__(name)
            
            self.frequency = frequency
            self.sampling = sampling
            self.carrierbottomlimit = carrierlimits[1]
            self.outputtopvalue = max(outputvalues)
            self.outputbottomvalue = min(outputvalues)
            self.grade = (carrierlimits[0] - carrierlimits[1])*frequency
            self.offset = offset*1/self.frequency
            while self.offset < 0:
                self.offset = self.offset + 1/frequency
            if carrierlimits[0] < carrierlimits[1]:
                self.grade = -self.grade
                self.offset = self.offset + 1/2/self.frequency
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.basesavetime = deltaTau.FunctionELement(name, "Q", 0)
            self.basesavestate = deltaTau.FunctionELement(name, "Q", 0)
            self.felementsin = []
            self.felementsout = []
            self.felementsstates = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout + self.felementsstates + [self.basesavetime]
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
                self.felementsstates.append(self.basesavestate.copy())
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            value = self.grade*(self.offset + time) - self.grade/self.frequency*int((time + self.offset)*self.frequency) + self.carrierbottomlimit
            if len(self.basesavetime.output.data) > 0:
                if self.sampling == "regular":
                    if int((time + self.offset)*self.frequency)/self.frequency > self.basesavetime.output.data[-1]:
                        self.basesavetime.setvalue(int((time + self.offset)*self.frequency)/self.frequency)
                        for i, e in enumerate(self.felementsstates):
                            e.setvalue(self.felementsin[i].input.data[-1])
            for i, _ in enumerate(self.felementsin):
                samplingvalue = self.felementsin[i].input.data[-1]
                if not self.sampling == "natural":
                    samplingvalue = self.felementsstates[i].value
                if samplingvalue < value:
                    self.felementsout[i].setvalue(self.outputbottomvalue)
                else:
                    self.felementsout[i].setvalue(self.outputtopvalue)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, sampling, frequency, carrierlimits, outputvalues, offset)

#symetricalPWM#################################################################

symetricalPWMargs = [[["natural", "single high", "single low", "double"], 100, [1, -1], [1, -1], 0],
                     ["sampling", "switching frequency", "carrier-limits", "output-values", "offset"],
                     ["", "Hz", "", "", "p.u."],
                     [str, float, list, list, float],
                     [bl.PropertyElement.noconstrains, bl.PropertyElement.biggerzero, bl.PropertyElement.listoftwofloats, bl.PropertyElement.listoftwofloats, bl.PropertyElement.noconstrains],
                     ["sampling type", "switching frequency in Hz", "values of carrier", "output values", "offset of carrier in p.u."]]

def symetricalPWM(name, sampling, frequency, carrierlimits, outputvalues, offset):
    """generates a symetrical PWM """
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name, sampling, frequency, carrierlimits, outputvalues, offset):
            super().__init__(name)
            
            self.frequency = frequency
            self.sampling = sampling
            self.carriertoplimit = max(carrierlimits)
            self.carrierbottomlimit = min(carrierlimits)
            self.outputtopvalue = max(outputvalues)
            self.outputbottomvalue = min(outputvalues)
            self.grade = 2*(self.carriertoplimit - self.carrierbottomlimit)*frequency
            self.offset = offset*1/self.frequency
            while self.offset < 0:
                self.offset = self.offset + 1/frequency
            if carrierlimits[0] < carrierlimits[1]:
                self.grade = -self.grade
                self.offset = self.offset + 1/2/self.frequency
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.basesavetime = deltaTau.FunctionELement(name, "Q", 0)
            self.basesavestate = deltaTau.FunctionELement(name, "Q", 0)
            self.felementsin = []
            self.felementsout = []
            self.felementsstates = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout + self.felementsstates + [self.basesavetime]
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
                self.felementsstates.append(self.basesavestate.copy())
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            value = self.grade*(self.offset + time) - self.grade/self.frequency*int((time + self.offset)*self.frequency) + self.carrierbottomlimit
            if value > self.carriertoplimit or value < self.carrierbottomlimit:
                value = -self.grade*(self.offset + time - 1/2/self.frequency) + self.grade/self.frequency*int((time + self.offset - 1/2/self.frequency)*self.frequency) + self.carriertoplimit
            if len(self.basesavetime.output.data) > 0:
                if self.sampling == "single high":
                    samplingoffset = 0.5/self.frequency
                    if self.offset > samplingoffset:
                        samplingoffset = -samplingoffset
                    if int((time + self.offset+ samplingoffset)*self.frequency)/self.frequency > self.basesavetime.output.data[-1]:
                        self.basesavetime.setvalue(int((time + self.offset +samplingoffset)*self.frequency)/self.frequency)
                        for i, e in enumerate(self.felementsstates):
                            e.setvalue(self.felementsin[i].input.data[-1])
                elif self.sampling == "double":
                    if int((time + self.offset)*2*self.frequency)/2/self.frequency > self.basesavetime.output.data[-1]:
                        self.basesavetime.setvalue(int((time + self.offset)*2*self.frequency)/2/self.frequency)
                        for i, e in enumerate(self.felementsstates):
                            e.setvalue(self.felementsin[i].input.data[-1])
                else:
                    if int((time + self.offset)*self.frequency)/self.frequency > self.basesavetime.output.data[-1]:
                        self.basesavetime.setvalue(int((time + self.offset)*self.frequency)/self.frequency)
                        for i, e in enumerate(self.felementsstates):
                            e.setvalue(self.felementsin[i].input.data[-1])
            for i, _ in enumerate(self.felementsin):
                samplingvalue = self.felementsin[i].input.data[-1]
                if not self.sampling == "natural":
                    samplingvalue = self.felementsstates[i].value
                if samplingvalue < value:
                    self.felementsout[i].setvalue(self.outputbottomvalue)
                else:
                    self.felementsout[i].setvalue(self.outputtopvalue)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]

    return UpdateCE(name, sampling, frequency, carrierlimits, outputvalues, offset)

#alphabetaarray################################################################

alphabetaarrayargs = [[], [], [], [], [], []]

def alphabetaarray(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            ab = list(2/3*np.array([1, -1/2, -1/2, 0, np.sqrt(3)/2, -np.sqrt(3)/2]))
            self.basegains = []
            for a in ab:
                self.basegains.append(deltaTau.FunctionELement(name, "G", a))
            self.baseada = deltaTau.FunctionELement(name, "A", [self.basegains[1].getoutput(), self.basegains[2].getoutput()])
            self.baseadb = deltaTau.FunctionELement(name, "A", [self.basegains[4].getoutput(), self.basegains[5].getoutput()])
            self.baseada.setinput(self.basegains[0].getoutput())
            self.baseadb.setinput(self.basegains[3].getoutput())
            self.base = self.basegains + [self.baseada, self.baseadb] 
            self.felements = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
           
            signals = self.inputs[0].copysignals()
            if len(signals) > 1:
                signals = signals[0:2]
            self.outputs[0].setarray(signals)
        
        def connect(self):
            """sets the outsignals"""
        
            if len(self.inputs[0].signals) == 3:
                inputs = self.inputs[0].getsignals()
                outputs = self.outputs[0].getsignals()
                if len(inputs) == len(outputs)/2*3:
                    for i in range(0, int(len(inputs)/3)):
                        model = copy.deepcopy(self.base)
                        model[0].setinput(inputs[i])
                        model[3].setinput(inputs[i])
                        model[1].setinput(inputs[i + int(len(inputs)/3)])
                        model[4].setinput(inputs[i + int(len(inputs)/3)])
                        model[2].setinput(inputs[i + int(len(inputs)/3*2)])
                        model[5].setinput(inputs[i + int(len(inputs)/3*2)])
                        model[6].setoutput(outputs[i])
                        model[7].setoutput(outputs[i + int(len(inputs)/3)])
                        self.felements = self.felements + model
                else:
                    raise TypeError("TypeError: input is of wrong size")
            else:
                raise TypeError("TypeError: input is of wrong size")
            
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#alphabetasingle###############################################################

alphabetasingleargs = [[], [], [], [], [], []]

def alphabetasingle(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            ab = list(2/3*np.array([1, -1/2, -1/2, 0, np.sqrt(3)/2, -np.sqrt(3)/2]))
            self.basegains = []
            for a in ab:
                self.basegains.append(deltaTau.FunctionELement(name, "G", a))
            self.baseada = deltaTau.FunctionELement(name, "A", [self.basegains[1].getoutput(), self.basegains[2].getoutput()])
            self.baseadb = deltaTau.FunctionELement(name, "A", [self.basegains[4].getoutput(), self.basegains[5].getoutput()])
            self.baseada.setinput(self.basegains[0].getoutput())
            self.baseadb.setinput(self.basegains[3].getoutput())
            self.base = self.basegains + [self.baseada, self.baseadb] 
            self.felements = []
            self.outputs = [deltaTau.FunctionArray(self), deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
            self.outputs[1].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputa = self.inputs[2].getsignals()
            inputb = self.inputs[1].getsignals()
            inputc = self.inputs[0].getsignals()
            outputa = self.outputs[0].getsignals()
            outputb = self.outputs[1].getsignals()
            if len(inputa) == len(inputb) and len(inputa) == len(inputc) and len(inputa) == len(outputa) and len(inputa) == len(outputb):
                for i, _ in enumerate(inputa):
                    model = copy.deepcopy(self.base)
                    model[0].setinput(inputa[i])
                    model[3].setinput(inputa[i])
                    model[1].setinput(inputb[i])
                    model[4].setinput(inputb[i])
                    model[2].setinput(inputc[i])
                    model[5].setinput(inputc[i])
                    model[6].setoutput(outputa[i])
                    model[7].setoutput(outputb[i])
                    self.felements = self.felements + model
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".α", self.outputs[0].getdata(), self.outputs[0].time()], [self.name+".β", self.outputs[1].getdata(), self.outputs[1].time()]]
        
    return UpdateCE(name)

#alphabetareversearray#########################################################

alphabetareversearrayargs = [[], [], [], [], [], []]

def alphabetareversearray(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            abc= list(np.array([1, 0, -1/2, np.sqrt(3)/2, -1/2, -np.sqrt(3)/2]))
            self.basegains = []
            for a in abc:
                self.basegains.append(deltaTau.FunctionELement(name, "G", a))
            self.baseada = deltaTau.FunctionELement(name, "A", [self.basegains[1].getoutput()])
            self.baseadb = deltaTau.FunctionELement(name, "A", [self.basegains[3].getoutput()])
            self.baseadc = deltaTau.FunctionELement(name, "A", [self.basegains[5].getoutput()])
            self.baseada.setinput(self.basegains[0].getoutput())
            self.baseadb.setinput(self.basegains[2].getoutput())
            self.baseadc.setinput(self.basegains[4].getoutput())
            self.base = self.basegains + [self.baseada, self.baseadb, self.baseadc] 
            self.felements = []
            self.outputs = [deltaTau.FunctionArray(self), deltaTau.FunctionArray(self), deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
           
            signals = self.inputs[0].copysignals()
            if len(signals) == 2:
                signals.append(signals[0].copy())
            self.outputs[0].setarray(signals)
        
        def connect(self):
            """sets the outsignals"""
            
            if len(self.inputs[0].signals) == 2:
                inputs = self.inputs[0].getsignals()
                outputs = self.outputs[0].getsignals()
                if len(inputs) == len(outputs)/3*2:
                    for i in range(0, int(len(inputs)/2)):
                        model = copy.deepcopy(self.base)
                        model[0].setinput(inputs[i])
                        model[2].setinput(inputs[i])
                        model[4].setinput(inputs[i])
                        model[1].setinput(inputs[i + int(len(inputs)/2)])
                        model[3].setinput(inputs[i + int(len(inputs)/2)])
                        model[5].setinput(inputs[i + int(len(inputs)/2)])
                        model[6].setoutput(outputs[i])
                        model[7].setoutput(outputs[i + int(len(inputs)/2)])
                        model[8].setoutput(outputs[i + int(len(inputs))])
                        self.felements = self.felements + model
                else:
                    raise TypeError("TypeError: input is of wrong size")
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#alphabetareversesingle########################################################

alphabetareversesingleargs = [[], [], [], [], [], []]

def alphabetareversesingle(name):
    """concats the inputvectors to an array"""
        
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            abc= list(np.array([1, 0, -1/2, np.sqrt(3)/2, -1/2, -np.sqrt(3)/2]))
            self.basegains = []
            for a in abc:
                self.basegains.append(deltaTau.FunctionELement(name, "G", a))
            self.baseada = deltaTau.FunctionELement(name, "A", [self.basegains[1].getoutput()])
            self.baseadb = deltaTau.FunctionELement(name, "A", [self.basegains[3].getoutput()])
            self.baseadc = deltaTau.FunctionELement(name, "A", [self.basegains[5].getoutput()])
            self.baseada.setinput(self.basegains[0].getoutput())
            self.baseadb.setinput(self.basegains[2].getoutput())
            self.baseadc.setinput(self.basegains[4].getoutput())
            self.base = self.basegains + [self.baseada, self.baseadb, self.baseadc] 
            self.felements = []
            self.outputs = [deltaTau.FunctionArray(self), deltaTau.FunctionArray(self), deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
            self.outputs[1].setarray(self.inputs[0].copysignals())
            self.outputs[2].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputa = self.inputs[1].getsignals()
            inputb = self.inputs[0].getsignals()
            outputa = self.outputs[0].getsignals()
            outputb = self.outputs[1].getsignals()
            outputc = self.outputs[2].getsignals()
            if len(inputa) == len(inputb) and len(inputa) == len(outputa) and len(inputa) == len(outputb) and len(inputa) == len(outputc):
                for i, _ in enumerate(inputa):
                    model = copy.deepcopy(self.base)
                    model[0].setinput(inputa[i])
                    model[2].setinput(inputa[i])
                    model[4].setinput(inputa[i])
                    model[1].setinput(inputb[i])
                    model[3].setinput(inputb[i])
                    model[5].setinput(inputb[i])
                    model[6].setoutput(outputa[i])
                    model[7].setoutput(outputb[i])
                    model[8].setoutput(outputc[i])
                    self.felements = self.felements + model
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".a", self.outputs[0].getdata(), self.outputs[0].time()], [self.name+".b", self.outputs[1].getdata(), self.outputs[1].time()], [self.name+".c", self.outputs[2].getdata(), self.outputs[2].time()]]
    
    return UpdateCE(name)

#dqarray#######################################################################

dqarrayargs = [[], [], [], [], [], []]

def dqarray(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.ain = deltaTau.FunctionELement(name, "D", 0,)
            self.bin = deltaTau.FunctionELement(name, "D", 0,)
            self.cin = deltaTau.FunctionELement(name, "D", 0,)
            self.phiin = deltaTau.FunctionELement(name, "D", 0,)
            self.aout = deltaTau.FunctionELement(name, "Q", 0)
            self.bout = deltaTau.FunctionELement(name, "Q", 0)
            self.base = [self.ain, self.bin, self.cin, self.phiin, self.aout, self.bout]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            if len(self.inputs[0]) > len(self.inputs[1]):
                signals = self.inputs[0].copysignals()
                if len(signals) > 1:
                    signals = signals[0:2]
                self.outputs[0].setarray(signals)
            else:
                d = deltaTau.FunctionArray(self)
                q = deltaTau.FunctionArray(self)
                d.setarray(self.inputs[1].copysignals())
                q.setarray(self.inputs[1].copysignals())
                self.outputs[0].setarray([d, q])
        
        def connect(self):
            """sets the outsignals"""
        
            if len(self.inputs[0].signals) == 3:
                inputs = self.inputs[1].getsignals()
                outputs = self.outputs[0].getsignals()
                inputphi = self.inputs[0].getsignals()
                if len(inputs) == len(outputs)/2*3:
                    if len(inputs) > 3:
                        for i in range(0, int(len(inputs)/3)):
                            model = copy.deepcopy(self.base)
                            model[0].setinput(inputs[i])
                            model[1].setinput(inputs[i + int(len(inputs)/3)])
                            model[2].setinput(inputs[i + int(len(inputs)/3*2)])
                            if len(inputphi) == len(inputs)/3:
                                model[3].setinput(inputphi[i])
                            elif len(inputphi) == 1:
                                model[3].setinput(inputphi[0])
                            else:
                                raise TypeError("TypeError: input is of wrong size")
                            model[4].setoutput(outputs[i])
                            model[5].setoutput(outputs[i + int(len(inputs)/3)])
                            self.felements = self.felements + model
                    elif len(inputs) == 3:
                        for i, _ in enumerate(inputphi):
                            model = copy.deepcopy(self.base)
                            model[0].setinput(inputs[0])
                            model[1].setinput(inputs[1])
                            model[2].setinput(inputs[2])
                            model[3].setinput(inputphi[i])
                            model[4].setoutput(outputs[i])
                            model[5].setoutput(outputs[i + len(inputphi)])
                            self.felements = self.felements + model
                            self.models.append(model)
                    else:
                        raise TypeError("TypeError: input is of wrong size")
                else:
                    raise TypeError("TypeError: input is of wrong size")
            else:
                raise TypeError("TypeError: input is of wrong size")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                model[4].setvalue(2/3*(m.cos(model[3].input.data[-1])*model[0].input.data[-1] + m.cos(model[3].input.data[-1] + 2/3*m.pi)*model[1].input.data[-1] + m.cos(model[3].input.data[-1] - 2/3*m.pi)*model[2].input.data[-1]))
                model[5].setvalue(2/3*(m.sin(model[3].input.data[-1])*model[0].input.data[-1] + m.sin(model[3].input.data[-1] + 2/3*m.pi)*model[1].input.data[-1] + m.sin(model[3].input.data[-1] - 2/3*m.pi)*model[2].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#dqsingle######################################################################

dqsingleargs = [[], [], [], [], [], []]

def dqsingle(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.ain = deltaTau.FunctionELement(name, "D", 0,)
            self.bin = deltaTau.FunctionELement(name, "D", 0,)
            self.cin = deltaTau.FunctionELement(name, "D", 0,)
            self.phiin = deltaTau.FunctionELement(name, "D", 0,)
            self.aout = deltaTau.FunctionELement(name, "Q", 0)
            self.bout = deltaTau.FunctionELement(name, "Q", 0)
            self.base = [self.ain, self.bin, self.cin, self.phiin, self.aout, self.bout]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self), deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            if len(self.inputs[0]) > len(self.inputs[3]):
                self.outputs[0].setarray(self.inputs[0].copysignals())
                self.outputs[1].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[3].copysignals())
                self.outputs[1].setarray(self.inputs[3].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputa = self.inputs[3].getsignals()
            inputb = self.inputs[2].getsignals()
            inputc = self.inputs[1].getsignals()
            inputphi = self.inputs[0].getsignals()
            outputa = self.outputs[0].getsignals()
            outputb = self.outputs[1].getsignals()
            if len(inputa) > 1:
                if len(inputa) == len(inputb) and len(inputa) == len(inputc) and len(inputa) == len(outputa) and len(inputa) == len(outputb):
                    for i, _ in enumerate(inputa):
                        model = copy.deepcopy(self.base)
                        model[0].setinput(inputa[i])
                        model[1].setinput(inputb[i])
                        model[2].setinput(inputc[i])
                        if len(inputphi) == len(inputa):
                            model[3].setinput(inputphi[i])
                        elif len(inputphi) == 1:
                            model[3].setinput(inputphi[0])
                        else:
                            raise TypeError("TypeError: input is of wrong size")
                        model[4].setoutput(outputa[i])
                        model[5].setoutput(outputb[i])
                        self.felements = self.felements + model
                        self.models.append(model)
                else:
                    raise TypeError("TypeError: input is of wrong size")
            elif len(inputa) == 1:
                if len(inputa) == len(inputb) and len(inputa) == len(inputc) and len(inputphi) == len(outputa) and len(inputphi) == len(outputb):
                    for i, _ in enumerate(inputphi):
                        model = copy.deepcopy(self.base)
                        model[0].setinput(inputa[0])
                        model[1].setinput(inputb[0])
                        model[2].setinput(inputc[0])
                        model[3].setinput(inputphi[i])
                        model[4].setoutput(outputa[i])
                        model[5].setoutput(outputb[i])
                        self.felements = self.felements + model
                        self.models.append(model)
                else:
                    raise TypeError("TypeError: input is of wrong size")
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                model[4].setvalue(2/3*(m.cos(model[3].input.data[-1])*model[0].input.data[-1] + m.cos(model[3].input.data[-1] + 2/3*m.pi)*model[1].input.data[-1] + m.cos(model[3].input.data[-1] - 2/3*m.pi)*model[2].input.data[-1]))
                model[5].setvalue(2/3*(m.sin(model[3].input.data[-1])*model[0].input.data[-1] + m.sin(model[3].input.data[-1] + 2/3*m.pi)*model[1].input.data[-1] + m.sin(model[3].input.data[-1] - 2/3*m.pi)*model[2].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".d", self.outputs[0].getdata(), self.outputs[0].time()], [self.name+".q", self.outputs[1].getdata(), self.outputs[1].time()]]
        
    return UpdateCE(name)

#dqreversearray################################################################

dqreversearrayargs = [[], [], [], [], [], []]

def dqreversearray(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.ain = deltaTau.FunctionELement(name, "D", 0,)
            self.bin = deltaTau.FunctionELement(name, "D", 0,)
            self.phiin = deltaTau.FunctionELement(name, "D", 0,)
            self.aout = deltaTau.FunctionELement(name, "Q", 0)
            self.bout = deltaTau.FunctionELement(name, "Q", 0)
            self.cout = deltaTau.FunctionELement(name, "Q", 0)
            self.base = [self.ain, self.bin, self.phiin, self.aout, self.bout, self.cout]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            if len(self.inputs[0]) > len(self.inputs[1]):
                signals = self.inputs[0].copysignals()
                if len(signals) == 2:
                    signals.append(signals[0].copy())
                self.outputs[0].setarray(signals)
            else:
                a = deltaTau.FunctionArray(self)
                b = deltaTau.FunctionArray(self)
                c = deltaTau.FunctionArray(self)
                a.setarray(self.inputs[1].copysignals())
                b.setarray(self.inputs[1].copysignals())
                c.setarray(self.inputs[1].copysignals())
                self.outputs[0].setarray([a, b, c])
        
        def connect(self):
            """sets the outsignals"""
        
            if len(self.inputs[0].signals) == 2:
                inputs = self.inputs[1].getsignals()
                outputs = self.outputs[0].getsignals()
                inputphi = self.inputs[0].getsignals()
                if len(inputs) == len(outputs)/3*2:
                    if len(inputs) > 2:
                        for i in range(0, int(len(inputs)/3)):
                            model = copy.deepcopy(self.base)
                            model[0].setinput(inputs[i])
                            model[1].setinput(inputs[i + int(len(inputs)/2)])
                            if len(inputphi) == len(inputs)/2:
                                model[2].setinput(inputphi[i])
                            elif len(inputphi) == 1:
                                model[2].setinput(inputphi[0])
                            else:
                                raise TypeError("TypeError: input is of wrong size")
                            model[3].setoutput(outputs[i])
                            model[4].setoutput(outputs[i + int(len(inputs)/2)])
                            model[5].setoutput(outputs[i + int(len(inputs))])
                            self.felements = self.felements + model
                    elif len(inputs) == 2:
                        for i, _ in enumerate(inputphi):
                            model = copy.deepcopy(self.base)
                            model[0].setinput(inputs[0])
                            model[1].setinput(inputs[1])
                            model[2].setinput(inputphi[i])
                            model[3].setoutput(outputs[i])
                            model[4].setoutput(outputs[i + len(inputphi)])
                            model[5].setoutput(outputs[i + 2*len(inputphi)])
                            self.felements = self.felements + model
                            self.models.append(model)
                    else:
                        raise TypeError("TypeError: input is of wrong size")
                else:
                    raise TypeError("TypeError: input is of wrong size")
            else:
                raise TypeError("TypeError: input is of wrong size")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                model[3].setvalue((m.cos(model[2].input.data[-1])*model[0].input.data[-1] + m.sin(model[2].input.data[-1])*model[1].input.data[-1]))
                model[4].setvalue((m.cos(model[2].input.data[-1] + 2/3*m.pi)*model[0].input.data[-1] + m.sin(model[2].input.data[-1] + 2/3*m.pi)*model[1].input.data[-1]))
                model[5].setvalue((m.cos(model[2].input.data[-1] - 2/3*m.pi)*model[0].input.data[-1] + m.sin(model[2].input.data[-1] - 2/3*m.pi)*model[1].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#dqreversesingle###############################################################

dqreversesingleargs = [[], [], [], [], [], []]

def dqreversesingle(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.ain = deltaTau.FunctionELement(name, "D", 0,)
            self.bin = deltaTau.FunctionELement(name, "D", 0,)
            self.phiin = deltaTau.FunctionELement(name, "D", 0,)
            self.aout = deltaTau.FunctionELement(name, "Q", 0)
            self.bout = deltaTau.FunctionELement(name, "Q", 0)
            self.cout = deltaTau.FunctionELement(name, "Q", 0)
            self.base = [self.ain, self.bin, self.phiin, self.aout, self.bout, self.cout]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self), deltaTau.FunctionArray(self), deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            if len(self.inputs[0]) > len(self.inputs[2]):
                self.outputs[0].setarray(self.inputs[0].copysignals())
                self.outputs[1].setarray(self.inputs[0].copysignals())
                self.outputs[2].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[2].copysignals())
                self.outputs[1].setarray(self.inputs[2].copysignals())
                self.outputs[2].setarray(self.inputs[2].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputa = self.inputs[2].getsignals()
            inputb = self.inputs[1].getsignals()
            inputphi = self.inputs[0].getsignals()
            outputa = self.outputs[0].getsignals()
            outputb = self.outputs[1].getsignals()
            outputc = self.outputs[2].getsignals()
            if len(inputa) > 1:
                if len(inputa) == len(inputb) and len(inputa) == len(outputa) and len(inputa) == len(outputb) and len(inputa) == len(outputc):
                    for i, _ in enumerate(inputa):
                        model = copy.deepcopy(self.base)
                        model[0].setinput(inputa[i])
                        model[1].setinput(inputb[i])
                        if len(inputphi) == len(inputa):
                            model[2].setinput(inputphi[i])
                        elif len(inputphi) == 1:
                            model[2].setinput(inputphi[0])
                        else:
                            raise TypeError("TypeError: input is of wrong size")
                        model[3].setoutput(outputa[i])
                        model[4].setoutput(outputb[i])
                        model[5].setoutput(outputc[i])
                        self.felements = self.felements + model
                        self.models.append(model)
                else:
                    raise TypeError("TypeError: input is of wrong size")
            elif len(inputa) == 1:
                if len(inputa) == len(inputb) and len(inputphi) == len(outputa) and len(inputphi) == len(outputb) and len(inputa) == len(outputc):
                    for i, _ in enumerate(inputphi):
                        model = copy.deepcopy(self.base)
                        model[0].setinput(inputa[0])
                        model[1].setinput(inputb[0])
                        model[2].setinput(inputphi[i])
                        model[3].setoutput(outputa[i])
                        model[4].setoutput(outputb[i])
                        model[5].setoutput(outputc[i])
                        self.felements = self.felements + model
                        self.models.append(model)
                else:
                    raise TypeError("TypeError: input is of wrong size")
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                model[3].setvalue((m.cos(model[2].input.data[-1])*model[0].input.data[-1] + m.sin(model[2].input.data[-1])*model[1].input.data[-1]))
                model[4].setvalue((m.cos(model[2].input.data[-1] + 2/3*m.pi)*model[0].input.data[-1] + m.sin(model[2].input.data[-1] + 2/3*m.pi)*model[1].input.data[-1]))
                model[5].setvalue((m.cos(model[2].input.data[-1] - 2/3*m.pi)*model[0].input.data[-1] + m.sin(model[2].input.data[-1] - 2/3*m.pi)*model[1].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".a", self.outputs[0].getdata(), self.outputs[0].time()], [self.name+".b", self.outputs[1].getdata(), self.outputs[1].time()], [self.name+".c", self.outputs[2].getdata(), self.outputs[2].time()]]
        
    return UpdateCE(name)

#alphabetatodqarray############################################################

alphabetatodqarrayargs = [[], [], [], [], [], []]

def alphabetatodqarray(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.ain = deltaTau.FunctionELement(name, "D", 0,)
            self.bin = deltaTau.FunctionELement(name, "D", 0,)
            self.phiin = deltaTau.FunctionELement(name, "D", 0,)
            self.aout = deltaTau.FunctionELement(name, "Q", 0)
            self.bout = deltaTau.FunctionELement(name, "Q", 0)
            self.base = [self.ain, self.bin, self.phiin, self.aout, self.bout]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            if len(self.inputs[0]) > len(self.inputs[1]):
                signals = self.inputs[0].copysignals()
                if len(signals) > 1:
                    signals = signals[0:2]
                self.outputs[0].setarray(signals)
            else:
                d = deltaTau.FunctionArray(self)
                q = deltaTau.FunctionArray(self)
                d.setarray(self.inputs[1].copysignals())
                q.setarray(self.inputs[1].copysignals())
                self.outputs[0].setarray([d, q])
        
        def connect(self):
            """sets the outsignals"""
        
            if len(self.inputs[0].signals) == 2:
                inputs = self.inputs[1].getsignals()
                outputs = self.outputs[0].getsignals()
                inputphi = self.inputs[0].getsignals()
                if len(inputs) == len(outputs):
                    if len(inputs) > 2:
                        for i in range(0, int(len(inputs)/2)):
                            model = copy.deepcopy(self.base)
                            model[0].setinput(inputs[i])
                            model[1].setinput(inputs[i + int(len(inputs)/2)])
                            if len(inputphi) == len(inputs)/2:
                                model[2].setinput(inputphi[i])
                            elif len(inputphi) == 1:
                                model[2].setinput(inputphi[0])
                            else:
                                raise TypeError("TypeError: input is of wrong size")
                            model[3].setoutput(outputs[i])
                            model[4].setoutput(outputs[i + int(len(inputs)/2)])
                            self.felements = self.felements + model
                    elif len(inputs) == 2:
                        for i, _ in enumerate(inputphi):
                            model = copy.deepcopy(self.base)
                            model[0].setinput(inputs[0])
                            model[1].setinput(inputs[1])
                            model[2].setinput(inputphi[i])
                            model[3].setoutput(outputs[i])
                            model[4].setoutput(outputs[i + len(inputphi)])
                            self.felements = self.felements + model
                            self.models.append(model)
                    else:
                        raise TypeError("TypeError: input is of wrong size")
                else:
                    raise TypeError("TypeError: input is of wrong size")
            else:
                raise TypeError("TypeError: input is of wrong size")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                model[3].setvalue((m.cos(model[2].input.data[-1])*model[0].input.data[-1] - m.sin(model[2].input.data[-1])*model[1].input.data[-1]))
                model[4].setvalue((m.sin(model[2].input.data[-1])*model[0].input.data[-1] + m.cos(model[2].input.data[-1])*model[1].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#alphabetatodqsingle###########################################################

alphabetatodqarrayargs = [[], [], [], [], [], []]

def alphabetatodqsingle(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.ain = deltaTau.FunctionELement(name, "D", 0,)
            self.bin = deltaTau.FunctionELement(name, "D", 0,)
            self.phiin = deltaTau.FunctionELement(name, "D", 0,)
            self.aout = deltaTau.FunctionELement(name, "Q", 0)
            self.bout = deltaTau.FunctionELement(name, "Q", 0)
            self.base = [self.ain, self.bin, self.phiin, self.aout, self.bout]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self), deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            if len(self.inputs[0]) > len(self.inputs[2]):
                self.outputs[0].setarray(self.inputs[0].copysignals())
                self.outputs[1].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[2].copysignals())
                self.outputs[1].setarray(self.inputs[2].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputa = self.inputs[2].getsignals()
            inputb = self.inputs[1].getsignals()
            inputphi = self.inputs[0].getsignals()
            outputa = self.outputs[0].getsignals()
            outputb = self.outputs[1].getsignals()
            if len(inputa) > 1:
                if len(inputa) == len(inputb) and len(inputa) == len(outputa) and len(inputa) == len(outputb):
                    for i, _ in enumerate(inputa):
                        model = copy.deepcopy(self.base)
                        model[0].setinput(inputa[i])
                        model[1].setinput(inputb[i])
                        if len(inputphi) == len(inputa):
                            model[2].setinput(inputphi[i])
                        elif len(inputphi) == 1:
                            model[2].setinput(inputphi[0])
                        else:
                            raise TypeError("TypeError: input is of wrong size")
                        model[3].setoutput(outputa[i])
                        model[4].setoutput(outputb[i])
                        self.felements = self.felements + model
                        self.models.append(model)
                else:
                    raise TypeError("TypeError: input is of wrong size")
            elif len(inputa) == 1:
                if len(inputa) == len(inputb) and len(inputphi) == len(outputa) and len(inputphi) == len(outputb):
                    for i, _ in enumerate(inputphi):
                        model = copy.deepcopy(self.base)
                        model[0].setinput(inputa[0])
                        model[1].setinput(inputb[0])
                        model[2].setinput(inputphi[i])
                        model[3].setoutput(outputa[i])
                        model[4].setoutput(outputb[i])
                        self.felements = self.felements + model
                        self.models.append(model)
                else:
                    raise TypeError("TypeError: input is of wrong size")
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                model[3].setvalue((m.cos(model[2].input.data[-1])*model[0].input.data[-1] - m.sin(model[2].input.data[-1])*model[1].input.data[-1]))
                model[4].setvalue((m.sin(model[2].input.data[-1])*model[0].input.data[-1] + m.cos(model[2].input.data[-1])*model[1].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".d", self.outputs[0].getdata(), self.outputs[0].time()], [self.name+".q", self.outputs[1].getdata(), self.outputs[1].time()]]
        
    return UpdateCE(name)

#dqroalphabetaarray############################################################

dqtoalphabetaarrayargs = [[], [], [], [], [], []]

def dqtoalphabetaarray(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.ain = deltaTau.FunctionELement(name, "D", 0,)
            self.bin = deltaTau.FunctionELement(name, "D", 0,)
            self.phiin = deltaTau.FunctionELement(name, "D", 0,)
            self.aout = deltaTau.FunctionELement(name, "Q", 0)
            self.bout = deltaTau.FunctionELement(name, "Q", 0)
            self.base = [self.ain, self.bin, self.phiin, self.aout, self.bout]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            if len(self.inputs[0]) > len(self.inputs[1]):
                signals = self.inputs[0].copysignals()
                if len(signals) > 1:
                    signals = signals[0:2]
                self.outputs[0].setarray(signals)
            else:
                d = deltaTau.FunctionArray(self)
                q = deltaTau.FunctionArray(self)
                d.setarray(self.inputs[1].copysignals())
                q.setarray(self.inputs[1].copysignals())
                self.outputs[0].setarray([d, q])
        
        def connect(self):
            """sets the outsignals"""
        
            if len(self.inputs[0].signals) == 2:
                inputs = self.inputs[1].getsignals()
                outputs = self.outputs[0].getsignals()
                inputphi = self.inputs[0].getsignals()
                if len(inputs) == len(outputs):
                    if len(inputs) > 2:
                        for i in range(0, int(len(inputs)/2)):
                            model = copy.deepcopy(self.base)
                            model[0].setinput(inputs[i])
                            model[1].setinput(inputs[i + int(len(inputs)/2)])
                            if len(inputphi) == len(inputs)/2:
                                model[2].setinput(inputphi[i])
                            elif len(inputphi) == 1:
                                model[2].setinput(inputphi[0])
                            else:
                                raise TypeError("TypeError: input is of wrong size")
                            model[3].setoutput(outputs[i])
                            model[4].setoutput(outputs[i + int(len(inputs)/2)])
                            self.felements = self.felements + model
                    elif len(inputs) == 2:
                        for i, _ in enumerate(inputphi):
                            model = copy.deepcopy(self.base)
                            model[0].setinput(inputs[0])
                            model[1].setinput(inputs[1])
                            model[2].setinput(inputphi[i])
                            model[3].setoutput(outputs[i])
                            model[4].setoutput(outputs[i + len(inputphi)])
                            self.felements = self.felements + model
                            self.models.append(model)
                    else:
                        raise TypeError("TypeError: input is of wrong size")
                else:
                    raise TypeError("TypeError: input is of wrong size")
            else:
                raise TypeError("TypeError: input is of wrong size")
            
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                model[3].setvalue((m.sin(model[2].input.data[-1])*model[0].input.data[-1] + m.cos(model[2].input.data[-1])*model[1].input.data[-1]))
                model[4].setvalue((m.cos(model[2].input.data[-1])*model[0].input.data[-1] - m.sin(model[2].input.data[-1])*model[1].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#dqtoalphabetasingle###########################################################

dqtoalphabetasingleargs = [[], [], [], [], [], []]

def dqtoalphabetasingle(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.ain = deltaTau.FunctionELement(name, "D", 0,)
            self.bin = deltaTau.FunctionELement(name, "D", 0,)
            self.phiin = deltaTau.FunctionELement(name, "D", 0,)
            self.aout = deltaTau.FunctionELement(name, "Q", 0)
            self.bout = deltaTau.FunctionELement(name, "Q", 0)
            self.base = [self.ain, self.bin, self.phiin, self.aout, self.bout]
            self.felements = []
            self.models = []
            self.outputs = [deltaTau.FunctionArray(self), deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felements
        
        def setarray(self):
            """sets the format"""
            
            if len(self.inputs[0]) > len(self.inputs[2]):
                self.outputs[0].setarray(self.inputs[0].copysignals())
                self.outputs[1].setarray(self.inputs[0].copysignals())
            else:
                self.outputs[0].setarray(self.inputs[2].copysignals())
                self.outputs[1].setarray(self.inputs[2].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputa = self.inputs[2].getsignals()
            inputb = self.inputs[1].getsignals()
            inputphi = self.inputs[0].getsignals()
            outputa = self.outputs[0].getsignals()
            outputb = self.outputs[1].getsignals()
            if len(inputa) > 1:
                if len(inputa) == len(inputb) and len(inputa) == len(outputa) and len(inputa) == len(outputb):
                    for i, _ in enumerate(inputa):
                        model = copy.deepcopy(self.base)
                        model[0].setinput(inputa[i])
                        model[1].setinput(inputb[i])
                        if len(inputphi) == len(inputa):
                            model[2].setinput(inputphi[i])
                        elif len(inputphi) == 1:
                            model[2].setinput(inputphi[0])
                        else:
                            raise TypeError("TypeError: input is of wrong size")
                        model[3].setoutput(outputa[i])
                        model[4].setoutput(outputb[i])
                        self.felements = self.felements + model
                        self.models.append(model)
                else:
                    raise TypeError("TypeError: input is of wrong size")
            elif len(inputa) == 1:
                if len(inputa) == len(inputb) and len(inputphi) == len(outputa) and len(inputphi) == len(outputb):
                    for i, _ in enumerate(inputphi):
                        model = copy.deepcopy(self.base)
                        model[0].setinput(inputa[0])
                        model[1].setinput(inputb[0])
                        model[2].setinput(inputphi[i])
                        model[3].setoutput(outputa[i])
                        model[4].setoutput(outputb[i])
                        self.felements = self.felements + model
                        self.models.append(model)
                else:
                    raise TypeError("TypeError: input is of wrong size")
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for model in self.models:
                model[3].setvalue((m.sin(model[2].input.data[-1])*model[0].input.data[-1] + m.cos(model[2].input.data[-1])*model[1].input.data[-1]))
                model[4].setvalue((m.cos(model[2].input.data[-1])*model[0].input.data[-1] - m.sin(model[2].input.data[-1])*model[1].input.data[-1]))
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name+".d", self.outputs[0].getdata(), self.outputs[0].time()], [self.name+".q", self.outputs[1].getdata(), self.outputs[1].time()]]
        
    return UpdateCE(name)

#cartesian2polar###############################################################

cartesian2polarargs = [[], [], [], [], [], []]

def cartesian2polar(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 0)
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout1 = []
            self.felementsout2 = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout1 + self.felementsout2
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            if self.inputs[0].checksize([[2], [[1], [1]]]):
                inputs = self.inputs[0].getsignals()
                outputs = self.outputs[0].getsignals()
                for i in range(0, len(inputs), 2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out1 = self.baseout.copy()
                    out2 = self.baseout.copy()
                    in1.setinput(inputs[i])
                    in2.setinput(inputs[i+1])
                    out1.setoutput(outputs[i])
                    out2.setoutput(outputs[i+1])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout1.append(out1)
                    self.felementsout2.append(out2)
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                polar = cm.polar(self.felementsin1[i].input.data[-1] + self.felementsin2[i].input.data[-1]*1j)
                self.felementsout1[i].setvalue(polar[0])
                self.felementsout2[i].setvalue(polar[1])
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#polar2cartesian###############################################################

polar2cartesianargs = [[], [], [], [], [], []]

def polar2cartesian(name):
    """concats the inputvectors to an array"""
    
    class UpdateCE(deltaTau.UpdateCE):
        
        def __init__(self, name):
            super().__init__(name)
            
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 0)
            self.felementsin1 = []
            self.felementsin2 = []
            self.felementsout1 = []
            self.felementsout2 = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin1 + self.felementsin2 + self.felementsout1 + self.felementsout2
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            if self.inputs[0].checksize([[2], [[1], [1]]]):
                inputs = self.inputs[0].getsignals()
                outputs = self.outputs[0].getsignals()
                for i in range(0, len(inputs), 2):
                    in1 = self.basein.copy()
                    in2 = self.basein.copy()
                    out1 = self.baseout.copy()
                    out2 = self.baseout.copy()
                    in1.setinput(inputs[i])
                    in2.setinput(inputs[i+1])
                    out1.setoutput(outputs[i])
                    out2.setoutput(outputs[i+1])
                    self.felementsin1.append(in1)
                    self.felementsin2.append(in2)
                    self.felementsout1.append(out1)
                    self.felementsout2.append(out2)
            else:
                raise TypeError("TypeError: input is of wrong size")
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin1):
                car = cm.rect(self.felementsin1[i].input.data[-1], self.felementsin2[i].input.data[-1])
                self.felementsout1[i].setvalue(car.real)
                self.felementsout2[i].setvalue(car.imag)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]
        
    return UpdateCE(name)

#timedelay#####################################################################

timedelayargs = [[0.1],
             ["time delay"],
             ["s"],
             [float],
             [bl.PropertyElement.biggerzero],
             ["delay time of the input signal"]]

def timedelay(name, delay):
    """delays the input by time delay"""
    
    class UpdateCE(deltaTau.UpdateCE):
    
        def __init__(self, name, delay):
            super().__init__(name)
            
            self.delay = delay
            self.basein = deltaTau.FunctionELement(name, "D", 0,)
            self.baseout = deltaTau.FunctionELement(name, "Q", 1)
            self.felementsin = []
            self.felementsout = []
            self.outputs = [deltaTau.FunctionArray(self)]
        
        def getFE(self):
            """returns the FElement"""
            
            return self.felementsin + self.felementsout
        
        def setarray(self):
            """sets the format"""
            
            self.outputs[0].setarray(self.inputs[0].copysignals())
        
        def connect(self):
            """sets the outsignals"""
            
            inputs = self.inputs[0].getsignals()
            outputs = self.outputs[0].getsignals()
            for i, s in enumerate(inputs):
                ein = self.basein.copy()
                eout = self.baseout.copy()
                ein.setinput(s)
                eout.setoutput(outputs[i])
                self.felementsin.append(ein)
                self.felementsout.append(eout)
        
        def update(self, major, time):
            """updates the voltagesource"""
            
            for i, _ in enumerate(self.felementsin):
                if time > self.delay:
                    self.felementsout[i].setvalue(np.array(self.felementsin[i].input.data)[np.array(self.felementsin[i].input.t) < time - self.delay][-1])
                else:
                    self.felementsout[i].setvalue(0)
            return 1
        
        def setpublicvariables(self):
            """returns the public variables"""
            
            self.publicvariables = [[self.name, self.outputs[0].getdata(), self.outputs[0].time()]]   
            
    return UpdateCE(name, delay)  

###############################################################################
# Library structure
###############################################################################

LIBRARY = bl.LibTree("functional elements")
sourceElements = bl.LibTree("Sources")
sourceElements.append(
    bl.FEType("Constant", "functionLib/constant.svg", baseimages.drawConstant, constantsource, *constantsourceargs, size=[1,1], interface=["", "o", "", ""])
    )
sourceElements.append(
    bl.FEType("Zero", "functionLib/zero.svg", baseimages.drawZero, zero, *zeroargs, size=[1,1], interface=["", "o", "", ""])
    )
sourceElements.append(
    bl.FEType("One", "functionLib/one.svg", baseimages.drawOne, one, *oneargs, size=[1,1], interface=["", "o", "", ""])
    )
sourceElements.append(
    bl.FEType("Sine", "functionLib/sinfunction.svg", baseimages.drawSinFunction, sin, *sinargs, size=[1,1], interface=["", "o", "", ""])
    )
sourceElements.append(
    bl.FEType("Saw-tooth", "functionLib/sawtooth.svg", baseimages.drawSawTooth, sawtooth, *sawtoothargs, size=[1,1], interface=["", "o", "", ""])
    )
sourceElements.append(
    bl.FEType("Triangular", "functionLib/triangular.svg", baseimages.drawTriangular, triangular, *triangularargs, size=[1,1], interface=["", "o", "", ""])
    )
sourceElements.append(
    bl.FEType("Step", "functionLib/step.svg", baseimages.drawStep, step, *stepargs, size=[1,1], interface=["", "o", "", ""])
    )
sourceElements.append(
    bl.FEType("Clock", "functionLib/clock.svg", baseimages.drawClock, clock, *clockargs, size=[1,1], interface=["", "o", "", ""])
    )
sourceElements.append(
    bl.FEType("Time", "functionLib/time.svg", baseimages.drawTime, time_, *timeargs, size=[1,1], interface=["", "o", "", ""])
    )
sourceElements.append(
    bl.FEType("Ramp", "functionLib/ramp.svg", baseimages.drawramp, ramp, *rampargs, size=[1,1], interface=["", "o", "", ""])
    )
mathElements = bl.LibTree("Math elements")
mathElements.append(
    bl.FEType("Absolute", "functionLib/abs.svg", baseimages.drawAbs, abs_, *absargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathElements.append(
    bl.FEType("Norm", "functionLib/norm.svg", baseimages.drawNorm, norm, *normargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathElements.append(
    bl.FEType("Gain", "functionLib/gain.svg", baseimages.drawGain, gain, *gainargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathElements.append(
    bl.FEType("Sum", "functionLib/sum.svg", baseimages.drawSum, summ, *sumargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathElements.append(
    bl.FEType("Adder", "functionLib/adder.svg", baseimages.drawAdder, adder, *adderargs, size=[1,1], interface=["i", "o", "", "i"])
    )
mathElements.append(
    bl.FEType("Maximum", "functionLib/max.svg", baseimages.drawMaximum, maximum, *maximumargs, size=[1,2], interface=["", "o", "", "", "i", "i"], sizechangeable=[[1, 25], False])
    )
mathElements.append(
    bl.FEType("Minimum", "functionLib/min.svg", baseimages.drawMinimum, minimum, *minimumargs, size=[1,2], interface=["", "o", "", "", "i", "i"], sizechangeable=[[1, 25], False])
    )
mathElements.append(
    bl.FEType("Offset", "functionLib/offset.svg", baseimages.drawOffset, offset, *offsetargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathElements.append(
    bl.FEType("Subtracter", "functionLib/subtractor.svg", baseimages.drawSubtractor, subtractor, *subtractorargs, size=[1,1], interface=["", "o", "i", "i"])
    )
mathElements.append(
    bl.FEType("Multiplikator", "functionLib/multiplicator.svg", baseimages.drawMultiplicator, multiplicator, *multiplicatorargs, size=[1,1], interface=["i", "o", "", "i"])
    )
mathElements.append(
    bl.FEType("Divider", "functionLib/divider.svg", baseimages.drawDivider, divider, *dividerargs, size=[1,1], interface=["", "o", "i", "i"], labels=["", "", "y", "x"])
    )
mathElements.append(
    bl.FEType("Signum", "functionLib/sig.svg", baseimages.drawSignum, signum, *signumargs , size=[1,1], interface=["", "o", "", "i"])
    )
mathfElements = bl.LibTree("Math function elements")
mathfElements.append(
    bl.FEType("Square", "functionLib/square.svg", baseimages.drawSquare, square, *squareargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Square-root", "functionLib/sqrt.svg", baseimages.drawSqrt, sqrt, *sqrtargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Exponential", "functionLib/exp.svg", baseimages.drawExp, exp, *expargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Natural Logarithm", "functionLib/ln.svg", baseimages.drawLn, ln, *lnargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Logarithm 2", "functionLib/log2.svg", baseimages.drawLog2, log2, *log2args, size=[1,1], interface=["", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Logarithm 10", "functionLib/log10.svg", baseimages.drawLog10, log10, *log10args, size=[1,1], interface=["", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Power", "functionLib/power.svg", baseimages.drawPower, power, *powerargs, size=[1,1], interface=["i", "o", "", "i"], labels=["x", "", "", "v"])
    )
mathfElements.append(
    bl.FEType("Round", "functionLib/round.svg", baseimages.drawRound, rounding, *roundargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Modulo", "functionLib/mod.svg", baseimages.drawMod, mod, *modargs, size=[1,1], interface=["i", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Remainder", "functionLib/rem.svg", baseimages.drawRem, rem, *remargs, size=[1,1], interface=["i", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Limiter", "functionLib/limiter.svg", baseimages.drawLimiter, limiter, *limiterargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Rate-limiter", "functionLib/ratelimiter.svg", baseimages.drawRateLimiter, ratelimiter, *ratelimiterargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Relay", "functionLib/hysteresis.svg", baseimages.drawHysteresis, relay, *relayargs, size=[1,1], interface=["", "o", "", "i"])
    )
mathfElements.append(
    bl.FEType("Dead-Zone", "functionLib/deadzone.svg", baseimages.drawDeadzone, deadzone, *deadzoneargs, size=[1,1], interface=["", "o", "", "i"])
    )
trigonometricElements = bl.LibTree("Trigonometric elements")
trigonometricElements.append(
    bl.FEType("Sine", "functionLib/sin.svg", baseimages.drawSin, sine, *sineargs, size=[1,1], interface=["", "o", "", "i"])
    )
trigonometricElements.append(
    bl.FEType("Cosine", "functionLib/cos.svg", baseimages.drawCos, cos, *cosargs, size=[1,1], interface=["", "o", "", "i"])
    )
trigonometricElements.append(
    bl.FEType("Tangent", "functionLib/tan.svg", baseimages.drawTan, tan, *tanargs, size=[1,1], interface=["", "o", "", "i"])
    )
trigonometricElements.append(
    bl.FEType("Arcsine", "functionLib/asin.svg", baseimages.drawAsin, arcsin, *arcsinargs, size=[1,1], interface=["", "o", "", "i"])
    )
trigonometricElements.append(
    bl.FEType("Arccosine", "functionLib/acos.svg", baseimages.drawAcos, arccos, *arccosargs, size=[1,1], interface=["", "o", "", "i"])
    )
trigonometricElements.append(
    bl.FEType("Arctangent", "functionLib/atan.svg", baseimages.drawAtan, arctan, *arctanargs, size=[1,1], interface=["", "o", "", "i"])
    )
functionTablesElements = bl.LibTree("Functions & Tables")
functionTablesElements.append(
    bl.FEType("1D Look-Up Table", "functionLib/1d.svg", baseimages.draw1d, lookup1d, *lookup1dargs, size=[1,1], interface=["", "o", "", "i"])
    )
functionTablesElements.append(
    bl.FEType("2D Look-Up Table", "functionLib/2d.svg", baseimages.draw2d, lookup2d, *lookup2dargs, size=[1,2], interface=["", "o", "", "", "i", "i"])
    )
controlElements = bl.LibTree("Control elements")
controlElements.append(
    bl.FEType("Integrator", "functionLib/integrator.svg", baseimages.drawIntegrator, integrator, *integratorargs, size=[1,1], interface=["", "o", "", "i"])
    )
controlElements.append(
    bl.FEType("Derivative", "functionLib/dt.svg", baseimages.drawDt, dt, *dtargs, size=[1,1], interface=["", "o", "", "i"])
    )
controlElements.append(
    bl.FEType("PI-Controller", "functionLib/PI.svg", baseimages.drawPI, PI, *PIargs, size=[1,1], interface=["", "o", "", "i"])
    )
controlElements.append(
    bl.FEType("PID-Controller", "functionLib/PID.svg", baseimages.drawPID, PID, *PIDargs, size=[1,1], interface=["", "o", "", "i"])
    )
controlElements.append(
    bl.FEType("Time delay", "functionLib/timedelay.svg", baseimages.drawTimedelay, timedelay, *timedelayargs, size=[1,1], interface=["", "o", "", "i"])
    )
controlElements.append(
    bl.FEType("PLL", "functionLib/PLL.svg", baseimages.drawPLL, PLL, *PLLargs, size=[1,1], interface=["", "o", "", "i"])
    )
controlElements.append(
    bl.FEType("PLL 3ph", "functionLib/PLL.svg", baseimages.drawPLL, PLL3ph, *PLL3phargs, size=[1,1], interface=["", "o", "", "i"], labels=["", "", "", "(3)"])
    )
routingElements = bl.LibTree("routing")
routingElements.append(
    bl.FEType("Array in", "functionLib/arrayin.svg", baseimages.drawArrayin, arrayin, *arrayinargs, size=[0,2], interface=["o","", "i", "i"], sizechangeable=[[1, 25], False])
    )
routingElements.append(
    bl.FEType("Array out", "functionLib/arrayout.svg", baseimages.drawArrayin, arrayout, *arrayoutargs, size=[0,2], interface=["o","o", "", "i"], sizechangeable=[False, [1, 25]])
    )
modulatorElements = bl.LibTree("Power electronic modulators")
modulatorElements.append(
    bl.FEType("sawtooth PWM", "functionLib/PWM.svg", baseimages.drawPWM, sawtoothPWM, *sawtoothPWMargs, size=[1,1], interface=["", "o", "", "i"])
    )
modulatorElements.append(
    bl.FEType("symetrical PWM", "functionLib/PWM.svg", baseimages.drawPWM, symetricalPWM, *symetricalPWMargs, size=[1,1], interface=["", "o", "", "i"])
    )
transformationElements = bl.LibTree("Transformations")
transformationElements.append(
    bl.FEType("cartesian to polar", "functionLib/c2p.svg", baseimages.drawcartesian2polar, cartesian2polar, *cartesian2polarargs, size=[1,1], interface=["", "o", "", "i"], labels=["", "(2)", "", "(2)"])
    )
transformationElements.append(
    bl.FEType("polar to cartesian", "functionLib/p2c.svg", baseimages.drawpolar2cartesian, polar2cartesian, *polar2cartesianargs, size=[1,1], interface=["", "o", "", "i"], labels=["", "(2)", "", "(2)"])
    )
transformationElements.append(
    bl.FEType("αβ (array)", "functionLib/alphabeta.svg", baseimages.drawAlphaBeta, alphabetaarray, *alphabetaarrayargs, size=[1,1], interface=["", "o", "", "i"], labels=["", "(2)", "", "(3)"])
    )
transformationElements.append(
    bl.FEType("αβ (singular)", "functionLib/alphabeta.svg", baseimages.drawAlphaBeta, alphabetasingle, *alphabetasingleargs, size=[1,3], interface=["", "o", "o", "", "", "i", "i", "i"], labels=["", "α", "β", "", "", "c", "b", "a"])
    )
transformationElements.append(
    bl.FEType("reverse αβ (array)", "functionLib/alphabetareverse.svg", baseimages.drawAlphaBetareverse, alphabetareversearray, *alphabetareversearrayargs, size=[1,1], interface=["", "o", "", "i"], labels=["", "(3)", "", "(2)"])
    )
transformationElements.append(
    bl.FEType("reverse αβ (singular)", "functionLib/alphabetareverse.svg", baseimages.drawAlphaBetareverse, alphabetareversesingle, *alphabetareversesingleargs, size=[1,3], interface=["", "o", "o", "o", "", "", "i", "i"], labels=["", "a", "b", "c", "", "", "β", "α"])
    )
transformationElements.append(
    bl.FEType("dq (array)", "functionLib/dq.svg", baseimages.drawdq, dqarray, *dqarrayargs, size=[1,1], interface=["", "o", "i", "i"], labels=["", "(2)", "φ", "(3)"])
    )
transformationElements.append(
    bl.FEType("dq (singular)", "functionLib/dq.svg", baseimages.drawdq, dqsingle, *dqsingleargs, size=[1,3], interface=["", "o", "o", "", "i", "i", "i", "i"], labels=["", "α", "β", "", "φ", "c", "b", "a"])
    )
transformationElements.append(
    bl.FEType("dq reverse (array)", "functionLib/dqreverse.svg", baseimages.drawdqreverse, dqreversearray, *dqreversearrayargs, size=[1,1], interface=["", "o", "i", "i"], labels=["", "(3)", "φ", "(2)"])
    )
transformationElements.append(
    bl.FEType("dq reverse (single)", "functionLib/dqreverse.svg", baseimages.drawdqreverse, dqreversesingle, *dqreversesingleargs, size=[1,3], interface=["", "o", "o", "o", "i", "", "i", "i"], labels=["", "a", "b", "c", "φ", "", "β", "α"])
    )
transformationElements.append(
    bl.FEType("αβ to dq (array)", "functionLib/alphabetatodq.svg", baseimages.drawAlphaBetatodq, alphabetatodqarray, *alphabetatodqarrayargs, size=[1,1], interface=["", "o", "i", "i"], labels=["", "(2)", "φ", "(2)"])
    )
transformationElements.append(
    bl.FEType("αβ to dq (singular)", "functionLib/alphabetatodq.svg", baseimages.drawAlphaBetatodq, alphabetatodqsingle, *alphabetatodqarrayargs, size=[1,2], interface=["", "o", "o", "i", "i", "i"], labels=["", "d", "q", "φ", "β", "α"])
    )
transformationElements.append(
    bl.FEType("dq to αβ (array)", "functionLib/dqtoalphabeta.svg", baseimages.drawdqtoAlphaBeta, dqtoalphabetaarray, *dqtoalphabetaarrayargs, size=[1,1], interface=["", "o", "i", "i"], labels=["", "(2)", "φ", "(2)"])
    )
transformationElements.append(
    bl.FEType("dq to αβ (singular)", "functionLib/dqtoalphabeta.svg", baseimages.drawdqtoAlphaBeta, dqtoalphabetasingle, *dqtoalphabetasingleargs, size=[1,2], interface=["", "o", "o", "i", "i", "i"], labels=["", "α", "β", "φ", "q", "d"])
    )
logicElements = bl.LibTree("logic elements")
logicElements.append(
    bl.FEType("Not", "functionLib/not.svg", baseimages.drawNot, Not, *notargs, size=[1,1], interface=["", "o", "", "i"])
    )
logicElements.append(
    bl.FEType("bigger than c", "functionLib/biggerthanconstant.svg", baseimages.drawBiggerThanConstant, biggerthanconstant, *biggerthanconstantargs, size=[1,1], interface=["", "o", "", "i"])
    )
logicElements.append(
    bl.FEType("bigger than", "functionLib/biggerthan.svg", baseimages.drawBiggerThan, bigger, *biggerargs, size=[1,2], interface=["", "o", "", "", "i", "i"])
    )
logicElements.append(
    bl.FEType("smaller than c", "functionLib/smallerthanconstant.svg", baseimages.drawSmallerThanConstant, smallerthanconstant, *smallerthanconstantargs, size=[1,1], interface=["", "o", "", "i"])
    )
logicElements.append(
    bl.FEType("smaller than", "functionLib/smallerthan.svg", baseimages.drawSmallerThan, smaller, *smallerargs, size=[1,2], interface=["", "o", "", "", "i", "i"])
    )
logicElements.append(
    bl.FEType("AND", "functionLib/and.svg", baseimages.drawAnd, And, *andargs, size=[1,2], interface=["", "o", "", "", "i", "i"])
    )
logicElements.append(
    bl.FEType("OR", "functionLib/or.svg", baseimages.drawOr, Or, *orargs, size=[1,2], interface=["", "o", "", "", "i", "i"])
    )
logicElements.append(
    bl.FEType("XOR", "functionLib/xor.svg", baseimages.drawXor, Xor, *xorargs, size=[1,2], interface=["", "o", "", "", "i", "i"])
    )
logicElements.append(
    bl.FEType("Phase detector", "functionLib/phasedetector.svg", baseimages.drawPhasedetector, phasedetector, *phasedetectorargs, size=[1,2], interface=["", "o", "", "", "i", "i"])
    )
timedomainfilterElements = bl.LibTree("Time domain filter elements")
timedomainfilterElements.append(
    bl.FEType("Periodic Average", "functionLib/periodicaverage.svg", baseimages.drawPeriodicAverage, periodicaverage, *periodicaverageargs, size=[1,1], interface=["", "o", "", "i"])
    )
delayElements = bl.LibTree("Delays")
delayElements.append(
    bl.FEType("Turn On Delay", "functionLib/turnondelay.svg", baseimages.drawTurnOnDelay, turnondelay, *turnondelayargs, size=[1,1], interface=["", "o", "", "i"])
    )
zdomainElements = bl.LibTree("Z domain")
zdomainElements.append(
    bl.FEType("Continuous to Discrete", "functionLib/c2d.svg", baseimages.drawContinuous2Discrete, continuous2discrete, *continuous2discreteargs, size=[1,1], interface=["", "o", "", "i"])
    )
zdomainElements.append(
    bl.FEType("Delay", "functionLib/delay.svg", baseimages.drawDelay, delay, *delayargs, size=[1,1], interface=["", "o", "", "i"])
    )
zdomainElements.append(
    bl.FEType("Discrete Integrator", "functionLib/discreteintegrator.svg", baseimages.drawDiscreteIntegrator, discreteintegrator, *discreteintegratorargs, size=[1,1], interface=["", "o", "", "i"])
    )
zdomainElements.append(
    bl.FEType("Discrete Derivator", "functionLib/discretederivator.svg", baseimages.drawDiscreteDerivator, discretederivator, *discretederivatorargs, size=[1,1], interface=["", "o", "", "i"])
    )
zdomainElements.append(
    bl.FEType("Discrete Mean Value", "functionLib/discretemean.svg", baseimages.drawDiscretemean, discretemeanvalue, *discretemeanvalueargs, size=[1,1], interface=["", "o", "", "i"])
    )
ADElements = bl.LibTree("Analog-digital")
ADElements.append(
    bl.FEType("Quantizer", "functionLib/quantizer.svg", baseimages.drawQuantizer, quantizer, *quantizerargs, size=[1,1], interface=["", "o", "", "i"])
    )
plotElements = bl.LibTree("Graphical output")
plotElements.append(
    bl.FEType("Plot Y", "functionLib/plot.svg", baseimages.drawPlot, ploty, *plotyargs, size=[1,1], interface=["", "", "", "i"], sizechangeable=[[1, 10], False])
    )
LIBRARY.append(sourceElements)
LIBRARY.append(mathElements)
mathElements.append(mathfElements)
mathElements.append(trigonometricElements)
LIBRARY.append(functionTablesElements)
LIBRARY.append(controlElements)
LIBRARY.append(routingElements)
LIBRARY.append(modulatorElements)
LIBRARY.append(transformationElements)
LIBRARY.append(logicElements)
LIBRARY.append(timedomainfilterElements)
LIBRARY.append(delayElements)
LIBRARY.append(zdomainElements)
LIBRARY.append(ADElements)
LIBRARY.append(plotElements)
            
"""prints the information about this file"""
if PRINT_INFO:
    printinfo()
    
savelibrary = open("./library/functionLib.lib", "wb")
pc.dump(LIBRARY, savelibrary, protocol=pc.HIGHEST_PROTOCOL)
savelibrary.close()
print("functionLib is saved")