# -*- coding: utf-8 -*-
"""
Created on Tue Aug  9 21:05:51 2022

@author: thocza
alpha 0.0.1
    first UI for tests
    and creation
alpha 0.0.2
    added the function to save and load subcircuits
alpha 0.0.3
    added undo and redo
beta 0.1.0
    language change to english
beta 0.1.1
    added new icons
beta 0.1.2
    changed the window positioning from absolute to relative
beta 0.1.3
    put some style constants to the style.py file
"""

# info variables
PROGRAM_NAME = "deltaTau Subcircuit type window GUI"
FILE_NAME = "subcircuittypeWindow.py"
FILE_CREATION = "2022-08-09"
FILE_UPDATE = "2022-11-08"
FILE_VERSION = "beat 0.1.3"
DT_VERSION = "0.12.0"
PRINT_INFO = True


# libraries
from PyQt5 import QtWidgets, QtCore, QtGui, QtOpenGL, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QStyle
import sys
import ctypes
import sys
import guigraphics as gg
from style import *

#static variables
FONTSIZE = 12


def printinfo():
    """shows all information about this file"""

    print(f"deltaTau {DT_VERSION}")
    print(PROGRAM_NAME)
    print("File: " + FILE_NAME + " Version: " + FILE_VERSION)
    print("Creation: " + FILE_CREATION + " Last Updated: " + FILE_UPDATE)

class SubCircuitTypeWindow(QWidget):
    """name Window for edditing the name of the circuit
        """
        
    namechange = QtCore.pyqtSignal(str)
    lockchange = QtCore.pyqtSignal(bool)
    closed = QtCore.pyqtSignal()
    load = QtCore.pyqtSignal(tuple)
    savesignal = QtCore.pyqtSignal(str)
    
    def __init__(self, name, lock=False, pos=None):
        """intits the Window
            also sets up the IOHandler for communication with the dt Framework
            sets name and icon
            """
        
        super().__init__()
        appicon = QtGui.QIcon()
        appicon.addFile("./icons/dtIcon.png", QtCore.QSize(1000, 1000))
        self.name = name
        self.lock = lock
        self.setWindowIcon(appicon)
        self.setWindowTitle("deltaTau")
        self.errordialog = None
        if pos == None:
            pos = QtCore.QPoint(0, 0)
        self.initUI(pos)
        self.file = None

    def initUI(self, pos):
        """inits the window"""
        
        self.setGeometry(pos.x() + 300, pos.y() + 300, 300, 170)
        self.setFixedSize(300, 170)
        self.setWindowTitle(f"deltaTau: {self.name}")
        
        self.hLayoutWindow = QtWidgets.QHBoxLayout(self)
        self.vLayoutWindow = QtWidgets.QVBoxLayout()
        
        self.hLayoutInputs = QtWidgets.QHBoxLayout()
        self.nameLabel = QtWidgets.QLabel()
        self.nameLabel.setMinimumSize(QtCore.QSize(75, 0))
        self.nameLabel.setMaximumSize(QtCore.QSize(75, 25))
        self.nameLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.nameLabel.setText("Name:")
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.nameEdit.setText(self.name)
        self.unitLabel = QtWidgets.QLabel()
        self.unitLabel.setMinimumSize(QtCore.QSize(30, 0))
        self.unitLabel.setMaximumSize(QtCore.QSize(30, 25))
        self.infoLabel = QtWidgets.QLabel()
        self.infoLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
        self.infoLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.infoLabel.setMaximumSize(QtCore.QSize(25, 25))
        self.infoLabel.setToolTip("Subcircuit name")
        
        self.hLayoutInputs.addWidget(self.nameLabel)
        self.hLayoutInputs.addWidget(self.nameEdit)
        self.hLayoutInputs.addWidget(self.unitLabel)
        self.hLayoutInputs.addWidget(self.infoLabel)
        
        self.hLayoutLock = QtWidgets.QHBoxLayout()
        self.lockLabel = QtWidgets.QLabel()
        self.lockLabel.setMinimumSize(QtCore.QSize(170, 0))
        self.lockLabel.setMaximumSize(QtCore.QSize(170, 25))
        self.lockLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.lockLabel.setText("Lock:")
        self.lockcheck = QtWidgets.QCheckBox()
        self.lockcheck.setChecked(self.lock)
        self.infoLockLabel = QtWidgets.QLabel()
        self.infoLockLabel.setPixmap(ICONS["Help"].pixmap(15, 15))
        self.infoLockLabel.setMinimumSize(QtCore.QSize(25, 0))
        self.infoLockLabel.setMaximumSize(QtCore.QSize(25, 25))
        self.infoLockLabel.setToolTip("Locks the subcircuit")
        
        self.hLayoutLock.addWidget(self.lockLabel)
        self.hLayoutLock.addWidget(self.lockcheck)
        self.hLayoutLock.addWidget(self.infoLockLabel)
        
        self.hLayoutLoadSave = QtWidgets.QHBoxLayout()
        
        self.savecircuitbutton = QtWidgets.QPushButton("Save subcircuit")
        self.savecircuitbutton.clicked.connect(self.savecircuit)
        self.loadbutton = QtWidgets.QPushButton("Load subcircuit")
        self.loadbutton.clicked.connect(self.loadcircuit)
        
        self.hLayoutLoadSave.addWidget(self.savecircuitbutton)
        self.hLayoutLoadSave.addWidget(self.loadbutton)
        
        self.hLayoutButtons = QtWidgets.QHBoxLayout()
        
        self.savebutton = QtWidgets.QPushButton("Save")
        self.savebutton.setIcon(ICONS["SaveFile"])
        self.savebutton.clicked.connect(self.save)
        self.okbutton = QtWidgets.QPushButton("OK")
        self.okbutton.setIcon(ICONS["Apply"])
        self.okbutton.clicked.connect(self.ok)
        self.cancelbutton = QtWidgets.QPushButton("Cancel")
        self.cancelbutton.setIcon(ICONS["Discard"])
        self.cancelbutton.clicked.connect(self.close)
        
        self.hLayoutButtons.addWidget(self.savebutton)
        self.hLayoutButtons.addWidget(self.okbutton)
        self.hLayoutButtons.addWidget(self.cancelbutton)
        
        self.vLayoutWindow.addLayout(self.hLayoutInputs)
        self.vLayoutWindow.addLayout(self.hLayoutLock)
        self.vLayoutWindow.addLayout(self.hLayoutLoadSave)
        self.vLayoutWindow.addLayout(self.hLayoutButtons)
        self.hLayoutWindow.addLayout(self.vLayoutWindow)
        
    def loadcircuit(self):
        """loads a circuit and emits it"""
        
        self.file = QtWidgets.QFileDialog().getOpenFileName(self, "Load Circuit", "", "circuit (*.dt)")
        if self.file[0] == "":
            return
        
    def savecircuit(self):
        """save the subcircuit"""
        
        file = QtWidgets.QFileDialog.getSaveFileName(self, "Save Circuit", self.name, "circiut (*.dt)")
        if file[0] == "":
            return
        file = file[0]
        self.savesignal.emit(file)
        
    def closeEvent(self, event):
        """close Window"""
        
        self.name = self.nameEdit.text()
        self.closed.emit()
        self.close()
        
    def save(self):
        """save input values"""
        
        self.name = self.nameEdit.text()
        self.namechange.emit(self.name)
        self.lockchange.emit(self.lockcheck.isChecked())
        if not self.file == None:
            self.load.emit(self.file)
    
    def ok(self):
        """save input values and close window when values are valid"""
        
        self.name = self.nameEdit.text()
        self.namechange.emit(self.name)
        self.lockchange.emit(self.lockcheck.isChecked())
        if not self.file == None:
            self.load.emit(self.file)
        self.closed.emit()
        self.close()
         
    def keyPressEvent(self, event):
        """save event when enter key is pressed"""
        
        if event.key() == QtCore.Qt. Key_Return:
            self.name = self.nameEdit.text()
            self.namechange.emit(self.name)
            self.lockchange.emit(self.lockcheck.isChecked())
            if not self.file == None:
                self.load.emit(self.file)
            self.closed.emit()
            self.close()
    

"""prints the information about this file"""
if PRINT_INFO:
    printinfo()

